package org.gcube.smartgears;


public interface ApplicationManager {

	public void onInit();
	
	public void onShutdown();
		
}
