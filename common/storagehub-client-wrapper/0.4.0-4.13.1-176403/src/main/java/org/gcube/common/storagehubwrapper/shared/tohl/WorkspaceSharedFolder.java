/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl;


/**
 * The Interface WorkspaceSharedFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Oct 3, 2018
 */
public interface WorkspaceSharedFolder extends WorkspaceFolder{


	/**
	 * Checks if is vre folder.
	 *
	 * @return true, if is vre folder
	 */
	public boolean isVreFolder();

}
