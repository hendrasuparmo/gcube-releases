/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.impl;

import java.io.Serializable;
import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 15, 2018
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AccountingEntry implements org.gcube.common.storagehubwrapper.shared.tohl.AccountingEntry, Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 2880510084929441352L;

	String user;
	Calendar date;
	String entryType;
	String version;
	String primaryType;

}
