/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.impl;

import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * The Class WorkspaceVREFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 20, 2018
 */
@NoArgsConstructor
@ToString(callSuper=true)
public class WorkspaceVREFolder extends WorkspaceFolder implements org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceVREFolder{
	/**
	 *
	 */
	private static final long serialVersionUID = 1984917908193397527L;
}
