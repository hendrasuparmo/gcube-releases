/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl;



/**
 * The Interface WorkspaceFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 15, 2018
 */
public interface WorkspaceFolder extends WorkspaceItem {

}
