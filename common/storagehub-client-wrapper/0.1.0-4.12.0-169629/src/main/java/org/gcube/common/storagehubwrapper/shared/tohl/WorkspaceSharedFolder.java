/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl;



/**
 * The Interface WorkspaceSharedFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 15, 2018
 */
public interface WorkspaceSharedFolder extends WorkspaceFolder{

}
