/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.items;

import java.util.Map;


/**
 * The Interface PropertyMap.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 22, 2018
 */
public interface PropertyMap {

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public Map<String, Object> getValues();
}
