/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.impl;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;



/**
 * The Class WorkspaceFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 20, 2018
 */

/**
 * Instantiates a new workspace folder.
 */
@Getter
@NoArgsConstructor
@ToString(callSuper=true)
public class WorkspaceFolder extends WorkspaceItem implements org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceFolder{
	/*
	 *
	 */
	private static final long serialVersionUID = -3767943529796942863L;



}
