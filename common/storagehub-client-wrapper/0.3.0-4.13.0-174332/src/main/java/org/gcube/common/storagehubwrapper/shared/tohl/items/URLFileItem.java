/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.items;


/**
 * The Interface FileItemURL.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jun 21, 2018
 */
public interface URLFileItem extends FileItem{


}
