package org.gcube.common.storagehub.client.dsl;

public enum ContainerType {

	FOLDER,
	FILE,
	GENERIC_ITEM
	
}
