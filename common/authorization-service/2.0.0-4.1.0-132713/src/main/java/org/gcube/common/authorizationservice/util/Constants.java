package org.gcube.common.authorizationservice.util;

public class Constants {

	public static final String PORTAL_ID = "gCubePortal";
	
	public static final String DEFAULT_TOKEN_QUALIFIER = "TOKEN";
}
