package org.gcube.common.authorization.library;

public enum ClientType {

	USER, SERVICE, EXTERNALSERVICE, CONTAINER
}
