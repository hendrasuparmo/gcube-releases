/**
 * 
 */
package org.gcube.common.geoserverinterface.cxml;


/**
 * @author ceras
 *
 */
public interface CXmlManager {
	
	public void manage(int index, CXml cXml);
	
}
