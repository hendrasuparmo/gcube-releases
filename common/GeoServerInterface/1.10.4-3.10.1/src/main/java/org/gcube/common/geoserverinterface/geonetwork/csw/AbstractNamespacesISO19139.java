package org.gcube.common.geoserverinterface.geonetwork.csw;

public abstract class AbstractNamespacesISO19139 {
	
	
	//Namespaces
	private final String namespaceCSW = "http://www.opengis.net/cat/csw/2.0.2";
	private final String namespaceDC = "http://purl.org/dc/elements/1.1/";
	private final String namespaceOWS = "http://www.opengis.net/ows";
	private final String namespaceGMD = "http://www.isotc211.org/2005/gmd";
	private final String namespaceGCO = "http://www.isotc211.org/2005/gco";
	private final String namespaceXLINK = "http://www.w3.org/1999/xlink";
	private final String namespaceSRV = "http://www.isotc211.org/2005/srv";
	private final String namespaceXSI = "http://www.w3.org/2001/XMLSchema-instance";
	private final String namespaceGML = "http://www.opengis.net/gml";
	private final String namespaceGTS = "http://www.isotc211.org/2005/gts";
	private final String namespaceGEONET = "http://www.fao.org/geonetwork";
	private final String namespaceGMX = "http://www.isotc211.org/2005/gmx";
	private final String namespaceWMS = "http://www.opengis.net/wms";
	private final String namespaceDCT = "http://purl.org/dc/terms/";
	
	//Prefixs
	private final String prefixCSW = "csw";
	private final String prefixGMD = "gmd";
	private final String prefixOWS = "ows";
	private final String prefixDC = "dc";
	private final String prefixDCT = "dct";
	private final String prefixGCO = "gco";
	private final String prefixXLINK= "xlink";
	private final String prefixSRV = "srv";
	private final String prefixXSI = "xsi";
	private final String prefixGML = "gml";
	private final String prefixGTS = "gts";
	private final String prefixGEONET = "geonet";
	private final String prefixGMX = "gmx";
	private final String prefixWMS = "wms";
	
	
	public String getNamespaceCSW() {
		return namespaceCSW;
	}
	public String getNamespaceDC() {
		return namespaceDC;
	}
	public String getNamespaceOWS() {
		return namespaceOWS;
	}
	public String getNamespaceGMD() {
		return namespaceGMD;
	}
	public String getNamespaceGCO() {
		return namespaceGCO;
	}
	public String getNamespaceXLINK() {
		return namespaceXLINK;
	}
	public String getNamespaceSRV() {
		return namespaceSRV;
	}
	public String getNamespaceXSI() {
		return namespaceXSI;
	}
	public String getNamespaceGML() {
		return namespaceGML;
	}
	public String getNamespaceGTS() {
		return namespaceGTS;
	}
	public String getNamespaceGEONET() {
		return namespaceGEONET;
	}
	public String getPrefixCSW() {
		return prefixCSW;
	}
	public String getPrefixGMD() {
		return prefixGMD;
	}
	public String getPrefixOWS() {
		return prefixOWS;
	}
	public String getPrefixDC() {
		return prefixDC;
	}
	public String getPrefixGCO() {
		return prefixGCO;
	}
	public String getPrefixXLINK() {
		return prefixXLINK;
	}
	public String getPrefixSRV() {
		return prefixSRV;
	}
	public String getPrefixGML() {
		return prefixGML;
	}
	public String getPrefixGTS() {
		return prefixGTS;
	}
	public String getPrefixGEONET() {
		return prefixGEONET;
	}
	public String getPrefixXSI() {
		return prefixXSI;
	}
	public String getPrefixGMX() {
		return prefixGMX;
	}
	public String getNamespaceGMX() {
		return namespaceGMX;
	}
	public String getPrefixWMS() {
		return prefixWMS;
	}
	public String getNamespaceWMS() {
		return namespaceWMS;
	}
	public String getNamespaceDCT() {
		return namespaceDCT;
	}
	public String getPrefixDCT() {
		return prefixDCT;
	}

}
