package org.gcube.common.geoserverinterface.geonetwork;

import java.util.ArrayList;

public interface GeoserverSortInterface {
	
	public ArrayList<String> sortGeoserverList(ArrayList<String> list);

}
