package org.gcube.common.dbinterface.persistence;

public class ObjectNotFoundException extends Exception{

	public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1946407984752400328L;

}
