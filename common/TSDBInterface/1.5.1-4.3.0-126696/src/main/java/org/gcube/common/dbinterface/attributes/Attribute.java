package org.gcube.common.dbinterface.attributes;

import java.io.Serializable;

public interface Attribute extends Serializable{

	public String getAttribute();
	
	public String getAttributeName();
}
