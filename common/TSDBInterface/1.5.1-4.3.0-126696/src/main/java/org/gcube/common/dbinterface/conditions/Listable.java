package org.gcube.common.dbinterface.conditions;

public interface Listable {

	public String asStringList();
	
}
