/**
 * 
 */
package org.gcube.common.homelibrary.model.exceptions;

/**
 * @author Valentina Marioli valentina.marioli@isti.cnr.it
 *
 */
public class RepositoryException extends WorkspaceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message the exception message.
	 */
	public RepositoryException(String message) {
		super(message);
	}
	
}
