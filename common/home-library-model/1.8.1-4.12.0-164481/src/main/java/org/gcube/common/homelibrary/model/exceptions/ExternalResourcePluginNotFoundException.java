/**
 * 
 */
package org.gcube.common.homelibrary.model.exceptions;

/**
 * @author gioia
 *
 */
public class ExternalResourcePluginNotFoundException extends WorkspaceException {

	private static final long serialVersionUID = -5322032383507940894L;

	/**
	 * @param message
	 */
	public ExternalResourcePluginNotFoundException(String message) {
		super(message);
	}

}
