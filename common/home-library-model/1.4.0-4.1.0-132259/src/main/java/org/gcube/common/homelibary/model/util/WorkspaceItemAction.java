/**
 * 
 */
package org.gcube.common.homelibary.model.util;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public enum WorkspaceItemAction {
	/**
	 * The item has been created.
	 */
	CREATED,
	
	/**
	 * The item has been renamed.
	 */
	RENAMED,
	
	/**
	 * The item has been moved. 
	 */
	MOVED,
	
	/**
	 * The item has been cloned. 
	 */
	CLONED,
	
	/**
	 * The item has been updates. 
	 */
	UPDATED;

}
