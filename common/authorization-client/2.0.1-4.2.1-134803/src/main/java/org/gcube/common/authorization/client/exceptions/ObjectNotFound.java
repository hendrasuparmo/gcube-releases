package org.gcube.common.authorization.client.exceptions;

public class ObjectNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ObjectNotFound() {
		super();
	}

	public ObjectNotFound(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectNotFound(String message) {
		super(message);
	}

	public ObjectNotFound(Throwable cause) {
		super(cause);
	}

	
}
