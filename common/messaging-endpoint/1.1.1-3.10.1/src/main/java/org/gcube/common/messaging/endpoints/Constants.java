package org.gcube.common.messaging.endpoints;

/**
 * 
 * @author andrea
 *
 */
public class Constants {

	public static String failoverPrefix ="failover://";
	public static String reconnectionDelay ="initialReconnectDelay";

}
