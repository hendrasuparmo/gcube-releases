/**
 * 
 */
package org.gcube.common.homelibrary.jcr.importing;
/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public enum ImportRequestType {
	
	/**
	 * A content manager item.
	 */
	CONTENT_MANAGER_ITEM,
	
	/**
	 * A query. 
	 */
	QUERY,
	
	/**
	 * An url. 
	 */
	URL;

}
