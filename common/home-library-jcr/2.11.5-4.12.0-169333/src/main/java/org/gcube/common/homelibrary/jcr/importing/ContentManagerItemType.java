/**
 * 
 */
package org.gcube.common.homelibrary.jcr.importing;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public enum ContentManagerItemType {
	
	DOCUMENT,
	ALTERNATIVE,
	PART,
	ANNOTATION,
	METADATA;
}
