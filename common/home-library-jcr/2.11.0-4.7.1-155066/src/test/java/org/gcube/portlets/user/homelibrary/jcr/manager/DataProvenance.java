package org.gcube.portlets.user.homelibrary.jcr.manager;


public enum DataProvenance {
	IMPORTED,COMPUTED
}
