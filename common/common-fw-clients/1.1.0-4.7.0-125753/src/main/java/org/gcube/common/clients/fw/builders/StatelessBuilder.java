package org.gcube.common.clients.fw.builders;

import org.gcube.common.clients.builders.StatelessBuilderAPI.Builder;

/**
 * A {@link Builder} for stateless gCore services.
 * 
 * @author Fabio Simeoni
 *
 * @param <P> the type of service proxies
 */
public interface StatelessBuilder<P> extends Builder<P> {}
