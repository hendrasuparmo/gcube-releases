package org.gcube.smartgears.handlers;

/**
 * Partial implementation of {@link Handler}.
 * 
 * @author Fabio Simeoni
 *
 * 
 */
public abstract class AbstractHandler {

	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
	//so far, largely a placeholder for future cross-handler behaviour
	
}
