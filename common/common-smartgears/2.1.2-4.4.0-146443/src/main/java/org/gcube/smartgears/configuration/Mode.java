package org.gcube.smartgears.configuration;

/**
 * The management mode the container or its applications.
 * 
 * @author Fabio Simeoni
 *
 */
public enum Mode {
	online,
	offline
}