<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>org.gcube.tools</groupId>
		<version>1.0.0</version>
		<relativePath />
	</parent>

	<groupId>org.gcube.core</groupId>
	<artifactId>common-smartgears</artifactId>
	<version>2.1.5-4.13.0-151409</version>
	<name>SmartGears</name>
	
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.gcube.distribution</groupId>
				<artifactId>gcube-bom</artifactId>
				<version>1.2.0-4.13.0-171821</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	<properties>
		<distroDirectory>distro</distroDirectory>
		<tomcat.version>7.0.42</tomcat.version>
		<jersey.version>1.17.1</jersey.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<scm>
		<connection>scm:svn:http://svn.d4science.research-infrastructures.eu/gcube/trunk/Common/${project.artifactId}</connection>
		<developerConnection>scm:svn:https://svn.d4science.research-infrastructures.eu/gcube/trunk/Common/${project.artifactId}</developerConnection>
		<url>http://svn.d4science.research-infrastructures.eu/gcube/trunk/Common/${project.artifactId}</url>
	</scm>

	<dependencies>

		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>authorization-client</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>common-authorization</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.data.publishing</groupId>
			<artifactId>document-store-lib</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.gcube.accounting</groupId>
			<artifactId>accounting-lib</artifactId>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.resources</groupId>
			<artifactId>registry-publisher</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.resources</groupId>
			<artifactId>common-gcore-resources</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-validator</artifactId>
			<version>1.0.0-4.13.0-125998</version>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-scope</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-events</artifactId>
			<version>1.0.1-4.13.0-126364</version>
		</dependency>

		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.0.1</version>
			<scope>provided</scope>
		</dependency>


		<!-- ***************** test ******************* -->

		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-depchain</artifactId>
			<version>2.0.0-beta-2</version>
			<type>pom</type>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-client</artifactId>
			<version>${jersey.version}</version>
			<scope>test</scope>
		</dependency>


		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-core</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-logging-log4j</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-jasper</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.10</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.0.13</version>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<version>1.9.0</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>${distroDirectory}/descriptor.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>servicearchive</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- excludes probe package from jar -->
			<plugin>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.3.2</version>
				<executions>
					<execution>
						<id>default-jar</id>
						<phase>package</phase>
						<goals>
							<goal>jar</goal>
						</goals>
						<configuration>
							<excludes>
								<exclude>**/probe/**/*</exclude>
							</excludes>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- include probe in attached war -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<primaryArtifact>false</primaryArtifact>
					<classifier>probe</classifier>

					<packagingIncludes>WEB-INF/classes/org/gcube/smartgears/probe/**/*</packagingIncludes>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
				<executions>
					<execution>
						<id>probe-war</id>
						<phase>package</phase>
						<goals>
							<goal>war</goal>
						</goals>
					</execution>
				</executions>
			</plugin>


			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.15</version>
				<configuration>
					<!-- tomcat annotation discovery won't work with the default manifest-only 
						jar -->
					<useManifestOnlyJar>false</useManifestOnlyJar>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.6</version>
				<executions>
					<!-- interpolates and copies configuration.properties -->
					<execution>
						<id>copy-configuration</id>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<phase>validate</phase>
						<configuration>
							<outputDirectory>src/main/resources/META-INF</outputDirectory>
							<overwrite>true</overwrite>
							<encoding>UTF-8</encoding>
							<resources>
								<resource>
									<directory>${distroDirectory}</directory>
									<includes>
										<include>smartgears-config.xml</include>
									</includes>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>
</project>