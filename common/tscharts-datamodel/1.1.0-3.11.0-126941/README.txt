
* Support library for gwt based portlet for managing time series graphs.
* Types decared here are used on client-side of TimeSeriesVisualizer GWT base application
  for a such reason, inside src a timeseries.gwt.xml file has been introduced.
  The module here defined must be included in the .gwt.xml file of GWT based application
  that will use such data on client side.
  
  Usage:
  <inherits name='org.gcube.portlets.admin.timeseries.timeseries'/>