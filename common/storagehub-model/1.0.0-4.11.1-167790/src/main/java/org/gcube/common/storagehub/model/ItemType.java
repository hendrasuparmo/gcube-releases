package org.gcube.common.storagehub.model;

public enum ItemType {
    FOLDER,
    FILE;
}
