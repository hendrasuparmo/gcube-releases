package org.gcube.common.storagehub.model.items;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.gcube.common.storagehub.model.annotations.RootNode;


@NoArgsConstructor
@Getter
@Setter
@RootNode("nthl:workspaceItem")
public class FolderItem extends Item {

	
}
