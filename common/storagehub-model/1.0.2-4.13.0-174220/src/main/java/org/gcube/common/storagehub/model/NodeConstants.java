package org.gcube.common.storagehub.model;

public class NodeConstants {

	
	public static final String ACCOUNTING_NAME ="hl:accounting";
	public static final String METADATA_NAME ="hl:metadata";
	public static final String OWNER_NAME ="hl:owner";
	public static final String CONTENT_NAME ="jcr:content";
	public static final String USERS_NAME ="hl:users";
	public static final String PROPERTY_NAME ="hl:property";
}
