package org.gcube.common.storagehub.model.exceptions;

public class InvalidItemException extends StorageHubException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidItemException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
