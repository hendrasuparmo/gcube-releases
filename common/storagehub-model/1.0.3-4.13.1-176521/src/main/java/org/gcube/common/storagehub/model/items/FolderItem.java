package org.gcube.common.storagehub.model.items;

import org.gcube.common.storagehub.model.annotations.RootNode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@Getter
@Setter
@RootNode("nthl:workspaceItem")
public class FolderItem extends Item {

	
	
}
