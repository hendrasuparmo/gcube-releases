package org.gcube.common.storagehub.model.exceptions;

public class UserNotAuthorizedException extends StorageHubException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotAuthorizedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserNotAuthorizedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserNotAuthorizedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserNotAuthorizedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
