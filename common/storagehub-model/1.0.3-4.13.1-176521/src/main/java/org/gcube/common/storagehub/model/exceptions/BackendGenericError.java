package org.gcube.common.storagehub.model.exceptions;

public class BackendGenericError extends StorageHubException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BackendGenericError() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BackendGenericError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BackendGenericError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BackendGenericError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
