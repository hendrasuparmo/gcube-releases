package org.gcube.common.storagehub.model.types;

public class Namespace {
	
	public static final String HL_NAMESPACE						= "hl:";
	public static final String JCR_NAMESPACE					= "jcr:";
	public static final String REP_NAMESPACE					= "rep:";
	public static final String NT_NAMESPACE 					= "nt:";
	
}
