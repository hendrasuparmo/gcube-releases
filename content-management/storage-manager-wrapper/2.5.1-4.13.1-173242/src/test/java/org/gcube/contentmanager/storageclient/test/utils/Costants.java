/**
 * 
 */
package org.gcube.contentmanager.storageclient.test.utils;


import org.gcube.contentmanager.storageclient.wrapper.MemoryType;


/**
 * @author Roberto Cirillo (ISTI-CNR) 2018
 *
 */
public final class Costants {
	
	public static final MemoryType DEFAULT_MEMORY_TYPE = MemoryType.PERSISTENT;
	public static final String DEFAULT_SCOPE_STRING="/gcube/devsec";
	public static final String DEFAULT_PASS_PHRASE="this is a phrasethis is a phrase";

}
