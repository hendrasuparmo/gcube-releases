package org.gcube.contentmanager.storageclient.wrapper;

/**
 * Define the Memory type used for storage backend
 * @author Roberto Cirillo (ISTI-CNR)
 *
 */
public enum MemoryType {
	 PERSISTENT, VOLATILE
}

