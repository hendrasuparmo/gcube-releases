package org.gcube.application.aquamaps.ecomodelling.generators.connectors.livemonitor;

public class SingleResource {

	public String resId;
	public double value;
	public SingleResource(String resid, double val){
		resId = resid;
		value = val;
	}
	
}
