package org.gcube.application.aquamaps.ecomodelling.generators.connectors;

public enum EnvelopeName {

	TEMPERATURE,
	SALINITY,
	PRIMARY_PRODUCTION,
	ICE_CONCENTRATION,
	LAND_DISTANCE
}
