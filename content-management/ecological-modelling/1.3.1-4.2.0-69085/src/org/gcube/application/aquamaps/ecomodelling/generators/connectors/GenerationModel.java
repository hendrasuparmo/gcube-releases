package org.gcube.application.aquamaps.ecomodelling.generators.connectors;

public enum GenerationModel {

	AQUAMAPS,
	REMOTE_AQUAMAPS
}
