package org.gcube.application.aquamaps.ecomodelling.generators.connectors.subconnectors;

public class HcafParameter {

	private String name;
	private String min;
	private String max;
	private String mean;
	
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMin() {
		return min;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getMax() {
		return max;
	}
	public void setMean(String mean) {
		this.mean = mean;
	}
	public String getMean() {
		return mean;
	}
	
}
