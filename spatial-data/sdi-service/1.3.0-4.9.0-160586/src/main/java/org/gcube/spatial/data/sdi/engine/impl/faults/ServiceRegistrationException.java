package org.gcube.spatial.data.sdi.engine.impl.faults;

public class ServiceRegistrationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1570185699121566715L;

	public ServiceRegistrationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceRegistrationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ServiceRegistrationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceRegistrationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServiceRegistrationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
