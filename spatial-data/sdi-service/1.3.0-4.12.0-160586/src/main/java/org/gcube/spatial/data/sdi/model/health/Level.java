package org.gcube.spatial.data.sdi.model.health;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum	
public enum Level{
	WARNING,ERROR,OK
}