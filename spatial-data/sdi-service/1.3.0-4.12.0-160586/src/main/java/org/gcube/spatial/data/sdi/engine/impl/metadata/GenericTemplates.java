package org.gcube.spatial.data.sdi.engine.impl.metadata;

public class GenericTemplates {

	public static class ThreddsCatalogTemplate{
		public static final String FILENAME="thredds_catalog.ftlx";
		
		public static final String CATALOG_PATH="CatalogPath";
		public static final String LOCATION="Location";
		public static final String DATASET_SCAN_NAME="DataSetScanName";
		public static final String DATASET_SCAN_ID="DataSetScanID";
		public static final String AUTHORITY_URL="AuthorityURL";
	}
	
}
