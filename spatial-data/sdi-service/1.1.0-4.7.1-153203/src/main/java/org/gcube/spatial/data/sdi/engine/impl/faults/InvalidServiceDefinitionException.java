package org.gcube.spatial.data.sdi.engine.impl.faults;

public class InvalidServiceDefinitionException extends ServiceRegistrationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5251767289981417513L;

	public InvalidServiceDefinitionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceDefinitionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceDefinitionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceDefinitionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceDefinitionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
	
}
