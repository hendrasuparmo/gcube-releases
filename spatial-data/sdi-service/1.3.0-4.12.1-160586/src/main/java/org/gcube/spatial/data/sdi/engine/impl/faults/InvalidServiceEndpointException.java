package org.gcube.spatial.data.sdi.engine.impl.faults;

public class InvalidServiceEndpointException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3683038636163570578L;

	public InvalidServiceEndpointException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceEndpointException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceEndpointException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceEndpointException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidServiceEndpointException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
