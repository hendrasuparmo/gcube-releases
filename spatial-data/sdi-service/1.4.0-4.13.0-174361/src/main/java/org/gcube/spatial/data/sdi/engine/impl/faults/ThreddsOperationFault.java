package org.gcube.spatial.data.sdi.engine.impl.faults;

public class ThreddsOperationFault extends ServiceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4389581996150834969L;

	public ThreddsOperationFault() {
		// TODO Auto-generated constructor stub
	}

	public ThreddsOperationFault(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ThreddsOperationFault(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ThreddsOperationFault(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ThreddsOperationFault(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
