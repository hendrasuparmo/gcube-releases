package org.gcube.spatial.data.sdi.model.credentials;

public enum AccessType {

	CKAN,ADMIN,CONTEXT_USER,CONTEXT_MANAGER
}
