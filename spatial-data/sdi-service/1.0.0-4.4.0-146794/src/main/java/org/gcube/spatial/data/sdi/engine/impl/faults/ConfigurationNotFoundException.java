package org.gcube.spatial.data.sdi.engine.impl.faults;

public class ConfigurationNotFoundException extends Exception {

	public ConfigurationNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public ConfigurationNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
