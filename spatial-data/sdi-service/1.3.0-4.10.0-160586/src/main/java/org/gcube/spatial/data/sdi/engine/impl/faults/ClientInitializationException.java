package org.gcube.spatial.data.sdi.engine.impl.faults;

public class ClientInitializationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4466379337497096292L;

	public ClientInitializationException() {
		// TODO Auto-generated constructor stub
	}

	public ClientInitializationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ClientInitializationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ClientInitializationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ClientInitializationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
