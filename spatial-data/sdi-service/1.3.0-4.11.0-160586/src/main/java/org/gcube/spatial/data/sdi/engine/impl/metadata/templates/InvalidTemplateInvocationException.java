package org.gcube.spatial.data.sdi.engine.impl.metadata.templates;

public class InvalidTemplateInvocationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8921135030360257131L;

	public InvalidTemplateInvocationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidTemplateInvocationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidTemplateInvocationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidTemplateInvocationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidTemplateInvocationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
