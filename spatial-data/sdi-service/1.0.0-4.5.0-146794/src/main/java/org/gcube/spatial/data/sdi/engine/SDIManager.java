package org.gcube.spatial.data.sdi.engine;

import org.gcube.spatial.data.sdi.model.ScopeConfiguration;

public interface SDIManager {

	public ScopeConfiguration getContextConfiguration();
	
}
