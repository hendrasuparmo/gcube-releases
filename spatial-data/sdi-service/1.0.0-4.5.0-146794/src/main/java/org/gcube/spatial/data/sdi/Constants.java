package org.gcube.spatial.data.sdi;

public class Constants {

	
	public static final String APPLICATION="SDI-Service";
	public static final String GEONETWORK_INTERFACE="GeoNetwork";
	public static final String GEONETWORK_CONFIGURATION_PATH="configuration";
	public static final String GEONETWORK_GROUPS_PATH="groups";
	
	public static final String SDI_INTERFACE="SDI";
}
