package org.gcube.spatial.data.geonetwork.iso;

public class MissingInformationException extends Exception {

	public MissingInformationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MissingInformationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MissingInformationException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MissingInformationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6926907389393350700L;

	
}
