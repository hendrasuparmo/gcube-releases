package org.gcube.spatial.data.geonetwork.model.faults;

public class ConfigurationException extends GeoNetworkException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8652824274753741239L;

	public ConfigurationException() {
		// TODO Auto-generated constructor stub
	}

	public ConfigurationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigurationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
