package org.gcube.spatial.data.geonetwork;

public enum LoginLevel {

	DEFAULT,
	SCOPE,
	PRIVATE,
	CKAN,
	ADMIN
	
}
