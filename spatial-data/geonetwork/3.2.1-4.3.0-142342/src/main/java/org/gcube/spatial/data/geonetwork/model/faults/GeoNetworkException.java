package org.gcube.spatial.data.geonetwork.model.faults;

public class GeoNetworkException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1198171132587190575L;

	public GeoNetworkException() {
		// TODO Auto-generated constructor stub
	}

	public GeoNetworkException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GeoNetworkException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public GeoNetworkException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GeoNetworkException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
