package org.gcube.spatial.data.geonetwork.model.faults;



public class InvalidInsertConfigurationException extends GeoNetworkException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5337545988854484414L;

	public InvalidInsertConfigurationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidInsertConfigurationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidInsertConfigurationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidInsertConfigurationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidInsertConfigurationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
