package org.gcube.usecases.ws.thredds.faults;

public class RemoteFileNotFoundException extends InternalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4333525223191661141L;

	public RemoteFileNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public RemoteFileNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RemoteFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RemoteFileNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RemoteFileNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
