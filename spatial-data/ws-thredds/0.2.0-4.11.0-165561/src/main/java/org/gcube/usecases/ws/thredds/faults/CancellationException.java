package org.gcube.usecases.ws.thredds.faults;

public class CancellationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 736538914489728352L;

	public CancellationException() {
		// TODO Auto-generated constructor stub
	}

	public CancellationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CancellationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CancellationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CancellationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
