package org.gcube.usecases.ws.thredds.faults;

public class WorkspaceInteractionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1121867242282602650L;

	public WorkspaceInteractionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorkspaceInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
