package org.gcube.usecases.ws.thredds.faults;

public class WorkspaceLockedException extends WorkspaceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4220255895750039163L;

	public WorkspaceLockedException() {
		// TODO Auto-generated constructor stub
	}

	public WorkspaceLockedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceLockedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceLockedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceLockedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
