package org.gcube.usecases.ws.thredds.faults;

public class UnableToLockException extends InternalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6926321942546668032L;

	public UnableToLockException() {
		// TODO Auto-generated constructor stub
	}

	public UnableToLockException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UnableToLockException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnableToLockException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnableToLockException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
