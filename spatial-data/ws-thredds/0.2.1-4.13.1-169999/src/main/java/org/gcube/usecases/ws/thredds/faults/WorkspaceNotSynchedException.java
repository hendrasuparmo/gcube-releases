package org.gcube.usecases.ws.thredds.faults;

public class WorkspaceNotSynchedException extends WorkspaceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -321145123900245553L;

	public WorkspaceNotSynchedException() {
		// TODO Auto-generated constructor stub
	}

	public WorkspaceNotSynchedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceNotSynchedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceNotSynchedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceNotSynchedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
