package org.gcube.usecases.ws.thredds.faults;

public class DataTransferPluginError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5928532367397696543L;

	public DataTransferPluginError() {
		// TODO Auto-generated constructor stub
	}

	public DataTransferPluginError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataTransferPluginError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DataTransferPluginError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataTransferPluginError(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
