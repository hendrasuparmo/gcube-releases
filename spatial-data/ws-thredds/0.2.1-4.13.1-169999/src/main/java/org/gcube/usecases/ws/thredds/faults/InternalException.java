package org.gcube.usecases.ws.thredds.faults;

public class InternalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3088246150497872471L;

	public InternalException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InternalException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
