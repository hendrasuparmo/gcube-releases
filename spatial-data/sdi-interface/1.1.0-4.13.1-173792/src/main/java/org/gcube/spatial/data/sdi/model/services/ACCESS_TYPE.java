package org.gcube.spatial.data.sdi.model.services;

public enum ACCESS_TYPE{
	CONFIDENTIAL,SHARED,PUBLIC
}