package org.gcube.resources.federation.fhnmanager.api.exception;

public abstract class FHNManagerException extends RuntimeException {

	public FHNManagerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FHNManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public FHNManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FHNManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FHNManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
