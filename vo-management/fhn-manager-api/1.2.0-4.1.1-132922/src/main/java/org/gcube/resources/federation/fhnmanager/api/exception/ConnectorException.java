package org.gcube.resources.federation.fhnmanager.api.exception;

public class ConnectorException extends FHNManagerException {

	public ConnectorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConnectorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ConnectorException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConnectorException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConnectorException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
