package org.gcube.resources.federation.fhnmanager.api;

public class Constants {

	public static final String SERVICE_CLASS = "ResourceManagement";

	public static final String SERVICE_NAME = "FHNManager";

	public static final String SERVICE_API_NAME = "REST-API";

}
