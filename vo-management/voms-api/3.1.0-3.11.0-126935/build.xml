<?xml version="1.0"?>
<project default="deploy">
	
	<!-- environment -->
	<property environment="env" />

	<!-- external environment -->
	<echo message="container.dir ->${env.GLOBUS_LOCATION}" level="info"/>
	<property name="container.dir" value="${env.GLOBUS_LOCATION}" />
	<property name="container.schema.dir" location="${container.dir}/share/schema" />

	<!-- load non-standard tasks -->
	<taskdef resource="ise/antelope/tasks/antlib.xml">
	  <classpath>
	    <pathelement location="${container.dir}/lib/AntelopeTasks_3.4.2.jar"/>
	  </classpath>
	</taskdef>
	
	<!-- discriminate between local and remote build -->
	<property name="etics.build" value="false" />
		
	<!-- service-specific locations -->	
	<property name="service.dir" location="." />
	<property name="schema.dir.name" value="schema" />
	<property name="schema.dir" location="${service.dir}/${schema.dir.name}"/>	
	<property name="service.dir" location="." />
	<property name="etc.dir.name" value="etc" />
	<property name="etc.dir" value="${service.dir}/${etc.dir.name}" />
	<property name="source.dir" value="${service.dir}/src" />
	
	<!-- test -->
	<property name="test.dir" value="${service.dir}/test" />
	
	<!-- load input properties  -->
	<property file="${etc.dir}/build.properties" />
	<stringutil property="package.dir" string="${package}"><!-- derive package.dir from ${package} -->
		<replace regex="\." replacement="/"/>
	</stringutil>	
	
	<!-- file defaults -->
	<property name="jarfile" value="${package}.jar" />	
	
	<!-- test -->
	<property name="jarfile.test" value="${package}.testsuite.jar" />	
	<if name="etics.build" value="true">	
		<property name="build.location" location="${service.dir}" />
		<property name="lib.location" value="${build.location}/lib" />
		<else>
			<property name="build.location" location="${env.BUILD_LOCATION}" />
			<property name="lib.location" value="${build.location}/${lib.dir}" />
		</else>
	</if>
		
	<!-- temporary build locations -->
	<property name="build.dir" location="${build.location}/build" />
	<property name="build.class.dir" location="${build.dir}/classes" />
	<property name="build.lib.dir" location="${build.dir}/lib" />
	<property name="build.schema.dir" location="${build.dir}/schema" />
	<property name="build.schema.service.dir" location="${build.schema.dir}/${package}" />		
	<property name="build.stubs.dir" location="${build.dir}/stubs-${package}" />
	<property name="build.stubs.src.dir" location="${build.stubs.dir}/src" />
	<property name="build.stubs.class.dir" location="${build.stubs.dir}/classes" />
	
	<!-- test -->
	<property name="build.test.dir" location="${build.dir}/test-${package}" />
	<property name="build.test.src.dir" location="${build.test.dir}/src" />
	<property name="build.test.class.dir" location="${build.test.dir}/classes" />

	<!-- misc defaults -->
	<property name="java.debug" value="on" />
	<property name="java.deprecation" value="off" />
		
	<!-- common filesets -->
	<fileset dir="${build.lib.dir}" id="garjars" />
	<property name="garjars.id" value="garjars" />
	
	<property name="stubs.dir.name" value="stubs" />	
	
		
	<!-- initialisation tasks -->
	<target name="init" depends="clean" description="creates build structures"> 
	
		<!-- input summary -->
		<echo message="Root Package -> ${package}" level="info"/>
		<echo message="Configuration -> ${etc.dir}" level="info"/>
		<echo message="External dependencies -> ${lib.location}" level="info"/>

		<!-- output summary -->
		<echo message="Lib Jar -> ${jarfile}" level="info"/>
		
		<!-- create dependency location, if it does not exist already -->
		<mkdir dir="${lib.location}" />
		
		<!-- create temporary build folders -->
		<mkdir dir="${build.dir}" />
		<mkdir dir="${build.class.dir}" />
		<delete dir="${build.lib.dir}" />
		<mkdir dir="${build.lib.dir}" />
		
		<!-- test -->
		<mkdir dir="${build.test.dir}" />
		<mkdir dir="${build.test.src.dir}" />
		<mkdir dir="${build.test.class.dir}"/>				
	</target>	
	   		
	<target name="build" depends="init" >
		<javac srcdir="${source.dir}" destdir="${build.class.dir}" debug="${java.debug}" deprecation="${java.deprecation}" verbose="true" description="compile stub classes">
			<include name="**/*.java" />
			<classpath>
				<fileset dir="${lib.location}">
					<include name="**/*.jar" />
				</fileset>
				<fileset dir="${container.dir}/lib">
					<include name="*.jar" />
				</fileset>
			</classpath>
		</javac>
		<copy toDir="${build.class.dir}/META-INF" overwrite="false"><!-- copy configuration info as well -->
			<fileset dir="${etc.dir}" casesensitive="yes" />
		</copy>
	</target>
	
	<target name="jar" depends="build">
		<jar destfile="${build.lib.dir}/${jarfile}" basedir="${build.class.dir}" />
	</target>
	
	<target name="deploy" depends="jar" description="deploy stub jar">
		<copy file="${build.lib.dir}/${jarfile}" toDir="${lib.location}"/>
		<if name="etics.build" value="false">	
			<copy file="${build.lib.dir}/${jarfile}" toDir="${container.dir}/lib"/>
		</if>	
	</target>	
	
	<!-- javadoc tasks -->
	<target name="doc">
		<javadoc access="public" author="true" sourcepath="${source.dir}" packagenames="*"
			destdir="doc/api" nodeprecated="false" nodeprecatedlist="false" 
			noindex="false" nonavbar="false" notree="false" 
			source="1.5" 
			splitindex="true" 
			use="true" version="true" failonerror="false">
			<classpath>
				<fileset dir="${lib.location}">
					<include name="**/*.jar" />
				</fileset>
				<fileset dir="${container.dir}/lib">
					<include name="*.jar" />
				</fileset>
			</classpath>
		</javadoc>
	</target>
		
	<target name="clean">
		<echo message="build.location ->${env.BUILD_LOCATION}" level="info"/>
		<echo message="lib.location ->${lib.location}" level="info"/>
		<echo message="build.dir ->${build.dir}" level="info"/>
		<echo message="lib.dir ->${lib.dir}" level="info"/>		

		<echo message="etics.build ->${etics.build}" level="info"/>
	
		<delete dir="${build.dir}" quiet="true"/>
		<delete file="${lib.location}/${jarfile}" quiet="true"/>
	</target>
		
	<target name="buildTest" depends="init" description="compiles test implementation">
		<javac srcdir="${test.dir}" destdir="${build.test.class.dir}" debug="${java.debug}" deprecation="${java.deprecation}">
			<classpath>
				<fileset dir="${lib.location}">
					<include name="**/*.jar" />
				</fileset>
				<fileset dir="${container.dir}/lib"> 
					<include name="*.jar" />
				</fileset>
			</classpath>
		</javac>
	</target>	
	
	<target name="jarTest" depends="buildTest">
		<jar destfile="${build.lib.dir}/${jarfile.test}" basedir="${build.test.class.dir}" />
	</target>	

	<target name="createSoftwareArchive">
	
		<!-- must match package names in profile.xml -->		
		<property name="api.packagename" value="voms-api"/>
		<property name="api.dir" location="${prefix}/${api.packagename}"/>
	
		<!-- create directories -->
		<mkdir dir="${prefix}"/>
		<mkdir dir="${api.dir}"/>
		<mkdir dir="${sa.test.dir}"/>
		
		<!-- profile -->
		<copy file="${sa.api}/etc/profile.xml" todir="${prefix}"/>
		
		<!-- api stuff -->
		<copy todir="${api.dir}">
			<fileset dir="${sa.api}/build/lib">
				<include name="${jarfile}"/>
			</fileset>
			<fileset dir="${sa.api}">
				<include name="doc/**"/>
			</fileset>			
		</copy>
		<echo message="${sa.api.vcsroot}/${sa.api.tag}" file="${api.dir}/svnpath.txt"/>
		
		<!-- doc stuff -->
		<copy todir="${prefix}">
			<fileset dir="${sa.api}">
				<include name="README"/>
				<include name="LICENSE"/>
				<include name="MAINTAINERS"/>
				<include name="CHANGELOG"/>
				<include name="INSTALL"/>
			</fileset>
		</copy>
	</target>
					
</project>
