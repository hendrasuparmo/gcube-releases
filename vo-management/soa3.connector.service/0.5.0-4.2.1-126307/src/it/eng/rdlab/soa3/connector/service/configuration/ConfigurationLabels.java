package it.eng.rdlab.soa3.connector.service.configuration;

public interface ConfigurationLabels 
{
	public static String SOA3_ENDPOINT = "SOA3_ENDPOINT";
	public static String SERVICE_NAME = "SERVICE_NAME";
	public static String AUTH_SESSION = "AUTHENTICATION_SESSION";
	public static String CERT_FILE = "CERT_FILE";
	public static String KEY_FILE = "KEY_FILE";
	public static String TRUST_DIR = "TRUST_DIR";
	public static String TRUST_FILE_EXTENSION = "TRUST_FILE_EXTENSION";
	public static String AUTHZ_ENABLED = "AUTHORIZATION_ENABLED";
	public static String DEFAULT_ORGANIZATION = "DEFAULT_ORGANIZATION";
	public static String GCUBE_SCOPE = "GCUBE_SCOPE";

}
