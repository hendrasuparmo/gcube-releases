package it.eng.rdlab.soa3.authn.rest.exceptions;

public class JSONParserException extends RuntimeException {

	private static final long serialVersionUID = 3522546155468671424L;

	public JSONParserException(String message, Throwable cause) {

		super(message, cause);
	}

}
