package it.eng.rdlab.soa3.assertion.manager;

public class SamlConstants 
{
	public static final String 	ROLE_ATTRIBUTE = "role",
								USERNAME_ATTRIBUTE = "uid",
								EXTERNAL_ROLE_NAME = "external";
}
