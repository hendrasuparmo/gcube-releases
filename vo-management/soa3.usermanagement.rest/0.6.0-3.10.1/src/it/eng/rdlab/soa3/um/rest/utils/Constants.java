package it.eng.rdlab.soa3.um.rest.utils;

/**
 * Some useful constants (LDAP management)
 * 
 * @author Ermanno Travaglino
 * @version 1.0
 * 
 * 
 */
public class Constants 
{

	/**
	 * people root node's id
	 */
	public static final String 	OU_PEOPLE = "people";
	/**
	 * groups root node's id
	 */
	public static final String 	OU_GROUPS = "groups";
	/**
     * roles root node's id
	 */
	public static final String 	OU_ROLES = "roles";





}
