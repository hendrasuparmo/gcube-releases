package org.gcube.soa3.connector.common.security;

public interface MessageConstants 
{
	
	public static final String 	BINARY_SECURITY_TOKEN_LABEL = "BinarySecurityToken",
								BINARY_SECURITY_TOKEN_PREFIX = "wsse",
								WSSE_NAMESPACE = "http://schemas.xmlsoap.org/ws/2002/04/secext",
								VALUE_TYPE_LABEL = "ValueType",
								ENCODING_TYPE_LABEL = "EncodingType",
								BASE64 = "wsse:Base64Binary",
								ID_LABEL = "Id",
								SECURITY_TOKEN = "SecurityToken";

}
