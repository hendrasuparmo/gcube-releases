package org.gcube.vomanagement.usermanagement.model;

public enum MembershipRequestStatus {
	REQUEST, APPROVED, DENIED;
}
