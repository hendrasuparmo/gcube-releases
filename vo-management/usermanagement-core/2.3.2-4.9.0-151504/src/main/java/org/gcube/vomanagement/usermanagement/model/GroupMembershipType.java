package org.gcube.vomanagement.usermanagement.model;

public enum GroupMembershipType {
	OPEN,
	RESTRICTED,
	PRIVATE;
}
