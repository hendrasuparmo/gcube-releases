package it.eng.rdlab.soa3.connector.utils;

public interface XMLConstants 
{
	
	public static final String 	SCHEMA_INSTANCE_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance",
								NIL_QN = "xsi:nil";

}
