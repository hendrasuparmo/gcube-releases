package org.gcube.vomanagement.vomsapi.util;

public interface VOMSFQANInfo 
{
	
	public String getFQAN ();
	
	public String getVoName();
	
	public void setVoName(String voName);
	
	public String getString ();

}
