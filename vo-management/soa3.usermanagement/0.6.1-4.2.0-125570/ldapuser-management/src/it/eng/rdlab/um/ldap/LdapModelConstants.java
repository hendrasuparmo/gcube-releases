package it.eng.rdlab.um.ldap;

public interface LdapModelConstants 
{
	public static final String 	OBJECT_CLASSES = "OBJECTCLASSES",
								OBJECT_CLASS_TOP = "top";

}
