package org.gcube.vomanagement.occi;

public class OcciConstants {

  public static final String OS_TPL = "os_tpl";
  public static final String RESOURCE_TPL = "resource_tpl";

  public static final String CORE_SUMMARY = "occi.core.summary";
  public static final String CORE_ID = "occi.core.id";
  public static final String CORE_TITLE = "occi.core.title";
  public static final String CORE_TARGET = "occi.core.target";

  public static final String NETWORKINTERFACE = "networkinterface";
  public static final String NETWORKINTERFACE_ADDRESS = "occi.networkinterface.address";
  public static final String NETWORKINTERFACE_INTERFACE = "occi.networkinterface.interface";
  public static final String NETWORKINTERFACE_MAC = "occi.networkinterface.mac";
  public static final String NETWORKINTERFACE_STATE = "occi.networkinterface.state";

  public static final String NETWORK_ADDRESS = "occi.network.address";
  public static final String NETWORK_ALLOCATION = "occi.network.allocation";
  public static final String NETWORK_GATEWAY = "occi.network.gateway";

  public static final String STORAGELINK = "storagelink";
  public static final String STORAGELINK_DEVICEID = "occi.storagelink.deviceid";
  public static final String STORAGELINK_STATE = "occi.storagelink.state";

  public static final String STORAGE = "storage";

}
