/**
 * 
 */
package org.gcube.portlets.admin.irbootstrapperportlet.gwt.server;

/**
 * @author Spyros Boutsis, NKUA
 *
 */
public class ConfigurationParserConstants {

	public static final String TYPES_ELEMENT = "types";
	public static final String DATATYPE_ELEMENT = "type";
	public static final String TASKTYPE_ELEMENT = "tasktype";
	public static final String JOBTYPE_ELEMENT = "jobtype";
	public static final String TASK_ELEMENT = "task";
	public static final String INPUT_ELEMENT = "input";
	public static final String OUTPUT_ELEMENT = "output";
	public static final String RUN_ELEMENT = "run";
	public static final String TASKDEFINITION_ELEMENT = "taskDefinition"; 
	public static final String PARALLEL_ELEMENT = "parallel";
	public static final String SEQUENTIAL_ELEMENT = "sequential";
	public static final String ASSIGN_ELEMENT = "assign";
	public static final String JOBDEFINITION_ELEMENT = "jobDefinition";
	public static final String JOBS_ELEMENT = "jobs";
	public static final String JOB_ELEMENT = "job";
	public static final String JOBINIT_ELEMENT = "initialization";
	public static final String CHAINEXECUTION_ELEMENT = "ChainExecution";
	public static final String CHAINCONNECTASSIGNMENTS_ELEMENT = "ChainConnectionAssignments";
											
	public static final String TYPE_ATTRIBUTE = "type";
	public static final String NAME_ATTRIBUTE = "name";
	public static final String DESCRIPTION_ATTRIBUTE = "description";
	public static final String CLASS_ATTRIBUTE = "class";
	public static final String TASKTYPE_ATTRIBUTE = "tasktype";
	public static final String JOBTYPE_ATTRIBUTE = "jobtype";
	public static final String ASSIGNTO_ATTRIBUTE = "to";
	public static final String ASSIGNVALUE_ATTRIBUTE = "value";
	public static final String ASSIGNVALUE_USERINPUTLABEL = "userInputLabel";
	public static final String EXTENDS_ATTRIBUTE = "extends";
	
	public static final String VARIABLE_START = "%";
}
