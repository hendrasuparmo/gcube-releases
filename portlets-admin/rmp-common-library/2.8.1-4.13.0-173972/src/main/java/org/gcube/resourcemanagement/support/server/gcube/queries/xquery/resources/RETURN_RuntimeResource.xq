<Resource>
        {$profiles//Resource/ID}
        <Type>{$profiles//Resource/Type/text()}</Type>
        <SubType>{$subtype}</SubType>
        <Scopes>{$scopes}</Scopes>
        <Name>{$profiles//Resource/Profile/Name/text()}</Name>
        <Host>{$profiles//Resource/Profile/RunTime/HostedOn/text()}</Host>
</Resource>