<Resource>
        {$profiles//Resource/ID}
        <Type>{$profiles//Resource/Type/text()}</Type>
        <SubType>{$subtype}</SubType>
        <Scopes>{$scopes}</Scopes>
        <Name>{$profiles//Resource/Profile/Name/text()}</Name>
        <Description>{$profiles//Resource/Profile/Description/text()}</Description>
</Resource>