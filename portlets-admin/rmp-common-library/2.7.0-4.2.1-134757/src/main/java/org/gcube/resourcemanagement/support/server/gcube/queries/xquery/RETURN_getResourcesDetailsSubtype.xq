<Resource>
        {$profiles//Resource/child::*}
        <SubType>{$subtype}</SubType>
        <gcf-version>{$gcf-version}</gcf-version>
        <ghn-version>{$ghn-version}</ghn-version>
        <scopes>{$scopes}</scopes>
        <ghn-name>{$ghn-name}</ghn-name>
</Resource>