package org.gcube.portlets.admin.resourcesweeper.client;

import org.gcube.portlets.admin.resourcesweeper.client.dialog.SweeperDialog;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Resource_sweeper implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		//### Just for testing leave commented
		 //new SweeperDialog("/gcube/devsec");
	}
}
