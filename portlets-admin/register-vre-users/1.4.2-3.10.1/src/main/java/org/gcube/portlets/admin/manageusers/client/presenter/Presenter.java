package org.gcube.portlets.admin.manageusers.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

public interface Presenter {
	public void go(HasWidgets container);
}
