<form class="form-horizontal geonetwork-publisher-metadata-form">
	<div class='spinner' style='display:none;'></div>
	
	<p class="header-description">ABSTRACT METADATA</p>
	<hr>
	
	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-general-purpose">Layer purpose</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-general-purpose" name="purpose" class="span11" rows="1" cols="50" placeholder="Please fill in the purpose of the Layer"></textarea>				
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-general-limitation">Layer limitations</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-general-limitation" name="limitation" class="span11" rows="1" cols="50" placeholder="Please fill in the limitation of the Layer"></textarea>				
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>
		</div>
	</div>
	
	<p class="header-description">AUTHOR</p>
	<hr>
	
	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-author-organisation-name">Auth. Organization</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-author-organisation-name" name="authorOrganisationName" class="span11" rows="1" cols="50" placeholder="Please fill in the organisation's name"></textarea>				
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<p class="header-description">DISTRIBUTOR</p>
	<hr>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-distributor-organisation-name">Distr. Organization</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-distributor-organisation-name" name="distributorOrganisationName" class="span11" rows="1" cols="50" placeholder="Please fill in the organisation's name"></textarea>				
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-distributor-individual-name">Distributor/s</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-distributor-individual-name" name="distributorIndividualName" class="span11" rows="1" cols="50" placeholder="Please fill in the distributor name(s)"></textarea>				
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-distributor-online-resource">URL of distribution</label>
		<div class="controls">
			<textarea class="span11" id="geonetwork-publisher-distributor-online-resource" name="distributorOnlineResource" rows="1" cols="50" placeholder="Please fill in the URL of distribution">http://</textarea>
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<p class="header-description">PROVIDER</p>
	<hr>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-provider-organisation-name">Prov. Organization</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-provider-organisation-name" name="providerOrganisationName" class="span11" rows="1" cols="50" placeholder="Please fill in the organisation's name"></textarea>
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-provider-individual-name">Providers</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-provider-individual-name" name="providerIndividualName" class="span11" rows="1" cols="50" placeholder="Please fill in the provider name(s)"></textarea>
			<span class="makeMeOrange">*</span>
			<span class="help-inline span11"></span>			
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="geonetwork-publisher-provider-online-resource">URL of provision</label>
		<div class="controls">
			<textarea id="geonetwork-publisher-provider-online-resource" name="providerOnlineResource" class="span11" rows="1" cols="50" placeholder="Please fill in the URL of provision">http://</textarea>
			<span class="makeMeOrange">*</span>	
			<span class="help-inline span11"></span>					
		</div>
	</div>
</form>