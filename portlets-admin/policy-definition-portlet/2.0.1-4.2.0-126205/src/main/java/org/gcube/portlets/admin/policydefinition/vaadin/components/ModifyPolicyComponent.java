package org.gcube.portlets.admin.policydefinition.vaadin.components;

import org.gcube.portlets.admin.policydefinition.common.util.TimeFormatHelper;
import org.gcube.portlets.admin.policydefinition.vaadin.containers.PoliciesContainer;
import org.gcube.portlets.admin.policydefinition.vaadin.listeners.CloseButtonListener;
import org.gcube.portlets.admin.policydefinition.vaadin.listeners.ConfirmUpdatePopupButtonListener;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ModifyPolicyComponent extends CustomComponent {

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	@AutoGenerated
	private AbsoluteLayout mainLayout;
	@AutoGenerated
	private Panel panel_4;
	@AutoGenerated
	private VerticalLayout verticalLayout_4;
	@AutoGenerated
	private CheckBox permitCheckBox;
	@AutoGenerated
	private Panel panel_3;
	@AutoGenerated
	private VerticalLayout verticalLayout_3;
	@AutoGenerated
	private PopupDateField endPopupDateField;
	@AutoGenerated
	private PopupDateField startPopupDateField;
	@AutoGenerated
	private Panel panel_1;
	@AutoGenerated
	private VerticalLayout verticalLayout_1;
	@AutoGenerated
	private TextField endTimeTextField;
	@AutoGenerated
	private TextField startTimeTextField;
	@AutoGenerated
	private CheckBox overridePermit;
	@AutoGenerated
	private CheckBox overrideDate;
	@AutoGenerated
	private CheckBox overrideTime;
	@AutoGenerated
	private PolicyTable policyTable;
	@AutoGenerated
	private Button cancel;
	@AutoGenerated
	private Button update;
	private static final long serialVersionUID = 6541565649374450747L;
	/**
	 * The constructor should first build the main layout, set the
	 * composition root and then do any custom initialization.
	 *
	 * The constructor will not be automatically regenerated by the
	 * visual editor.
	 */
	public ModifyPolicyComponent(Table policyTable, String service) {
		buildMainLayout();
		setCompositionRoot(mainLayout);
		
		this.policyTable.setContainerDataSource(PoliciesContainer.getPoliciesContainer(policyTable));
		
		startTimeTextField.addValidator(new RegexpValidator(TimeFormatHelper.TIME_REGEX, true,
                "{0} does not match the right format"));
		endTimeTextField.addValidator(new RegexpValidator(TimeFormatHelper.TIME_REGEX, true,
                "{0} does not match the right format"));

		startPopupDateField.setDateFormat("dd/MM/yyyy");
		startPopupDateField.setInputPrompt("dd/MM/yyyy");
		endPopupDateField.setDateFormat("dd/MM/yyyy");
		endPopupDateField.setInputPrompt("dd/MM/yyyy");

		cancel.addListener(new CloseButtonListener());
		update.focus();
		update.addListener(
				new ConfirmUpdatePopupButtonListener(
						service, 
						this.policyTable,
						startTimeTextField,
						endTimeTextField,
						startPopupDateField,
						endPopupDateField,
						permitCheckBox, 
						overridePermit, 
						overrideDate, 
						overrideTime, 
						cancel));
		
	}

	@AutoGenerated
	private AbsoluteLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new AbsoluteLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("740px");
		mainLayout.setHeight("400px");
		mainLayout.setMargin(false);
		
		// top-level component properties
		setWidth("740px");
		setHeight("400px");
		
		// update
		update = new Button();
		update.setCaption("Update");
		update.setImmediate(true);
		update.setWidth("-1px");
		update.setHeight("-1px");
		mainLayout.addComponent(update, "top:354.0px;left:20.0px;");
		
		// cancel
		cancel = new Button();
		cancel.setCaption("Cancel");
		cancel.setImmediate(true);
		cancel.setWidth("66px");
		cancel.setHeight("-1px");
		mainLayout.addComponent(cancel, "top:354.0px;right:574.0px;");
		
		// policyTable
		policyTable = new PolicyTable();
		policyTable.setImmediate(false);
		policyTable.setWidth("700px");
		policyTable.setHeight("180px");
		mainLayout.addComponent(policyTable, "top:20.0px;left:20.0px;");
		
		// overrideTime
		overrideTime = new CheckBox();
		overrideTime.setCaption("Modify Time Range");
		overrideTime.setImmediate(false);
		overrideTime.setWidth("-1px");
		overrideTime.setHeight("-1px");
		mainLayout.addComponent(overrideTime, "top:328.0px;left:41.0px;");
		
		// overrideDate
		overrideDate = new CheckBox();
		overrideDate.setCaption("Modify Date Range");
		overrideDate.setImmediate(false);
		overrideDate.setWidth("-1px");
		overrideDate.setHeight("-1px");
		mainLayout.addComponent(overrideDate, "top:329.0px;left:280.0px;");
		
		// overridePermit
		overridePermit = new CheckBox();
		overridePermit.setCaption("Modify Permit/Deny");
		overridePermit.setImmediate(false);
		overridePermit.setWidth("-1px");
		overridePermit.setHeight("-1px");
		mainLayout.addComponent(overridePermit, "top:329.0px;left:540.0px;");
		
		// panel_1
		panel_1 = buildPanel_1();
		mainLayout.addComponent(panel_1, "top:220.0px;left:20.0px;");
		
		// panel_3
		panel_3 = buildPanel_3();
		mainLayout.addComponent(panel_3, "top:221.0px;left:260.0px;");
		
		// panel_4
		panel_4 = buildPanel_4();
		mainLayout.addComponent(panel_4, "top:220.0px;left:520.0px;");
		
		return mainLayout;
	}

	@AutoGenerated
	private Panel buildPanel_1() {
		// common part: create layout
		panel_1 = new Panel();
		panel_1.setImmediate(false);
		panel_1.setWidth("192px");
		panel_1.setHeight("101px");
		
		// verticalLayout_1
		verticalLayout_1 = buildVerticalLayout_1();
		panel_1.setContent(verticalLayout_1);
		
		return panel_1;
	}

	@AutoGenerated
	private VerticalLayout buildVerticalLayout_1() {
		// common part: create layout
		verticalLayout_1 = new VerticalLayout();
		verticalLayout_1.setImmediate(false);
		verticalLayout_1.setWidth("100.0%");
		verticalLayout_1.setHeight("100.0%");
		verticalLayout_1.setMargin(false);
		
		// startTimeTextField
		startTimeTextField = new TextField();
		startTimeTextField.setCaption("Start time range (hh:mm)");
		startTimeTextField.setImmediate(false);
		startTimeTextField.setWidth("-1px");
		startTimeTextField.setHeight("-1px");
		verticalLayout_1.addComponent(startTimeTextField);
		verticalLayout_1.setComponentAlignment(startTimeTextField,
				new Alignment(48));
		
		// endTimeTextField
		endTimeTextField = new TextField();
		endTimeTextField.setCaption("End time range (hh:mm)");
		endTimeTextField.setImmediate(false);
		endTimeTextField.setWidth("-1px");
		endTimeTextField.setHeight("-1px");
		verticalLayout_1.addComponent(endTimeTextField);
		verticalLayout_1.setComponentAlignment(endTimeTextField, new Alignment(
				20));
		
		return verticalLayout_1;
	}

	@AutoGenerated
	private Panel buildPanel_3() {
		// common part: create layout
		panel_3 = new Panel();
		panel_3.setImmediate(false);
		panel_3.setWidth("220px");
		panel_3.setHeight("99px");
		
		// verticalLayout_3
		verticalLayout_3 = buildVerticalLayout_3();
		panel_3.setContent(verticalLayout_3);
		
		return panel_3;
	}

	@AutoGenerated
	private VerticalLayout buildVerticalLayout_3() {
		// common part: create layout
		verticalLayout_3 = new VerticalLayout();
		verticalLayout_3.setImmediate(false);
		verticalLayout_3.setWidth("100.0%");
		verticalLayout_3.setHeight("100.0%");
		verticalLayout_3.setMargin(false);
		
		// startPopupDateField
		startPopupDateField = new PopupDateField();
		startPopupDateField.setCaption("Start date");
		startPopupDateField.setImmediate(false);
		startPopupDateField.setWidth("-1px");
		startPopupDateField.setHeight("-1px");
		startPopupDateField.setResolution(4);
		verticalLayout_3.addComponent(startPopupDateField);
		verticalLayout_3.setComponentAlignment(startPopupDateField,
				new Alignment(48));
		
		// endPopupDateField
		endPopupDateField = new PopupDateField();
		endPopupDateField.setCaption("End date");
		endPopupDateField.setImmediate(false);
		endPopupDateField.setWidth("-1px");
		endPopupDateField.setHeight("-1px");
		endPopupDateField.setResolution(4);
		verticalLayout_3.addComponent(endPopupDateField);
		verticalLayout_3.setComponentAlignment(endPopupDateField,
				new Alignment(20));
		
		return verticalLayout_3;
	}

	@AutoGenerated
	private Panel buildPanel_4() {
		// common part: create layout
		panel_4 = new Panel();
		panel_4.setImmediate(false);
		panel_4.setWidth("180px");
		panel_4.setHeight("101px");
		
		// verticalLayout_4
		verticalLayout_4 = buildVerticalLayout_4();
		panel_4.setContent(verticalLayout_4);
		
		return panel_4;
	}

	@AutoGenerated
	private VerticalLayout buildVerticalLayout_4() {
		// common part: create layout
		verticalLayout_4 = new VerticalLayout();
		verticalLayout_4.setImmediate(false);
		verticalLayout_4.setWidth("100.0%");
		verticalLayout_4.setHeight("100.0%");
		verticalLayout_4.setMargin(false);
		
		// permitCheckBox
		permitCheckBox = new CheckBox();
		permitCheckBox.setCaption("Permit");
		permitCheckBox.setImmediate(false);
		permitCheckBox.setWidth("-1px");
		permitCheckBox.setHeight("-1px");
		verticalLayout_4.addComponent(permitCheckBox);
		verticalLayout_4.setComponentAlignment(permitCheckBox,
				new Alignment(48));
		
		return verticalLayout_4;
	}

}
