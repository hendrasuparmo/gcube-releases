/**
 * 
 */
package org.gcube.portlets.admin.gcubereleases.client.view;

import com.google.gwt.user.client.ui.HTML;

/**
 * The Class DividerElement.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 19, 2015
 */
public class DividerElement extends HTML{
	
	/**
	 * Instantiates a new divider element.
	 */
	public DividerElement() {
		setHTML("<div class=\"divider\" style=\"padding: 0 1%;\"><hr></div>");
	}

}
