/**
 * 
 */
package org.gcube.portlets.admin.gcubereleases.client.manage;

/**
 * The Interface FormCompleted.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 19, 2015
 */
public interface FormCompleted {

	/**
	 * Do form completed.
	 */
	void doFormCompleted();
}
