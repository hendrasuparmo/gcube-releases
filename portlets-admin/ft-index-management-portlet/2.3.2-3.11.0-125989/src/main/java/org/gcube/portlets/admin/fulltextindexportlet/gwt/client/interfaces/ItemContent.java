package org.gcube.portlets.admin.fulltextindexportlet.gwt.client.interfaces;

/** An interface used to get an identifier for an Index related item */
public interface ItemContent {

    /**
     * Returns an Identifier for the index related Item
     * 
     * @return - an Identifier for the Item
     */
    public String getID();
}
