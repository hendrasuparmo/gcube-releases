/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.shared.export;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ExportType {
	CSV,
	JSON,
	XML;
		
}
