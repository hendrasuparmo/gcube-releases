package org.gcube.portlets.admin.accountingmanager.client.maindata.charts;


/**
 * Accounting Chart Specification
 * 
 * @author "Giancarlo Panichi" email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class AccountingChartSpec {
	private AccountingChartPanel chart;

	public AccountingChartPanel getChart() {
		return chart;
	}

	public void setChart(AccountingChartPanel chart) {
		this.chart = chart;
	}

}
