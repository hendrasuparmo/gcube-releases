package org.gcube.portlets.admin.accountingmanager.client.maindata.charts;


/**
 * Accounting Chart Specification
 * 
   * @author Giancarlo Panichi
 *
 *
 */
public class AccountingChartSpec {
	private AccountingChartPanel chart;

	public AccountingChartPanel getChart() {
		return chart;
	}

	public void setChart(AccountingChartPanel chart) {
		this.chart = chart;
	}

}
