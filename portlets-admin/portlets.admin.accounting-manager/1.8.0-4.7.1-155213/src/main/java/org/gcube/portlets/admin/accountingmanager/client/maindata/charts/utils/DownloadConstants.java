package org.gcube.portlets.admin.accountingmanager.client.maindata.charts.utils;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DownloadConstants {
	public static final String DOWNLOAD = "Download";
	public static final String DOWNLOAD_CSV = "Download CSV";
	public static final String DOWNLOAD_XML = "Download XML";
	public static final String DOWNLOAD_JSON = "Download JSON";
	public static final String DOWNLOAD_PNG = "Download PNG Image";
	public static final String DOWNLOAD_JPG = "Download JPG Image";
	public static final String DOWNLOAD_PDF = "Download PDF Document";
	public static final String DOWNLOAD_SVG = "Download SVG Vector Image";

}
