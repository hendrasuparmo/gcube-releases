package org.gcube.portlets.admin.accountingmanager.client.monitor;

/**
 * 
 * @author Giancarlo Panichi email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public enum MonitorRequestType {
	TimeOut, Period;
}
