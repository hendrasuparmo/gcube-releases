/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.client.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * 
 * @author "Giancarlo Panichi" <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public interface AccountingManagerResources extends ClientBundle {

	public static final AccountingManagerResources INSTANCE = GWT
			.create(AccountingManagerResources.class);

	@Source("AccountingManager.css")
	AccountingManagerCSS accountingManagerCSS();

	@Source("accounting-manager_128.png")
	ImageResource accountingManager128();

	@Source("accounting-storage_128.png")
	ImageResource accountingStorage128();

	@Source("accounting-service_128.png")
	ImageResource accountingService128();

	@Source("accounting-portlet_128.png")
	ImageResource accountingPortlet128();

	@Source("accounting-task_128.png")
	ImageResource accountingTask128();

	@Source("accounting-job_128.png")
	ImageResource accountingJob128();

	@Source("accounting-manager_48.png")
	ImageResource accountingManager48();

	@Source("accounting-storage_48.png")
	ImageResource accountingStorage48();

	@Source("accounting-service_48.png")
	ImageResource accountingService48();

	@Source("accounting-portlet_48.png")
	ImageResource accountingPortlet48();

	@Source("accounting-task_48.png")
	ImageResource accountingTask48();

	@Source("accounting-job_48.png")
	ImageResource accountingJob48();

	@Source("accounting-manager_32.png")
	ImageResource accountingManager32();

	@Source("accounting-storage_32.png")
	ImageResource accountingStorage32();

	@Source("accounting-service_32.png")
	ImageResource accountingService32();

	@Source("accounting-portlet_32.png")
	ImageResource accountingPortlet32();

	@Source("accounting-task_32.png")
	ImageResource accountingTask32();

	@Source("accounting-job_32.png")
	ImageResource accountingJob32();

	@Source("accounting-manager_24.png")
	ImageResource accountingManager24();

	@Source("accounting-storage_24.png")
	ImageResource accountingStorage24();

	@Source("accounting-service_24.png")
	ImageResource accountingService24();

	@Source("accounting-portlet_24.png")
	ImageResource accountingPortlet24();

	@Source("accounting-task_24.png")
	ImageResource accountingTask24();

	@Source("accounting-job_24.png")
	ImageResource accountingJob24();

	@Source("accounting-reload_24.png")
	ImageResource accountingReload24();

	@Source("accounting-reload_32.png")
	ImageResource accountingReload32();

	@Source("accounting-reload_48.png")
	ImageResource accountingReload48();

	@Source("accounting-chart-variable-axis_24.png")
	ImageResource accountingChartVariableAxis24();

	@Source("accounting-chart-variable-axis_32.png")
	ImageResource accountingChartVariableAxis32();

	@Source("accounting-filter_24.png")
	ImageResource accountingFilter24();

	@Source("accounting-filter_32.png")
	ImageResource accountingFilter32();

	@Source("accounting-download_24.png")
	ImageResource accountingDownload24();

	@Source("accounting-byte_32.png")
	ImageResource accountingByte32();

	@Source("accounting-byte_24.png")
	ImageResource accountingByte24();

	@Source("accounting-unit-kB_32.png")
	ImageResource accountingUnitkB32();

	@Source("accounting-unit-kB_24.png")
	ImageResource accountingUnitkB24();

	@Source("accounting-unit-MB_32.png")
	ImageResource accountingUnitMB32();

	@Source("accounting-unit-MB_24.png")
	ImageResource accountingUnitMB24();

	@Source("accounting-unit-GB_32.png")
	ImageResource accountingUnitGB32();

	@Source("accounting-unit-GB_24.png")
	ImageResource accountingUnitGB24();

	@Source("accounting-unit-TB_32.png")
	ImageResource accountingUnitTB32();

	@Source("accounting-unit-TB_24.png")
	ImageResource accountingUnitTB24();

	@Source("accounting-unit-ms_32.png")
	ImageResource accountingUnitms32();

	@Source("accounting-unit-ms_24.png")
	ImageResource accountingUnitms24();

	@Source("accounting-unit-s_32.png")
	ImageResource accountingUnits32();

	@Source("accounting-unit-s_24.png")
	ImageResource accountingUnits24();

	@Source("accounting-unit-m_32.png")
	ImageResource accountingUnitm32();

	@Source("accounting-unit-m_24.png")
	ImageResource accountingUnitm24();

	@Source("accounting-unit-h_32.png")
	ImageResource accountingUnith32();

	@Source("accounting-unit-h_24.png")
	ImageResource accountingUnith24();

	@Source("accounting-download_32.png")
	ImageResource accountingDownload32();

	@Source("accounting-file_24.png")
	ImageResource accountingFile24();

	@Source("accounting-file_32.png")
	ImageResource accountingFile32();

	@Source("accounting-file-png_24.png")
	ImageResource accountingFilePNG24();

	@Source("accounting-file-png_32.png")
	ImageResource accountingFilePNG32();

	@Source("accounting-file-jpg_24.png")
	ImageResource accountingFileJPG24();

	@Source("accounting-file-jpg_32.png")
	ImageResource accountingFileJPG32();

	@Source("accounting-file-pdf_24.png")
	ImageResource accountingFilePDF24();

	@Source("accounting-file-pdf_32.png")
	ImageResource accountingFilePDF32();

	@Source("accounting-file-svg_24.png")
	ImageResource accountingFileSVG24();

	@Source("accounting-file-svg_32.png")
	ImageResource accountingFileSVG32();

}
