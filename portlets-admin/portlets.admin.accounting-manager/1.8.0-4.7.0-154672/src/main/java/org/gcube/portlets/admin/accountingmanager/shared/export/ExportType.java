/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.shared.export;

/**
 * 
  * @author Giancarlo Panichi 
 *
 *
 */
public enum ExportType {
	CSV,
	JSON,
	XML;
		
}
