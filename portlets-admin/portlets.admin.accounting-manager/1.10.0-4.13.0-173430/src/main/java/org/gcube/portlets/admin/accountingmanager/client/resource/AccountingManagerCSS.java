/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.client.resource;

import com.google.gwt.resources.client.CssResource;

/**
 * 
  * @author Giancarlo Panichi 
 *
 *
 */
public interface AccountingManagerCSS extends CssResource {
	
    @ClassName("ribbon")
    public String getRibbon(); 

	
}
