package org.gcube.portlets.admin.accountingmanager.client.type;

/**
 * 
  * @author Giancarlo Panichi 
 *
 *
 */
public enum SessionExpiredType {
	EXPIRED,
	EXPIREDONSERVER;
	
}
