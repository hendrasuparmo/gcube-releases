/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.client.resource;

import com.google.gwt.resources.client.CssResource;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public interface AccountingManagerCSS extends CssResource {
	
    @ClassName("ribbon")
    public String getRibbon(); 

	
}
