package org.gcube.portlets.admin.accountingmanager.shared.data.response;

import java.io.Serializable;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class SeriesResponse implements Serializable{
	
	private static final long serialVersionUID = -2746968605225884841L;

	public SeriesResponse(){
		super();
	}
	
}
