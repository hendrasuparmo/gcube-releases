package org.gcube.portlets.admin.accountingmanager.shared;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	
	public static final String APPLICATION_ID = "org.gcube.portlets.admin.accountingmanager.server.portlet.AccountingManagerPortlet";
	public static final String ACCOUNTING_MANAGER_ID = "AccountingManagerId";
	public static final String AM_LANG_COOKIE = "AMLangCookie";	
	public static final String AM_LANG = "AMLang";
	public static final String DEFAULT_USER = "giancarlo.panichi";
	//public final static String DEFAULT_USER = "test.user";
	//public final static String DEFAULT_SCOPE = "/gcube/devNext";
	//public final static String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	
	//public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	
	public static final String DEFAULT_SCOPE = "/gcube";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	public static final String EXPORT_SERVLET="ExportServlet";
	public static final String EXPORT_SERVLET_TYPE_PARAMETER="ExportServletType";
	public static final String EXPORT_SERVLET_ACCOUNTING_TYPE_PARAMETER = "AccountingType";

	public static final String SESSION_ACCOUNTING_STATE = "ACCOUNTING_STATE";

	
}
