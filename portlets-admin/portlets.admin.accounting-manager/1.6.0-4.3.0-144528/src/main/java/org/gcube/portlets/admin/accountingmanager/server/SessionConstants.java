package org.gcube.portlets.admin.accountingmanager.server;

/**
 * 
 * @author Giancarlo Panichi email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class SessionConstants {
	
	//Context
	public static final String ACCOUNTING_CACHE = "ACCOUNTING_CACHE";
	public static final String ACCOUNTING_CLIENT_MONITOR_TIME_OUT_PERIODMILLIS = "ACCOUNTING_CLIENT_MONITOR_TIME_OUT_PERIODMILLIS";

	//Session
	public static final String TASK_WRAPPER_MAP = "TASK_WRAPPER_MAP";
	public static final String TASK_REQUEST_QUEUE = "TASK_REQUEST_QUEUE";
	

}
