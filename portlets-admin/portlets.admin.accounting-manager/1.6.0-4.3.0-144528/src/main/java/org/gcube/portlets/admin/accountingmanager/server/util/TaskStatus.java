package org.gcube.portlets.admin.accountingmanager.server.util;

/**
 * 
 * @author Giancarlo Panichi email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public enum TaskStatus {
	STARTED,
	RUNNING,
	COMPLETED,
	ERROR;
}
