package org.gcube.portlets.admin.wfdocviewer.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SelectedReportEventHandler extends EventHandler {
	void onSelectedReport(SelectedReportEvent event);
}
