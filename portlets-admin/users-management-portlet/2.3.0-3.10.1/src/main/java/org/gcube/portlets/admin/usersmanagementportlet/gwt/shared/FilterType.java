package org.gcube.portlets.admin.usersmanagementportlet.gwt.shared;

public enum FilterType {
	LITERAL,
	INTEGER,
	BOOLEAN
}
