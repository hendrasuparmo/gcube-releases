package gr.cite.bluebridge.portlets.admin.usersmanagementportlet.mail.templates;

import java.util.Base64;
import java.util.Date;

import org.gcube.common.portal.GCubePortalConstants;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.portal.mailing.templates.AbstractTemplate;
import org.gcube.common.portal.mailing.templates.Template;
import org.gcube.portal.mailing.message.Constants;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeMembershipRequest;
import org.gcube.vomanagement.usermanagement.model.GCubeTeam;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;

public class TemplateUserHasBeenDissAssociatedWithGCubeTeam extends AbstractTemplate implements Template {
	private final String encodedTemplateHTML = "PCEtLSBJbmxpbmVyIEJ1aWxkIFZlcnNpb24gNDM4MGI3NzQxYmI3NTlkNmNiOTk3NTQ1ZjNhZGQyMWFkNDhmMDEwYiAtLT4KPCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBYSFRNTCAxLjAgU3RyaWN0Ly9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSL3hodG1sMS9EVEQveGh0bWwxLXN0cmljdC5kdGQiPgo8aHRtbCB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJtaW4taGVpZ2h0OiAxMDAlOyBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmM2YzICFpbXBvcnRhbnQ7Ij4KICA8aGVhZD4KICAgIDxtZXRhIGh0dHAtZXF1aXY9IkNvbnRlbnQtVHlwZSIgY29udGVudD0idGV4dC9odG1sOyBjaGFyc2V0PXV0Zi04IiAvPgogICAgPG1ldGEgbmFtZT0idmlld3BvcnQiIGNvbnRlbnQ9IndpZHRoPWRldmljZS13aWR0aCIgLz4KICAgIDx0aXRsZT5UaXRsZTwvdGl0bGU+CiAgPC9oZWFkPgoKPGJvZHkgc3R5bGU9IndpZHRoOiAxMDAlICFpbXBvcnRhbnQ7IG1pbi13aWR0aDogMTAwJTsgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlOyAtbXMtdGV4dC1zaXplLWFkanVzdDogMTAwJTsgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94OyAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7IGJveC1zaXppbmc6IGJvcmRlci1ib3g7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmM2YzICFpbXBvcnRhbnQ7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmM2YzZjMgIWltcG9ydGFudCI+ICAKICA8dGFibGUgY2xhc3M9ImJvZHkiIGRhdGEtbWFkZS13aXRoLWZvdW5kYXRpb249IiIgc3R5bGU9ImJvcmRlci1zcGFjaW5nOiAwOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmM2YzICFpbXBvcnRhbnQ7IGhlaWdodDogMTAwJTsgd2lkdGg6IDEwMCU7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBiZ2NvbG9yPSIjZjNmM2YzICFpbXBvcnRhbnQiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIGNsYXNzPSJmbG9hdC1jZW50ZXIiIGFsaWduPSJjZW50ZXIiIHZhbGlnbj0idG9wIiBzdHlsZT0id29yZC13cmFwOiBicmVhay13b3JkOyAtd2Via2l0LWh5cGhlbnM6IGF1dG87IC1tb3otaHlwaGVuczogYXV0bzsgaHlwaGVuczogYXV0bzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZSAhaW1wb3J0YW50OyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGZsb2F0OiBub25lOyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwIGF1dG87IHBhZGRpbmc6IDA7Ij4KICAgICAgICAgIDxjZW50ZXIgZGF0YS1wYXJzZWQ9IiIgc3R5bGU9IndpZHRoOiAxMDAlOyBtaW4td2lkdGg6IDU4MHB4OyI+CiAgICAgICAgICAgIDx0YWJsZSBhbGlnbj0iY2VudGVyIiBjbGFzcz0id3JhcHBlciBoZWFkZXIgZmxvYXQtY2VudGVyIiBzdHlsZT0id2lkdGg6IDEwMCU7IGJvcmRlci1zcGFjaW5nOiAwOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGZsb2F0OiBub25lOyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMDsiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIGNsYXNzPSJ3cmFwcGVyLWlubmVyIiBzdHlsZT0id29yZC13cmFwOiBicmVhay13b3JkOyAtd2Via2l0LWh5cGhlbnM6IGF1dG87IC1tb3otaHlwaGVuczogYXV0bzsgaHlwaGVuczogYXV0bzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZSAhaW1wb3J0YW50OyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiIHZhbGlnbj0idG9wIj4KICAgICAgICAgICAgICAgICAgPHRhYmxlIGFsaWduPSJjZW50ZXIiIGNsYXNzPSJjb250YWluZXIiIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogaW5oZXJpdDsgd2lkdGg6IDU4MHB4OyBiYWNrZ3JvdW5kOiAjZmVmZWZlOyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmZWZlZmUiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIHN0eWxlPSJ3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7IC13ZWJraXQtaHlwaGVuczogYXV0bzsgLW1vei1oeXBoZW5zOiBhdXRvOyBoeXBoZW5zOiBhdXRvOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlICFpbXBvcnRhbnQ7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCIgdmFsaWduPSJ0b3AiPgogICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz0icm93IGNvbGxhcHNlIiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwb3NpdGlvbjogcmVsYXRpdmU7IGRpc3BsYXk6IHRhYmxlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGggY2xhc3M9InNtYWxsLTYgbGFyZ2UtNiBjb2x1bW5zIGZpcnN0IiBzdHlsZT0id2lkdGg6IDI5OHB4OyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwIGF1dG87IHBhZGRpbmc6IDAgMCAxNnB4OyIgYWxpZ249ImxlZnQiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgd2lkdGg6IDEwMCU7IHBhZGRpbmc6IDA7Ij48dGJvZHk+PHRyIHN0eWxlPSJ2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPjx0aCBzdHlsZT0iY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij4gPGltZyBzcmM9Int7R0FURVdBWV9MT0dPOlVSTH19IiBzdHlsZT0id2lkdGg6IDIwMHB4OyBvdXRsaW5lOiBub25lOyB0ZXh0LWRlY29yYXRpb246IG5vbmU7IC1tcy1pbnRlcnBvbGF0aW9uLW1vZGU6IGJpY3ViaWM7IG1heC13aWR0aDogMTAwJTsgY2xlYXI6IGJvdGg7IGRpc3BsYXk6IGJsb2NrOyIgYWx0PSJ7e0dBVEVXQVlfTkFNRX19IiB0aXRsZT0ie3tHQVRFV0FZX05BTUV9fSI+PC90aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+PC90aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3M9InNtYWxsLTYgbGFyZ2UtNiBjb2x1bW5zIGxhc3QiIHN0eWxlPSJ3aWR0aDogMjk4cHg7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMCAwIDE2cHg7IiBhbGlnbj0ibGVmdCI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGFibGUgc3R5bGU9ImJvcmRlci1zcGFjaW5nOiAwOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyB3aWR0aDogMTAwJTsgcGFkZGluZzogMDsiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRoIHN0eWxlPSJjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9InRleHQtcmlnaHQiIHN0eWxlPSJ0ZXh0LWFsaWduOiByaWdodDsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMCAwIDEwcHg7IHBhZGRpbmc6IDA7IiBhbGlnbj0icmlnaHQiPjwvcD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RoPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT48L3RoPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT48L3RkPgogICAgICAgICAgICAgICAgICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+PC90ZD4KICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT48dGFibGUgYWxpZ249ImNlbnRlciIgY2xhc3M9ImNvbnRhaW5lciBib2R5LWJvcmRlciBmbG9hdC1jZW50ZXIiIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogY2VudGVyOyB3aWR0aDogNTgwcHg7IGZsb2F0OiBub25lOyBib3JkZXItdG9wLXdpZHRoOiA4cHg7IGJvcmRlci10b3AtY29sb3I6ICMyMjVmOTc7IGJvcmRlci10b3Atc3R5bGU6IHNvbGlkOyBiYWNrZ3JvdW5kOiAjZmVmZWZlOyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmZWZlZmUiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIHN0eWxlPSJ3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7IC13ZWJraXQtaHlwaGVuczogYXV0bzsgLW1vei1oeXBoZW5zOiBhdXRvOyBoeXBoZW5zOiBhdXRvOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlICFpbXBvcnRhbnQ7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCIgdmFsaWduPSJ0b3AiPgogICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz0icm93IiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwb3NpdGlvbjogcmVsYXRpdmU7IGRpc3BsYXk6IHRhYmxlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGggY2xhc3M9InNtYWxsLTEyIGxhcmdlLTEyIGNvbHVtbnMgZmlyc3QgbGFzdCIgc3R5bGU9IndpZHRoOiA1NjRweDsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMCBhdXRvOyBwYWRkaW5nOiAwIDE2cHggMTZweDsiIGFsaWduPSJsZWZ0Ij4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGggc3R5bGU9ImNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGFibGUgY2xhc3M9InNwYWNlciIgc3R5bGU9ImJvcmRlci1zcGFjaW5nOiAwOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyB3aWR0aDogMTAwJTsgcGFkZGluZzogMDsiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIGhlaWdodD0iMTZweCIgc3R5bGU9ImZvbnQtc2l6ZTogMTZweDsgbGluZS1oZWlnaHQ6IDE2cHg7IHdvcmQtd3JhcDogYnJlYWstd29yZDsgLXdlYmtpdC1oeXBoZW5zOiBhdXRvOyAtbW96LWh5cGhlbnM6IGF1dG87IGh5cGhlbnM6IGF1dG87IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2UgIWltcG9ydGFudDsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgbXNvLWxpbmUtaGVpZ2h0LXJ1bGU6IGV4YWN0bHk7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiIHZhbGlnbj0idG9wIj4mbmJzcDs8L3RkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PC90Ym9keT48L3RhYmxlPjxoNCBzdHlsZT0iY29sb3I6IGluaGVyaXQ7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyB3b3JkLXdyYXA6IG5vcm1hbDsgZm9udC1zaXplOiAyNHB4OyBtYXJnaW46IDAgMCAxMHB4OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPkhpIHt7UkVRVUVTVElOR19VU0VSX0ZJUlNUX05BTUV9fSw8YnI+PGEgaHJlZj0ie3tVU0VSX1ZSRU1FTUJFUl9QUk9GSUxFX1VSTH19IiBzdHlsZT0iY29sb3I6ICMyMTk5ZTg7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyB0ZXh0LWRlY29yYXRpb246IG5vbmU7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiPnt7VVNFUl9GVUxMTkFNRX19PC9hPiBoYXMganVzdCByZW1vdmVkIHlvdSBmcm9tIHRoZSB7e1NFTEVDVEVEX1RFQU1fTkFNRX19IGdyb3VwIGluIHRoZSB7e1NFTEVDVEVEX1ZSRV9OQU1FfX0gVlJFLjwvaDQ+CgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzPSJleHBhbmRlciIgc3R5bGU9InZpc2liaWxpdHk6IGhpZGRlbjsgd2lkdGg6IDA7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PC90aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+PC90aD4KICAgICAgICAgICAgICAgICAgICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+Cgo8L3RkPgogICAgICAgICAgICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+PC9jZW50ZXI+CiAgICAgICAgPC90ZD4KICAgICAgPC90cj48L3Rib2R5PjwvdGFibGU+Cgo8L2JvZHk+PC9odG1sPgoKCg==";
	private final String encodedTemplateTEXT = "e3tHQVRFV0FZX05BTUV9fQotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkhpIHt7UkVRVUVTVElOR19VU0VSX0ZJUlNUX05BTUV9fSwKCnt7VVNFUl9GVUxMTkFNRX19IGhhcyBqdXN0IHJlbW92ZWQgeW91IGZyb20gdGhlIHt7U0VMRUNURURfVEVBTV9OQU1FfX0gZ3JvdXAgaW4gdGhlIHt7U0VMRUNURURfVlJFX05BTUV9fSBWUkUuCgo=";

	private GCubeGroup theRequestedVRE;
	private GCubeTeam teamToBeAssociatedWith;
	private GCubeUser theRequestingUser;
	private GCubeUser theManagerUser;
	private Date originalRequestDate;
	private String vreURL;

	/**
	 * 
	 * @param theRequestingUser an instance of @see {@link GCubeUser} representing the user who requested access
	 * @param teamToBeAssociatedWith an instance of @see {@link GCubeTeam} representing the team the user will join
	 * @param theManagerUser an instance of @see {@link GCubeUser} representing the manager who approved the request
	 * @param theRequestedVRE instance of @see {@link GCubeGroup} of the current VRE
	 * @param originalRequestDate the request date as in the associated {@link GCubeMembershipRequest}
	 * @param gatewayName gateway name can be obtained with {@link PortalContext#getGatewayName(javax.servlet.http.HttpServletRequest)}
	 * @param gatewayURL gateway URL name can be obtained with {@link PortalContext#getGatewayURL(javax.servlet.http.HttpServletRequest)}
	 */
	
	public TemplateUserHasBeenDissAssociatedWithGCubeTeam(
			GCubeGroup theRequestedVRE, GCubeTeam teamToBeAssociatedWith, GCubeUser theRequestingUser,
			GCubeUser theManagerUser, Date originalRequestDate,
			String gatewayName, String gatewayURL) {
		
		super(gatewayName, gatewayURL);
		
		this.theRequestingUser = theRequestingUser;
		this.theManagerUser = theManagerUser;
		this.theRequestedVRE = theRequestedVRE;		
		this.originalRequestDate = originalRequestDate;
		this.teamToBeAssociatedWith = teamToBeAssociatedWith;
		this.vreURL = new StringBuffer(gatewayURL)
				.append(GCubePortalConstants.PREFIX_GROUP_URL)
				.append("/").append(theRequestedVRE.getGroupName().toLowerCase()).toString();
	}

	@Override
	public String compile(String templateContent) {
		String userProfileLink = new StringBuffer(vreURL)
				.append("/").append(getUserProfileLink(theManagerUser.getUsername())).toString();

		return new String(Base64.getDecoder().decode(templateContent))
				.replace("{{REQUESTING_USER_FIRST_NAME}}", theRequestingUser.getFirstName())
				.replace("{{GATEWAY_LOGO:URL}}", getGatewayLogoURL())			
				.replace("{{GATEWAY_NAME}}", getGatewayName())
				.replace("{{USER_FULLNAME}}", theManagerUser.getFullname())
				.replace("{{SELECTED_VRE_NAME}}", theRequestedVRE.getGroupName())
				.replace("{{VRE_URL}}", vreURL)
				.replace("{{REQUESTING_USER_EMAIL}}", theRequestingUser.getEmail())
				.replace("{{MANAGE_REQUEST_DATE}}", originalRequestDate.toString())
				.replace("{{USER_VREMEMBER_PROFILE_URL}}", userProfileLink)
				.replace("{{SELECTED_TEAM_NAME}}", teamToBeAssociatedWith.getTeamName())
				;				
	}

	@Override
	public String getTextHTML() {
		return compile(encodedTemplateHTML);
	}

	@Override
	public String getTextPLAIN() {
		return compile(encodedTemplateTEXT);
	}
	
	private String getUserProfileLink(String username) {
		return "profile?"+ new String(
				Base64.getEncoder().encodeToString(Constants.USER_PROFILE_OID.getBytes())+
				"="+
						new String( Base64.getEncoder().encodeToString(username.getBytes()) )
						);
	}
}