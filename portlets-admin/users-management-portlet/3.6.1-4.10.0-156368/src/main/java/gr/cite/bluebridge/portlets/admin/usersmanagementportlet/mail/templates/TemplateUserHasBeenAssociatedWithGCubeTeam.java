package gr.cite.bluebridge.portlets.admin.usersmanagementportlet.mail.templates;

import java.util.Base64;
import java.util.Date;

import org.gcube.common.portal.GCubePortalConstants;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.portal.mailing.templates.AbstractTemplate;
import org.gcube.common.portal.mailing.templates.Template;
import org.gcube.portal.mailing.message.Constants;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeMembershipRequest;
import org.gcube.vomanagement.usermanagement.model.GCubeTeam;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;

public class TemplateUserHasBeenAssociatedWithGCubeTeam extends AbstractTemplate implements Template {
	private final String encodedTemplateHTML = "PCEtLSBJbmxpbmVyIEJ1aWxkIFZlcnNpb24gNDM4MGI3NzQxYmI3NTlkNmNiOTk3NTQ1ZjNhZGQyMWFkNDhmMDEwYiAtLT4KPCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBYSFRNTCAxLjAgU3RyaWN0Ly9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSL3hodG1sMS9EVEQveGh0bWwxLXN0cmljdC5kdGQiPgo8aHRtbCB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJtaW4taGVpZ2h0OiAxMDAlOyBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmM2YzICFpbXBvcnRhbnQ7Ij4KICA8aGVhZD4KICAgIDxtZXRhIGh0dHAtZXF1aXY9IkNvbnRlbnQtVHlwZSIgY29udGVudD0idGV4dC9odG1sOyBjaGFyc2V0PXV0Zi04IiAvPgogICAgPG1ldGEgbmFtZT0idmlld3BvcnQiIGNvbnRlbnQ9IndpZHRoPWRldmljZS13aWR0aCIgLz4KICAgIDx0aXRsZT5UaXRsZTwvdGl0bGU+CiAgPC9oZWFkPgogIDxib2R5IHN0eWxlPSJ3aWR0aDogMTAwJSAhaW1wb3J0YW50OyBtaW4td2lkdGg6IDEwMCU7IC13ZWJraXQtdGV4dC1zaXplLWFkanVzdDogMTAwJTsgLW1zLXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7IC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDsgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94OyBib3gtc2l6aW5nOiBib3JkZXItYm94OyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgYmFja2dyb3VuZC1jb2xvcjogI2YzZjNmMyAhaW1wb3J0YW50OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBiZ2NvbG9yPSIjZjNmM2YzICFpbXBvcnRhbnQiPgogIDx0YWJsZSBjbGFzcz0iYm9keSIgZGF0YS1tYWRlLXdpdGgtZm91bmRhdGlvbj0iIiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IGJhY2tncm91bmQtY29sb3I6ICNmM2YzZjMgIWltcG9ydGFudDsgaGVpZ2h0OiAxMDAlOyB3aWR0aDogMTAwJTsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmM2YzZjMgIWltcG9ydGFudCI+PHRyIHN0eWxlPSJ2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPjx0ZCBjbGFzcz0iZmxvYXQtY2VudGVyIiBhbGlnbj0iY2VudGVyIiB2YWxpZ249InRvcCIgc3R5bGU9IndvcmQtd3JhcDogYnJlYWstd29yZDsgLXdlYmtpdC1oeXBoZW5zOiBhdXRvOyAtbW96LWh5cGhlbnM6IGF1dG87IGh5cGhlbnM6IGF1dG87IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2UgIWltcG9ydGFudDsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogY2VudGVyOyBmbG9hdDogbm9uZTsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMCBhdXRvOyBwYWRkaW5nOiAwOyI+CiAgICAgICAgICA8Y2VudGVyIGRhdGEtcGFyc2VkPSIiIHN0eWxlPSJ3aWR0aDogMTAwJTsgbWluLXdpZHRoOiA1ODBweDsiPgogICAgICAgICAgICA8dGFibGUgYWxpZ249ImNlbnRlciIgY2xhc3M9IndyYXBwZXIgaGVhZGVyIGZsb2F0LWNlbnRlciIgc3R5bGU9IndpZHRoOiAxMDAlOyBib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogY2VudGVyOyBmbG9hdDogbm9uZTsgbWFyZ2luOiAwIGF1dG87IHBhZGRpbmc6IDA7Ij48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIGNsYXNzPSJ3cmFwcGVyLWlubmVyIiBzdHlsZT0id29yZC13cmFwOiBicmVhay13b3JkOyAtd2Via2l0LWh5cGhlbnM6IGF1dG87IC1tb3otaHlwaGVuczogYXV0bzsgaHlwaGVuczogYXV0bzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZSAhaW1wb3J0YW50OyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiIHZhbGlnbj0idG9wIj4KICAgICAgICAgICAgICAgICAgPHRhYmxlIGFsaWduPSJjZW50ZXIiIGNsYXNzPSJjb250YWluZXIiIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogaW5oZXJpdDsgd2lkdGg6IDU4MHB4OyBiYWNrZ3JvdW5kOiAjZmVmZWZlOyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmZWZlZmUiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIHN0eWxlPSJ3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7IC13ZWJraXQtaHlwaGVuczogYXV0bzsgLW1vei1oeXBoZW5zOiBhdXRvOyBoeXBoZW5zOiBhdXRvOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlICFpbXBvcnRhbnQ7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCIgdmFsaWduPSJ0b3AiPgogICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz0icm93IGNvbGxhcHNlIiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwb3NpdGlvbjogcmVsYXRpdmU7IGRpc3BsYXk6IHRhYmxlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGggY2xhc3M9InNtYWxsLTYgbGFyZ2UtNiBjb2x1bW5zIGZpcnN0IiBzdHlsZT0id2lkdGg6IDI5OHB4OyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwIGF1dG87IHBhZGRpbmc6IDAgMCAxNnB4OyIgYWxpZ249ImxlZnQiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgd2lkdGg6IDEwMCU7IHBhZGRpbmc6IDA7Ij48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRoIHN0eWxlPSJjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPiA8aW1nIHNyYz0ie3tHQVRFV0FZX0xPR086VVJMfX0iIHN0eWxlPSJ3aWR0aDogMjAwcHg7IG91dGxpbmU6IG5vbmU7IHRleHQtZGVjb3JhdGlvbjogbm9uZTsgLW1zLWludGVycG9sYXRpb24tbW9kZTogYmljdWJpYzsgbWF4LXdpZHRoOiAxMDAlOyBjbGVhcjogYm90aDsgZGlzcGxheTogYmxvY2s7IiBhbHQ9Int7R0FURVdBWV9OQU1FfX0iIHRpdGxlPSJ7e0dBVEVXQVlfTkFNRX19IiAvPjwvdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PC90YWJsZT48L3RoPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz0ic21hbGwtNiBsYXJnZS02IGNvbHVtbnMgbGFzdCIgc3R5bGU9IndpZHRoOiAyOThweDsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMCBhdXRvOyBwYWRkaW5nOiAwIDAgMTZweDsiIGFsaWduPSJsZWZ0Ij4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwYWRkaW5nOiAwOyI+PHRyIHN0eWxlPSJ2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPjx0aCBzdHlsZT0iY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPSJ0ZXh0LXJpZ2h0IiBzdHlsZT0idGV4dC1hbGlnbjogcmlnaHQ7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDAgMCAxMHB4OyBwYWRkaW5nOiAwOyIgYWxpZ249InJpZ2h0Ij48L3A+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj48L3RhYmxlPjwvdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PC90Ym9keT48L3RhYmxlPjwvdGQ+CiAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT48L3RkPgogICAgICAgICAgICAgIDwvdHI+PC90YWJsZT48dGFibGUgYWxpZ249ImNlbnRlciIgY2xhc3M9ImNvbnRhaW5lciBib2R5LWJvcmRlciBmbG9hdC1jZW50ZXIiIHN0eWxlPSJib3JkZXItc3BhY2luZzogMDsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogY2VudGVyOyB3aWR0aDogNTgwcHg7IGZsb2F0OiBub25lOyBib3JkZXItdG9wLXdpZHRoOiA4cHg7IGJvcmRlci10b3AtY29sb3I6ICMyMjVmOTc7IGJvcmRlci10b3Atc3R5bGU6IHNvbGlkOyBiYWNrZ3JvdW5kOiAjZmVmZWZlOyBtYXJnaW46IDAgYXV0bzsgcGFkZGluZzogMDsiIGJnY29sb3I9IiNmZWZlZmUiPjx0Ym9keT48dHIgc3R5bGU9InZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCI+PHRkIHN0eWxlPSJ3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7IC13ZWJraXQtaHlwaGVuczogYXV0bzsgLW1vei1oeXBoZW5zOiBhdXRvOyBoeXBoZW5zOiBhdXRvOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlICFpbXBvcnRhbnQ7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IGNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCIgdmFsaWduPSJ0b3AiPgogICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz0icm93IiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwb3NpdGlvbjogcmVsYXRpdmU7IGRpc3BsYXk6IHRhYmxlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGggY2xhc3M9InNtYWxsLTEyIGxhcmdlLTEyIGNvbHVtbnMgZmlyc3QgbGFzdCIgc3R5bGU9IndpZHRoOiA1NjRweDsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMCBhdXRvOyBwYWRkaW5nOiAwIDE2cHggMTZweDsiIGFsaWduPSJsZWZ0Ij4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwYWRkaW5nOiAwOyI+PHRyIHN0eWxlPSJ2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPjx0aCBzdHlsZT0iY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyBmb250LXNpemU6IDE2cHg7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz0ic3BhY2VyIiBzdHlsZT0iYm9yZGVyLXNwYWNpbmc6IDA7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHRleHQtYWxpZ246IGxlZnQ7IHdpZHRoOiAxMDAlOyBwYWRkaW5nOiAwOyI+PHRib2R5Pjx0ciBzdHlsZT0idmVydGljYWwtYWxpZ246IHRvcDsgdGV4dC1hbGlnbjogbGVmdDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij48dGQgaGVpZ2h0PSIxNnB4IiBzdHlsZT0iZm9udC1zaXplOiAxNnB4OyBsaW5lLWhlaWdodDogMTZweDsgd29yZC13cmFwOiBicmVhay13b3JkOyAtd2Via2l0LWh5cGhlbnM6IGF1dG87IC1tb3otaHlwaGVuczogYXV0bzsgaHlwaGVuczogYXV0bzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZSAhaW1wb3J0YW50OyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB0ZXh0LWFsaWduOiBsZWZ0OyBtc28tbGluZS1oZWlnaHQtcnVsZTogZXhhY3RseTsgY29sb3I6ICMwYTBhMGE7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IiBhbGlnbj0ibGVmdCIgdmFsaWduPSJ0b3AiPsKgPC90ZD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT48aDQgc3R5bGU9ImNvbG9yOiBpbmhlcml0OyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgd29yZC13cmFwOiBub3JtYWw7IGZvbnQtc2l6ZTogMjRweDsgbWFyZ2luOiAwIDAgMTBweDsgcGFkZGluZzogMDsiIGFsaWduPSJsZWZ0Ij5IaSB7e1JFUVVFU1RJTkdfVVNFUl9GSVJTVF9OQU1FfX0sPGJyIC8+PGEgaHJlZj0ie3tVU0VSX1ZSRU1FTUJFUl9QUk9GSUxFX1VSTH19IiBzdHlsZT0iY29sb3I6ICMyMTk5ZTg7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogbm9ybWFsOyB0ZXh0LWFsaWduOiBsZWZ0OyBsaW5lLWhlaWdodDogMS4zOyB0ZXh0LWRlY29yYXRpb246IG5vbmU7IG1hcmdpbjogMDsgcGFkZGluZzogMDsiPnt7VVNFUl9GVUxMTkFNRX19PC9hPiBoYXMganVzdCBtYWRlIHlvdSBhIG1lbWJlciBvZiB0aGUge3tTRUxFQ1RFRF9URUFNX05BTUV9fSBncm91cCBpbiB0aGUge3tTRUxFQ1RFRF9WUkVfTkFNRX19IFZSRS48L2g0PgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJyIC8+PHAgc3R5bGU9ImNvbG9yOiAjMGEwYTBhOyBmb250LWZhbWlseTogSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgdGV4dC1hbGlnbjogbGVmdDsgbGluZS1oZWlnaHQ6IDEuMzsgZm9udC1zaXplOiAxNnB4OyBtYXJnaW46IDAgMCAxMHB4OyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPllvdSBjYW4gYWNjZXNzIHRoZSBWUkUgYXQge3tWUkVfVVJMfX0gdXNpbmcgeW91ciBlbWFpbCB7e1JFUVVFU1RJTkdfVVNFUl9FTUFJTH19LjwvcD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RoPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz0iZXhwYW5kZXIiIHN0eWxlPSJ2aXNpYmlsaXR5OiBoaWRkZW47IHdpZHRoOiAwOyBjb2xvcjogIzBhMGEwYTsgZm9udC1mYW1pbHk6IEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IHRleHQtYWxpZ246IGxlZnQ7IGxpbmUtaGVpZ2h0OiAxLjM7IGZvbnQtc2l6ZTogMTZweDsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyIgYWxpZ249ImxlZnQiPjwvdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PC90YWJsZT48L3RoPgogICAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+PC90YWJsZT4KPC90ZD4KICAgICAgICAgICAgICAgIDwvdHI+PC90Ym9keT48L3RhYmxlPjwvY2VudGVyPgogICAgICAgIDwvdGQ+CiAgICAgIDwvdHI+PC90YWJsZT48L2JvZHk+CjwvaHRtbD4KCg==";
	private final String encodedTemplateTEXT = "e3tHQVRFV0FZX05BTUV9fQotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkhpIHt7UkVRVUVTVElOR19VU0VSX0ZJUlNUX05BTUV9fSwKCnt7VVNFUl9GVUxMTkFNRX19IGhhcyBqdXN0IG1hZGUgeW91IGEgbWVtYmVyIG9mIHRoZSB7e1NFTEVDVEVEX1RFQU1fTkFNRX19IGdyb3VwIGluIHRoZSB7e1NFTEVDVEVEX1ZSRV9OQU1FfX0gVlJFLgoKWW91IGNhbiBhY2Nlc3MgdGhlIFZSRSBhdCB7e1ZSRV9VUkx9fSB1c2luZyB5b3VyIGVtYWlsIHt7UkVRVUVTVElOR19VU0VSX0VNQUlMfX0KCg";

	private GCubeGroup theRequestedVRE;
	private GCubeTeam teamToBeAssociatedWith;
	private GCubeUser theRequestingUser;
	private GCubeUser theManagerUser;
	private Date originalRequestDate;
	private String vreURL;

	/**
	 * 
	 * @param theRequestingUser an instance of @see {@link GCubeUser} representing the user who requested access
	 * @param teamToBeAssociatedWith an instance of @see {@link GCubeTeam} representing the team the user will join
	 * @param theManagerUser an instance of @see {@link GCubeUser} representing the manager who approved the request
	 * @param theRequestedVRE instance of @see {@link GCubeGroup} of the current VRE
	 * @param originalRequestDate the request date as in the associated {@link GCubeMembershipRequest}
	 * @param gatewayName gateway name can be obtained with {@link PortalContext#getGatewayName(javax.servlet.http.HttpServletRequest)}
	 * @param gatewayURL gateway URL name can be obtained with {@link PortalContext#getGatewayURL(javax.servlet.http.HttpServletRequest)}
	 */
	
	public TemplateUserHasBeenAssociatedWithGCubeTeam(
			GCubeGroup theRequestedVRE, GCubeTeam teamToBeAssociatedWith, GCubeUser theRequestingUser,
			GCubeUser theManagerUser, Date originalRequestDate,
			String gatewayName, String gatewayURL) {
		
		super(gatewayName, gatewayURL);
		
		this.theRequestingUser = theRequestingUser;
		this.theManagerUser = theManagerUser;
		this.theRequestedVRE = theRequestedVRE;		
		this.originalRequestDate = originalRequestDate;
		this.teamToBeAssociatedWith = teamToBeAssociatedWith;
		this.vreURL = new StringBuffer(gatewayURL)
				.append(GCubePortalConstants.PREFIX_GROUP_URL)
				.append("/").append(theRequestedVRE.getGroupName().toLowerCase()).toString();
	}

	@Override
	public String compile(String templateContent) {
		String userProfileLink = new StringBuffer(vreURL)
				.append("/").append(getUserProfileLink(theManagerUser.getUsername())).toString();

		return new String(Base64.getDecoder().decode(templateContent))
				.replace("{{REQUESTING_USER_FIRST_NAME}}", theRequestingUser.getFirstName())
				.replace("{{GATEWAY_LOGO:URL}}", getGatewayLogoURL())			
				.replace("{{GATEWAY_NAME}}", getGatewayName())
				.replace("{{USER_FULLNAME}}", theManagerUser.getFullname())
				.replace("{{SELECTED_VRE_NAME}}", theRequestedVRE.getGroupName())
				.replace("{{VRE_URL}}", vreURL)
				.replace("{{REQUESTING_USER_EMAIL}}", theRequestingUser.getEmail())
				.replace("{{MANAGE_REQUEST_DATE}}", originalRequestDate.toString())
				.replace("{{USER_VREMEMBER_PROFILE_URL}}", userProfileLink)
				.replace("{{SELECTED_TEAM_NAME}}", teamToBeAssociatedWith.getTeamName())
				;				
	}

	@Override
	public String getTextHTML() {
		return compile(encodedTemplateHTML);
	}

	@Override
	public String getTextPLAIN() {
		return compile(encodedTemplateTEXT);
	}
	
	private String getUserProfileLink(String username) {
		return "profile?"+ new String(
				Base64.getEncoder().encodeToString(Constants.USER_PROFILE_OID.getBytes())+
				"="+
						new String( Base64.getEncoder().encodeToString(username.getBytes()) )
						);
	}
}