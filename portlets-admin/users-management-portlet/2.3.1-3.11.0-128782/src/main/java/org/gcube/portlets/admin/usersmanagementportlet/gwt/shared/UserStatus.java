package org.gcube.portlets.admin.usersmanagementportlet.gwt.shared;

/**
 * UserStatus class
 * 
 * @author Panagiota Koltsida, NKUA
 *
 */
public class UserStatus {
	public static final String EDIT_OK = "edit_ok";
	public static final String EDIT_FAILED = "edit_failed";
	public static final String USER_REMOVED = "user_removed";
};
