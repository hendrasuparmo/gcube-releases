package org.gcube.portlets.admin.usersmanagementportlet.gwt.shared;

public class RecipientTypeConstants {

	public static final String EMAIL_TO = "to";
	public static final String EMAIL_CC = "cc";
	public static final String EMAIL_BCC = "bcc";
}
