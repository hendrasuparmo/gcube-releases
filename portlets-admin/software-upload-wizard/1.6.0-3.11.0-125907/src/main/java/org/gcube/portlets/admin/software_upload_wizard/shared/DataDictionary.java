package org.gcube.portlets.admin.software_upload_wizard.shared;

public class DataDictionary {

	public final static String ARTIFACT_ID = "ARTIFACT_ID";
	public final static String ARTIFACT_GROUPID = "ARTIFACT_GROUPID";
	public final static String ARTIFACT_VERSION = "ARTIFACT_VERSION";
	public final static String ARTIFACT_ISSNAPSHOT = "ARTIFACT_ISSNAPSHOT";
	public final static String ARTIFACT_FILENAME = "ARTIFACT_FILENAME";
	
}
