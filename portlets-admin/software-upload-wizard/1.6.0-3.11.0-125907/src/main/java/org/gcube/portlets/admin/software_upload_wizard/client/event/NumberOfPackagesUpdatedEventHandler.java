package org.gcube.portlets.admin.software_upload_wizard.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NumberOfPackagesUpdatedEventHandler extends EventHandler {
	
	public void onNumberOfPackagesUpdated(NumberOfPackagesUpdatedEvent event);

}
