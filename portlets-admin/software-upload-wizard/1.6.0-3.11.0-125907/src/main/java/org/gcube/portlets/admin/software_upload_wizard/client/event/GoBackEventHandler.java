package org.gcube.portlets.admin.software_upload_wizard.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface GoBackEventHandler extends EventHandler {
	public void onBackButtonPressed(GoBackEvent event);
}
