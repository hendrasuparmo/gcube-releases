package org.gcube.portlets.admin.software_upload_wizard.server.util;

public interface Stringer {
	
	public String toString(Object o);

}
