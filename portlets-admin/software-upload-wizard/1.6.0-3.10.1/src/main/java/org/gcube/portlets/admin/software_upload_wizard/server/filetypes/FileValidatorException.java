package org.gcube.portlets.admin.software_upload_wizard.server.filetypes;

public class FileValidatorException extends Exception {
	public FileValidatorException(String message) {
		super(message);
	}
}
