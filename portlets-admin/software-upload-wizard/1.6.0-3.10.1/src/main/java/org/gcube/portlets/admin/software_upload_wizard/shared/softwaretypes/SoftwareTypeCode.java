package org.gcube.portlets.admin.software_upload_wizard.shared.softwaretypes;

public enum SoftwareTypeCode {
	 WebApp, Library, gCubeWebService, gCubePlugin, gCubePatch, AnySoftware, SoftwareRegistration
}
