package org.gcube.portlets.admin.software_upload_wizard.server.softwaremanagers.scope;

public interface ScopeAvailable {
	
	public boolean isAvailableForScope(String scope);

}
