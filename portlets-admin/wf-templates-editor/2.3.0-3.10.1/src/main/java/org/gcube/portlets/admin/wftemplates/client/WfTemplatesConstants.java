package org.gcube.portlets.admin.wftemplates.client;

import com.google.gwt.core.client.GWT;
/**
 * <code> WfTemplatesConstants </code> 
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 * @version May 2011 (0.1) 
 */
public class WfTemplatesConstants {
	
	public static final String GRAPH_IMAGE = GWT.getModuleBaseURL() + "../images/chart_organisation.png";
	
	public static final String LOADING = GWT.getModuleBaseURL() + "../images/loading.gif";	
	/**
	 * 
	 */
	public static final String ADD_STATIC_IMAGE = GWT.getModuleBaseURL() + "../images/add.png";
	
	/**
	 * 
	 */
	public static final String TRASH_IMAGE = GWT.getModuleBaseURL() + "../images/trashbin.png";
}
