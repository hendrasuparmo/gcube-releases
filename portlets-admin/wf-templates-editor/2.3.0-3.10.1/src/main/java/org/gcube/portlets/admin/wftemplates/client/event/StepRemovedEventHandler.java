package org.gcube.portlets.admin.wftemplates.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface StepRemovedEventHandler extends EventHandler {
	 void onStepRemoved(StepRemovedEvent stepRemoved);
}
