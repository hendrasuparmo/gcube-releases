package org.gcube.portlets.admin.wfdocslibrary.shared;

public enum PermissionType  {
	ADD_DISCUSSION, DELETE, DELETE_DISCUSSION, EDIT_PERMISSIONS, UPDATE, UPDATE_DISCUSSION, VIEW;
}
