package org.gcube.portlets.admin.wfdocslibrary.client;

/**
 * <code> WfDocsLibrary </code> class is the WfDocsLibrary entrypoint
 *
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 * @version April 2011 (0.1) 
 */
public class WfDocsLibrary  {
	public static final String LAST_WORKFLOW_ID = "Workflodocs.lastworkflowid";
	public static final String WORKFLOW_ID_ATTRIBUTE = "Workflodocs.workflowid";
	public static final String WORKFLOW_READONLY_ATTRIBUTE = "Workflodocs.readonly";
	public static final String WORKFLOW_GIVEN_NAME = "Workflodocs.name";

	
	public static final String START_STEP_LABEL = "Start";
	public static final String END_STEP_LABEL = "End";
	
	public void onModuleLoad() {	}
}
