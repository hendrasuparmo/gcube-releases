package org.gcube.portlets.admin.vredeployer.shared.deployreport;

/**
 * 
 * @author massi
 *
 */
public enum DeployStatus {
	WAIT,
	RUN,
	FINISH,
	FAIL,
	SKIP,
	PENDING;	
}
