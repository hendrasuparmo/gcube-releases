package org.gcube.portlets.admin.vredeployer.client.model;

public enum NodeType {
	/**
	 * INFO
	 */
	INFO, 
	/**
	 * CONTENT
	 */
	CONTENT,
	/**
	 * COLLECTION
	 */
	COLLECTION,
	/**
	 * METADATA
	 */	
	METADATA,
	/**
	 * FUNCTIONALITY
	 */	
	FUNCTIONALITY,
	/**
	 * REPORT
	 */	
	REPORT,
	/**
	 * ARCHITECTURE
	 */	
	ARCHITECTURE;	
}
