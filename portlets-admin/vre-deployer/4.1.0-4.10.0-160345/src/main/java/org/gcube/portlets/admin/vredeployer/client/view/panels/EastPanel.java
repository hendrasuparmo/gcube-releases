package org.gcube.portlets.admin.vredeployer.client.view.panels;

import org.gcube.portlets.admin.vredeployer.client.control.Controller;

import com.extjs.gxt.ui.client.widget.ContentPanel;

public class EastPanel  extends ContentPanel {  
	/**
	 * 
	 */
	private Controller controller;

	/**
	 * constructor
	 * @param c -
	 */
	public EastPanel(Controller c) {

		controller = c;
		setWidth("100%");
		setHeight("100%");
	}

}
