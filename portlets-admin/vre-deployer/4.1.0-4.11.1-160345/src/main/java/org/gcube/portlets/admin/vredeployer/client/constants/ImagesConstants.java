package org.gcube.portlets.admin.vredeployer.client.constants;

import com.google.gwt.core.client.GWT;

public class ImagesConstants {
	public static final String INFO_IMAGE = GWT.getModuleBaseURL() + "../images/information.png";
	public static final String CONTENT_IMAGE = GWT.getModuleBaseURL() + "../images/content.png";
	public static final String COLLECTIONS_IMAGE = GWT.getModuleBaseURL() + "../images/collections.png";
	public static final String METADATA_IMAGE = GWT.getModuleBaseURL() + "../images/metadata.gif";

	public static final String FUNC_IMAGE = GWT.getModuleBaseURL() + "../images/functionality.png";
	public static final String ARCH_IMAGE = GWT.getModuleBaseURL() + "../images/architecture.png";
	
	public static final String CLOUD_ICON = GWT.getModuleBaseURL() + "../images/cloud.png";
	public static final String CLOUD_IMAGE = GWT.getModuleBaseURL() + "../images/cloud-gcube.png";
	


}
