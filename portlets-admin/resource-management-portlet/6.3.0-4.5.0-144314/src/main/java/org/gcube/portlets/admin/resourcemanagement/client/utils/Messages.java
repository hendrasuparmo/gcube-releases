/****************************************************************************
 *  This software is part of the gCube Project.
 *  Site: http://www.gcube-system.org/
 ****************************************************************************
 * The gCube/gCore software is licensed as Free Open Source software
 * conveying to the EUPL (http://ec.europa.eu/idabc/eupl).
 * The software and documentation is provided by its authors/distributors
 * "as is" and no expressed or
 * implied warranty is given for its use, quality or fitness for a
 * particular case.
 ****************************************************************************
 * Filename: Messages.java
 ****************************************************************************
 * @author <a href="mailto:daniele.strollo@isti.cnr.it">Daniele Strollo</a>
 ***************************************************************************/

package org.gcube.portlets.admin.resourcemanagement.client.utils;

/**
 * @author Daniele Strollo (ISTI-CNR)
 *
 */
public class Messages {
	public static final String UPDATE_SW_REPOSITORY_FAILURE = "You are not allowed to require the Software Repository Upgrade.";
	public static final String UPDATE_SW_REPOSITORY_INSERT_URL = "Insert the URL for upgrade description.";
	public static final String NO_SCOPE_SELECTED = "No current scope selected.";
}
