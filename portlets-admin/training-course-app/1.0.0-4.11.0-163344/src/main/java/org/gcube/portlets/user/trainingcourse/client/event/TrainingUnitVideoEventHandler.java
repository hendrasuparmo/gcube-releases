package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface TrainingUnitVideoEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 1, 2018
 */
public interface TrainingUnitVideoEventHandler extends EventHandler {
	
	/**
	 * On video event.
	 *
	 * @param trainingUnitVideoEvent the training unit video event
	 */
	void onVideoEvent(TrainingUnitVideoEvent trainingUnitVideoEvent);
}