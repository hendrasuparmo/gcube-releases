package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface TrainingUnitQuestionnaireEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 25, 2018
 */
public interface TrainingUnitQuestionnaireEventHandler extends EventHandler {
	
	/**
	 * On questionnaire event.
	 *
	 * @param trainingUnitQuestionnaireEvent the training unit questionnaire event
	 */
	void onQuestionnaireEvent(TrainingUnitQuestionnaireEvent trainingUnitQuestionnaireEvent);
}