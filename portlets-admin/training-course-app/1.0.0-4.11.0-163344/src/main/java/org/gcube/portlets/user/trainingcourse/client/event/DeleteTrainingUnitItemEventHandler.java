package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface DeleteTrainingUnitItemEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 13, 2018
 */
public interface DeleteTrainingUnitItemEventHandler extends EventHandler {

	

	/**
	 * On remove training unit item.
	 *
	 * @param deleteUnitItem the delete unit item
	 */
	void onRemoveTrainingUnitItem(DeleteTrainingUnitItemEvent deleteUnitItem);
}