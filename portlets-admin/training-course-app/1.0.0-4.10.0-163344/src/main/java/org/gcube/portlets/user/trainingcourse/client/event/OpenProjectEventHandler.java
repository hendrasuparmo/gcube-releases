package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;



// TODO: Auto-generated Javadoc
/**
 * The Interface LoadListOfCourseEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 10, 2018
 */
public interface OpenProjectEventHandler extends EventHandler {
	

	void onLoadProject(OpenProjectEvent openProjectEvent);
}