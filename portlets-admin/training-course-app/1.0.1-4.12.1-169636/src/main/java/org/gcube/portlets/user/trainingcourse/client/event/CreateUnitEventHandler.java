package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;



// TODO: Auto-generated Javadoc
/**
 * The Interface CreateFolderEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 12, 2018
 */
public interface CreateUnitEventHandler extends EventHandler {
	
	/**
	 * On create folder.
	 *
	 * @param createFolderEvent the create folder event
	 */
	void onCreateFolder(CreateUnitEvent createFolderEvent);
}