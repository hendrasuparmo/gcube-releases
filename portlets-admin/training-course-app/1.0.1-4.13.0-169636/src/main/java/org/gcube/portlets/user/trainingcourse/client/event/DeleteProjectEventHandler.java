package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface DeleteProjectEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 19, 2018
 */
public interface DeleteProjectEventHandler extends EventHandler {
	
	/**
	 * On delete project.
	 *
	 * @param deleteProjectEvent the delete project event
	 */
	void onDeleteProject(DeleteProjectEvent deleteProjectEvent);
}