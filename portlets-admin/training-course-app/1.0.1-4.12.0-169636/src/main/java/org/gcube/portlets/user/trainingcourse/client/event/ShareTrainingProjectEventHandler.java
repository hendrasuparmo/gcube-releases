package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;



// TODO: Auto-generated Javadoc
/**
 * The Interface ShareTrainingProjectEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 16, 2018
 */
public interface ShareTrainingProjectEventHandler extends EventHandler {
	
	/**
	 * On share project.
	 *
	 * @param shareTrainingProjectEvent the share training project event
	 */
	void onShareProject(ShareTrainingProjectEvent shareTrainingProjectEvent);
}