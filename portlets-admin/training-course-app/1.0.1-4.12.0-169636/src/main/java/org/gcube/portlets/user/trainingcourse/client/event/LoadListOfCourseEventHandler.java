package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;



// TODO: Auto-generated Javadoc
/**
 * The Interface LoadListOfCourseEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 10, 2018
 */
public interface LoadListOfCourseEventHandler extends EventHandler {
	

	/**
	 * On load list of courses.
	 *
	 * @param loadListOfCourseEvent the load list of course event
	 */
	void onLoadListOfCourses(LoadListOfCourseEvent loadListOfCourseEvent);
}