package org.gcube.portlets.user.trainingcourse.client.event;

import com.google.gwt.event.shared.EventHandler;

// TODO: Auto-generated Javadoc
/**
 * The Interface ShowMoreInfoEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 16, 2018
 */
public interface ShowMoreInfoEventHandler extends EventHandler {
	

	/**
	 * On show more info.
	 *
	 * @param showMoreInfoEvent the show more info event
	 */
	void onShowMoreInfo(ShowMoreInfoEvent showMoreInfoEvent);
}