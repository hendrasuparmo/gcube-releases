package org.gcube.portlets.admin.authportletmanager.shared;

/**
 * CostantsSharing usage for project
 * @author "Alessandro Pieve " <a
 *         href="mailto:alessandro.pieve@isti.cnr.it">alessandro.pieve@isti.cnr.it</a>
 * 
 */
public class ConstantsSharing {

	public static final String APPLICATION_ID = "org.gcube.portlets.admin.accountingmanager.server.portlet.AccountingManagerPortlet";
	public static final String ACCOUNTING_MANAGER_ID = "AccountingManagerId";
	public static final String AM_LANG_COOKIE = "AMLangCookie";	
	public static final String AM_LANG = "AMLang";
	
	public final static String DEFAULT_USER = "alessandro.pieve";
	
	public static final boolean DEBUG_MODE = false;
	public static final String DEBUG_TOKEN = "afdaa1c6-493b-405e-801d-b219e056f564|98187548";
	public static final boolean MOCK_UP = false;
	public static final String DEFAULT_ROLE = "OrganizationMember";
	public final static String DEFAULT_SCOPE = "/gcube/devNext";
	
	
	/***
	 * For client 
	 */
	public static final String LOADINGSTYLE = "x-mask-loading";
	public static final String SERVER_ERROR = "Sorry, an error has occurred on the server when";
	public static final String TRY_AGAIN = "Try again";
	public static int WIDTH_DIALOG_LOADER = 200;
	public static int HEIGHT_DIALOG_LOADER = 80;
	public static String Star="ALL";
	public static String StarIcon="*";
	public static String StarLabel="ALL SERVICE";
	public static String TagCaller="@";
	public static String TagRole="#";
	public static String TagService="$";
	public static String TagAccess="*";
	public static String TagTime="$";
	public static String TagType="*";

	
}
