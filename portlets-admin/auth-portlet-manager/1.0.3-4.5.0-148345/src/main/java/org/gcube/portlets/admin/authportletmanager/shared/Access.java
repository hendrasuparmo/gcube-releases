package org.gcube.portlets.admin.authportletmanager.shared;

public enum Access {
	  ALL,
	  ACCESS,
	  DELETE,
	  EXECUTE,
	  WRITE;
}

