package org.gcube.portlets.admin.authportletmanager.shared;

/**
 * 
 * @author "Alessandro Pieve " <a
 *         href="mailto:alessandro.pieve@isti.cnr.it">alessandro.pieve@isti.cnr.it</a>
 * 
 */
public class Constants {

	public static final String APPLICATION_ID = "org.gcube.portlets.admin.accountingmanager.server.portlet.AccountingManagerPortlet";
	public static final String ACCOUNTING_MANAGER_ID = "AccountingManagerId";
	public static final String AM_LANG_COOKIE = "AMLangCookie";	
	public static final String AM_LANG = "AMLang";
	public final static String DEFAULT_USER = "alessandro.pieve";


	
	public static final boolean DEBUG_MODE = true;
	public static final boolean TEST_ENABLE = false;
 
	
	public static final String DEFAULT_ROLE = "OrganizationMember";
	public final static String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	
	
	
	


	
}
