package org.gcube.portlets.admin.wfroleseditor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditRoleEventHandler extends EventHandler {
  void onEditRole(EditRoleEvent event);
}
