package org.gcube.portlets.admin.wfroleseditor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditRoleCancelledEventHandler extends EventHandler {
  void onEditRoleCancelled(EditRoleCancelledEvent event);
}
