package org.gcube.portlets.admin.wfroleseditor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddRoleEventHandler extends EventHandler {
  void onAddRole(AddRoleEvent event);
}
