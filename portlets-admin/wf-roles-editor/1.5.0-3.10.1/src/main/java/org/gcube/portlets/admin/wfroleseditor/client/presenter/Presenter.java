package org.gcube.portlets.admin.wfroleseditor.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;
/**
 * <code> Presenter </code> class 
 * 
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 * @version May 2011 (0.1) 
 */
public abstract interface Presenter {
  public abstract void go(final HasWidgets container);
}
