package org.gcube.portlets.admin.fhn_manager_portlet.shared.communication;

public enum ProgressStatus {

	PENDING,ONGOING,SUCCESS,ERROR
	
}
