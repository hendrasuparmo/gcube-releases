package org.gcube.portlets.admin.fhn_manager_portlet.shared;

public class Constants {

	public static final String APPLICATION_DIV_IDENTIFIER="GUI_DIV";
	public static final String SERVICE_RELATIVE_PATH="Manager";
	
	public static final String URL_REGEX="^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	
	
	
	public static final String EMPTY_GRID="No elements to display";
	
	
	// FILTERS
	public static final String SERVICE_PROFILE_ID="SERVICE_PROFILE_ID";
	public static final String VM_PROVIDER_ID="VM_PROVIDER_ID";
	public static final String VM_TEMPLATE_ID="VM_TEMPLATE_ID";
	public static final String REMOTE_NODE_ID="REMOTE_NODE_ID";
	
	
}
