<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%-- Uncomment below lines to add portlet taglibs to jsp
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<portlet:defineObjects />
--%>
<link rel="stylesheet"	href="<%=request.getContextPath()%>/fhnManager.css"	type="text/css">
<link rel="stylesheet"	href="<%=request.getContextPath()%>/xmlverbatim.css"	type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script
	src='<%=request.getContextPath()%>/fhnmanager/js/jquery-1.10.1.min.js'></script>
<script
	src='<%=request.getContextPath()%>/fhnmanager/js/bootstrap.min.js'></script>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/fhnmanager/fhnmanager.nocache.js"></script>
<div id="GUI_DIV">
</div>


