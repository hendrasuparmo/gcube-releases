package org.gcube.portlets.admin.fhn_manager_portlet.shared.model.exceptions;

public class MissingParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7308266670364892927L;

	public MissingParameterException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MissingParameterException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MissingParameterException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissingParameterException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MissingParameterException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
