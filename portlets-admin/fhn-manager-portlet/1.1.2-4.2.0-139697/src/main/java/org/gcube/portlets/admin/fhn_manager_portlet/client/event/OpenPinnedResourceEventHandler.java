package org.gcube.portlets.admin.fhn_manager_portlet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface OpenPinnedResourceEventHandler extends EventHandler {

	public void onOpenPinnedResource(OpenPinnedResourceEvent theEvent);
	
}
