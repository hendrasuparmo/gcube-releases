package org.gcube.portlets.admin.fhn_manager_portlet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface StopNodeEventHandler extends EventHandler {

	public void onStopNode(StopNodeEvent theEvent);
	
}
