package org.gcube.portlets.admin.fhn_manager_portlet.client.event;

import java.util.Map;

public interface GenericParameterEvent {

	public void setParameters(Map<String,String> parameters);
	
}
