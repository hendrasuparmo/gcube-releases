package org.gcube.portlets.admin.vredefinition.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NextButtonEventHandler extends EventHandler {
	void onNextButton(NextButtonEvent event);
	
}
