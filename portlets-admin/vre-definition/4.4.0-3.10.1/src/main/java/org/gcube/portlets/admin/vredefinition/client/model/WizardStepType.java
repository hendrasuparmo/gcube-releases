package org.gcube.portlets.admin.vredefinition.client.model;

public enum WizardStepType {

	VREDefinitionStart,
	VREDescription,
	VREFunctionalities,
	VREDefinitionEnd;
}
