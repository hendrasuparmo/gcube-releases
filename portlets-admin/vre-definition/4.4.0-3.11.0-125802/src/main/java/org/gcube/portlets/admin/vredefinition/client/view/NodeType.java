package org.gcube.portlets.admin.vredefinition.client.view;

public enum NodeType {
	/**
	 * INFO
	 */
	INFO, 
	/**
	 * CONTENT
	 */
	CONTENT,
	/**
	 * COLLECTION
	 */
	COLLECTION,
	/**
	 * METADATA
	 */	
	METADATA,
	/**
	 * FUNCTIONALITY
	 */	
	FUNCTIONALITY,
	/**
	 * REPORT
	 */	
	REPORT,
	/**
	 * ARCHITECTURE
	 */	
	ARCHITECTURE;	

}
