package org.gcube.portlets.admin.vredefinition.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TreeNodeFunctionalityEventHandler extends EventHandler {

	void onClick(TreeNodeFunctionalityEvent event);
}
