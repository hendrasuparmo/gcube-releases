package org.gcube.portlets.admin.dataminermanagerdeployer.client.application.menu;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
interface MenuUiHandlers extends UiHandlers {

	void setContentPush();
}
