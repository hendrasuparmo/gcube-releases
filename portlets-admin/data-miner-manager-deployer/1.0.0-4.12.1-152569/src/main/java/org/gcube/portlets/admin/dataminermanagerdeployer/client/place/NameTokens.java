package org.gcube.portlets.admin.dataminermanagerdeployer.client.place;


/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class NameTokens {
	public static final String HOME = "home";
	public static final String HELP = "help";
	public static final String DEPLOY = "deploy";
	
	public static String getHome() {
		return HOME;
	}

	public static String getHelp() {
		return HELP;
	}

	public static String getDeploy() {
		return DEPLOY;
	}
	
}
