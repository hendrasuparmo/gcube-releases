package org.gcube.portlets.admin.dataminermanagerdeployer.shared;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String APPLICATION_ID = "org.gcube.portlets.admin.dataminermanagerdeployer.portlet.DataMinerManagerDeployer";
	public static final String DATA_MINER_MANAGER_DEPLOYER_ID = "DataMinerManagerDeployerId";
	public static final String DATA_MINER_MANAGER_DEPLOYER_LANG_COOKIE = "DataMinerDeployerLangCookie";
	public static final String DATA_MINER_MANAGER_DEPLOYER_LANG = "DataMinerDeployerLang";

	public static final String TOKEN = "token";

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public final static String DEFAULT_TOKEN = "ae1208f0-210d-47c9-9b24-d3f2dfcce05f-98187548";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	// IS
	public static final String POOL_MANAGER_SERVICE_NAME = "dataminer-pool-manager";
	public static final String POOL_MANAGER_SERVICE_CLASS = "DataAnalysis";

	// ClientMonitor
	public static final int CLIENT_MONITOR_PERIODMILLIS = 2000;
	public static final int CLIENT_MONITOR_TIME_OUT_PERIODMILLIS = 1800000;

	// Session
	public static final String CURR_GROUP_ID = "CURR_GROUP_ID";
	public static final String CURR_USER_ID = "CURR_USER_ID";

}
