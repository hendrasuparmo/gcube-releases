package org.gcube.portlets.admin.searchmanagerportlet.gwt.shared;

public class SMConstants {

	public static final String TEMPIDOFFSET = "#SMP_";
	
	public static final String SEARCHABLE = "Searchable";
	
	public static final String PRESENTABLE = "Presentable";
	
	public static final String 	BRIDGING_STATUS = "SMP_isBridgingComplete";
}
