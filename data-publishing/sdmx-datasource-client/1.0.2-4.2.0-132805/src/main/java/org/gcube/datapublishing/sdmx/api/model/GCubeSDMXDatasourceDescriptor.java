package org.gcube.datapublishing.sdmx.api.model;

public interface GCubeSDMXDatasourceDescriptor {
	
	public String getRest_url_V2_1();

	public String getRest_url_V2();

	public String getRest_url_V1();
	
	public String getPublishInterfaceUrl();

}
