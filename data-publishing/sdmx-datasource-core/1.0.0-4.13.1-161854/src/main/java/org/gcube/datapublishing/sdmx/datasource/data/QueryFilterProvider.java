package org.gcube.datapublishing.sdmx.datasource.data;

import java.util.List;

public interface QueryFilterProvider 
{
	
	public List<String> getRequestedColumns ();

}
