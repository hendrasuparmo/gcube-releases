package org.gcube.datapublishing.sdmx.datasource.datatype;

public interface DataTypeManager {

	public DataTypeBean getDataType (String acceptHeader);
	
}
