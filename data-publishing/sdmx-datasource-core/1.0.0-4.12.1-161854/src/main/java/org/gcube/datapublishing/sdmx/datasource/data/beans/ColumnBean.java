package org.gcube.datapublishing.sdmx.datasource.data.beans;

public interface ColumnBean {

	String getConcept();

	String getId();

}