package org.gcube.datapublishing.sdmx.datasource.data;

public interface SDMXMetadataProviderGenerator {

	public SDMXMetadataProvider getMetadataProvider ();
	
}
