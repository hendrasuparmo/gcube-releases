package gr.cite.regional.data.collection.dataaccess.daos;

import gr.cite.regional.data.collection.dataaccess.entities.DataModel;

public interface DataModelDao extends Dao<DataModel, Integer> {

}
