package gr.cite.regional.data.collection.dataaccess.daos;

import gr.cite.regional.data.collection.dataaccess.entities.Annotation;

public interface AnnotationDao extends Dao<Annotation, Integer> {

}
