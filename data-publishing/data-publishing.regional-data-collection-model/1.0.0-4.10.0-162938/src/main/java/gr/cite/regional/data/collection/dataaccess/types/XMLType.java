package gr.cite.regional.data.collection.dataaccess.types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class XMLType implements UserType {
	
	private final int[] sqlTypesSupported = new int[]{Types.VARCHAR};
	
	@Override
	public int[] sqlTypes() {
		return sqlTypesSupported;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class returnedClass() {
		return String.class;
	}
	
	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == null) {
			return y == null;
		} else {
			return x.equals(y);
		}
	}
	
	@Override
	public int hashCode(Object x) throws HibernateException {
		return x == null ? null : x.hashCode();
	}
	
	public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws HibernateException, SQLException {
		assert (names.length == 1);
		String xmldoc = rs.getString(names[0]);
		return rs.wasNull() ? null : xmldoc;
	}
	
	public void nullSafeSet(PreparedStatement st, Object value, int index) throws HibernateException, SQLException {
		if (value == null) {
			st.setNull(index, Types.NULL);
		} else {
			st.setObject(index, value, Types.OTHER);
		}
	}
	
	@Override
	public Object deepCopy(Object value) throws HibernateException {
		if (value == null)
			return null;
		return new String((String) value);
	}
	
	@Override
	public boolean isMutable() {
		return false;
	}
	
	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (String) value;
	}
	
	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}
	
	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}
	
	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {
		return nullSafeGet(rs, names, owner);
	}
	
	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
		nullSafeSet(st, value, index);
		
	}
}