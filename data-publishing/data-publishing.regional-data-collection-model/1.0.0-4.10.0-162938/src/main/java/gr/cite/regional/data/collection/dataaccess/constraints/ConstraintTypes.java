package gr.cite.regional.data.collection.dataaccess.constraints;

public final class ConstraintTypes {
	public static final String ATTRIBUTE_CODELIST = "attributeCodelist";
	public static final String ATTRIBUTE_CODELIST_SUBSET = "attributeCodelistSubset";
	public static final String ATTRIBUTE_DATATYPE = "attributeDatatype";
	public static final String ATTRIBUTE_MANDATORY = "attributeMandatory";
	public static final String ENTITY_NO_DUPLICATES = "entityNoDuplicates";
}
