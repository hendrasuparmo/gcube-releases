package org.gcube.datapublishing.sdmx.datasource.tabman.querymanager.impl;

import org.gcube.datapublishing.sdmx.datasource.data.GenericQueryBuilderBasicImpl;

public class TabmanQueryBuilder extends GenericQueryBuilderBasicImpl<TabmanQueryImpl> {

	
	public TabmanQueryBuilder() {
		super (new TabmanQueryImpl());
	}

}
	

