The gCube System - sdmx-datasource-tabman
--------------------------------------------------
 
${description}
 
 
This software is part of the gCube Framework (https://www.gcube-system.org/): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of
European Union programmes including:
* the Sixth Framework Programme for Research and Technological Development -
DILIGENT (grant no. 004260);
* the Seventh Framework Programme for research, technological development and
demonstration - D4Science (grant no. 212488), D4Science-II (grant no.
239019),ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine
(grant no. 283644);
* the H2020 research and innovation programme - BlueBRIDGE (grant no. 675680),
EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant
no. 654119), SoBigData (grant no. 654024), AGINFRA PLUS (grant no. 731001).
 
 
Version
--------------------------------------------------
 
1.0.1-4.13.0-165841 (2018-11-29)
 
Please see the file named "changelog.xml" in this directory for the release notes.
 
 
Authors
--------------------------------------------------
 
* Ciro Formisano (ciro.formisano@eng.it), ENG, Roma, Engineering Ingegneria Informatica S.p.A..

aintainers
-----------
 
* Ciro Formisano (ciro.formisano@eng.it), ENG, Roma, Engineering Ingegneria Informatica S.p.A..
 
Download information
--------------------------------------------------
 
Source code is available from SVN: 
    https://svn.d4science.research-infrastructures.eu/gcube/trunk/data-publishing/sdmx-datasource/sdmx-datasource-tabman
 
Binaries can be downloaded from the gCube website: 
   https://www.gcube-system.org/
 
 
Installation
--------------------------------------------------
 
This component is part of SDMX Data Source service
    https://wiki.gcube-system.org/gcube/index.php/Data_Source  
 
Documentation 
--------------------------------------------------
 
 
Support 
--------------------------------------------------
 
Bugs and support requests can be reported in the gCube issue tracking tool:
    https://support.d4science.org/projects/gcube/
 
 
Licensing
--------------------------------------------------
 
This software is licensed under the terms you may find in the file named "LICENSE" in this directory.