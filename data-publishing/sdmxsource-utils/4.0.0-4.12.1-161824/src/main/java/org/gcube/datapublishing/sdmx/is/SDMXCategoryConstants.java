package org.gcube.datapublishing.sdmx.is;

public interface SDMXCategoryConstants {

	final String 	TYPE_SDMX = "SDMX",
					TYPE_SDMX_DATA_SOURCES = "SDMXDataSources";
			
}
