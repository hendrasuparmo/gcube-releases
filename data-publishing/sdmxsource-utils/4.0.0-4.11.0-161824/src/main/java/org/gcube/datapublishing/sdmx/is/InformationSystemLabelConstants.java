package org.gcube.datapublishing.sdmx.is;

public interface InformationSystemLabelConstants {

	final String 	SECONDARY_TYPE_LABEL = "SecondaryType",
						NAME_LABEL = "Name",
						CATEGORY_LABEL ="Category";
}
