package org.gcube.datapublishing.sdmx.security.model;

public interface Credentials {

	public String getType ();
}
