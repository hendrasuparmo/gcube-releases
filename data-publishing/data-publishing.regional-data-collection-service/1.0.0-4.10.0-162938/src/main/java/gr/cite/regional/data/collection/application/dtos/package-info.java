@XmlSchema(
	xmlns = {
		@XmlNs(prefix = "dc", namespaceURI = "http://purl.org/dc/elements/1.1/"),
		@XmlNs(prefix = "dcterms", namespaceURI = "http://purl.org/dc/terms/")
	}, elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)

package gr.cite.regional.data.collection.application.dtos;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;