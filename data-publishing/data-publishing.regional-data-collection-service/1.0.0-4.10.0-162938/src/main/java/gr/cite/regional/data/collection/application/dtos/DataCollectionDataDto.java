package gr.cite.regional.data.collection.application.dtos;

public class DataCollectionDataDto extends DataSubmissionDataDto {
    private String attributes;

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }
}
