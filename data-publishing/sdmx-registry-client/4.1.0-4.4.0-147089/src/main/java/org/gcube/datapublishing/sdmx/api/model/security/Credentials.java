package org.gcube.datapublishing.sdmx.api.model.security;

public interface Credentials {

	public String getType ();
}
