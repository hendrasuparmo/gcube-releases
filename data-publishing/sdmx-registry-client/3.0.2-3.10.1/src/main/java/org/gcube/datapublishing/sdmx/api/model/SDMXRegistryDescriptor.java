package org.gcube.datapublishing.sdmx.api.model;

public interface SDMXRegistryDescriptor {

	public String getUrl(SDMXRegistryInterfaceType interfaceType);

}