package org.gcube.datapublishing.sdmx.impl.reports;

public enum OperationStatus {
Success, Warning, Failure
}
