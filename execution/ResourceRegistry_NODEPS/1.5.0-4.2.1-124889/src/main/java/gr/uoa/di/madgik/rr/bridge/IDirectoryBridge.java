//package gr.uoa.di.madgik.rr.bridge;
//
//import gr.uoa.di.madgik.rr.ResourceRegistryException;
//
//import java.util.Set;
//
//public interface IDirectoryBridge
//{
//	public void refreshEditables(boolean incomingOnly);
//	public void setEditableTargets(Set<String> targets);
//	public void setReadOnlyTargets(Set<String> targets);
//	
//	public void loadTargets() throws ResourceRegistryException;
//	
//	public void doBridging() throws ResourceRegistryException;
//	
//}
