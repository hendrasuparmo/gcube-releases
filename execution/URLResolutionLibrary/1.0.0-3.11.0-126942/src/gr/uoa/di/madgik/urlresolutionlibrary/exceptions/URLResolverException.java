package gr.uoa.di.madgik.urlresolutionlibrary.exceptions;

/**
 * 
 * @author Alex Antoniadis
 * 
 */
public class URLResolverException extends Exception {
	private static final long serialVersionUID = 1L; 
	
	public URLResolverException(String string) {
		super(string);
	}
}
