package gr.uoa.di.madgik.urlresolutionlibrary.exceptions;

/**
 * 
 * @author Alex Antoniadis
 * 
 */
public class ParseException extends URLResolverException {
	private static final long serialVersionUID = 1L;

	public ParseException(String string) {
		super(string);
	}

}
