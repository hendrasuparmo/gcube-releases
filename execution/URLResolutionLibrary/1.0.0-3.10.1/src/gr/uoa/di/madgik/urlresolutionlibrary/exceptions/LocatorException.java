package gr.uoa.di.madgik.urlresolutionlibrary.exceptions;

/**
 * 
 * @author Alex Antoniadis
 * 
 */
public class LocatorException extends URLResolverException {
	private static final long serialVersionUID = 1L;

	public LocatorException(String string) {
		super(string);
	}

}
