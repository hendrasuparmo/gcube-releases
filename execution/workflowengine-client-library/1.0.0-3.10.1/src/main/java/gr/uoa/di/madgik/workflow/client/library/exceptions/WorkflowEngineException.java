package gr.uoa.di.madgik.workflow.client.library.exceptions;

public class WorkflowEngineException extends Exception {

	private static final long serialVersionUID = 1L;

	public WorkflowEngineException(Throwable cause) {
		super(cause);
	}
}
