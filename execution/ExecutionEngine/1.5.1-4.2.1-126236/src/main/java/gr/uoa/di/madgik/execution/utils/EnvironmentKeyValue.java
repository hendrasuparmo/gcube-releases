package gr.uoa.di.madgik.execution.utils;

public class EnvironmentKeyValue
{
	public String Key;
	public String Value;

	public EnvironmentKeyValue()
	{
	}
	
	public EnvironmentKeyValue(String Key,String Value)
	{
		this.Key=Key;
		this.Value=Value;
	}
}
