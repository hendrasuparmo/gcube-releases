package gr.uoa.di.madgik.execution.plan.element.variable;

public interface IInputOutputParameter extends IInputParameter, IOutputParameter
{

}
