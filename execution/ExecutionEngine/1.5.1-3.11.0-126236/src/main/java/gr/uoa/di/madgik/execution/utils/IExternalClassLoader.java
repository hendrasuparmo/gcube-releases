package gr.uoa.di.madgik.execution.utils;

public interface IExternalClassLoader
{
	public Class<?> FindClass(String className);
}
