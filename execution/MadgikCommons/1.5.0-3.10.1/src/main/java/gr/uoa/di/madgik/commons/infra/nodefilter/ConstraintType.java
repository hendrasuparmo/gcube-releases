package gr.uoa.di.madgik.commons.infra.nodefilter;

public enum ConstraintType {
	WEAK, STRONG, BOTH, NONE;
}
