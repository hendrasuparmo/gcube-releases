package gr.uoa.di.madgik.commons.channel.events;

/**
 * Specialization of the {@link ChannelStateEvent} abstract class to mark events
 * that contain payload additional to the control fields
 * 
 * @author gpapanikos
 */
public abstract class ChannelPayloadStateEvent extends ChannelStateEvent
{
}
