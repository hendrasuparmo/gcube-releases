package gr.uoa.di.madgik.workflow.adaptor.search.test;

import gr.uoa.di.madgik.workflow.adaptor.search.searchsystemplan.PlanNode;

public interface Generator 
{
	public PlanNode generate();
}
