package gr.uoa.di.madgik.environment.accounting.properties;

/**
 * @author jgerbe
 * 
 */
public enum JobProperties {
	jobId, jobQualifier, jobName, jobStart, jobEnd, jobStatus, vmsUsed, wallDuration;
}
