package gr.uoa.di.madgik.environment.is.elements.plot.localenv;

import java.util.HashSet;
import java.util.Set;

public class PlotLocalEnvironment
{
	public Set<PlotLocalEnvironmentFile> Files=new HashSet<PlotLocalEnvironmentFile>();
	public Set<PlotLocalEnvironmentVariable> Variables=new HashSet<PlotLocalEnvironmentVariable>();
}
