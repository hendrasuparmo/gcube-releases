package gr.uoa.di.madgik.environment.cms.elements;

public class Collection 
{
	public String id;
	public String name;
	public String description;
	public String creationTime;
	public boolean isUserCollection;
}

