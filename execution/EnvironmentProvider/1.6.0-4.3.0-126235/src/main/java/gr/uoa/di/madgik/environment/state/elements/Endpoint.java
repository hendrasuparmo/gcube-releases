package gr.uoa.di.madgik.environment.state.elements;

public interface Endpoint 
{
	public String getHostName() throws Exception;
	public String getPort() throws Exception;

}
