package gr.uoa.di.madgik.environment.is.elements.invocable.types;


public class ReflectableDescriptionItem
{
	public String Name=null;
	public String Type=null;
	public String EngineType=null;
	public IReflectableDescription Reflectable=null;
}
