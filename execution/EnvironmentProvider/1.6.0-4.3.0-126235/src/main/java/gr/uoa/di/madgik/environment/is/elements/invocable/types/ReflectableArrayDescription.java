package gr.uoa.di.madgik.environment.is.elements.invocable.types;


public class ReflectableArrayDescription implements IReflectableDescription
{
	private static final long serialVersionUID = -5366872518106331386L;
	public String Type=null;
	public ReflectableDescription Reflectable=null;
}
