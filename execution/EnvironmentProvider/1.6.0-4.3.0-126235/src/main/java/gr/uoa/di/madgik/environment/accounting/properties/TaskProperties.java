package gr.uoa.di.madgik.environment.accounting.properties;

/**
 * @author jgerbe
 * 
 */
public enum TaskProperties {
	jobId, refHost, refVM, domain, usageStart, usageEnd, usagePhase, inputFilesNumber, inputFilesSize, outputFilesNumber, outputFilesSize, overallNetworkIn, overallNetworkOut, cores, processors,
}
