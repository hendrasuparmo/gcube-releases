//package gr.uoa.di.madgik.workflow.adaptor.hive.utils.elementconstructors;
//
//import gr.uoa.di.madgik.execution.datatype.NamedDataType;
//import gr.uoa.di.madgik.execution.exception.ExecutionValidationException;
//import gr.uoa.di.madgik.searchlibrary.operatorlibrary.Unary;
//import gr.uoa.di.madgik.workflow.adaptor.hive.utils.NodeExecutionInfo;
//
//public interface ElementConstructor {
//	public NodeExecutionInfo contructPlanElement(Class<? extends Unary> unaryClass, NamedDataType[] inputLocator) throws ExecutionValidationException, Exception;
//}
