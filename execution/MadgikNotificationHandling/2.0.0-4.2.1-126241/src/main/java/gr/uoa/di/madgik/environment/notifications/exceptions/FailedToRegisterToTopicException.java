package gr.uoa.di.madgik.environment.notifications.exceptions;

public class FailedToRegisterToTopicException extends Exception {

	public FailedToRegisterToTopicException(Throwable cause) {
		super(cause);
	}
}