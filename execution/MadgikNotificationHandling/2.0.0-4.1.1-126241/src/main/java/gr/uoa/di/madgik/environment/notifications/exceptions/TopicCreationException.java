package gr.uoa.di.madgik.environment.notifications.exceptions;

public class TopicCreationException extends Exception {

	public TopicCreationException(Throwable cause) {
		super(cause);
	}
}