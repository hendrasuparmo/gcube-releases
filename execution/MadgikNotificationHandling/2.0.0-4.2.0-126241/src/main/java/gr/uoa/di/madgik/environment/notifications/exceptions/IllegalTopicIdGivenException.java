package gr.uoa.di.madgik.environment.notifications.exceptions;

public class IllegalTopicIdGivenException extends Exception {
	
	public IllegalTopicIdGivenException() {
		super("Illegal topic ID given.");
	}

}
