package gr.uoa.di.madgik.workflow.adaptor.datatransformation.plan.nodes;

/**
 * Abstract class of an execution node consisting an execution workflow plan.
 * 
 * @author john.gerbesiotis - DI NKUA
 *
 */
public abstract class PlanNode {
	
}
