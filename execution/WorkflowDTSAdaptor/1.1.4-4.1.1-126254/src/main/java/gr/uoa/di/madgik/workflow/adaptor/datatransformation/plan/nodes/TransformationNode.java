package gr.uoa.di.madgik.workflow.adaptor.datatransformation.plan.nodes;

/**
 * Represents an execution node of a workflow plan, other than the first one in
 * path of every transformation workflow plan, responsible for executing a
 * non-composite transformation.
 * 
 * @author john.gerbesiotis - DI NKUA
 * 
 */
public class TransformationNode extends PlanNode {

}
