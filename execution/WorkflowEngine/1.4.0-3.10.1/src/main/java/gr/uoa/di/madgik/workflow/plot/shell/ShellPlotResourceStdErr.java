package gr.uoa.di.madgik.workflow.plot.shell;

import gr.uoa.di.madgik.execution.plan.element.variable.IInputOutputParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class ShellPlotResourceStdErr implements IPlotResource
{
	public IInputOutputParameter Output=null;
	public boolean IsFile=false;

}
