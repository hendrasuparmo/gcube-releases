package gr.uoa.di.madgik.workflow.plot.shell;

import gr.uoa.di.madgik.execution.plan.element.variable.IInputParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class ShellPlotResourceParameter implements IPlotResource
{
	public String Name=null;
	public IInputParameter Parameter=null;
}
