package gr.uoa.di.madgik.workflow.plot.ws;

import gr.uoa.di.madgik.execution.plan.element.variable.IOutputParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class WSPlotResourceMethodOutput implements IPlotResource
{
//	public String Signature=null;
	public int MethodOrder;
	public IOutputParameter Output=null;
}
