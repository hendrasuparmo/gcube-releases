package gr.uoa.di.madgik.workflow.plot.pojo;

import gr.uoa.di.madgik.execution.plan.element.variable.IParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class PojoPlotResourceParameter implements IPlotResource
{
//	public String MethodSignature;
	public int MethodOrder=-1;
	public String ParameterName;
	public IParameter Parameter;
}
