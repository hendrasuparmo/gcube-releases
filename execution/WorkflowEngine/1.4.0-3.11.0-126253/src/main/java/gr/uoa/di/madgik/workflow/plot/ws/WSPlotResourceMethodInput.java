package gr.uoa.di.madgik.workflow.plot.ws;

import gr.uoa.di.madgik.execution.plan.element.variable.IInputParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class WSPlotResourceMethodInput implements IPlotResource
{
//	public String Signature=null;
	public int MethodOrder;
	public IInputParameter Input=null;

}
