package gr.uoa.di.madgik.workflow.plot.shell;

import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class ShellPlotResourceEnvironmentVariable implements IPlotResource
{
	public String Name;
	public String Value;
}
