package gr.uoa.di.madgik.workflow.adaptor.utils;

import gr.uoa.di.madgik.workflow.adaptor.IWorkflowAdaptor;

/**
 * The Interface IAdaptorResources is a tagging interface. Its implementers will be containers of information
 * that are needed for the creation and execution of the handled workflow. These are passed to the respective 
 * {@link IWorkflowAdaptor}.
 * 
 * @author gpapanikos
 */
public interface IAdaptorResources
{
	
}
