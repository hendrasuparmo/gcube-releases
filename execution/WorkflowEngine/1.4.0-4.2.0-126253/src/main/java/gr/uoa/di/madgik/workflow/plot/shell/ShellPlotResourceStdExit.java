package gr.uoa.di.madgik.workflow.plot.shell;

import gr.uoa.di.madgik.execution.plan.element.variable.IOutputParameter;
import gr.uoa.di.madgik.workflow.plot.commons.IPlotResource;

public class ShellPlotResourceStdExit implements IPlotResource
{
	public IOutputParameter Output=null;
	public boolean IsFile=false;

}
