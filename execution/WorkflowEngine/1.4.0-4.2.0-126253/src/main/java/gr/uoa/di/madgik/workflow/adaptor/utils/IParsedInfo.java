package gr.uoa.di.madgik.workflow.adaptor.utils;

/**
 * The Interface IParsedInfo is a tagging interface. Its implementers will be containers of information
 * produced by an {@link IWorkflowParser} and retrieved through the {@link IWorkflowParser#GetParsedInfo()}
 * 
 * @author gpapanikos
 */
public interface IParsedInfo
{

}
