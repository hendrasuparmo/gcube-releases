#!/bin/sh

cd ../../../MadgikCommons
./build.sh
cd ../EnvironmentProvider
./build.sh
cd ../InformationSystem
./build.sh
cd ../StorageSystem
./build.sh
cd ../ReportingFramework
./build.sh
cd ../GCubeEnvironmentProvider
./build.sh
cd ../gRS2
./build.sh
cd ../gRSBridge
./build.sh
cd ../ExecutionEngine
./build.sh
cd ../ExecutionEngineService
./build.sh
cd ../WorkflowEngine
./build.sh
cd ../WorkflowEngineService
./build.sh
cd ../WorkflowEngineServiceClient
./build.sh
