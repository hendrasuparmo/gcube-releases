/**
 * 
 */
package org.gcube.accounting.analytics.persistence;


/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class AccountingPersistenceQueryFactory {
	
	public static AccountingPersistenceQuery getInstance() {
		return AccountingPersistenceQuery.getInstance();
	}
}
