package org.gcube.documentstore.persistence;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;
import org.gcube.testutility.ScopedTest;
import org.gcube.testutility.TestUsageRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FalbackTest extends ScopedTest {
	
	private static Logger logger = LoggerFactory.getLogger(FalbackTest.class);
	
	@Test
	public void test() throws Exception{
		File f = new File("test.log");
		
		AccountingPersistenceFactory.getPersistence();
		
		Record record = TestUsageRecord.createTestServiceUsageRecord();
		FallbackPersistenceBackend fallbackPersistenceBackend = new FallbackPersistenceBackend(f);
		fallbackPersistenceBackend.reallyAccount(record);
		
		
		try(BufferedReader br = new BufferedReader(new FileReader(f))) {
		    for(String line; (line = br.readLine()) != null; ) {
		    	try {
		    		Record r = RecordUtility.getRecord(line);
		    		logger.debug(r.toString());
		    	} catch(Exception e){
		    		logger.error("Was not possible parse line {} to obtain a valid Record. Going to writing back this line as string fallback file.", line, e);
		    		
		    	}
		    }
		} catch (FileNotFoundException e) {
			logger.error("File non trovato", e);
		} catch (IOException e) {
			logger.error("IOException", e);
		}
		
	}
	
	
}
