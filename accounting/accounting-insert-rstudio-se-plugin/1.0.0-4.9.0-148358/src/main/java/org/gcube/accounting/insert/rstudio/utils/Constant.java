package org.gcube.accounting.insert.rstudio.utils;
public class Constant {
	
	//CONSTANT for connection Db	
	public static final Integer CONNECTION_TIMEOUT=15;
	public static final Integer NUM_RETRY=6;
	public static final Integer CONNECTION_TIMEOUT_BUCKET=15;
	public static final Integer VIEW_TIMEOUT_BUCKET=120;
	public static final Integer MAX_REQUEST_LIFE_TIME=120;
	
	
	
	public static final long  GIGABYTE = 1024L * 1024L * 1024L;
	public static final long  MEGABYTE = 1024L * 1024L;
	public static final long  KILOBYTE =  1024L;
}