package org.gcube.accounting.accounting.summary.access;

public class ParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6606149726716378377L;

	public ParameterException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ParameterException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ParameterException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ParameterException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
