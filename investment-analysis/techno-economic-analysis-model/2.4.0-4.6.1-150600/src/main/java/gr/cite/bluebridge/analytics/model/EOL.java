package gr.cite.bluebridge.analytics.model;

public class EOL {
	private int cage;
	private int nets;
	private int anchorsSystem;
	private int autofeedingMachine;
	private int supportEquipment;

	public int getCage() {
		return cage;
	}

	public void setCage(int cage) {
		this.cage = cage;
	}

	public int getNets() {
		return nets;
	}

	public void setNets(int nets) {
		this.nets = nets;
	}

	public int getAnchorsSystem() {
		return anchorsSystem;
	}

	public void setAnchorsSystem(int anchorsSystem) {
		this.anchorsSystem = anchorsSystem;
	}

	public int getAutofeedingMachine() {
		return autofeedingMachine;
	}

	public void setAutofeedingMachine(int autofeedingMachine) {
		this.autofeedingMachine = autofeedingMachine;
	}

	public int getSupportEquipment() {
		return supportEquipment;
	}

	public void setSupportEquipment(int supportEquipment) {
		this.supportEquipment = supportEquipment;
	}
}
