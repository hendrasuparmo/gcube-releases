package gr.cite.bluebridge.analytics.endpoint;

public class DatabaseProfile {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
