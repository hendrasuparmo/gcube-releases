package search.library.util.cql.query.tree;

public class OperationTypeConstants {
	
	public static final String start = "Start";
	
	public static final String gCQLQuery = "gCQLQuery";
	
	public static final String scopedClause = "ScopedClause";
	
	public static final String searchClause = "SearchClause";
	
	public static final String index = "Index";
	
	public static final String term = "Term";
	
	public static final String identifier = "Identifier";
	
	public static final String booleanGroup = "BooleanGroup";
	
	public static final String comparitor = "Comparitor";
	
	public static final String namedComparitor = "NamedComparitor";
	
	public static final String booleann = "Boolean";
	
	public static final String comparitorSymbol = "ComparitorSymbol";
	
	public static final String relation = "Relation";
	
	public static final String modifierList = "ModifierList";
	
	public static final String modifier = "Modifier";
	
	public static final String cqlQuery = "CQLQuery";
	
	public static final String booleanAnd = "and";
	
	public static final String booleanOr = "or";
	
	public static final String booleanNot = "not";
	
	public static final String booleanProx = "prox";
	
	public static final String sortSpec = "SortSpec";
	
	public static final String projectSpec = "ProjectSpec";
	
	public static final String fuseSpec = "FuseSpec";

}
