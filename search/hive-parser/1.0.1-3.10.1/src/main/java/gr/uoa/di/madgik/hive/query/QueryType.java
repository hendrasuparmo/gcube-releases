package gr.uoa.di.madgik.hive.query;

public enum QueryType {
	CREATE, LOAD, QUERY, INSERT, DROP, ADD
}
