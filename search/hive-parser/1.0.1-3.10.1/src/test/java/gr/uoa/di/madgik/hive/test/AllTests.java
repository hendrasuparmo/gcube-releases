package gr.uoa.di.madgik.hive.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ Submitions.class, SubmitionsRev.class })
public class AllTests {

}

