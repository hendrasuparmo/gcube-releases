package gr.uoa.di.madgik.hive.parse.responses;

public class QueryResponse extends ParseResponse{
	private String fromTable;
	private String fromPattern;
	private String insertTable;
	private String insertPatern;
	private String distribMode;
	private String script;
}
