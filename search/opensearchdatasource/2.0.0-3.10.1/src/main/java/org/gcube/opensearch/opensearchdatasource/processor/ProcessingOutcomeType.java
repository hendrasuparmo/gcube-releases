package org.gcube.opensearch.opensearchdatasource.processor;

public enum ProcessingOutcomeType {
	query,
	collectionId,
	language
}
