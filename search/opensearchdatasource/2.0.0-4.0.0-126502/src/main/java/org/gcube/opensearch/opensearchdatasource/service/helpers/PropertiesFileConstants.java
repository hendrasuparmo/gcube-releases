package org.gcube.opensearch.opensearchdatasource.service.helpers;

public class PropertiesFileConstants {
	
	public static final String USE_RR_ADAPTOR_PROP = "useRRAdaptor";
	
	public static final String HOSTNAME_PROP = "hostname";
	
	public static final String SCOPE_PROP = "scope";
	
	public static final String RESOURCES_FOLDERNAME_PROP = "resourcesFoldername";
	
	public static final String CACHE_REFRESH_INTERVALMILLIS_PROP = "cacheRefreshIntervalMillis";

	public static final String FACTORIES_PROP = "factories";
	
	public static final String MAP_DELIM = "###";
	
	public static final String MAP_EQ = "=";

}
