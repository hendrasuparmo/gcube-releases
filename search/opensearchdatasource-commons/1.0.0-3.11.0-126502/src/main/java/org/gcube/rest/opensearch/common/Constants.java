package org.gcube.rest.opensearch.common;

public class Constants {
	public static final String SERVICE_CLASS = "Search";
	public static final String SERVICE_NAME = "OpenSearchDataSource";
	public static final String ENDPOINT_KEY = "resteasy-servlet";
	
	public static final String RESOURCE_CLASS = "OpenSearchDataSourceResources";
	public static final String RESOURCE_NAME_PREF = "OpenSearchDataSourceResource";
	public static final String DEFAULT_RESOURCES_FOLDERNAME = "/tmp/resources/";
	
	public static final String PROPERTIES_FILE = "deploy.properties";
}
