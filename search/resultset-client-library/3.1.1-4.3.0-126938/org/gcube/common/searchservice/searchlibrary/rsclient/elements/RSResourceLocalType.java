package org.gcube.common.searchservice.searchlibrary.rsclient.elements;

/**
 * Indicates thet the resource is of local type
 * 
 * @author UoA
 */
public class RSResourceLocalType implements RSResourceType{
	/**
	 * Creates a new {@link RSResourceLocalType} 
	 */
	public RSResourceLocalType(){}
}
