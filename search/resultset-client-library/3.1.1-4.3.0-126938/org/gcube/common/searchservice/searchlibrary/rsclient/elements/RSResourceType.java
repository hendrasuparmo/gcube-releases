package org.gcube.common.searchservice.searchlibrary.rsclient.elements;

/**
 * Abstract interface that all resource locator types must inherit 
 * 
 * @author UoA
 */
abstract public interface RSResourceType {
}
