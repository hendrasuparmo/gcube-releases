
/**
 * Demo bean class
 * @author jackal
 *
 */
public class DemoBean {
	/**
	 * Just a param
	 */
	private String hello;

	/**
	 * 
	 * @return The String param
	 */
	public String getHello() {
		return hello;
	}

	/**
	 * @param hello the param
	 */
	public void setHello(String hello) {
		this.hello = hello;
	}
	
	
	
}
