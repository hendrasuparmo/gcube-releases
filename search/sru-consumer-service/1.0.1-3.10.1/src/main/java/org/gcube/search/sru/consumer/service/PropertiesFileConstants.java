package org.gcube.search.sru.consumer.service;

public class PropertiesFileConstants {
	
	public static final String HOSTNAME_PROP = "hostname";
	
	public static final String USE_RR_ADAPTOR_PROP = "useRR";
	
	public static final String SCOPE_PROP = "scope";
	
	public static final String RESOURCES_FOLDERNAME_PROP = "resourcesFoldername";
	
}
