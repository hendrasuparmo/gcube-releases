package org.gcube.search.sru.geonetwork.commons.api;

import org.gcube.rest.commons.resourceawareservice.ResourceAwareServiceRestAPI;

public interface SruGeoNwServiceFactoryAPI extends ResourceAwareServiceRestAPI {

}
