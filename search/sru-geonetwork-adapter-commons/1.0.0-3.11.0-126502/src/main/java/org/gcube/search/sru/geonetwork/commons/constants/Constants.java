package org.gcube.search.sru.geonetwork.commons.constants;

public class Constants {
	public static final String ENDPOINT_KEY = "resteasy-servlet";
	
	public static final String RESOURCE_CLASS = "SRUResources";
	public static final String RESOURCE_NAME_PREF = "SRUGeonetwork";
	
	public static final String XSLT2DC_FILE_NAME = "gmd2dc.xsl";
	public static final String GEONETWORK_FILE_NAME = "geonetwork.properties";
	public static final String PROPERTIES_FILE_NAME = "deploy.properties";
	public static final String HOSTNAME_NAME = "hostname";
	public static final String PORT_NAME = "port";
	public static final String SCOPE = "scope";
	public static final String RESOURCES_FOLDERNAME = "resourcesFoldername";
	
}
//scope=/gcube/devNext
//resourcesFoldername=/tmp/resources/sru-geonetwork
//#splitLists=false
//hostname=dionysus.di.uoa.gr
//port=8080