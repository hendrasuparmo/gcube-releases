package org.gcube.search.sru.geonetwork.service.exceptions;

public class GeonetworkAccessException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public GeonetworkAccessException(String message){
		super(message);
	}
	
	
}
