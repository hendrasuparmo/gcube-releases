package org.gcube.search.sru.geonetwork.service.exceptions;

public class CqlException extends Exception{

	private static final long serialVersionUID = 1L;

	public CqlException(String message){
		super(message);
	}
	
}
