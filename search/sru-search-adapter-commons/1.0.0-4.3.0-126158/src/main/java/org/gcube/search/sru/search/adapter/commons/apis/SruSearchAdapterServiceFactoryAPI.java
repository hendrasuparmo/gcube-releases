package org.gcube.search.sru.search.adapter.commons.apis;

import org.gcube.rest.commons.resourceawareservice.ResourceAwareServiceRestAPI;

public interface SruSearchAdapterServiceFactoryAPI extends ResourceAwareServiceRestAPI {
}
