package gr.uoa.di.madgik.searchlibrary.operatorlibrary.comparator;

public enum ComparisonMode { 
	COMPARE_LONGINTS,  
	COMPARE_DOUBLES,
	COMPARE_LONGINT_DOUBLE,
	COMPARE_DOUBLE_LONGINT,
	COMPARE_DATES,
	COMPARE_STRINGS
}