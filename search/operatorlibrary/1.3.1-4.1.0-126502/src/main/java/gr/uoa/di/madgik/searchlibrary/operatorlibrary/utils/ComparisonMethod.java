package gr.uoa.di.madgik.searchlibrary.operatorlibrary.utils;

public enum ComparisonMethod {
	PROVIDED_MODE,
	DETECT_MODE,
	FULL_COMPARISON
}
