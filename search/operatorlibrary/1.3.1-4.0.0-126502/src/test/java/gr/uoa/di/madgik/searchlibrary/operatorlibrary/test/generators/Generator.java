package gr.uoa.di.madgik.searchlibrary.operatorlibrary.test.generators;

public interface Generator<T> {
	T next();
}
