package gr.uoa.di.madgik.searchlibrary.operatorlibrary.utils;

public class OperatorLibraryConstants {
	public static final String RESULTSNO_EVENT = "resultsNumber";
	public static final String RESULTSNOFINAL_EVENT = "resultsNumberFinal";
}
