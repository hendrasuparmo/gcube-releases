package gr.uoa.di.madgik.searchlibrary.operatorlibrary.sort;

public enum SortAlgorithm {
	OFFLINE,
	ONLINE
}
