package gr.uoa.di.madgik.searchlibrary.operatorlibrary.booleancompare;

/**
 * The Operation set defines the set of permitted operations
 * @author UoA
 */
public interface OperationSet {

	/**
	 * GREATER_THAN
	 */
	public static String GREATER_THAN = "GreaterThan";
	/**
	 * LOWER_THAN
	 */
	public static String LOWER_THAN = "LowerThan";
	/**
	 * GREATER_EQUAL
	 */
	public static String GREATER_EQUAL = "GreaterEq";
	/**
	 * LOWER_EQUAL
	 */
	public static String LOWER_EQUAL = "LowerEq";
	/**
	 * EQUAL
	 */
	public static String EQUAL = "Eq";
	/**
	 * NOT_EQUAL
	 */
	public static String NOT_EQUAL = "NotEq";
	/**
	 * LIKE
	 */
	public static String LIKE = "Like";
	/**
	 * PLUS
	 */
	public static String PLUS = "Plus";
	/**
	 * MINUS
	 */
	public static String MINUS = "Minus";
	/**
	 * TIMES
	 */
	public static String TIMES = "Times";
	/**
	 * DIVIDED_BY
	 */
	public static String DIVIDED_BY = "Div";
	/**
	 * MAX
	 */
	public static String MAX = "Max";
	/**
	 * MIN
	 */
	public static String MIN = "Min";
	/**
	 * SIZE
	 */
	public static String SIZE = "Size";
	/**
	 * AVERAGE
	 */
	public static String AVERAGE = "Avg";
	/**
	 * SUM
	 */
	public static String SUM = "Sum";
	/**
	 * ID
	 */
	public static String ID = "ID";
	/**
	 * FIELD_NAME
	 */
	public static String FIELD_NAME = "FieldName";
	/**
	 * XPATH
	 */
	public static String XPATH = "XPath";
	/**
	 * TOKEN
	 */
	public static String TOKEN = "Token";
	
}
