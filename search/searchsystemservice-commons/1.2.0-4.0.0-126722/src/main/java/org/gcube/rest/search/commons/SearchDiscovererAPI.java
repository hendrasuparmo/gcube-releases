package org.gcube.rest.search.commons;

import java.util.Set;

public interface SearchDiscovererAPI {

	public Set<String> discoverSearchSystemRunninInstances(String scope);
}