package org.gcube.opensearch.opensearchlibrary.utils;

/**
 * The values of the SyndicationRight OpenSearch element
 * 
 * @author gerasimos.farantatos
 *
 */
public enum SyndicationRight {
	OPEN,
	LIMITED,
	PRIVATE,
	CLOSED
}