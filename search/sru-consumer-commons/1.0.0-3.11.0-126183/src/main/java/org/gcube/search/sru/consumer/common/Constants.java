package org.gcube.search.sru.consumer.common;

public class Constants {
	public static final String SERVICE_CLASS = "Search";
	public static final String SERVICE_NAME = "SruConsumerDatasource";
	public static final String ENDPOINT_KEY = "resteasy-servlet";
	
	public static final String RESOURCE_CLASS = "SruConsumerResources";
	public static final String RESOURCE_NAME_PREF = "SruConsumerResource";
	public static final String DEFAULT_RESOURCES_FOLDERNAME = "/tmp/resources/sru-consumer";
	
	public static final String PROPERTIES_FILE = "deploy.properties";
}
