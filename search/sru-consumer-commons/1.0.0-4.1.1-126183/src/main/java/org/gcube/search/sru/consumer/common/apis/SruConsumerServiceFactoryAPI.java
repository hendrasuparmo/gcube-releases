package org.gcube.search.sru.consumer.common.apis;

import org.gcube.rest.commons.resourceawareservice.ResourceAwareServiceRestAPI;

public interface SruConsumerServiceFactoryAPI extends
		ResourceAwareServiceRestAPI {

}
