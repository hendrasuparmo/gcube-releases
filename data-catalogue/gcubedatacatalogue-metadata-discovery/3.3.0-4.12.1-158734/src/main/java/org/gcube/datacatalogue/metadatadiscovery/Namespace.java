/**
 *
 */
package org.gcube.datacatalogue.metadatadiscovery;

/**
 * The Interface Namespace.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Apr 26, 2017
 */
public interface Namespace {

	public static final String Separator = ":";
	public String id = null;
	public String name = null;
	public String title = null;
	public String description = "";
}
