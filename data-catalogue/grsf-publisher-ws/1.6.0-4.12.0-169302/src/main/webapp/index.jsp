<html>
<body>
	<h2>The GRSF publisher web service is up and running!</h2>
	<div>
		This service has been implemented to serve the needs of GRSF records
		publication. You can find all the details about this service <a
			href="https://wiki.gcube-system.org/gcube/GCube_Data_Catalogue_for_GRSF"
			target="_blank">here</a>
	</div>
</body>
</html>
