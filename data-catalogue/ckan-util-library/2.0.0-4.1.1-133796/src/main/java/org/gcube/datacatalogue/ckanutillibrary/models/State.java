package org.gcube.datacatalogue.ckanutillibrary.models;

/**
 * The current state of this group/user
 * @author Costantino Perciante at ISTI-CNR 
 * (costantino.perciante@isti.cnr.it)
 */
public enum State{
	DELETED, ACTIVE
}