package org.gcube.datacatalogue.ckanutillibrary.models;

/**
 * Roles that user can have into the member table (the sys_admin is into the user table)
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public enum RolesIntoOrganization{
	MEMBER,
	EDITOR,
	ADMIN
}