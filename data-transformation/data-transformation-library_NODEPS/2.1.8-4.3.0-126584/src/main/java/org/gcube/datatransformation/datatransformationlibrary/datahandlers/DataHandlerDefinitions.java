package org.gcube.datatransformation.datatransformationlibrary.datahandlers;

/**
 * @author Dimitris Katris, NKUA
 *
 * <p>
 * <tt>DataElement</tt> attributes used by <tt>DataSources</tt> and <tt>DataSinks</tt>.  
 * </p>
 */
public class DataHandlerDefinitions {
	/**
	 * Content object id attribute name.
	 */
	public static final String ATTR_CONTENT_OID = "ContentOID";

	/**
	 * Metadata object id attribute name.
	 */
	public static final String ATTR_METADATA_OID = "MetadataOID";

	/**
	 * Metadata collection id attribute name.
	 */
	public static final String ATTR_METADATACOL_ID = "MetadataColID";
	
	/**
	 * Document Name attribute name.
	 */
	public static final String ATTR_DOCUMENT_NAME = "DocumentName";

	/**
	 * Document Name attribute name.
	 */
	public static final String ATTR_DOCUMENT_LANGUAGE = "language";

	/**
	 * Document Name attribute name.
	 */
	public static final String ATTR_COLLECTION_ID = "CollectionID";
	
	/**
	 * Content Object
	 */
	public static final String ATTR_CONTENT_OBJECT = "ContentObject";
}
