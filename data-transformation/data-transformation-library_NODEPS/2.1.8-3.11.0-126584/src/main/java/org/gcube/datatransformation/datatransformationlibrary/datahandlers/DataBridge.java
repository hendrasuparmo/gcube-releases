package org.gcube.datatransformation.datatransformationlibrary.datahandlers;

/**
 * @author Dimitris Katris, NKUA
 * 
 * <p><tt>DataBridge</tt> implements both {@link DataSource} and {@link DataSink} functionality.</p>
 * 
 * <p>Data Bridges are used internally by the library.</p>
 */
public interface DataBridge extends DataSource, DataSink {

}
