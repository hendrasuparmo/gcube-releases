package org.gcube.datatransformation.datatransformationlibrary.imanagers.queries;

/**
 * @author Dimitris Katris, NKUA
 * <p>
 * Query object for program parameters.
 * </p>
 */
public class ProgramParametersQueryObject extends QueryObject{
	/**
	 * The transformation program id.
	 */
	public String transformationProgramID;
	/**
	 * The transformation unit id.
	 */
	public String transformationUnitID;
}
