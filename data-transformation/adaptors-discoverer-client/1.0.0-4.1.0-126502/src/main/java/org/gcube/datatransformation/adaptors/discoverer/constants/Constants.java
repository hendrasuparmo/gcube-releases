package org.gcube.datatransformation.adaptors.discoverer.constants;

import org.gcube.datatransformation.adaptors.common.constants.ConstantNames;


public class Constants {

	public final String ENDPOINT_ENTRYNAME = ConstantNames.ENDPOINT_KEY;
}
