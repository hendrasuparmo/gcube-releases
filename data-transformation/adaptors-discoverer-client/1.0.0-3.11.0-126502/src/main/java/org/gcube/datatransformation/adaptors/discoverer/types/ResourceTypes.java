package org.gcube.datatransformation.adaptors.discoverer.types;

public enum ResourceTypes {
	DB, OAI_PMH, TREE;
}
