package org.gcube.datatransformation.datatransformationlibrary.imanagers.queries;

/**
 * @author Dimitris Katris, NKUA
 * <p>
 * Query object for description of tu or tp.
 * </p>
 */
public class DescriptionQueryObject extends QueryObject{
	/**
	 * The transformation program id.
	 */
	public String transformationProgramID;
	/**
	 * The transformation unit id.
	 */
	public String transformationUnitID;
}
