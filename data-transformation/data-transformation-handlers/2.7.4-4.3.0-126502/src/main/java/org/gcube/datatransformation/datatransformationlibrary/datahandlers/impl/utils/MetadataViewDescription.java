package org.gcube.datatransformation.datatransformationlibrary.datahandlers.impl.utils;

public class MetadataViewDescription {
	
	public enum CreationParameters {
		RELATEDBY_COLLID, COLLECTIONNAME, DESCRIPTION, INDEXABLE, 
		USER, METADATANAME, METADATALANG, METADATAURI, GENERATEDBY_COLLID, GENERATEDBY_URI, EDITABLE 
	}
}
