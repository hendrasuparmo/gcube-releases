package org.gcube.datatransformation.client.library.exceptions;

public class EmptySourceException extends Exception {

	public EmptySourceException(String string) {
		super(string);
	}

}
