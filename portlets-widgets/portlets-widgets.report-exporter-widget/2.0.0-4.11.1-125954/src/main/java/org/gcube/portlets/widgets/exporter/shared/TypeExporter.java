package org.gcube.portlets.widgets.exporter.shared;

public enum TypeExporter {
	
	DOCX,
	
	PDF,
	
	HTML,
	
	XML

}
