package org.gcube.portlets.widgets.exporter.shared;

import java.io.Serializable;

public class OpenXMLConverterException extends ReportExporterException {

	
	private static final long serialVersionUID = 1582384177082175426L;
	

	public OpenXMLConverterException(){	
	}
	
	public OpenXMLConverterException(String error) {
		super(error);
	}
	
	
}

