package org.gcube.portlets.widgets.workspaceuploader.server.upload;


/**
 * The Class UploadCanceledException.
 * thrown when the client cancels the upload transfer.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Sep 8, 2015
 */
public class UploadCanceledException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 4311434519628111824L;
}
