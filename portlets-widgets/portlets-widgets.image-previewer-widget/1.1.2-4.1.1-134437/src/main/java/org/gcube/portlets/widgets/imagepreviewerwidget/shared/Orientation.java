package org.gcube.portlets.widgets.imagepreviewerwidget.shared;

import java.io.Serializable;

/**
 * Image orientations
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public enum Orientation implements Serializable {
	UNDEFINED,
	DO_NOT_ROTATE,
	ROTATE_180,
	ROTATE_90,
	ROTATE_270
}
