/**
 *
 */
package org.gcube.portlets.widgets.wstaskexecutor.shared;



/**
 * The Enum GcubeScopeType.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 12, 2018
 */
public enum GcubeScopeType {

	ROOT,
	VO,
	VRE
}
