package org.gcube.portlets.widgets.pickitem.client.uibinder;

public interface SelectableItem {
	String getItemName();
}
