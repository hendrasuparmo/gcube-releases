package org.gcube.datacatalogue.grsf_manage_widget.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface EnableConfirmButtonEventHandler extends EventHandler {

	void onEvent(EnableConfirmButtonEvent event);
	
}
