package org.gcube.portlets.widget.collectionsindexedwords.client.exceptions;

import java.io.Serializable;

public class OnlyOpensearchException extends Exception implements Serializable{
	

	private static final long serialVersionUID = 1L;

	public OnlyOpensearchException() {
		super();
	}

	
	
}
