package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.icons;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {
	
	@Source("file.png")
	ImageResource fileIcon();
	
	@Source("folder.png")
	ImageResource folderIcon();
}
