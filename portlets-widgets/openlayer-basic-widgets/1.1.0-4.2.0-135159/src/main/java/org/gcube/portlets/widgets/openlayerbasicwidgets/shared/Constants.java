package org.gcube.portlets.widgets.openlayerbasicwidgets.shared;

/**
 * 
 * @author Giancarlo Panichi
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;
	
	public static final String DEFAULT_USER = "giancarlo.panichi";
	public static final String DEFAULT_ROLE = "OrganizationMember";
	public static final String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	public static final String DEFAULT_FOLDER_ID = "";

}
