package org.gcube.portlets.widgets.wsthreddssync.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface ShowMonitorSyncStatusEventHandler.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 16, 2018
 */
public interface ShowMonitorSyncStatusEventHandler extends EventHandler {
	


	/**
	 * On show sync status.
	 *
	 * @param showSyncStatusEvent the show sync status event
	 */
	void onShowMonitorSyncStatus(ShowMonitorSyncStatusEvent showSyncStatusEvent);
}