/**
 * 
 */
package org.gcube.portlets.widgets.workspacesharingwidget.client;

/**
 * 
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Sep 22, 2014
 *
 * Defines customizable labels used by MultiDrag Dialog
 *
 * Sets following parameters to display custom labels into Dialog
 */
public abstract class MultiDragConstants {
	
	public static String HEADING_DIALOG = "Group dragging contacts";
	public static String ALL_CONTACTS_LEFT_LIST = "All Contacts";
	public static String SHARE_WITH_RIGHT_LIST = "Share with...";

}
