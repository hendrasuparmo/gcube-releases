/**
 * 
 */
package org.gcube.portlets.widgets.wsexplorer.client.view;

import org.gcube.portlets.widgets.wsexplorer.shared.Item;



/**
 * The Interface SelectionItem.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 29, 2015
 */
public interface SelectionItem {

	/**
	 * Gets the selected item.
	 *
	 * @return the selected item
	 */
	Item getSelectedItem();

}