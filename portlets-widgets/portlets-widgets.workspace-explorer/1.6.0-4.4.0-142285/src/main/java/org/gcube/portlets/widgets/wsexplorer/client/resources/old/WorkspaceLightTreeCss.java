/**
 * 
 */
package org.gcube.portlets.widgets.wsexplorer.client.resources.old;

import com.google.gwt.resources.client.CssResource;

/**
 * The Interface WorkspaceLightTreeCss.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jul 8, 2015
 */
public interface WorkspaceLightTreeCss extends CssResource {
	
	/**
	 * Name error.
	 *
	 * @return the string
	 */
	public String nameError();
	
}
