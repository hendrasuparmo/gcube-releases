/**
 *
 */
package org.gcube.portlets.widgets.wsexplorer.shared;


/**
 * The Enum ItemType.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 */
public enum ItemType {
	PRIVATE_FOLDER,
	SHARED_FOLDER, 
	VRE_FOLDER, 
	FOLDER, //MANDATORY
	EXTERNAL_IMAGE,
	EXTERNAL_FILE,
	EXTERNAL_PDF_FILE,
	EXTERNAL_URL,
	QUERY,
	CALENDAR,
	REPORT_TEMPLATE,
	REPORT,
	CSV,
	MOVIE,
	ZIP,
	RAR,
	HTML,
	XML,
	TEXT_PLAIN,
	DOCUMENT,
	PRESENTATION,
	SPREADSHEET,
	METADATA,
	PDF_DOCUMENT,
	IMAGE_DOCUMENT,
	URL_DOCUMENT,
	GCUBE_ITEM,
	TIME_SERIES,
	WORKFLOW_REPORT,
	UNKNOWN_TYPE;
}
