/**
 * 
 */
package org.gcube.portlets.widgets.file_dw_import_wizard.client.rpc;


public class ImportServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2415709826152997218L;

	/**
	 * 
	 */
	protected ImportServiceException() {
		super();
	}

	/**
	 * @param message
	 */
	public ImportServiceException(String message) {
		super(message);
	}

}
