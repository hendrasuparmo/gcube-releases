/**
 * 
 */
package org.gcube.portlets.widgets.file_dw_import_wizard.client.fileimport;


public class ImportState {
	
	public static final int CREATE_STATE_SA_CREATION = 0;
	public static final int CREATE_STATE_STORE = 1;
	public static final int CREATE_STATE_APPROVE = 2;

}
