package org.gcube.portlets.widgets.userselection.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface DialogImages extends ClientBundle {
	@Source("feeds-loader.gif")
	ImageResource loader();
}
