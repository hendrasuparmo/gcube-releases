package org.gcube.portlets.widgets.StatisticalManagerAlgorithmsWidget.client;

public interface SubmissionHandler {

	
	public void onSubmit(SubmissionParameters params);
	
}
