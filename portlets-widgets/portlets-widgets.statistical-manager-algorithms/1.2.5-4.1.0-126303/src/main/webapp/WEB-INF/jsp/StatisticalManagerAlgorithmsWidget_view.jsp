<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
 
<!--                                           -->
<!-- The module reference below is the link    -->
<!-- between html and your Web Toolkit module  -->		
                                           
<script src='<%=request.getContextPath()%>/StatisticalManagerAlgorithmsWidget/StatisticalManagerAlgorithmsWidget.nocache.js'></script>
    
<link rel="stylesheet" href="<%= request.getContextPath()%>/StatisticalManagerAlgorithmsWidget.css" type="text/css">
<link rel="stylesheet" href="<%= request.getContextPath()%>/ExtGWT/css/gxt-all.css" type="text/css">
 
<div class="contentDiv" id="contentDiv"></div>
