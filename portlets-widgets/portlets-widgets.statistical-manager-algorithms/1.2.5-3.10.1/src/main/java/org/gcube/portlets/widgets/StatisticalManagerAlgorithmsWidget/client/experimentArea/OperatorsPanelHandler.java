/**
 * 
 */
package org.gcube.portlets.widgets.StatisticalManagerAlgorithmsWidget.client.experimentArea;

import org.gcube.portlets.widgets.StatisticalManagerAlgorithmsWidget.client.bean.Operator;




/**
 * @author ceras
 *
 */
public interface OperatorsPanelHandler {

	/**
	 * @param operatorPanel 
	 * @param operator
	 */
	void addOperator(OperatorPanel operatorPanel, Operator operator);

}
