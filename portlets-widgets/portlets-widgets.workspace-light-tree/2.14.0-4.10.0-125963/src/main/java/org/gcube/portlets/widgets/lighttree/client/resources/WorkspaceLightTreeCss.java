/**
 * 
 */
package org.gcube.portlets.widgets.lighttree.client.resources;

import com.google.gwt.resources.client.CssResource;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public interface WorkspaceLightTreeCss extends CssResource {
	
	public String nameError();
	
}
