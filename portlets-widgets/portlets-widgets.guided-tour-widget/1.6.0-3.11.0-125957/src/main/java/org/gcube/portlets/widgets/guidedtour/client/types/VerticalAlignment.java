package org.gcube.portlets.widgets.guidedtour.client.types;

public enum VerticalAlignment {
	ALIGN_TOP, ALIGN_MIDDLE, ALIGN_BOTTOM;
}
