package org.gcube.portlets.widgets.guidedtour.shared;

public enum TourLanguage {
	EN, FR, DE, IT, JP, KP, ZH, ES, PT;
}
