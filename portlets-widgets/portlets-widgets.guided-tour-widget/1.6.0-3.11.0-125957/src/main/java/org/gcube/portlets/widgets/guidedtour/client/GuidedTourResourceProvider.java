/**
 * 
 */
package org.gcube.portlets.widgets.guidedtour.client;

import org.gcube.portlets.widgets.guidedtour.resources.client.GuidedTourResource;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface GuidedTourResourceProvider {
	
	public GuidedTourResource getResource();

}
