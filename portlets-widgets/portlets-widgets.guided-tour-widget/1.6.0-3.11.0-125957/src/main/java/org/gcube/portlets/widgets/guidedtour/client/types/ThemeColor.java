package org.gcube.portlets.widgets.guidedtour.client.types;
/**
 * @author Massimiliano Assante ISTI-CNR
 * 
 * @version 1.0 Jan 12th 2012
 */
public enum ThemeColor {
	AQUAMARINE, BLACK, BLUE, GREEN, KHAKI, LIME, MAGENTA, ORANGE, PINK, RED, SILVER, YELLOW;
}
