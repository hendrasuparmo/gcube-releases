package org.gcube.portlets.widgets.dataminermanagerwidget.client.type;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum DataMinerWorkAreaEventType {
	OPEN, UPDATE;
}
