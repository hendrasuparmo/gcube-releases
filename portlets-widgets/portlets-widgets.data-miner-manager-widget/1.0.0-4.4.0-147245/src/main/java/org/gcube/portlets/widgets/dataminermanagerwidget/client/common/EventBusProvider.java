/**
 * 
 */
package org.gcube.portlets.widgets.dataminermanagerwidget.client.common;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * 
 * @author Giancarlo Panichi email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class EventBusProvider {

	public static final EventBus INSTANCE = new SimpleEventBus();

}
