package org.gcube.portlets.widgets.dataminermanagerwidget.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum MenuType {
		HOME, DATA_SPACE, EXPERIMENT, COMPUTATIONS;
}
