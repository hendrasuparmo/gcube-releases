/**
 * 
 */
package org.gcube.portlets.widgets.dataminermanagerwidget.client.common;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class EventBusProvider {

	public static final EventBus INSTANCE = new SimpleEventBus();

}
