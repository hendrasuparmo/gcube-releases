package org.gcube.portlets.widgets.dataminermanagerwidget.client.type;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum DataMinerWorkAreaElementType {
	InputDataSets, OutputDataSets, Computations;
}
