package org.gcube.portlets.widgets.dataminermanagerwidget.client.type;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum DataMinerWorkAreaRegionType {
	DataSets, Computations;
}
