package org.gcube.portlets.widgets.githubconnector.shared;

/**
 * 
 * @author Giancarlo Panichi
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String APPLICATION_ID = "org.gcube.portlets.widgets.githubconnector.portlet.GitHubConnectorPortlet";
	public static final String GITHUB_CONNECTOR_ID = "GitHubConnectorId";
	public static final String GITHUB_CONNECTOR_LANG_COOKIE = "GitHubConnectorLangCookie";
	public static final String GITHUB_CONNECTOR_LANG = "GitHubConnectorLang";

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public final static String DEFAULT_TOKEN = "6af6eaff-35bd-4405-b747-f63246d0212a-98187548";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	public static final String DEFAULT_FOLDER_ID = "";
	public static final String DEFAULT_REPOSITORY_OWNER = "jonan";
	public static final String DEFAULT_REPOSITORY_NAME = "jonan.github.io";
	
	

}
