package org.gcube.portlets.widgets.githubconnector.client.wizard;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author giancarlo
 * 
 *
 */
public interface WizardMessages  extends Messages {

	//
	@DefaultMessage("Back")
	String buttonBackLabel();

	@DefaultMessage("Next")
	String buttonNextLabel();

	@DefaultMessage("Finish")
	String buttonFinishLabel();

	
}
