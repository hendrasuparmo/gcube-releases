package org.gcube.portlets.widgets.netcdfbasicwidgets.shared;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public static final String DEFAULT_ROLE = "OrganizationMember";
	public static final String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public static final String DEFAULT_TOKEN = "ae1208f0-210d-47c9-9b24-d3f2dfcce05f-98187548";
	
	public static final String NETCDF_DATA_MAP="NETCDF_DATA_MAP";
	public static final int SAMPLE_LENGHT = 200;
}
