<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<!--                                           -->
<!-- The module reference below is the link    -->
<!-- between html and your Web Toolkit module  -->
<!-- <link rel="stylesheet" href="<%=request.getContextPath()%>/olbasicwidgets/reset.css" type="text/css"/> -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/OpenLayerBasicWidgets.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/ol.css"
	type="text/css">	
	
<script
	src='<%=request.getContextPath()%>/js/ol.js'></script>
<script
	src='<%=request.getContextPath()%>/js/jquery-1.11.0.min.js'></script>
<script
	src='<%=request.getContextPath()%>/olbasicwidgets/olbasicwidgets.nocache.js'></script>


<div class="contentDiv" id="contentDiv"></div>
