package org.gcube.portlets_widgets.catalogue_sharing_widget.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ShareCatalogue implements EntryPoint {
	
  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {

  }
}
