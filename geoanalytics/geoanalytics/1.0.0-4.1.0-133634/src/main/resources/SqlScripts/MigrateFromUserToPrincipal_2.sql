BEGIN TRANSACTION;

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('00000000-0000-0000-0000-000000000001', 'a75ec613-ee71-329d-b0ae-e57df66f49e7','___System_Usr___', 1,  '2013-09-10 18:31:05.699', '2014-01-21 17:40:48.175', '00000000-0000-0000-0000-000000000001', '7dcc6b07-9652-4d13-8e96-f3d5bf03ed6a');
		
INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('c08c3d2d-fe38-4345-bf3e-080cbb5050b0', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'TestUser1', 0, '2013-09-15 05:20:44.223', '2013-10-18 19:02:13.16', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', '4965b36f-60a1-4e15-9df6-88334669add6');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES  ('391919a9-dd92-45b4-a3f3-8a74009e6da6', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'JHavoc', 1, '2013-09-16 22:47:35.801', '2013-10-18 18:28:08.898', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', '7d9b7b90-fde2-4ded-abe9-fd62a9d9b0e4');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES  ('9de84a39-5d9f-4f36-bd9c-d549987ef677', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'JohnD', 1, '2013-09-15 22:07:18.372', '2013-10-18 18:28:28.722', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'ec0069bf-7773-49c6-a14d-d8a63827a5b5');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('d0f5c503-8b68-4645-a398-3a767f034f0a', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'RHawk', 1, '2013-09-16 22:50:00.818', '2013-10-18 18:28:36.363', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'e9d98035-9cda-4ab4-856a-e4e42a7221d0');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('bab51858-12ca-480f-a82c-85fb9efe91b3', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'RMustang', 1, '2013-09-16 22:32:50.252', '2013-10-18 18:29:45.948', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'bb61cbf8-81c1-49c4-84e2-0ba5c0be2217');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES  ('58f79c09-ea8f-4e6a-9362-70964f70ccb4', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'SKimbley', 1, '2013-09-17 19:26:18.67', '2013-10-18 18:29:55.431', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'e4695c1f-5fb6-4c39-832a-eadb1ba8bb19');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData") 
VALUES  ('0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'EdElric', 1, '2013-09-16 20:44:43.96', '2013-11-20 18:43:01.812', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', '1a92a02f-68e8-40e9-9b59-7828b3197685');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES  ('a5c570ee-7b87-408b-a386-e3b4a3d1ff7f', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'TestCUser1', 1, '2013-11-26 18:13:03.543', '2013-11-26 18:13:03.544', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'a5aa85ef-de68-4e85-a3c1-3287a18d8aa2');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('c6db653c-569d-4a8d-978e-9de5c0bdb614', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'Armstrong', 1, '2013-09-16 22:24:55.003', '2014-02-26 19:23:56.79', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', '736f188e-6f9a-4a31-9b28-1d87a3ffc066');

INSERT INTO "Principal" ("PRCP_ID", "PRCP_Class", "PRCP_Name", "PRCP_IsActive", "PRCP_CreationDate", "PRCP_LastUpdate", "PRCP_Creator", "PRCP_PrincipalData")
VALUES ('10d03553-5ce9-4c41-9416-3d6cca2d1369', 'a75ec613-ee71-329d-b0ae-e57df66f49e7', 'AElric', 1, '2013-09-16 22:20:15.153', '2014-02-27 10:05:09.667', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', '1062511e-76bf-42af-a565-c04d3d92565d');


insert into "Principal" values ('fdb41769-da11-11e4-b908-00155d10e309', '468a26f1-8bac-3513-a6d9-1fbea8f74dea', 'User', null, null, null, 1, null, now(), now(), null, '00000000-0000-0000-0000-000000000001');
insert into "Principal" values ('e41b6f3f-da15-11e4-b908-00155d10e309', '468a26f1-8bac-3513-a6d9-1fbea8f74dea', 'AppAdmin', null, null, null, 1, null, now(), now(), null, '00000000-0000-0000-0000-000000000001');
insert into "Principal" values ('6128b8e4-da15-11e4-b908-00155d10e309', '468a26f1-8bac-3513-a6d9-1fbea8f74dea', 'Guest', null, null, null, 1, null, now(), now(), null, '00000000-0000-0000-0000-000000000001');


insert into "PrincipalMembership" values('7780b52d-984a-4c84-b8d0-2f4372036229', '00000000-0000-0000-0000-000000000001', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('9ed71b8f-e810-48c1-9deb-756084926757','c08c3d2d-fe38-4345-bf3e-080cbb5050b0', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('90bfbabc-583d-49d7-8b7c-0f25b65bb3bf', '391919a9-dd92-45b4-a3f3-8a74009e6da6', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('20f3d25e-3f0e-4c05-928b-8ced915f611f', '9de84a39-5d9f-4f36-bd9c-d549987ef677', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('8c87d2fa-aede-4797-aa73-c9d296b06873', 'd0f5c503-8b68-4645-a398-3a767f034f0a', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('1f9661e7-37e9-464f-bbb3-26fd56be6146', 'bab51858-12ca-480f-a82c-85fb9efe91b3', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('22c21c29-5901-433e-9493-f34be4b7365d', '58f79c09-ea8f-4e6a-9362-70964f70ccb4', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('e6329f63-066e-41f3-b3d6-5150259ec5f3', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('e8887a3f-bb81-4bc7-9547-501d7ce7af1c', 'a5c570ee-7b87-408b-a386-e3b4a3d1ff7f', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('5dc638a2-c533-44af-b313-87fffd4d0c20', 'c6db653c-569d-4a8d-978e-9de5c0bdb614', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('840f2c23-e8ff-4fd7-a0a5-ba7d0a489d31', '10d03553-5ce9-4c41-9416-3d6cca2d1369', 'fdb41769-da11-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('9c28dc22-da88-4d87-812a-1f4ae37ef666', '00000000-0000-0000-0000-000000000001', 'e41b6f3f-da15-11e4-b908-00155d10e309', now(), now());
insert into "PrincipalMembership" values('3b63d825-700c-4cf6-a428-a565bd192294', '0b7c1c68-8308-4fa5-9a74-3a56cccaba79', 'e41b6f3f-da15-11e4-b908-00155d10e309', now(), now());

----------------------
----------------------
----------------------

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('7dcc6b07-9652-4d13-8e96-f3d5bf03ed6a', 'admin', 'sys@example.com', '__System_User__', '__SU__', 1, '2013-09-10 18:31:05.699', '2014-01-21 17:40:48.175', '3001-01-31 23:59:59');
            
INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('4965b36f-60a1-4e15-9df6-88334669add6', '461433', 'testUser1@cite.gr', 'Test User1',  'TU2', 0, '2013-09-15 05:20:44.223', '2013-10-18 19:02:13.16', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('7d9b7b90-fde2-4ded-abe9-fd62a9d9b0e4', '435464', 'j.havoc@gmail.com', 'Jean Havoc',  'JH', 1, '2013-09-16 22:47:35.801', '2013-10-18 18:28:08.898', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('ec0069bf-7773-49c6-a14d-d8a63827a5b5', '427364', 'john.doe@example.com', 'John Doe',  'JD', 1, '2013-10-18 18:28:28.722', '2013-10-18 18:28:28.722', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('e9d98035-9cda-4ab4-856a-e4e42a7221d0', '427393', 'r.hawkeye@gmail.com', 'Riza Hawkeye',  'RH', 1, '2013-10-18 18:28:36.363', '2013-10-18 18:28:36.363', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('bb61cbf8-81c1-49c4-84e2-0ba5c0be2217', '435454', 'r.mustang@gmail.com', 'Roy Mustang',  'RM', 1, '2013-10-18 18:29:45.948', '2013-10-18 18:29:45.948', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('e4695c1f-5fb6-4c39-832a-eadb1ba8bb19', '427392', 's.kimbley@gmail.com', 'Solf J. Kimbley',  'SK', 1, '2013-10-18 18:29:55.431', '2013-10-18 18:29:55.431', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('1a92a02f-68e8-40e9-9b59-7828b3197685', 'edel', 'g.farantatos@gmail.com', 'Edward Elric',  'EE', 1, '2013-11-20 18:43:01.812', '2013-11-20 18:43:01.812', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('a5aa85ef-de68-4e85-a3c1-3287a18d8aa2', 'tcu1', 'tcu1@gmail.com', 'Test User 1',  'TCU1', 1, '2013-11-26 18:13:03.544', '2013-11-26 18:13:03.544', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('736f188e-6f9a-4a31-9b28-1d87a3ffc066', 'edel', 'a.armstrong@gmail.com', 'Alex Louis Armstrong',  'AA', 1, '2014-02-26 19:23:56.79', '2014-02-26 19:23:56.79', '3001-01-31 23:59:59');

INSERT INTO "PrincipalData"("PRNCD_ID", "PRNCD_Credential", "PRNCD_Email", "PRNCD_FullName", "PRNCD_Initials", "PRNCD_IsActive", "PRNCD_CreationDate", "PRNCD_LastUpdate", "PRNCD_ExpirationDate")
VALUES ('1062511e-76bf-42af-a565-c04d3d92565d', 'ael', 'a.elric@gmail.com', 'Alfonse Elric',  'AA', 1, '2014-02-27 10:05:09.667', '2014-02-27 10:05:09.667', '3001-01-31 23:59:59');

COMMIT;