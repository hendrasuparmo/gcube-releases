Hibernate 						4.3.0.Final			GNU LGPL
Hibernate Spatial (patched) 	4.0.M1				GNU LGPL
Spring 							4.2.4.RELEASE		Apache Licence v2.0
GeoTools 						9.3					GNU LGPL
GeoServerInterface 				1.10.2-2.16.0		EUPL					---Provider Implemented, Not Used
GeoServerManager				1.3.0				MIT License
PostGIS JDBC Driver				2.0.3				GNU LGPL
c3p0 JDBC Connection Pool		0.9.1.2				GNU LGPL v2.0

JUnit							4.11				CPL v1.0
Mockito							1.9.5				MIT License
Slf4j							1.6.1				MIT License

Tomcat 							7.0					Apache Licence v2.0	
JBoss AS 						7.2					GNU LGPL				---Not Used
PostgresQL						9.3.1				PostgresQL License
PostGIS							2.1.0				GNU GPL
Maven							3.1.0				Apache License v2.0
GeoServer						2.3.3				GNU GPL v2.0

OpenLayers						2.13.1				BSD
JQuery							1.10.2				http://code.jquery.com/jquery-1.10.2.min.js
Twitter Bootstrap				3.0.0				http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js
													http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css
Bootstrap Date picker			1.2.0				http://eternicode.github.io/bootstrap-datepicker/
Bootstrap Date time picker							http://tarruda.github.io/bootstrap-datetimepicker/
DataTables						1.9.4				BSD datatables.net
jQuery MouseWheel Plugin        3.1.3
jScrollPane                     2.0.18				MIT License http://jscrollpane.kelvinluck.com
Twitter Typeahead.js			0.9.3				MIT License
Bootstrap TagAutoComlete							MIT License http://sandglaz.github.com/bootstrap-tagautocomplete
css3-mediaqueries.js by Wouter van der Graaf        MIT License				---media query support for older browsers than IE9