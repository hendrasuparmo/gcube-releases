package gr.cite.geoanalytics.logicallayer;

public interface LogicalLayerBroker extends LayerOperations, NodePicker, NodeDataSource {

}
