package gr.cite.geoanalytics.notifications;

public enum EventType 
{
	SystemShutDown,
	WorkflowTaskReminder
}
