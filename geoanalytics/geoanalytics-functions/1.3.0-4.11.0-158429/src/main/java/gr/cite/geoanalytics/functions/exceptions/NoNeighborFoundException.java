package gr.cite.geoanalytics.functions.exceptions;

public class NoNeighborFoundException extends Exception{

	private static final long serialVersionUID = -9128431757234293398L;

	public NoNeighborFoundException (String msg){
		super(msg);
	}
	
}
