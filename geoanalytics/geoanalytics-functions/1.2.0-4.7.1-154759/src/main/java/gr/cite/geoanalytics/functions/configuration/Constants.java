package gr.cite.geoanalytics.functions.configuration;

/*
 * DO NOT EVER EDIT THIS CLASS, UNLESS YOU KNOW WHAT YOUR'RE DOING... THESE ARE SYSTEMIC FIELDS WHICH ARE CREATED OR NEEDED AT RUNTIME...
 */
public class Constants {

	
	public static String CONTEXT_FILENAME = "application.xml";
	public static String GENERATED_PROPS_FILENAME = "runtime.properties";
	
	
}
