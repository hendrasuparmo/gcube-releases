package gr.cite.geoanalytics.functions.functions;

public interface Function {
	
	double execute(double x, double y) throws Exception ;
	
}
