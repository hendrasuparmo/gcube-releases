<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	
	<description>
		Function which run on Geoanalytics. Each function returns a Layer with the result data which can be viewed on Geoanalytics.
		Provides predefined and user defined functions.
	</description>
	
	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>org.gcube.tools</groupId>
		<version>1.0.0</version>
	</parent>

	<modelVersion>4.0.0</modelVersion>

	<groupId>gr.cite.geoanalytics</groupId>
	<artifactId>geoanalytics-functions</artifactId>
	<version>1.3.0-4.8.0-158429</version>
	<packaging>jar</packaging>

	<name>geoanalytics-functions</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
<!-- 		<geotools.version>16.2</geotools.version> -->
		<geotools.version>17.0</geotools.version>
		<org.slf4j.version>1.7.7</org.slf4j.version>
		<org.apache.spark.version>2.1.0</org.apache.spark.version>
		<geoanalytics.commons.version>[1.0.0-SNAPSHOT, 2.0.0-SNAPSHOT)</geoanalytics.commons.version>
		<geoanalytics.clustermanager.version>[2.2.0-SNAPSHOT, 3.0.0-SNAPSHOT)</geoanalytics.clustermanager.version>
	</properties>

	<repositories>
		<repository>
			<id>osgeo</id>
			<name>Open Source Geospatial Foundation Repository</name>
			<url>http://download.osgeo.org/webdav/geotools/</url>
		</repository>
		<repository>
	        <snapshots>
	            <enabled>true</enabled>
	        </snapshots>
	        <id>boundless</id>
	        <name>Boundless Maven Repository</name>
	        <url>http://repo.boundlessgeo.com/main</url>
	    </repository>
	    
		<repository>
			<id>geotoolkit</id>
			<url>http://maven.geotoolkit.org</url>
		</repository>
	    
	    
	    <repository>
		  <id>jsi.sourceforge.net</id>
		  <name>sourceforge jsi repository</name>
		  <url>https://clojars.org/repo</url>
		</repository>
	    
<!-- 		<repository> -->
<!-- 			<id>osgeo</id> -->
<!-- 			<name>Open Source Geospatial Foundation Repository</name> -->
<!-- 			<url>http://download.osgeo.org/webdav/geotools/</url> -->
<!-- 		</repository> -->
<!-- 		<repository> -->
<!-- 			<id>maven2-repository.dev.java.net</id> -->
<!-- 			<name>Java.net repository</name> -->
<!-- 			<url>http://download.java.net/maven/2</url> -->
<!-- 		</repository> -->
	</repositories>
	
	<dependencies>
	
		<dependency>
			<groupId>gr.cite.bluebridge.analytics</groupId>
			<artifactId>techno-economic-analysis-model</artifactId>
			<version>[2.0.0, 3.0.0)</version>
			<exclusions>
				<exclusion>
					<groupId>com.fasterxml.jackson.core</groupId>
					<artifactId>jackson-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.fasterxml.jackson.core</groupId>
					<artifactId>jackson-annotations</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.gcube.data</groupId>
			<artifactId>simul-fish-growth-data-base</artifactId>
			<version>1.5.0-4.8.0-158095</version>
		</dependency>
		
		<dependency>
	        <groupId>junit</groupId>
	        <artifactId>junit</artifactId>
	        <version>4.11</version>
	        <scope>test</scope> 
	    </dependency>
		
		<dependency>
		    <groupId>org.geotools</groupId>
		    <artifactId>gt-wfs</artifactId>
		    <version>16.3</version>
		</dependency>
		
		<dependency>
	        <groupId>org.geotools</groupId>
	        <artifactId>gt-process</artifactId>
	        <version>${geotools.version}</version>
	    </dependency>
		<dependency>
			<groupId>org.geotools</groupId>
			<artifactId>gt-shapefile</artifactId>
			<version>${geotools.version}</version>
		</dependency>
		<dependency>
			<groupId>org.geotools</groupId>
			<artifactId>gt-epsg-hsql</artifactId>
			<version>${geotools.version}</version>
<!-- 			<exclusions> -->
<!-- 			  	<exclusion> -->
<!-- 			  		<groupId>jgridshift</groupId> -->
<!--     				<artifactId>jgridshift</artifactId> -->
<!-- 			  	</exclusion> -->
<!-- 			</exclusions> -->
		</dependency>
		<dependency>
			<groupId>org.geotools</groupId>
			<artifactId>gt-cql</artifactId>
			<version>${geotools.version}</version>
		</dependency>
		<dependency>
		  <groupId>org.geotools</groupId>
		  <artifactId>gt-geotiff</artifactId>
		  <version>${geotools.version}</version>
		</dependency>
		
		<dependency>
		  <groupId>org.geotools</groupId>
		  <artifactId>gt-geojson</artifactId>
		  <version>${geotools.version}</version>
		</dependency>
		
		<dependency>
            <groupId>org.geotools</groupId>
            <artifactId>gt-swing</artifactId>
            <version>${geotools.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.geotools</groupId>
            <artifactId>gt-render</artifactId>
            <version>${geotools.version}</version>
        </dependency>
        
        <dependency>
	        <groupId>org.geotools.jdbc</groupId>
	        <artifactId>gt-jdbc-postgis</artifactId>
	        <version>${geotools.version}</version>
	    </dependency>
		
		<dependency> <!-- Spark dependency -->
	      <groupId>org.apache.spark</groupId>
	      <artifactId>spark-core_2.11</artifactId>
	      <version>${org.apache.spark.version}</version>
	      <scope>provided</scope>
	    </dependency>
	    
	    <dependency>
		    <groupId>org.apache.spark</groupId>
		    <artifactId>spark-sql_2.11</artifactId>
		    <version>${org.apache.spark.version}</version>
		    <scope>provided</scope>
		</dependency>

	    
	    <dependency>
            <groupId>org.geotools</groupId>
            <artifactId>gt-coverage</artifactId>
            <version>${geotools.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.geotools</groupId>
            <artifactId>gt-image</artifactId>
            <version>${geotools.version}</version>
        </dependency>
        <dependency>
	        <groupId>org.geotools</groupId>
	        <artifactId>gt-wms</artifactId>
	        <version>${geotools.version}</version>
	    </dependency>
	    
	    
	    <dependency>
	        <groupId>net.sourceforge.javacsv</groupId>
	        <artifactId>javacsv</artifactId>
	        <version>2.0</version>
	    </dependency>
    
    	<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-encryption</artifactId>
			<version>1.0.3-4.8.0-142751</version>
		</dependency>
		
	    <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.6.5</version>
        </dependency>
        
        <dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-client</artifactId>
			<version>2.7.2</version>
			<scope>provided</scope>
		</dependency>
	    
<!-- 		<dependency> -->
<!-- 			<groupId>org.gcube.resources.discovery</groupId> -->
<!-- 			<artifactId>ic-client</artifactId> -->
<!-- 			<version>[1.0.0, 2.0.0)</version> -->
<!-- 		</dependency> -->
	    
<!-- 	    <dependency> -->
<!-- 	    	<groupId>gr.cite.gaap</groupId> -->
<!-- 			<artifactId>datatransfer-objects</artifactId> -->
<!-- 			<version>[2.0.0-SNAPSHOT, 3.0.0-SNAPSHOT)</version> -->
<!-- 	    </dependency> -->
	    
	    <dependency>
	    	<groupId>gr.cite.gos</groupId>
  			<artifactId>GeospatialOperationServiceClient</artifactId>
  			<version>[0.0.1-SNAPSHOT, 1.0.0-SNAPSHOT)</version>
	    </dependency>
	    
	    <dependency>
	        <groupId>com.opencsv</groupId>
	        <artifactId>opencsv</artifactId>
	        <version>3.9</version>
	    </dependency>
	    
	    <dependency>
		    <groupId>commons-io</groupId>
		    <artifactId>commons-io</artifactId>
		    <version>2.5</version>
		</dependency>
		
		 <dependency>
		    <groupId>org.apache.httpcomponents</groupId>
		    <artifactId>httpclient</artifactId>
		    <version>4.4</version>
		</dependency>


		<dependency>
        	<groupId>org.slf4j</groupId>
        	<artifactId>slf4j-api</artifactId>
        	<version>${org.slf4j.version}</version>
    	</dependency>

    	<dependency>
        	<groupId>org.slf4j</groupId>
        	<artifactId>slf4j-log4j12</artifactId>
        	<version>${org.slf4j.version}</version>
    	</dependency>

		<dependency>
		  <groupId>gr.cite.geoanalytics</groupId>
		  <artifactId>geoanalytics-functions-common</artifactId>
		  <version>${geoanalytics.commons.version}</version>
		</dependency>


		<dependency>
			<groupId>gr.cite</groupId>
			<artifactId>clustermanager</artifactId>
			<version>${geoanalytics.clustermanager.version}</version>
		</dependency>


		<dependency>
		    <groupId>net.sourceforge.jsi</groupId>
		    <artifactId>jsi</artifactId>
		    <version>1.0.0</version>
		</dependency>
		
		<dependency>
			<groupId>javax.cache</groupId>
			<artifactId>cache-api</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>org.ehcache</groupId>
			<artifactId>ehcache</artifactId>
			<version>3.1.3</version>
		</dependency>

<!-- <dependency> -->
<!-- 	<groupId>commons-configuration</groupId> -->
<!-- 	<artifactId>commons-configuration</artifactId> -->
<!-- 	<version>1.10</version> -->
<!-- </dependency> -->
	    
	</dependencies>

	<build>
		<plugins>
		<plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
			<plugin>
	            <inherited>true</inherited>
	            <groupId>org.apache.maven.plugins</groupId>
	            <artifactId>maven-compiler-plugin</artifactId>
	            <configuration>
	              <encoding>UTF-8</encoding>
	                <source>1.8</source>
	                <target>1.8</target>
	            </configuration>
	        </plugin>
            
            
            <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<version>2.4.2</version>
				<configuration>
					<outputDirectory>${project.build.directory}/classes/lib</outputDirectory>
					<overWriteReleases>false</overWriteReleases>
					<overWriteSnapshots>false</overWriteSnapshots>
					<overWriteIfNewer>false</overWriteIfNewer>
					<filters>
						<filter>
							<artifact>*:*</artifact>
							<excludes>
								<exclude>META-INF/*.SF</exclude>
								<exclude>META-INF/*.DSA</exclude>
								<exclude>META-INF/*.RSA</exclude>
							</excludes>
						</filter>
					</filters>
					
					<transformers>
<!-- 						<transformer -->
<!-- 							implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer"> -->
<!-- 							<resource>reference.conf</resource> -->
<!-- 						</transformer> -->
                            <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
                            <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                <manifestEntries>
                                	<Main-Class>gr.cite.geoanalytics.functions.experiments.FeatureExploreRandomNPV</Main-Class>
                                	<Specification-Title>Java Advanced Imaging Image I/O Tools</Specification-Title>
									<Specification-Version>1.1</Specification-Version>
									<Specification-Vendor>Sun Microsystems, Inc.</Specification-Vendor>
									<Implementation-Title>com.sun.media.imageio</Implementation-Title>
									<Implementation-Version>1.1</Implementation-Version>
									<Implementation-Vendor>Sun Microsystems, Inc.</Implementation-Vendor>
                                </manifestEntries>
                            </transformer>
                            <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                          		<resource>META-INF/registryFile.jai</resource>
                      		</transformer>
                        </transformers>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

            
            
		</plugins>
	</build>
</project>
