package gr.cite.geoanalytics.functions.kpi.test;

import gr.cite.geoanalytics.functions.kpi.IrrSeaBassFunction;
import gr.cite.geoanalytics.functions.kpi.IrrSeaBreamFunction;
import gr.cite.geoanalytics.functions.kpi.NpvSeaBassFunction;
import gr.cite.geoanalytics.functions.kpi.NpvSeaBreamFunction;

public class TestKpiFunctions {

	public static void main(String[] args) {
//		long startTime = System.currentTimeMillis();
//
//		String scope = "/gcube/preprod/preECO";
//
//		final NpvSeaBassFunction npvSeaBass = new NpvSeaBassFunction(scope);
//		final NpvSeaBreamFunction npvSeaBream = new NpvSeaBreamFunction(scope);
//		final IrrSeaBassFunction irrSeaBass = new IrrSeaBassFunction(scope);
//		final IrrSeaBreamFunction irrSeaBream = new IrrSeaBreamFunction(scope);
//
//		double longitude = 23.7;
//		double latitude = 37.9;
//
//		try {
//			System.out.println(npvSeaBass.execute(longitude, latitude));
//			System.out.println(npvSeaBream.execute(longitude, latitude));
//			System.out.println(irrSeaBass.execute(longitude, latitude));
//			System.out.println(irrSeaBream.execute(longitude, latitude));
//		} catch (Exception e) {
//			System.out.println("Could not execute NPV function with lat long [" + latitude + ", " + longitude + "]");
//			e.printStackTrace();
//		}
//
//		System.out.println("Took: " + ((double) (System.currentTimeMillis() - startTime) / 1000));
	}
}
