package gr.cite.gaap.datatransferobjects.plugin;

public class FunctionLayerConfigMessenger{

	private String requiredLayerName;
	private String layerID;
	
	
	public String getRequiredLayerName() {
		return requiredLayerName;
	}
	public void setRequiredLayerName(String requiredLayerName) {
		this.requiredLayerName = requiredLayerName;
	}
	public String getLayerID() {
		return layerID;
	}
	public void setLayerID(String layerID) {
		this.layerID = layerID;
	}
	
	
}

	
	
