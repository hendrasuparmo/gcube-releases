package gr.cite.gaap.datatransferobjects;

public class TaxonomyTermInfo {
	private String taxonomy = null;
	private String term = null;

	public String getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
}
