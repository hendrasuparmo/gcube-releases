package gr.cite.gaap.datatransferobjects;

public class DummyModel {

	private String layer1;
	private String newLayerName;

	public String getNewLayerName() {
		return newLayerName;
	}
	public void setNewLayerName(String newLayerName) {
		this.newLayerName = newLayerName;
	}
	public String getLayer1() {
		return layer1;
	}
	public void setLayer1(String layer1) {
		this.layer1 = layer1;
	}
}
