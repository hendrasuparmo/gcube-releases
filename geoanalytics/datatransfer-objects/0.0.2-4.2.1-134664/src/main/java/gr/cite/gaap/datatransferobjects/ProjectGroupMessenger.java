package gr.cite.gaap.datatransferobjects;

public class ProjectGroupMessenger {
	String[] users;
	UserinfoObject uio;
	
	public String[] getUsers() {
		return users;
	}
	public void setUsers(String[] users) {
		this.users = users;
	}
	public UserinfoObject getUio() {
		return uio;
	}
	public void setUio(UserinfoObject uio) {
		this.uio = uio;
	}
	
}
