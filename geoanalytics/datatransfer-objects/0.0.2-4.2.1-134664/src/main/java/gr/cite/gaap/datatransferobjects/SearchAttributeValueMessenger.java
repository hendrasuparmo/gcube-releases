package gr.cite.gaap.datatransferobjects;

public class SearchAttributeValueMessenger {
	private String geographicTaxonomy = null;
	private String taxonomy = null;

	public String getGeographicTaxonomy() {
		return geographicTaxonomy;
	}

	public void setGeographicTaxonomy(String geographyTaxonomy) {
		this.geographicTaxonomy = geographyTaxonomy;
	}

	public String getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}
}
