/**
 * 
 */
package gr.cite.gaap.datatransferobjects;

/**
 * @author vfloros
 *
 */
public class ProjectParticipantInfo {
	private String individualName = "";
	private String projectGroupName = "";
	public String getIndividualName() {
		return individualName;
	}
	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}
	public String getProjectGroupName() {
		return projectGroupName;
	}
	public void setProjectGroupName(String projectGroupName) {
		this.projectGroupName = projectGroupName;
	}
}
