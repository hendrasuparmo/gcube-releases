package gr.cite.repo.auth.filters;

public class SessionAttributes {
	
	public static final String LOGGED_IN_ATTRNAME = "samlLoggedIn";
	
	public static final String USERNAME_IN_ATTRNAME = "username";
	
	public static final String EMAIL_IN_ATTRNAME = "email";
	
	public static final String SAML_NAME_ID_ATTRNAME = "samlNameID";
	public static final String SAML_SESSION_IDS_ATTRNAME = "samlSessionIDs";
	
	public static final String USER_ROLE = "userRole";
	
}
