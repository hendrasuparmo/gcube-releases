package gr.cite.commons.util.datarepository.utils;

public class DataRepositoryUtils {
	public static final String PERSIST = "persist";
	public static final String UPDATE = "update";
	public static final String RETRIEVE = "retrieve";
	public static final String DELETE = "delete";
	public static final String LIST_IDS = "listIds";
	public static final String PERSIST_TO_FOLDER = "persistToFolder";
	public static final String UPDATE_TO_FOLDER = "updateToFolder";
	public static final String UPDATE_TO_FOLDER_MULTIPLE_FILES = "updateToFolderMultipleFiles";
	public static final String ADD_TO_FOLDER = "addToFolder";
	public static final String ADD_TO_FOLDER_MULTIPLE_FILES = "addToFolderMultipleFiles";
	public static final String LIST_FOLDER = "listFolder";
	public static final String GET_TOTAL_SIZE = "getTotalSize";
	public static final String GET_LAST_SWEEP = "getLastSweep";
	public static final String GET_SWEEP_SIZE_REDUCTION = "getSweepSizeReduction";
	public static final String CLOSE = "close";
}
