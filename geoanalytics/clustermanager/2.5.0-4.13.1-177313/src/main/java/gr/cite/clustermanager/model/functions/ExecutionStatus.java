package gr.cite.clustermanager.model.functions;

public enum ExecutionStatus {
	FAILED,
	INPROGRESS,
	QUEUED,
	SUCCEEDED
}
