package gr.cite.clustermanager.exceptions;

public class NoExecutionDetailsFound extends Exception {

	private static final long serialVersionUID = -3746224747969352156L;

	public NoExecutionDetailsFound(String message) {
        super(message);
    }
	
}
