package gr.cite.clustermanager.exceptions;

public class NoGosInstancesException extends Exception {

	private static final long serialVersionUID = -2033388441691255131L;

	public NoGosInstancesException(String message) {
        super(message);
    }
	
	
}
