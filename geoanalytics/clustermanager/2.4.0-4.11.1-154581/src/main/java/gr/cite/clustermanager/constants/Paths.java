package gr.cite.clustermanager.constants;

public class Paths {

	public static final String LAYERS_OF_GEOSERVERS_ZK_PATH = "/layers";
	public static final String GOS_NODES_INFORMATION_ZK_PATH = "/gosnodes";
	public static final String FUNCTION_EXECUTION_STATUS = "/functions";
	
}
