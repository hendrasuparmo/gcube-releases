package gr.cite.clustermanager.exceptions;

public class NoAvailableGos extends Exception {

	private static final long serialVersionUID = -2734480843400988386L;

	public NoAvailableGos(String message) {
		super(message);
	}
	
}
