package gr.cite.geoanalytics.dataaccess.entities.layer;

public class ImportType {
	public static final String TSV = "TSV";
	public static final String WFS = "WFS";
	public static final String SHAPEFILE = "ShapeFile";
	public static final String GEOTIFF = "GeoTIFF";
}
