package gr.cite.geoanalytics.dataaccess.typedefinition;

public interface DatabaseColumnType {
	public String getType(DataType dt);
}
