package gr.cite.geoanalytics.environmental.data.retriever.model;

public enum Unit {
	CELCIUS, KELVIN
}