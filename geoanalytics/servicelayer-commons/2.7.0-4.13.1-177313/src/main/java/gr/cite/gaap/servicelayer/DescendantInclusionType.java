package gr.cite.gaap.servicelayer;

public enum DescendantInclusionType
{
	ALL,
	EXCLUDE_USER_TAXONOMIES,
	INCLUDE_TAXONOMIES_OF_USER,
	INCLUDE_TAXONOMIES_OF_CUSTOMER
}