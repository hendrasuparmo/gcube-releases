package gr.cite.geoanalytics.dataaccess.entities.annotation.dao;

import java.util.UUID;

import gr.cite.geoanalytics.dataaccess.dao.Dao;
import gr.cite.geoanalytics.dataaccess.entities.annotation.Annotation;

public interface AnnotationDao extends Dao<Annotation, UUID>
{

}
