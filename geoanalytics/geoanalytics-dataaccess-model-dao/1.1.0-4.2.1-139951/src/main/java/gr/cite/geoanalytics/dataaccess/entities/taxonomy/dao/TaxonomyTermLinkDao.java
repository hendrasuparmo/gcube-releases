package gr.cite.geoanalytics.dataaccess.entities.taxonomy.dao;

import gr.cite.geoanalytics.dataaccess.dao.Dao;
import gr.cite.geoanalytics.dataaccess.entities.taxonomy.TaxonomyTermLink;
import gr.cite.geoanalytics.dataaccess.entities.taxonomy.TaxonomyTermLinkPK;

public interface TaxonomyTermLinkDao extends Dao<TaxonomyTermLink, TaxonomyTermLinkPK>
{

}
