package gr.cite.geoanalytics.dataaccess.entities.geocode.dao;

import gr.cite.geoanalytics.dataaccess.dao.Dao;
import gr.cite.geoanalytics.dataaccess.entities.geocode.TaxonomyTermLink;
import gr.cite.geoanalytics.dataaccess.entities.geocode.TaxonomyTermLinkPK;

public interface TaxonomyTermLinkDao extends Dao<TaxonomyTermLink, TaxonomyTermLinkPK>
{

}
