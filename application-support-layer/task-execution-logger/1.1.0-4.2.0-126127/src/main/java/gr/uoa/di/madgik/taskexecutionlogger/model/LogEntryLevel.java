package gr.uoa.di.madgik.taskexecutionlogger.model;

public enum LogEntryLevel { 
	INFORMATION, 
	WARNING,
	ERROR;
};
