package gr.uoa.di.madgik.taskexecutionlogger.utils;

import java.io.File;

public class Constants {

	public static final String CONFIG_FILE = "config.properties";
	
	public static final String FILENAME_PROPERTY_NAME = "file_name";
	
	public static final String PATH_PROPERTY_NAME = "path";
	
	public static final String DIRECTORY_NAME = "executionLogs";
	
	public static final String LOGS_FILE_NAME = "workflows.log";
	
	public static final String dateFormat = "yyyyMMdd";
	
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	public static final String  FILE_SEPARATOR = File.separator;
}
