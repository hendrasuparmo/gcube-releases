package org.gcube.application.framework.http.anonymousaccess.management;

public class AccessConstants {

	public static final String ShowCollectionInfos = "ShowCollectionInfos";

	public static final String ShowContentInfos = "ShowContentInfos";

	public static final String GetContent = "GetContent";

	public static final String GetMedatada = "GetMetadata";

	public static final String ShowCollections = "ShowCollections";

	public static final String GetThumbnails = "GetThumbnails";

	public static final String Search = "Search";

	public static final String ShowVREs = "ShowVREs";

	public static final String openAccessConfiguration = "OpenAccessConfiguration";

}
