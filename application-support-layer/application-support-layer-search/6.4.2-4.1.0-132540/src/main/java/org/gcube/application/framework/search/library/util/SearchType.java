package org.gcube.application.framework.search.library.util;

public class SearchType {

	public static final String Browse = "browse";
	
	public static final String BrowseFields = "browseFields";
	
	public static final String AdvancedSearch = "advancedSearch";
	
	public static final String SimpleSearch = "simpleSearch";
	
	public static final String GeoSearch = "geoSearch";
	
	public static final String GoogleSearch = "googleSearch";
	
	public static final String GenericSearch  = "genericSearch";
	
	public static final String PreviousSearch = "previousSearch";
	
	public static final String NoSearch = "noSearch";
	
	public static final String QuickSearch = "quickSearch";
	
}
