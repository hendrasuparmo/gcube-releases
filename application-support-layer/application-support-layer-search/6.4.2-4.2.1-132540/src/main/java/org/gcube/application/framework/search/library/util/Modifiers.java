package org.gcube.application.framework.search.library.util;

public class Modifiers {
	
	public final static String SortAscending = "sort.ascending";
	
	public final static String SortDescending = "sort.descending ";
	
	public final static String browseDistinct = "distinct";

}
