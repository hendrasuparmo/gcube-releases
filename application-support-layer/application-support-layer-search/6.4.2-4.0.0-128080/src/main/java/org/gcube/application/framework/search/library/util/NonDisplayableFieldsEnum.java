package org.gcube.application.framework.search.library.util;

public enum NonDisplayableFieldsEnum {
	ObjectID, docStatistics, rank
}
