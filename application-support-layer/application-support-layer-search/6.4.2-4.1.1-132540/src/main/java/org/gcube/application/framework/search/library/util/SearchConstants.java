package org.gcube.application.framework.search.library.util;

public class SearchConstants {
	
	public static final String CollectionField = "gDocCollectionID";
	
	public static final String LanguageField = "gDocCollectionLang";
	
	public static final String CollectionsRelation = "==";
	 
	public static final String LanguagesRelation = "==";
	
	public static final String CommonFieldRelation = "=";
	
	public static final String FTS_Field = "allIndexes";
	
	public static final String BrowseFieldRelation = "==";
	
	public static final String GEO_Field = "geo";
	
	public static final String GEO_Relation = "geosearch";
	
	public static final String geoInclusionMod = "inclusion";
	
	public static final String geoRankerMod = "ranker";
	
	public static final String geoRefinerMod = "refiner";
	
	public static final String intersect = "0";
	
	public static final String contains = "1";
	
	public static final String inside = "2";
	
	public static final String genericRanker = "GenericRanker";
	
	public static final String temporalRefiner = "TemporalRefiner";
	
	public static final String timeSpanRefiner = "TimeSpanRefiner";
	
	public static final String geoCollectionMod = "colID";
	
	public static final String geoLanguageMod = "lang";
	
	public static final String CommonOpenSearchFieldRelation = "=";

	public static final String SNIPPET = "Snippet";
	
	public static final String TITLE = "title";

}
