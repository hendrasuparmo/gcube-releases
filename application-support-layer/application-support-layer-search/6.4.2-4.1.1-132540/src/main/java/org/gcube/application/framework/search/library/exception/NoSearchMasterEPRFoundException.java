package org.gcube.application.framework.search.library.exception;

public class NoSearchMasterEPRFoundException extends Exception {
	
	public NoSearchMasterEPRFoundException() {
		super("No Search Master EPR found in IS");
	}
	
}