package org.gcube.application.framework.search.library.model;

public class SearchASLException extends Exception {

	public SearchASLException(Exception e) {
		super(e);
	}

	public SearchASLException(String string, Exception e) {
		super(string, e);
	}

	private static final long serialVersionUID = 1L;
	

}
