package org.gcube.application.framework.search.library.model;

public class Capability {
	
	String name;
	String id;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	

}
