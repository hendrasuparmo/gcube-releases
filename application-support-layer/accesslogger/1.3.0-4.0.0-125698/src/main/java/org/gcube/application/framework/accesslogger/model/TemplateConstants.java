package org.gcube.application.framework.accesslogger.model;

/**
 * Constants that are used to construct the message that will be logged
 * 
 * @author Panagiota Koltsida, NKUA
 *
 */
public class TemplateConstants {

	protected static final String COLLECTION_NAME = "collectionName";
	
	protected static final String COLLECTION_ID = "collectionID";
	
	protected static final String CONTENT_NAME = "contentName";
	
	protected static final String CONTENT_ID = "contentID";
	
	protected static final String OBJECT_NAME = "objectName";
	
	protected static final String OBJECT_ID = "objectID";
	
	protected static final String ANNOTATION_TYPE = "annotationType";
	
	protected static final String ANNOTATION_NAME = "annotationName";
	
	protected static final String ANNOTATION_EDITOR = "annotationEditor";
	
	protected static final String TERM = "term";
	
	protected static final String VALUE = "value";
	
	protected static final String OPERATOR = "operator";
	
	protected static final String LANGUAGE = "language";
	
	protected static final String eqChar = " = ";
	
	protected static final String andchar = " AND ";
	
	protected static final String separateCharacters = " | ";
	
	protected static final String BROWSE_TERM = "Browse by";
	
	protected static final String DISTINCT = "DISTINCT";
}
