package org.gcube.application.framework.accesslogger.model;

/**
 * All possible entry types for the access log
 * 
 * @author Panagiota Koltsida, NKUA
 *
 */
public class EntryTypeConstants {
	
	protected static final String GENERIC_ENTRY = "Generic_Entry";
	
	protected static final String LOGIN_VRE_ENTRY = "Login_To_VRE";
	
	protected static final String ADVANCED_SEARCH_ENTRY = "Advanced_Search";
	
	protected static final String ABSTRACT_SEARCH_ENTRY = "Abstract_Search";
	
	protected static final String SIMPLE_SEARCH_ENTRY = "Simple_Search";
	
	protected static final String SEMANTIC_ENRICHMENT_ENTRY = "Semantic_Enrichment";
	
	protected static final String QUICK_SEARCH_ENTRY = "Quick_Search";
	
	protected static final String GOOGLE_SEARCH_ENTRY = "Google_Search";
	
	protected static final String BROWSE_COLLECTION_ENTRY = "Browse_Collection";
	
	protected static final String PREVIOUS_SEARCH_ENTRY = "Search_Previous_Results";
	
	protected static final String CREATE_ANNOTATIONS_ENTRY = "Create_Annotation";
	
	protected static final String EDIT_ANNOTATIONS_ENTRY = "Edit_Annotation";
	
	protected static final String DELETE_ANNOTATIONS_ENTRY = "Delete_Annotation";
	
	protected static final String RETRIEVE_CONTENT_ENTRY = "Retrieve_Content";
	
}
