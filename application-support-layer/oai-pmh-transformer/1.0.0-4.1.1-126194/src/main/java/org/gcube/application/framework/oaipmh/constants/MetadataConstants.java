package org.gcube.application.framework.oaipmh.constants;


public class MetadataConstants {

	public static String DCNAME = "oai_dc";
	
	public static String OAIDC_SCHEMA = "http://www.openarchives.org/OAI/2.0/oai_dc.xsd";
	public static String OAIDC_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/";
	public static String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
	
}
