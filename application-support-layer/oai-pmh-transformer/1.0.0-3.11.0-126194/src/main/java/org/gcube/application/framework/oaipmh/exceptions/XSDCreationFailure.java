package org.gcube.application.framework.oaipmh.exceptions;

public class XSDCreationFailure extends Exception {
	
	private static final long serialVersionUID = 1L;

	public XSDCreationFailure(String message) {
		super(message);
	}

}
