package org.gcube.applicationsupportlayer.social.ex;

@SuppressWarnings("serial")
public class ApplicationProfileNotFoundException extends Exception {
	 public ApplicationProfileNotFoundException(String message) {
	    super(message);
	  }
}
