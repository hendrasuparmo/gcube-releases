package org.gcube.application.framework.contentmanagement.content.impl;

public enum DigitalObjectType {
	OAI,
	SPD,
	FIGIS,
	Generic
}
