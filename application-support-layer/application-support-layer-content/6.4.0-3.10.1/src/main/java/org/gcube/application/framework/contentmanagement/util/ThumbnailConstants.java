package org.gcube.application.framework.contentmanagement.util;

public class ThumbnailConstants {

	public static final String EQUAL = "EQUAL"; 
	
	public static final String CEIL = "CEIL"; 
	
	public static final String FLOOR = "FLOOR"; 
	
	public static final String FORCE_CREATE = "FORCE_CREATE"; 
	
	public static final String NO_FORCE_CREATE = "NO_FORCE_CREATE"; 
	
	public static final String contenttypeParamsPropertyType = "dts:contenttypeparameters";
}
