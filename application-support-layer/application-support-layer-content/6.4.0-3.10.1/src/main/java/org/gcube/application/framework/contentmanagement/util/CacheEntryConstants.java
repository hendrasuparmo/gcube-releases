package org.gcube.application.framework.contentmanagement.util;

public class CacheEntryConstants {
	
	public static final String searchConfigCache = "searchConfiguration";
	
	public static final String genericResourceCache = "genericResources";
	
	public static final String collectionCache = "collections";
	
	public static final String contentCache = "content";
	
	public static final String metadataCache = "metadata";
	
	public static final String thumbnailCache = "thumbnail";
	
	public static final String schemataCache = "schemata";
	
	public static final String abstractFieldsCache = "abstractFields";
	
	public static final String newContentCache = "newContent";
	
	public static final String resourceDiscoveryCache = "resourceDiscovery";
}
