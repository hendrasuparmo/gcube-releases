package org.gcube.application.framework.contentmanagement.util;

public class ElementTypeConstants {
	
	public final static String mainDoc = "mainDoc";
	
	public final static String alternativeRep = "alternative";
	
	public final static String annotation = "annotation";
	
	public final static String metadata = "metadata";
	
	public final static String part = "part";

}
