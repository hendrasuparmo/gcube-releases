package org.gcube.dataanalysis.dataminer.poolmanager.datamodel;

public class Action {

  private String name;
  private String description;
  private String script;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getScript() {
    return script;
  }

  public void setScript(String script) {
    this.script = script;
  }

}
