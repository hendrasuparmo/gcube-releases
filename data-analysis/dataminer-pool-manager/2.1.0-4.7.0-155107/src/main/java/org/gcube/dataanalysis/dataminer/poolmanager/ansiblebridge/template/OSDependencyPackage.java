package org.gcube.dataanalysis.dataminer.poolmanager.ansiblebridge.template;

import org.gcube.dataanalysis.dataminer.poolmanager.datamodel.Dependency;

public class OSDependencyPackage extends DependencyPackage {

  public OSDependencyPackage(Dependency d) {
    super(d);
  }

}
