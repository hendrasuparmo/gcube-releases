package org.gcube.dataanalysis.dataminer.poolmanager.ansible.model;

public class AnsibleHost {

  private String name;

  public AnsibleHost(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
