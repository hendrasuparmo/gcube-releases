package org.gcube.dataanalysis.dataminer.poolmanager.datamodel;

public class AlgoResource  {

	protected String id;

	public AlgoResource() {
	}

	public AlgoResource(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
