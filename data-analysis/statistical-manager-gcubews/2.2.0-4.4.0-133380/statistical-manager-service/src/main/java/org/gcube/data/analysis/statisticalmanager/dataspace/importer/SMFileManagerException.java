package org.gcube.data.analysis.statisticalmanager.dataspace.importer;

import org.gcube.data.analysis.statisticalmanager.exception.StatisticalManagerException;

public class SMFileManagerException extends StatisticalManagerException{

	private static final long serialVersionUID = -5477559275117495164L;
	
	public SMFileManagerException(String message) {
		super(message);
	}

}
