package org.gcube.data.analysis.statisticalmanager.exception;

public class AlgorithmManagementException extends StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AlgorithmManagementException() {
		// TODO Auto-generated constructor stub
	}

	public AlgorithmManagementException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AlgorithmManagementException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AlgorithmManagementException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AlgorithmManagementException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
