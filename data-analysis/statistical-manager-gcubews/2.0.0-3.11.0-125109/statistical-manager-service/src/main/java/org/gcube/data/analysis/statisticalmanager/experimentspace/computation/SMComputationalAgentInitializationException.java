package org.gcube.data.analysis.statisticalmanager.experimentspace.computation;

import org.gcube.data.analysis.statisticalmanager.exception.StatisticalManagerException;

public class SMComputationalAgentInitializationException extends
		StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5210673505710401287L;

	public SMComputationalAgentInitializationException(String message) {
		super(message);
	}
}
