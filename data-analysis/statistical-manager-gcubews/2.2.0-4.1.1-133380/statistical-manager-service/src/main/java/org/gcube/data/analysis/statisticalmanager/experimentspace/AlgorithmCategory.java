package org.gcube.data.analysis.statisticalmanager.experimentspace;

public enum AlgorithmCategory {
	
	DISTRIBUTIONS,
	
	EVALUATORS,
	
	MODELS,
	
	TRANSDUCERS,
	
	CLUSTERERS,
	
}
