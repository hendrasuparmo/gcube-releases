package org.gcube.data.analysis.statisticalmanager.exception;

public class HibernateManagementException extends StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4778675015567087895L;

	public HibernateManagementException() {
		// TODO Auto-generated constructor stub
	}

	public HibernateManagementException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public HibernateManagementException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public HibernateManagementException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public HibernateManagementException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
