package org.gcube.data.analysis.statisticalmanager.exception;


public class SMResourcesNotAvailableException extends
		StatisticalManagerException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4901783488659225750L;

	public SMResourcesNotAvailableException(String message) {
		super(message);
	}
}
