package org.gcube.data.analysis.statisticalmanager.exception;

public class PersistenceManagementException extends StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4628539045970576218L;

	public PersistenceManagementException() {
		// TODO Auto-generated constructor stub
	}

	public PersistenceManagementException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PersistenceManagementException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PersistenceManagementException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PersistenceManagementException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
