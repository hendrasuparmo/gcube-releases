package org.gcube.data.analysis.statisticalmanager.exception;

public class HLManagementException extends StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9176267412997574667L;

	public HLManagementException() {
		// TODO Auto-generated constructor stub
	}

	public HLManagementException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public HLManagementException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public HLManagementException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public HLManagementException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
