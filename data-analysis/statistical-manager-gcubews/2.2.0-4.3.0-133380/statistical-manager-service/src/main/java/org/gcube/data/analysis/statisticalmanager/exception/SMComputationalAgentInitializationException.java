package org.gcube.data.analysis.statisticalmanager.exception;


public class SMComputationalAgentInitializationException extends
		StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5210673505710401287L;

	public SMComputationalAgentInitializationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SMComputationalAgentInitializationException(String message,
			Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SMComputationalAgentInitializationException(String message,
			Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SMComputationalAgentInitializationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SMComputationalAgentInitializationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
