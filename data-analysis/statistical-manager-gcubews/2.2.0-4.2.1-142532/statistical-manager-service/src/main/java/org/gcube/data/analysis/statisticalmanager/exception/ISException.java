package org.gcube.data.analysis.statisticalmanager.exception;

public class ISException extends StatisticalManagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1886162008496615336L;

	public ISException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ISException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ISException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ISException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ISException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
