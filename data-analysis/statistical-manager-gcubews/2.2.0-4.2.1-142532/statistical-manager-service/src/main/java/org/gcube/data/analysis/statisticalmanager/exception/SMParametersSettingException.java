package org.gcube.data.analysis.statisticalmanager.exception;


public class SMParametersSettingException extends StatisticalManagerException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 364945176259290972L;
	
	public SMParametersSettingException(String message){
		super(message);
	}

	public SMParametersSettingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SMParametersSettingException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SMParametersSettingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SMParametersSettingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
	
}
