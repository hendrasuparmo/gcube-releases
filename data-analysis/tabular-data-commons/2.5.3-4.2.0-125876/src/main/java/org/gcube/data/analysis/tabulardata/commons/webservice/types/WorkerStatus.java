package org.gcube.data.analysis.tabulardata.commons.webservice.types;

public enum WorkerStatus {

	PENDING, INITIALIZING, VALIDATING_DATA, IN_PROGRESS, SUCCEDED, FAILED, ABORTED
}
