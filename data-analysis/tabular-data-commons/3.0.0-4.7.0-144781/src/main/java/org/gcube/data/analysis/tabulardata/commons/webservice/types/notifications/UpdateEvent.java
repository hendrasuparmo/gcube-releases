package org.gcube.data.analysis.tabulardata.commons.webservice.types.notifications;

public enum UpdateEvent {

	BROKEN_LINK, NEW_VERSION, CREATED, MODIFIED
	
}
