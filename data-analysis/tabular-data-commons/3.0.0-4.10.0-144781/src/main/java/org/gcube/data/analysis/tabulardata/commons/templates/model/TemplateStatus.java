package org.gcube.data.analysis.tabulardata.commons.templates.model;

public enum TemplateStatus {

	INITIALIZING,
	STOPPED,
	FAILED,
	RUNNING,
	SUCCEEDED
}
