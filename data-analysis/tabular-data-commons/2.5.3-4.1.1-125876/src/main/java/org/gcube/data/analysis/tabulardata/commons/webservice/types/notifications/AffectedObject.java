package org.gcube.data.analysis.tabulardata.commons.webservice.types.notifications;

public enum AffectedObject {

	RESOURCE, TEMPLATE, TABULAR_RESOURCE
}
