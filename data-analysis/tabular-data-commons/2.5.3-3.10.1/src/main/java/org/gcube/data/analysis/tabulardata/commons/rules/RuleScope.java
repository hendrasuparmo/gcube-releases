package org.gcube.data.analysis.tabulardata.commons.rules;

public enum RuleScope {

	TABLE, 
	COLUMN
}
