package org.gcube.data.analysis.tabulardata.commons.utils;

public class Constants {

	public static final String tabular_data_auth_header="tabular-data-auth-header";
	
	private static final String TNS = "http://gcube-system.org/";
	
	public static final String HISTORY_TNS = TNS+"history";
	
	public static final String OPERATION_TNS = TNS+"operation";
	
	public static final String QUERY_TNS = TNS+"query";
	
	public static final String RULE_TNS = TNS+"rule";
	
	public static final String TABULAR_RESOURCE_TNS = TNS+"tabularresource";
	
	public static final String TASK_TNS = TNS+"task";
	
	public static final String TEMPLATE_TNS = TNS+"template";
	
	public static final String EXT_RESOURCE_TNS =TNS+"externalresource";


	
}
