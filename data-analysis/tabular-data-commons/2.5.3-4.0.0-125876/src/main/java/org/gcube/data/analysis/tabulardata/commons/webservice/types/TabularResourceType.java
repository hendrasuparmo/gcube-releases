package org.gcube.data.analysis.tabulardata.commons.webservice.types;

public enum TabularResourceType {

	FLOW, 
	STANDARD
}
