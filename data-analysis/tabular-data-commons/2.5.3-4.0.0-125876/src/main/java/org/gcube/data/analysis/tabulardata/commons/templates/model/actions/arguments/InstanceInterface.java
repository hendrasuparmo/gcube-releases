package org.gcube.data.analysis.tabulardata.commons.templates.model.actions.arguments;

public interface InstanceInterface {

	String getDescription();
	String getName();
	boolean isValid();
	boolean isLeaf();
}
