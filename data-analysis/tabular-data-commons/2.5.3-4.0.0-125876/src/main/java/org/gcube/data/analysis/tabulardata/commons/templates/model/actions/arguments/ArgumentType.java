package org.gcube.data.analysis.tabulardata.commons.templates.model.actions.arguments;


public enum ArgumentType {

	ExistingTemplateColumn,
	NewTemplateColumn,
	String,
	Integer,
	ColumnCategory,
	DataType,
	PeriodType
	
}
