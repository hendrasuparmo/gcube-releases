package org.gcube.data.analysis.tabulardata.commons.webservice.types;

public enum TaskStepClassifier {

	PREPROCESSING,
	PROCESSING,
	POSTPROCESSING,
	DATAVALIDATION
}
