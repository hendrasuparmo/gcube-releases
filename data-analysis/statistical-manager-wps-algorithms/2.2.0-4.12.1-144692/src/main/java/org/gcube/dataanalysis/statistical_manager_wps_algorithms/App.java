package org.gcube.dataanalysis.statistical_manager_wps_algorithms;

import org.gcube.dataanalysis.statistical_manager_wps_algorithms.class_generator.ClassesGenerator;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String [] args)
	{
		ClassesGenerator generator = new ClassesGenerator();
		generator.engine();
	}
}
