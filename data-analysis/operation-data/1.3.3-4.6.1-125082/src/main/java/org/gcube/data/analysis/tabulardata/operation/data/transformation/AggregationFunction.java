package org.gcube.data.analysis.tabulardata.operation.data.transformation;

public enum AggregationFunction{
	AVG,
	COUNT,	
	MAX,
	MIN,
	SUM,
	ST_EXTENT
}