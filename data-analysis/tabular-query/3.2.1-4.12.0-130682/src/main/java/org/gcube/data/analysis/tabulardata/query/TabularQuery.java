package org.gcube.data.analysis.tabulardata.query;

import java.util.Iterator;

public interface TabularQuery extends TabularBaseQuery<TabularQuery, Iterator<Object[]>>  {
	
}