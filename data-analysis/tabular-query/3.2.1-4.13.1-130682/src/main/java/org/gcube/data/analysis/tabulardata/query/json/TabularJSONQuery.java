package org.gcube.data.analysis.tabulardata.query.json;

import org.gcube.data.analysis.tabulardata.query.TabularBaseQuery;

public interface TabularJSONQuery extends TabularBaseQuery<TabularJSONQuery, String>{

}