package org.gcube.dataanalysis.ecoengine.models.cores.aquamaps;

public enum EnvelopeName {

	TEMPERATURE,
	SALINITY,
	PRIMARY_PRODUCTION,
	ICE_CONCENTRATION,
	LAND_DISTANCE
}
