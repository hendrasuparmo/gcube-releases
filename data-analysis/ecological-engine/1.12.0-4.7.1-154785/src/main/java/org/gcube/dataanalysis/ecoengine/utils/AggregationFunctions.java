package org.gcube.dataanalysis.ecoengine.utils;

public enum AggregationFunctions {

	SUM,
	AVG
}
