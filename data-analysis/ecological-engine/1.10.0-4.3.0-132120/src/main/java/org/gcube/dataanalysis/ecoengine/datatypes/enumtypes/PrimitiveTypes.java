package org.gcube.dataanalysis.ecoengine.datatypes.enumtypes;

public enum PrimitiveTypes {
	STRING,
	NUMBER,
	ENUMERATED,
	CONSTANT,
	RANDOM,
	FILE,
	MAP,
	BOOLEAN,
	IMAGES
	
}
