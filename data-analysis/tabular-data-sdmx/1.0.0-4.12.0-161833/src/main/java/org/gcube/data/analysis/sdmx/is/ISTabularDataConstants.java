package org.gcube.data.analysis.sdmx.is;

import org.gcube.datapublishing.sdmx.is.SDMXCategoryConstants;

public interface ISTabularDataConstants extends SDMXCategoryConstants{
							
			
	final String 	NAME_PREFIX = "TableAssociations.";
}
