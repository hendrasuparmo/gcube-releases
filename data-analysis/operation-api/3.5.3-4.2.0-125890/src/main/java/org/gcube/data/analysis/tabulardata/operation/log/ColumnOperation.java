package org.gcube.data.analysis.tabulardata.operation.log;

public enum ColumnOperation {

	ADDED,
	REMOVED
}
