package org.gcube.data.analysis.tabulardata.operation.worker;

public interface AbortListener {

	void onAbort();
}
