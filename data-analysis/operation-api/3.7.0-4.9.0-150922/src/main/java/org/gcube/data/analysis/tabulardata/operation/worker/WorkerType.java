package org.gcube.data.analysis.tabulardata.operation.worker;

public enum WorkerType {

	DATA, METADATA, RESOURCE, VALIDATION
}
