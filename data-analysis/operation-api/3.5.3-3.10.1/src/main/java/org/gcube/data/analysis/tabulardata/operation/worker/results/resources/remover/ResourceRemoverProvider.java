package org.gcube.data.analysis.tabulardata.operation.worker.results.resources.remover;


public interface ResourceRemoverProvider {

	ResourceRemover getResourceRemover();
	
}
