package org.gcube.data.analysis.tabulardata.operation;

public enum OperationScope {

	VOID, TABLE, COLUMN

}
