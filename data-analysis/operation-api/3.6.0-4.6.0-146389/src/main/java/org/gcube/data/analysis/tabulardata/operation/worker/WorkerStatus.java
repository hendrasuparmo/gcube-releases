package org.gcube.data.analysis.tabulardata.operation.worker;

public enum WorkerStatus {

	INITIALIZING, IN_PROGRESS, SUCCEDED, FAILED, ABORTED

}
