package org.gcube.data.analysis.statisticalmanager;

public enum SMOperationType {
	
	IMPORTED,
	
	COMPUTED,
	
	SYSTEM

}
