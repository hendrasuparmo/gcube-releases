package org.gcube.data.analysis.tabulardata.cube.data;

public enum Condition {

	UPDATE, DELETE, INSERT
}
