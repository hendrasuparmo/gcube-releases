package org.gcube.dataanalysis.wps.statisticalmanager.synchserver.utils;

public interface Observer {

	public void isFinished(Observable o);	
	
	public void isStarted(Observable o);	
}
