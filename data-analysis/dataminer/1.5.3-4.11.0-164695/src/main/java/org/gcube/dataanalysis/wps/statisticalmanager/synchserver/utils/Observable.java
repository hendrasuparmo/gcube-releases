package org.gcube.dataanalysis.wps.statisticalmanager.synchserver.utils;


public interface Observable {

	public void setObserver(Observer o);
}
