<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="/ "
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-170</gml:X><gml:Y>-44.6</gml:Y></gml:coord>
      <gml:coord><gml:X>172</gml:X><gml:Y>1.91</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>

  <gml:featureMember>
    <ogr:qt_temp fid="qt_temp.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-159.0,-19.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:long>-159</ogr:long>
      <ogr:lat>-19.5</ogr:lat>
      <ogr:temp>28.5</ogr:temp>
      <ogr:Component_1>-122.34798</ogr:Component_1>
      <ogr:Component_2>-12.744011</ogr:Component_2>
      <ogr:Component_3>-49.863771</ogr:Component_3>
    </ogr:qt_temp>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:qt_temp fid="qt_temp.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>172.0,1.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:long>172</ogr:long>
      <ogr:lat>1.91</ogr:lat>
      <ogr:temp>27.6</ogr:temp>
      <ogr:Component_1>206.543116</ogr:Component_1>
      <ogr:Component_2>-10.420278</ogr:Component_2>
      <ogr:Component_3>-6.905119</ogr:Component_3>
    </ogr:qt_temp>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:qt_temp fid="qt_temp.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-170.0,0.017</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:long>-170</ogr:long>
      <ogr:lat>0.017</ogr:lat>
      <ogr:temp>25.9</ogr:temp>
      <ogr:Component_1>-134.615727</ogr:Component_1>
      <ogr:Component_2>-11.25202</ogr:Component_2>
      <ogr:Component_3>-30.997144</ogr:Component_3>
    </ogr:qt_temp>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:qt_temp fid="qt_temp.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-169.0,-44.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:long>-169</ogr:long>
      <ogr:lat>-44.6</ogr:lat>
      <ogr:temp>15.3</ogr:temp>
      <ogr:Component_1>-130.848563</ogr:Component_1>
      <ogr:Component_2>1.672295</ogr:Component_2>
      <ogr:Component_3>-74.84697</ogr:Component_3>
    </ogr:qt_temp>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:qt_temp fid="qt_temp.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-169.0,-43.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:long>-169</ogr:long>
      <ogr:lat>-43.6</ogr:lat>
      <ogr:temp>16</ogr:temp>
      <ogr:Component_1>-130.905515</ogr:Component_1>
      <ogr:Component_2>0.921018</ogr:Component_2>
      <ogr:Component_3>-73.886585</ogr:Component_3>
    </ogr:qt_temp>
  </gml:featureMember>
</ogr:FeatureCollection>