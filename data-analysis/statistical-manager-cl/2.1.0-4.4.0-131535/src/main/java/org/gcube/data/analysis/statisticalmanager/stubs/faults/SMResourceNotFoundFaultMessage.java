package org.gcube.data.analysis.statisticalmanager.stubs.faults;

import javax.xml.ws.WebFault;

@WebFault(name="SMResourceNotFoundFaultMessage")
public class SMResourceNotFoundFaultMessage extends StatisticalManagerFault{

}
