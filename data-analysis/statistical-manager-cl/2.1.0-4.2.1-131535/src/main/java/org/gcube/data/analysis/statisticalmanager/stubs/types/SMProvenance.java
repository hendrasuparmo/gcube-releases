package org.gcube.data.analysis.statisticalmanager.stubs.types;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum(String.class)
public enum SMProvenance {
	IMPORTED,

	COMPUTED,

	SYSTEM
}
