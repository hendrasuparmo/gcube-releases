package org.gcube.data.analysis.tabulardata.metadata.notification;

import java.io.Serializable;

public interface NotificationObject extends Serializable{

	String getHumanReadableDescription();
	
}
