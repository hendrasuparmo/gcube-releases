package org.gcube.data.analysis.tabulardata.task;

import org.gcube.data.analysis.tabulardata.model.table.Table;

public interface RunnableTask {

	void run(Table table);
}
