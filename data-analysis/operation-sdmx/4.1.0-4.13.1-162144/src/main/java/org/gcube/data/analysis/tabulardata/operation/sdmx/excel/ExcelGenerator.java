package org.gcube.data.analysis.tabulardata.operation.sdmx.excel;

public interface ExcelGenerator {

	public boolean generateExcel (String fileName, String folderName);
}
