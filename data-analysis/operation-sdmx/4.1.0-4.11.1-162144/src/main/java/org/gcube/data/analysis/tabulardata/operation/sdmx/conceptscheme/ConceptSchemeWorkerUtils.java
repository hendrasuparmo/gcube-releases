package org.gcube.data.analysis.tabulardata.operation.sdmx.conceptscheme;

import org.gcube.data.analysis.tabulardata.operation.sdmx.WorkerUtils;

public class ConceptSchemeWorkerUtils extends WorkerUtils {

	public static final String OBS_VALUE_COLUMN = "obsValueColumn";
	public static final String DATASOURCE_BASE_URL = "datasourceBaseUrl";

}
