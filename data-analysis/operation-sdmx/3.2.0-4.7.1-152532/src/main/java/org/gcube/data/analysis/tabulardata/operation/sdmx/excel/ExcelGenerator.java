package org.gcube.data.analysis.tabulardata.operation.sdmx.excel;

public interface ExcelGenerator {

	public void generateExcel (String fileName, String folderName);
}
