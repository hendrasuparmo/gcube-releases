package org.gcube.data.analysis.tabulardata.operation.sdmx.datastructuredefinition;

import org.gcube.data.analysis.tabulardata.operation.sdmx.codelist.WorkerUtils;

public class DataStructureDefinitionWorkerUtils extends WorkerUtils {

	public static final String OBS_VALUE_COLUMN = "obsValueColumn";
	public static final String DATASOURCE_BASE_URL = "datasourceBaseUrl";

}
