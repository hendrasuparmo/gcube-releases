package org.gcube.data.analysis.dataminermanagercl.shared;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class Constants {
	public static final boolean DEBUG = false;
	public static final boolean TEST_ENABLE = false;
	
	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public final static String DEFAULT_TOKEN = "ae1208f0-210d-47c9-9b24-d3f2dfcce05f-98187548";
	
	//public final static String DEFAULT_SCOPE = "/gcube/preprod/preVRE";
	//public final static String DEFAULT_TOKEN = "04269c7d-dab7-498a-841d-8d38ae2d482b-98187548";
	

	
	public static final String DATAMINER_SERVICE_CATEGORY = "DataAnalysis";
	public static final String DATA_MINER_SERVICE_NAME = "DataMiner";
	
	
	public static final String[] ClassificationNames = { "User Perspective" };
	// "Computation Perspective"};
	public static final String UserClassificationName = ClassificationNames[0];
	// public final static String computationClassificationName =
	// classificationNames[1];

	// WPS Data Miner
	public static final String WPSWebProcessingService = "WebProcessingService";
	public static final String WPSCancelComputationServlet = "CancelComputationServlet";
	
}
