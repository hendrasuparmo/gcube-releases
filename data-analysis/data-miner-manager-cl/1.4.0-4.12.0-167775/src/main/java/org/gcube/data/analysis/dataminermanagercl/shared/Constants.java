package org.gcube.data.analysis.dataminermanagercl.shared;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class Constants {
	public static final boolean DEBUG = false;
	public static final boolean TEST_ENABLE = false;
	
	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public final static String DEFAULT_TOKEN = "";
	
	public static final String DATAMINER_SERVICE_CATEGORY = "DataAnalysis";
	public static final String DATA_MINER_SERVICE_NAME = "DataMiner";

	// WPS Data Miner
	public static final String WPSWebProcessingService = "WebProcessingService";
	public static final String WPSCancelComputationServlet = "CancelComputationServlet";
	
}
