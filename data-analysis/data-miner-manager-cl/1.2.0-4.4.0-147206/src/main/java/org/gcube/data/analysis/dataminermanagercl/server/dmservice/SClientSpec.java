package org.gcube.data.analysis.dataminermanagercl.server.dmservice;

/**
 * Specification
 * 
 * @author "Giancarlo Panichi" email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class SClientSpec {
	private SClient sClient;

	public SClient getSClient() {
		return sClient;
	}

	public void setSClient(SClient sClient) {
		this.sClient = sClient;
	}

}
