package org.gcube.data.analysis.dataminermanagercl.shared.parameters;

/**
 * 
 * @author Giancarlo Panichi email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public enum ParameterType {
	OBJECT, TABULAR, FILE, ENUM, LIST, COLUMN, COLUMN_LIST, TABULAR_LIST, WKT, DATE, TIME;
}