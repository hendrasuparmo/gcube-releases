package org.gcube.data.analysis.excel;

public interface Table 
{
	
	public String getExcelTableName ();

	public String getOriginalTableName ();
}
