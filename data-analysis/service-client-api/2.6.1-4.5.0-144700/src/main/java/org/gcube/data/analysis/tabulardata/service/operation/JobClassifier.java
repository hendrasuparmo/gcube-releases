package org.gcube.data.analysis.tabulardata.service.operation;

public enum JobClassifier {
	
	PREPROCESSING,
	PROCESSING,
	POSTPROCESSING,
	DATAVALIDATION
	
	
}
