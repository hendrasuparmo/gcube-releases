package org.gcube.data.analysis.tabulardata.service.operation;

public interface BatchOptions {

	public OnErrorBehavior getBehavior();

}