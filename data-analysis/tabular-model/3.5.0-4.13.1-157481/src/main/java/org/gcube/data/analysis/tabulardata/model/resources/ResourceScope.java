package org.gcube.data.analysis.tabulardata.model.resources;

public enum ResourceScope {

	GLOBAL, LOCAL
	
}
