package org.gcube.data.analysis.tabulardata.model.metadata;

import org.gcube.data.analysis.tabulardata.model.metadata.table.TableMetadata;

public interface DataDependentMetadata extends TableMetadata {

}
