package org.gcube.data.analysis.tabulardata.model.resources;

public enum ResourceType {

	CHART, GUESSER, MAP, CODELIST, CSV, SDMX, JSON, GENERIC_FILE, GENERIC_TABLE, WEB
	
}
