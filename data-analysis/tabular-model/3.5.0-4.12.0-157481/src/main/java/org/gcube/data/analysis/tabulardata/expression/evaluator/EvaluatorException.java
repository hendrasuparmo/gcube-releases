package org.gcube.data.analysis.tabulardata.expression.evaluator;

public class EvaluatorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7424505003719931654L;

	public EvaluatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public EvaluatorException(String message) {
		super(message);
	}
	
	

}
