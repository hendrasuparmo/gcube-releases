package org.gcube.data.analysis.tabulardata.model.metadata;

import org.gcube.data.analysis.tabulardata.metadata.Metadata;

public interface CubeMetadata extends Metadata {
	
	public boolean isInheritable();

}
