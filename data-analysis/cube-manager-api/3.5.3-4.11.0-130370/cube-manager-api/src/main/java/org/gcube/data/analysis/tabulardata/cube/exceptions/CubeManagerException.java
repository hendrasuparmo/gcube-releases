package org.gcube.data.analysis.tabulardata.cube.exceptions;

public class CubeManagerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -18059704949428002L;

	public CubeManagerException() {
		super();
	}

	public CubeManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public CubeManagerException(String message) {
		super(message);
	}

	public CubeManagerException(Throwable cause) {
		super(cause);
	}

}
