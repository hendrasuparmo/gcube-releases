package org.gcube.rest.index.common;

public class Constants {
	public static final String SERVICE_CLASS = "Index";
	public static final String SERVICE_NAME = "FullTextIndexNode";
	public static final String ENDPOINT_KEY = "resteasy-servlet";
	
	public static final String RESOURCE_CLASS = "IndexResources";
	public static final String RESOURCE_NAME_PREF = "IndexResource";
	public static final String DEFAULT_RESOURCES_FOLDERNAME = "/tmp/resources";
	
	public static final String PROPERTIES_FILE = "deploy.properties";
}
