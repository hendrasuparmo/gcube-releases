package org.gcube.textextractor.entities;

import java.io.Serializable;

public class Pair implements Serializable{

	private static final long serialVersionUID = 1L;
	public String type;
	public String value;
	
}