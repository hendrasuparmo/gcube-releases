package org.gcube.textextractor.entities;

import java.io.Serializable;

public class Binding implements Serializable{

	private static final long serialVersionUID = 1L;
	public Pair uri;
	public Pair label_str;
	public Pair lang;
}