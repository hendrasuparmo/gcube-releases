package org.gcube.elasticsearch.exceptions;

public class ElasticSearchHelperException extends Exception {

	public ElasticSearchHelperException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
