//package helpers;
//
//import org.gcube.indexmanagement.common.FullTextIndexType;
//
//
///**
// * Representation of an index'es IndexType and IndexFormat
// */
//public class FullTextIndexTypeFTTest extends FullTextIndexType {
//	private static final long serialVersionUID = 1L;
//
//	public FullTextIndexTypeFTTest(String indexTypeName, String scope) {
//    	super(indexTypeName,scope);
//    }
//    
//    public String retrieveIndexTypeGenericResource(String scope) throws Exception {
//    	return Utils.readFile("src/test/resources/ft_test_indextype.xml");
//    };
//}
