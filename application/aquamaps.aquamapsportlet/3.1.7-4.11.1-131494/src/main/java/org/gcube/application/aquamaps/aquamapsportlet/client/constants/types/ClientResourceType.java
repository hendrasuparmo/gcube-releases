package org.gcube.application.aquamaps.aquamapsportlet.client.constants.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientResourceType implements IsSerializable{
	HSPEC,HSPEN,HCAF
}
