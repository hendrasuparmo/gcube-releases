package org.gcube.application.aquamaps.aquamapsportlet.client.constants.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientFilterType implements IsSerializable{
	begins,contains,is,ends,greater_then,smaller_then
}
