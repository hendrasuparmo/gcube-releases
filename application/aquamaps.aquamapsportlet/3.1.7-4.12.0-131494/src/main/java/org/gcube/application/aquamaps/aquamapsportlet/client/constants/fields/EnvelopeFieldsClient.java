package org.gcube.application.aquamaps.aquamapsportlet.client.constants.fields;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum EnvelopeFieldsClient implements IsSerializable{
	Depth,
	Temperature,
	Salinity,
	IceConcentration,
	LandDistance,
	PrimaryProduction,
}
