package org.gcube.application.aquamaps.aquamapsportlet.client.constants.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum AreaType implements IsSerializable{
EEZ,
FAO,
LME
}
