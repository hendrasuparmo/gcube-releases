package org.gcube.application.aquamaps.aquamapsportlet.client.constants.fields;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum LocalObjectFields implements IsSerializable {

	
	title,
	type,
	bbox,
	species,
	threshold ,
	gis ,	
	
}
