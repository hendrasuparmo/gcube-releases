package org.gcube.application.aquamaps.aquamapsportlet.client.constants.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientObjectType implements IsSerializable{
	Biodiversity,SpeciesDistribution
}
