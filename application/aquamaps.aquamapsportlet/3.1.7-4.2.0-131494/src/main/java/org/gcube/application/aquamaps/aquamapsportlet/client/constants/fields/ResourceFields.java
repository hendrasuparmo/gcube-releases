package org.gcube.application.aquamaps.aquamapsportlet.client.constants.fields;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ResourceFields implements IsSerializable{
	searchid,
	title,
	tablename,
	description,
	author,
	disclaimer,
	provenience,
	generationtime,
	sourcehcaf,
	sourcehspen,
	sourcehspec,
	parameters,
	status,
	sourcehcaftable,
	sourcehspentable,
	sourcehspectable,
	type,
	algorithm,
	defaultsource
}
