package org.gcube.application.aquamaps.aquamapsportlet.client.constants;

public class Tags {
	
	
	
	public static final String sort="sort";
	public static final String dir="dir";
	public static final String ASC="ASC";
	
	public static final String JSONUTF8="application/json; charset=utf-8";
	public static final String TOTAL_COUNT="totalcount";
	public static final String DATA="data";
	public static final String EMPTY_JSON="{\""+DATA+"\":[],\""+TOTAL_COUNT+"\":0}";
	
	//*********** Config
	
	
	
	public static final String SPECIES_FILTER="SPECIES_FILTER";
	
	
	public static final String IMPORT_PROGRESS="IMPORT_PROGRESS";
	
	
	
	public static final String filterSummary="filterSummary";
	
	public static final String PHYLOGENY_LEVEL="phylogenyLevel";
	
	public static final String AREA_MAP_TYPE="areaMapType";
	public static final String TREE_TYPE="treeType";
	public static final String START="start";
	public static final String LIMIT="limit";
	public static final String AQUAMAPS_TITLE="aquamaps_title";
	public static final String AQUAMAPS_ID="aquamaps_id";
	public static final String AQUAMAPS_TYPE="type";
	
	
	public static final String lastFetchedBasket="lastBasket";
	
	
	public static final String SELECTION_attribute_NAME="selection";
	public static final String SELECTED_Species="selectedSpecies";
	public static final String SELECTED_AREAS="selectedAreas"; 
	public static final String SETTINGS="Settings";
	
	public static final String WEIGHTS="weights";
	public static final String HSPEC="hspec";
	public static final String Threshold="threshold";
	
	public static final String submittedJobId="submittedJobId";	
	public static final String submittedObjectType="submittedObjectType";
	public static final String submittedObjectDate="submittedObjectDate";
	public static final String submittedJobStatus="submittedJobStatus";
	public static final String submittedObjectStatus="submittedStatus";
	public static final String submittedShowAquaMaps="submittedObjectFlag";
	public static final String submittedFilterByTitle="submittedFilterByTitle";
	
	public static final String showFAO="showFao";
	public static final String showLME="showLME";
	public static final String showEEZ="showEEZ";
	
	public static final String gatherImagesOnPublicPort="gatherImagesOnPublicPort";
 	public static final String publicHost="publicHost";
 	
 	//*************
 	
 	
// 	public static final String defaultHCAFID="defaultHCAFID";
// 	public static final String defaultHSPENID="defaultHSPENID";
// 	public static final String defaultHSPECID="defaultHSPECID";
 	public static final String DEFAULT_ALGORITHM="DEFAULT_ALGORITHM";
}