package org.gcube.application.aquamaps.aquamapsportlet.servlet.utils.log;

public class Common {

	static final String OBJECT_TITLE=" TITLE ";
	static final String OBJECT_TYPE=" TYPE ";
	static final String OBJECT_ID=" ID ";
	static final String HSPEC_ID=" HSPEC ";
	static final String SPECIES_COUNT=" SPECIES_COUNT ";
	static final String GIS=" GIS ";
	
	static final String ATTRIBUTE_SEPARATOR=" | ";
	static final String ATTRIBUTE_VALUE=" = ";
	
	
}
