package org.gcube.application.aquamaps.aquamapsportlet.client.constants.fields;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum AreaFields implements IsSerializable{
type,
code,
name
}
