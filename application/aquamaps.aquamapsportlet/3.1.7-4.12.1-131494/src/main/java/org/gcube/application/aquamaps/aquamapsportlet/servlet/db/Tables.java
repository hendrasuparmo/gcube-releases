package org.gcube.application.aquamaps.aquamapsportlet.servlet.db;

public enum Tables {

	Basket,
	Species,
	Objects,
	Objects_Basket,
	fetchedBasket,
	AreaSelections,
	Area,
	kingdom,
	phylum,
	order_table,
	family_table,
	class_table
}
