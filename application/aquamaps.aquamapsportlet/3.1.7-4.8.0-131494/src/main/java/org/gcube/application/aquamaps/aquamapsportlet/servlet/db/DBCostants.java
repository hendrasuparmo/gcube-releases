package org.gcube.application.aquamaps.aquamapsportlet.servlet.db;

public class DBCostants {

	public static final String userID="userid";
	public static final String environment="environment";
	public static final String envelopes="envelopes";
	public static final String predictions="predictions";
	public static final String objectID="objectid";
	public static final String perturbations="perturbations";
	public static final String weights="weights";
	public static final String customized="customized";
}
