package gr.uoa.di.madgik.visualisations.client;

import com.google.gwt.user.client.ui.AbsolutePanel;

public class VisWidget extends AbsolutePanel{
	
	public VisWidget(){
		super();
	}
	
	public VisWidget(String width, String height){
		super();
		super.setSize(width, height);
	}
	
}
