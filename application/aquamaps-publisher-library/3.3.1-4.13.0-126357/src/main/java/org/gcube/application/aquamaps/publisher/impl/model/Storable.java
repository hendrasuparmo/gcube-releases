package org.gcube.application.aquamaps.publisher.impl.model;

public interface Storable {

	public String getId();
	public void setId(String id);
}
