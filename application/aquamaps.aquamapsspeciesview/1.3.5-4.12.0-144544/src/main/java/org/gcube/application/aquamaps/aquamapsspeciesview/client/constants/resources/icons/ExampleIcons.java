package org.gcube.application.aquamaps.aquamapsspeciesview.client.constants.resources.icons;

import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.ImageBundle;

@SuppressWarnings("deprecation")
public interface ExampleIcons extends ImageBundle{
	
	@Resource("arrow_refresh.png")
	 AbstractImagePrototype refresh();
	
	@Resource("images.png")
	 AbstractImagePrototype images();
	
	@Resource("text_columns.png")
	 AbstractImagePrototype text_columns();
	
	@Resource("text_list_bullets.png")
	 AbstractImagePrototype text_list_bullets();
	
	@Resource("photos.png")
	 AbstractImagePrototype photos();
	
	@Resource("cross.png")
	 AbstractImagePrototype error();
	
	@Resource("disk.png")
	 AbstractImagePrototype disk();
	
	@Resource("world.png")
	 AbstractImagePrototype world();
}
