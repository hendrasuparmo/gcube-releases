package org.gcube.application.aquamaps.aquamapsspeciesview.client;

public interface Reloadable {

	public void reload();
	
}
