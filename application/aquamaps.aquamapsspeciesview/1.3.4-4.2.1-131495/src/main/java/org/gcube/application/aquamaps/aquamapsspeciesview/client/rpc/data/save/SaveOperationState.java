package org.gcube.application.aquamaps.aquamapsspeciesview.client.rpc.data.save;

public enum SaveOperationState {

	RETRIEVING_FILES,
	SAVING_FILES,
	COMPLETED,
	ERROR
}
