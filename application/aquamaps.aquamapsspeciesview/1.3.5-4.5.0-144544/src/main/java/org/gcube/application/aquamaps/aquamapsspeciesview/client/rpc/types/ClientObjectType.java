package org.gcube.application.aquamaps.aquamapsspeciesview.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientObjectType implements IsSerializable{
	Biodiversity,SpeciesDistribution;
	private ClientObjectType() {
		// TODO Auto-generated constructor stub
	}
}
