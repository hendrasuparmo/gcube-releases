package org.gcube.application.aquamaps.aquamapsspeciesview.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientFieldType implements IsSerializable{
	INTEGER,BOOLEAN,STRING,DOUBLE;
	private ClientFieldType() {
		// TODO Auto-generated constructor stub
	}
}
