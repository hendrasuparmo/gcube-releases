package org.gcube.application.aquamaps.aquamapsspeciesview.servlet.db;

public enum Tables {

	MAPS,
	kingdom,
	phylum,
	order_table,
	family_table,
	class_table
}
