package org.gcube.application.aquamaps.aquamapsspeciesview.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientResourceType implements IsSerializable{
	HSPEC,HSPEN,HCAF,OCCURRENCECELLS;
	private ClientResourceType() {
		// TODO Auto-generated constructor stub
	}
}
