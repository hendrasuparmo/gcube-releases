package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum MetaSourceFields {


		searchid,
		title,
		tablename,
		description,
		author,
		disclaimer,
		provenience,
		generationtime,
		sourcehcafids,
		sourcehspenids,
		sourcehspecids,
		sourceoccurrencecellsids,
		parameters,
		status,
		sourcehcaftables,
		sourcehspentables,
		sourcehspectables,
		sourceoccurrencecellstables,
		type,
		algorithm,
		defaultsource,
		rowcount
	
}
