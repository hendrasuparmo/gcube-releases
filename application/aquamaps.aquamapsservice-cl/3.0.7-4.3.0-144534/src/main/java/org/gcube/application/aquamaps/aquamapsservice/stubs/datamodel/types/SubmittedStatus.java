package org.gcube.application.aquamaps.aquamapsservice.stubs.datamodel.types;

public enum SubmittedStatus {
	Pending,Simulating,Generating,Publishing,Completed,Error
}
