package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum EnvelopeFields {
	
//		Salinity,
//		PrimProd,
//		Depth,
//		Temp,
//		IceCon,
//		LandDist
	
		Depth,
		Temperature,
		Salinity,
		IceConcentration,
		LandDistance,
		PrimaryProduction,
		
		
}
