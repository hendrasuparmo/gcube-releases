package org.gcube.application.aquamaps.aquamapsservice.stubs.datamodel.types;

import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("logic")
public enum LogicType {
	HSPEC,HSPEN,HCAF
}
