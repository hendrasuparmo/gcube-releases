package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum FilesFields {

	published,
	nameHuman,
	path,
	type,
	owner
	
}
