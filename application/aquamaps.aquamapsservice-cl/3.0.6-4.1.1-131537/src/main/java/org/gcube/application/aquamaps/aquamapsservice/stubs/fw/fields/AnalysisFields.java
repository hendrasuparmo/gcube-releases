package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum AnalysisFields {

	id,
	title,
	author,
	description,
	status,
	
	submissiontime,
	starttime,
	endtime,
	currentphasepercent,
	
	reportid,
	
	type,
	
	archivelocation,
	
	sources,
	performedanalysis
}
