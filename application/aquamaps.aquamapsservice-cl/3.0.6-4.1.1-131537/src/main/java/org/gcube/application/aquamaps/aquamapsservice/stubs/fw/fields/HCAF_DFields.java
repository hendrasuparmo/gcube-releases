package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum HCAF_DFields {

	depthmin,
	depthmax,
	depthmean,
	depthsd  ,
	sstanmean ,
	sstansd   ,
	sstmnmax  ,
	sstmnmin  ,
	sstmnrange,
	sbtanmean ,
	salinitymean,
	salinitysd  ,
	salinitymax ,
	salinitymin ,
	salinitybmean,
	primprodmean ,
	iceconann    ,
	iceconspr    ,
	iceconsum    ,
	iceconfal    ,
	iceconwin    
}
