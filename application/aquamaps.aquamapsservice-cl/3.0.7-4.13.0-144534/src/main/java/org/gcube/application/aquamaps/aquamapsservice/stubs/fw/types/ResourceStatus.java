package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.types;

public enum ResourceStatus {
	Completed,Error,Importing,Generating
}
