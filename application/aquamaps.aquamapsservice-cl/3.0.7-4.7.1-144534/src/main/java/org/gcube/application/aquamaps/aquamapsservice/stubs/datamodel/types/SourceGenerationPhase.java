package org.gcube.application.aquamaps.aquamapsservice.stubs.datamodel.types;

public enum SourceGenerationPhase {
	uploading,pending,datageneration,mapgeneration,completed,error
}
