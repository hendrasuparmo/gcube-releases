package org.gcube.application.aquamaps.aquamapsservice.stubs.fw.fields;

public enum OccurrenceCellsFields {

	speccode    ,
	goodcell    ,
	infaoarea   ,
	inboundbox  ,
	centerlat   ,
	centerlong  ,
	faoaream    ,
	recordid    ,
	
}
