package org.gcube.application.datamanagementfacilityportlet.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientResourceType implements IsSerializable{

	HSPEC,HSPEN,HCAF,OCCURRENCECELLS
	
	
}
