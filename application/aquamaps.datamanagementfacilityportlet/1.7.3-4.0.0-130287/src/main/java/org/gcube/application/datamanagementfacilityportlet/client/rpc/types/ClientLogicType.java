package org.gcube.application.datamanagementfacilityportlet.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ClientLogicType implements IsSerializable{
	HSPEC,HSPEN,HCAF
}
