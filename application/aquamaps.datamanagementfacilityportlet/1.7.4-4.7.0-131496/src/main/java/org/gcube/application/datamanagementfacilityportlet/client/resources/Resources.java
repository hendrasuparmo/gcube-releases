package org.gcube.application.datamanagementfacilityportlet.client.resources;
import static com.google.gwt.core.client.GWT.create;

import org.gcube.application.datamanagementfacilityportlet.client.resources.icons.ExampleIcons;

public class Resources {

  public static final ExampleIcons ICONS = create(ExampleIcons.class);

}
