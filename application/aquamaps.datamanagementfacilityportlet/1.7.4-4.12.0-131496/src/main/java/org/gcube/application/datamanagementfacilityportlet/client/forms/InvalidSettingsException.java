package org.gcube.application.datamanagementfacilityportlet.client.forms;

public class InvalidSettingsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8259711295502304569L;

	
	public InvalidSettingsException(String msg) {
		super(msg);
	}
}
