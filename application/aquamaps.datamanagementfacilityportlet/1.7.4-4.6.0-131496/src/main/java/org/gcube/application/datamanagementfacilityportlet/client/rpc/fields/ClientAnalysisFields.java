package org.gcube.application.datamanagementfacilityportlet.client.rpc.fields;

public enum ClientAnalysisFields {
	id,
	title,
	author,
	description,
	status,
	
	submissiontime,
	starttime,
	endtime,
	currentphasepercent,
	
	reportid,
	
	type,
	
	archivelocation,
	
	sources,
}
