package org.gcube.application.datamanagementfacilityportlet.client.rpc.types;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum GroupGenerationRequestPhase implements IsSerializable{

	uploading,pending,datageneration,mapgeneration,completed,error
	
}
