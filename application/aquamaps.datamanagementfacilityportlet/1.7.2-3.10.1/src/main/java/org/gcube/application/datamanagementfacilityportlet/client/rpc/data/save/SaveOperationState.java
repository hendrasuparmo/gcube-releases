package org.gcube.application.datamanagementfacilityportlet.client.rpc.data.save;

public enum SaveOperationState {

	RETRIEVING_FILES,
	SAVING_FILES,
	COMPLETED,
	ERROR
}
