package org.gcube.application.datamanagementfacilityportlet.client.rpc.data.save;

public enum SaveOperationType {

	RESOURCE,ANALYSIS,CUSTOM_QUERY
	
}
