package org.gcube.resource.management.quota.manager.persistence.entities;

/**
 * EntityConstants
 *  
 * @author Alessandro Pieve (alessandro.pieve@isti.cnr.it)
 *
 */
public class EntityConstants {

	public static final String STORAGE_QUOTA="STORAGE"; 
	public static final String SERVICE_QUOTA="SERVICE"; 
	
}
