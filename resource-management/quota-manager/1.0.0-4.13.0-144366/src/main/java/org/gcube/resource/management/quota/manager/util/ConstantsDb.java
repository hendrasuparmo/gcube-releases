package org.gcube.resource.management.quota.manager.util;

public class ConstantsDb {
	//CONNECTION DB
	//public static final String DB_DRIVER_TEST = "org.h2.Driver";
	//
	//public static final String DB_CONNECTION_TEST = "jdbc:h2:~/DbQuota;";
	//public static final String DB_USER = "sa";
	//public static final String DB_PASSWORD = "";
	
	public static final String DB_DRIVER = "org.postgresql.Driver";
	public static final String SEPARATOR="_";
	public static final String DATABASE_PREFIX="quotausage";
	public static final String DATABASE_TYPE_STORAGE="storage";
	public static final String DATABASE_TYPE_SERVICE="service";
	public static final String DATABASE_PERIOD_TOTAL="total";
}
