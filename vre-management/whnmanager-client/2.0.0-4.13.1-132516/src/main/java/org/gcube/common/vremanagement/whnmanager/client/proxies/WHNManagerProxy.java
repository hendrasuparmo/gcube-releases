package org.gcube.common.vremanagement.whnmanager.client.proxies;


import org.gcube.resourcemanagement.whnmanager.api.WhnManager;

/**
 * 
 * @author Roberto Cirillo  (CNR)
 *
 */
public interface WHNManagerProxy extends WhnManager{
	
	
}
