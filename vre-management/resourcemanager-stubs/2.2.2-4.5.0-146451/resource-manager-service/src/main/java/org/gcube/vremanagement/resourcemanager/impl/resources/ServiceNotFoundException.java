package org.gcube.vremanagement.resourcemanager.impl.resources;

/** 
 * Service not found exception, it occurs when a RI was not deployed by this RM instance
 * 
 */

public class ServiceNotFoundException extends Exception {

	private static final long serialVersionUID = -2819887269639710947L;

}
