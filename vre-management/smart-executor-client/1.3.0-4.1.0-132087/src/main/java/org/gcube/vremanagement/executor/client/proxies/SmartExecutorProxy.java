/**
 * 
 */
package org.gcube.vremanagement.executor.client.proxies;

import org.gcube.vremanagement.executor.api.SmartExecutor;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 * 
 */
public interface SmartExecutorProxy extends SmartExecutor {

}
