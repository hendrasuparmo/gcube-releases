/**
 * 
 */
package org.gcube.vremanagement.executor.client.proxies;

import org.gcube.vremanagement.executor.api.SmartExecutor;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 */
public interface SmartExecutorProxy extends SmartExecutor {

}
