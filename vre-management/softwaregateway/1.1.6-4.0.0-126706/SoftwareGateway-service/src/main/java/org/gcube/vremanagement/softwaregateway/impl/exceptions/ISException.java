package org.gcube.vremanagement.softwaregateway.impl.exceptions;

public class ISException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public ISException(String msg){
		super(msg);
	}
}
