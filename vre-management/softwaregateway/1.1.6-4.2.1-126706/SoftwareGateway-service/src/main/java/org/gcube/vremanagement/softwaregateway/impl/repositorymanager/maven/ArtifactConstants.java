package org.gcube.vremanagement.softwaregateway.impl.repositorymanager.maven;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public abstract class ArtifactConstants {

	/**
	 * Default Packaging type;
	 */
	public static final String DEFAULT_PACKAGING = "tar.gz";
	
	
	/**
	 * Dependency Scope
	 */
	public static final String DEFAULT_DEPENDENCY_SCOPE = "runtime";
	
	
}
