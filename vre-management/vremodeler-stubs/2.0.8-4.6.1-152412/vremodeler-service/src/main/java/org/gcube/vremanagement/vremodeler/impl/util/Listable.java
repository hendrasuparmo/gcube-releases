package org.gcube.vremanagement.vremodeler.impl.util;

import java.util.List;

public interface Listable {
	
	public List<String> getAsStringList();
}
