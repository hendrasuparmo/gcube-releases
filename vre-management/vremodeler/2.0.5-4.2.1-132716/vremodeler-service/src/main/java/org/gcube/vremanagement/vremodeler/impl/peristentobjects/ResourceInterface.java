package org.gcube.vremanagement.vremodeler.impl.peristentobjects;

public interface ResourceInterface {

	public abstract String getId();

	public abstract String getType();

	public abstract String getDescription();

	public abstract String getName();
	
	public abstract String getResourceType();

}