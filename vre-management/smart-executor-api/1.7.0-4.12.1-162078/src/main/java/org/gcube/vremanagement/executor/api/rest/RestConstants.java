package org.gcube.vremanagement.executor.api.rest;

public class RestConstants {
	
	

	public static final String REST_PATH_PART = "rest";
	
	public static final String PLUGINS_PATH_PART = "plugins";
	
	public static final String SCHEDULED_PATH_PART = "scheduled";
	
	public static final String ITERATION_NUMBER_PARAM = "iterationNumber";
	
	public static final String GLOBALLY_PARAM = "globally";
	
	
}
