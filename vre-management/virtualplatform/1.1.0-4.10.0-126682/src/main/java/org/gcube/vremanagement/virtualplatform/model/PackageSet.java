package org.gcube.vremanagement.virtualplatform.model;

import java.util.HashSet;
/**
 * A set of {@link Package}
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 * @param <E>
 */
public class PackageSet<E> extends HashSet<E> {

	private static final long serialVersionUID = 5398548937569575128L;

}
