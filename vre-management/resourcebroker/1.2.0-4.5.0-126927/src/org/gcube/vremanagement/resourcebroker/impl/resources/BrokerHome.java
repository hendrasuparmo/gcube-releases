/****************************************************************************
 *  This software is part of the gCube Project.
 *  Site: http://www.gcube-system.org/
 ****************************************************************************
 * The gCube/gCore software is licensed as Free Open Source software
 * conveying to the EUPL (http://ec.europa.eu/idabc/eupl).
 * The software and documentation is provided by its authors/distributors
 * "as is" and no expressed or
 * implied warranty is given for its use, quality or fitness for a
 * particular case.
 ****************************************************************************
 * Filename: BrokerHome.java
 ****************************************************************************
 * @author <a href="mailto:daniele.strollo@isti.cnr.it">Daniele Strollo</a>
 ***************************************************************************/

package org.gcube.vremanagement.resourcebroker.impl.resources;

import org.gcube.common.core.contexts.GCUBEStatefulPortTypeContext;
import org.gcube.common.core.state.GCUBEWSHome;
import org.gcube.vremanagement.resourcebroker.impl.contexts.StatefulBrokerContext;

/**
 * @author Daniele Strollo (ISTI-CNR)
 *
 */
public class BrokerHome
extends GCUBEWSHome {

	@Override
	public final GCUBEStatefulPortTypeContext getPortTypeContext() {
		return StatefulBrokerContext.getContext();
	}

}
