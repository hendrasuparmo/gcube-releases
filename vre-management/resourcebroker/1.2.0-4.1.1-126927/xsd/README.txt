This folder contains a mirror of the xsd schema files defined in the
gCore distribution packet.

They have been copied to avoid conflicts in case of changes.

The main purpose is to validate instances of service profiles.

Taken from:
$GLOBUS_LOCATION/share/schema/gcube/common/core/profiles
