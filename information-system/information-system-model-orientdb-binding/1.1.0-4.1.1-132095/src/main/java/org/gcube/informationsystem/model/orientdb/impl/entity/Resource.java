/**
 * 
 */
package org.gcube.informationsystem.model.orientdb.impl.entity;

import com.tinkerpop.frames.VertexFrame;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 */
public interface Resource extends
		org.gcube.informationsystem.model.entity.Resource, Entity, VertexFrame {

}
