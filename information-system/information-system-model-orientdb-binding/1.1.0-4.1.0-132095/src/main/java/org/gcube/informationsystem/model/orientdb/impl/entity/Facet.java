/**
 * 
 */
package org.gcube.informationsystem.model.orientdb.impl.entity;

import com.tinkerpop.frames.VertexFrame;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *         https://redmine.d4science.org/projects/bluebridge/wiki/Facets
 */
public interface Facet extends org.gcube.informationsystem.model.entity.Facet,
		Entity, VertexFrame {

}
