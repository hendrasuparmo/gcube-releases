package org.gcube.informationsystem.resourceregistry.api.exceptions.context;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 * 
 */
public class ContextMoveException extends ContextException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8119058749936021156L;

	public ContextMoveException(String message) {
		super(message);
	}


}
