package org.gcube.informationsystem.resourceregistry.api.rest;

public class SharingPath {
	
	public static final String SHARING_PATH_PART = "sharing";
	
	public static final String CONTEXTS_PATH_PART = ContextPath.CONTEXTS_PATH_PART;
	
}
