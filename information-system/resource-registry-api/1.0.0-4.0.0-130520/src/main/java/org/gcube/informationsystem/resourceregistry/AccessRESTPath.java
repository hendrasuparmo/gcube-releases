/**
 * 
 */
package org.gcube.informationsystem.resourceregistry;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class AccessRESTPath {
	
	public static final String ACCESS_PATH_PART = "access";
	
	public static final String QUERY_PARAM = "query";
	public static final String FETCH_PLAN_PARAM = "fetchPlan";
	
	public static final String FACET_PATH_PART = "facet";
	public static final String FACET_ID_PATH_PARAM = "facetId";
	public static final String FACET_TYPE_PATH_PARAM = "facetType";
	
	public static final String RESOURCE_PATH_PART = "resource";
	public static final String RESOURCE_ID_PATH_PARAM = "resourceId";
	public static final String RESOURCE_TYPE_PATH_PARAM = "resourceType";
	
	public static final String SCHEMA_PATH_PART = "schema";
	
}
