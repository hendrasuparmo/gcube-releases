/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SchemaPath {
	
	public static final String SCHEMA_PATH_PART = "schema";
	
	public static final String FACET_PATH_PART = ERPath.FACET_PATH_PART;
	public static final String RESOURCE_PATH_PART = ERPath.RESOURCE_PATH_PART;
	
	public static final String EMBEDDED_PATH_PART = ERPath.EMBEDDED_PATH_PART;
	
	public static final String CONSISTS_OF_PATH_PART = ERPath.CONSISTS_OF_PATH_PART;
	public static final String IS_RELATED_TO_PATH_PART = ERPath.IS_RELATED_TO_PATH_PART;
	
}
