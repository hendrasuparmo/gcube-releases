/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ERPath {
	
	public static final String ER_PATH_PART = "er";
	
	protected static final String EMBEDDED_PATH_PART = "embedded";
	
	protected static final String ENTITY_PATH_PART = "entity";
	
	public static final String FACET_PATH_PART = "facet";
	public static final String RESOURCE_PATH_PART = "resource";
	
	protected static final String RELATION_PATH_PART = "relation";
	
	public static final String CONSISTS_OF_PATH_PART = "consistsOf";
	public static final String IS_RELATED_TO_PATH_PART = "isRelatedTo";
	
	public static final String SOURCE_PATH_PART = "source";
	public static final String TARGET_PATH_PART = "target";
	
	public static final String ADD_PATH_PART = "add";
	public static final String REMOVE_PATH_PART = "remove";
	
}
