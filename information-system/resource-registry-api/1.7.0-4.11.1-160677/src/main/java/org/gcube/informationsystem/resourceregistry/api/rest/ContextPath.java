/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextPath {
	
	public static final String CONTEXT_PATH_PART = "context";
	
	public static final String ALL_PATH_PART = "all";
}
