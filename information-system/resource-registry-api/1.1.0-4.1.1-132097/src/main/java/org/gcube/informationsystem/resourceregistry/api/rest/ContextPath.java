/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 */
public class ContextPath {
	
	public static final String CONTEXT_PATH_PART = "context";
	
	public static final String RENAME_PATH_PART = "rename";
	public static final String MOVE_PATH_PART = "move";
	
	
	public static final String PARENT_CONTEXT_ID_PARAM = "parentContextId";
	public static final String CONTEXT_ID_PARAM = "contextId";
	public static final String NAME_PARAM = "name";
	
	
}
