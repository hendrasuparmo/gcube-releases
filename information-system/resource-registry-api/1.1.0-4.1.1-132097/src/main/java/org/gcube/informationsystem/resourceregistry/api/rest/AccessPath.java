/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 */
public class AccessPath {
	
	public static final String ACCESS_PATH_PART = "access";
	
	public static final String QUERY_PARAM = "query";
	public static final String FETCH_PLAN_PARAM = "fetchPlan";
	
	public static final String FACET_PATH_PART = "facet";
	public static final String RESOURCE_PATH_PART = "resource";
	
	public static final String SCHEMA_PATH_PART = "schema";
	public static final String INSTANCE_PATH_PART = "instance";
	
}
