/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.exceptions.context;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 */
public class ContextCreationException extends ContextException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7235498402448768270L;

	public ContextCreationException(String message) {
		super(message);
	}
}
