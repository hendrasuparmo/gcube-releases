/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 */
public class SchemaPath {
	
	public static final String SCHEMA_PATH_PART = "schema";
	
	public static final String FACET_PATH_PART = "facet";
	public static final String RESOURCE_PATH_PART = "resource";
	
	public static final String EMBEDDED_PATH_PART = "embedded";
	
	public static final String SCHEMA_PARAM = "schema";
	
	public static final String CONSIST_OF_PATH_PART = "consistOf";
	public static final String RELATED_TO_PATH_PART = "relatedTo";
}
