package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SchemaPath {
	
	public static final String SCHEMA_PATH_PART = "schema";
	public static final String POLYMORPHIC_PARAM = AccessPath.POLYMORPHIC_PARAM;
	
}