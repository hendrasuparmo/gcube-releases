package org.gcube.informationsystem.resourceregistry.api.rest;

public class InstancePath {
	
	public static final String INSTANCES_PATH_PART = "instances";
	
	public static final String POLYMORPHIC_PARAM = AccessPath.POLYMORPHIC_PARAM;
	
}
