package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypePath {
	
	public static final String TYPES_PATH_PART = "types";
	
	public static final String POLYMORPHIC_PARAM = AccessPath.POLYMORPHIC_PARAM;
	
}