/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextPath {
	
	public static final String CONTEXTS_PATH_PART = "contexts";

}
