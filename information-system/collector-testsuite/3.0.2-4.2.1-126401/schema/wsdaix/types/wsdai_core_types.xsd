<?xml version="1.0" encoding="UTF-8" ?>
<xsd:schema targetNamespace="http://www.ggf.org/namespaces/2005/12/WS-DAI" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:wsdai="http://www.ggf.org/namespaces/2005/12/WS-DAI" xmlns:wsa="http://www.w3.org/2005/08/addressing">
    <xsd:import namespace="http://www.w3.org/2005/08/addressing" schemaLocation="./ws-addressing-0805.xsd" />
    <!-- general types -->
    <!-- A type that holds the abstract name (a URI) of a data resource -->
    <xsd:simpleType name="DataResourceAbstractNameType">
        <xsd:restriction base="xsd:anyURI"></xsd:restriction>
    </xsd:simpleType>
    <xsd:element name="DataResourceAbstractName" type="wsdai:DataResourceAbstractNameType" />
    <!-- The address of a data resource is a WS-Addressing End Point Reference -->
    <!-- DAIS will use the EPR <ReferenceParameters> element to hold the -->
    <!-- DataResourceAbstractName -->
    <xsd:complexType name="DataResourceAddressType">
        <xsd:complexContent>
            <xsd:extension base="wsa:EndpointReferenceType" />
        </xsd:complexContent>
    </xsd:complexType>
    <xsd:element name="DataResourceAddress" type="wsdai:DataResourceAddressType" />
    <xsd:complexType name="DataResourceAddressListType">
        <xsd:sequence>
            <xsd:element ref="wsdai:DataResourceAddress" minOccurs="1" maxOccurs="unbounded" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="DataResourceAddressList" type="wsdai:DataResourceAddressListType" />
    <!-- the wrapper for input/output datasets -->
    <xsd:complexType name="DatasetDataType" mixed="true">
        <xsd:sequence>
            <xsd:any namespace="##any" minOccurs="0" maxOccurs="unbounded" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="DatasetData" type="wsdai:DatasetDataType" />
    <!-- the base type wrapper for input/output datasets -->
    <!-- the DatasetFormatURI indicates the format of    -->
    <!-- the ##any of DatasetData                        -->
    <!-- It is expected that this type will be extended  -->
    <!-- by the realisations                             -->
    <xsd:complexType name="DatasetType">
        <xsd:sequence>
            <xsd:element ref="wsdai:DatasetFormatURI" />
            <xsd:element ref="wsdai:DatasetData" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="Dataset" type="wsdai:DatasetType" />
    <!-- the base type for query expressions -->
    <xsd:complexType name="ExpressionType">
        <xsd:attribute name="Language" type="xsd:anyURI" />
    </xsd:complexType>
    <xsd:element name="Expression" type="wsdai:ExpressionType" />
    <!--  the base type for requests that include an abstract resource name parameter -->
    <xsd:complexType name="BaseRequestType">
        <xsd:sequence>
            <xsd:element ref="wsdai:DataResourceAbstractName" />
        </xsd:sequence>
    </xsd:complexType>
    <!--  the base types for requests that also specify a return type -->
    <xsd:complexType name="RequestType">
        <xsd:complexContent>
            <xsd:extension base="wsdai:BaseRequestType">
                <xsd:sequence>
                    <xsd:element ref="wsdai:DatasetFormatURI" minOccurs="0" maxOccurs="1" />
                </xsd:sequence>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>
    <xsd:element name="Request" type="wsdai:RequestType" />
    <!--  the base type for factory requests that include an abstract resource name parameter -->
    <!--  a port type QName and a configuration document -->
    <xsd:complexType name="FactoryRequestType">
        <xsd:complexContent>
            <xsd:extension base="wsdai:BaseRequestType">
                <xsd:sequence>
                    <xsd:element ref="wsdai:PortTypeQName" minOccurs="0" maxOccurs="1" />
                    <xsd:element ref="wsdai:ConfigurationDocument" minOccurs="0" maxOccurs="1" />
                    <xsd:element name="PreferredTargetService" type="wsa:EndpointReferenceType" minOccurs="0" maxOccurs="1" />
                </xsd:sequence>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>
    <xsd:element name="FactoryRequest" type="wsdai:FactoryRequestType" />
    <!-- Static Properties -->
    <!-- An open description of the data resource -->
    <xsd:element name="DataResourceDescription">
        <xsd:complexType mixed="true">
            <xsd:sequence>
                <xsd:any minOccurs="0" maxOccurs="unbounded" processContents="lax" />
            </xsd:sequence>
        </xsd:complexType>
    </xsd:element>
    <!-- The address of the data resource that created this data resource -->
    <xsd:element name="ParentDataResource" type="wsdai:DataResourceAddressType" />
    <!-- The mapping between message type and resource type. -->
    <!-- For direct access, where results are returned directly -->
    <!-- the DatasetType refers to the supported dataset types. -->
    <xsd:element name="MessageQName" type="xsd:QName" />
    <xsd:element name="DatasetFormatURI" type="xsd:anyURI" />
    <xsd:complexType name="DatasetMapType">
        <xsd:sequence>
            <xsd:element ref="wsdai:MessageQName" />
            <xsd:element ref="wsdai:DatasetFormatURI" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="DatasetMap" type="wsdai:DatasetMapType" />
    <!-- For indirect access where a new resource results the  -->
    <!-- ConfigurationType refers to the QName of the configuration  -->
    <!-- document that identifies the required port type and -->
    <!-- provides inital property values -->
    <xsd:element name="PortTypeQName" type="xsd:QName" />
    <xsd:element name="ConfigurationDocumentQName" type="xsd:QName" />
    <xsd:complexType name="ConfigurationMapType">
        <xsd:sequence>
            <xsd:element ref="wsdai:MessageQName" />
            <xsd:element ref="wsdai:PortTypeQName" />
            <xsd:element ref="wsdai:ConfigurationDocumentQName" />
            <xsd:element name="DefaultConfigurationDocument">
                <xsd:complexType mixed="true">
                    <xsd:sequence>
                        <xsd:element ref="wsdai:ConfigurationDocument" />
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ConfigurationMap" type="wsdai:ConfigurationMapType" />
    <!-- The mapping between message type and language type. -->
    <!-- The language here is the language used to form the  -->
    <!-- request expression, e.g. SQL99 -->
    <xsd:element name="LanguageURI" type="xsd:anyURI" />
    <xsd:complexType name="LanguageMapType">
        <xsd:sequence>
            <xsd:element ref="wsdai:MessageQName" />
            <xsd:element ref="wsdai:LanguageURI" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="LanguageMap" type="wsdai:LanguageMapType" />
    <!-- Is the data resource managed by the data service -->
    <!-- or by an external data management system -->
    <xsd:element name="DataResourceManagement">
        <xsd:simpleType>
            <xsd:restriction base="xsd:token">
                <xsd:enumeration value="ExternallyManaged" />
                <xsd:enumeration value="ServiceManaged" />
            </xsd:restriction>
        </xsd:simpleType>
    </xsd:element>
    <!-- Configurable properties -->
    <xsd:element name="Readable" type="xsd:boolean" default="true" />
    <xsd:element name="Writeable" type="xsd:boolean" />
    <xsd:element name="ConcurrentAccess" type="xsd:boolean" />
    <xsd:element name="TransactionInitiation">
        <xsd:simpleType>
            <xsd:restriction base="xsd:token">
                <xsd:enumeration value="NotSupported" />
                <xsd:enumeration value="Automatic" />
                <xsd:enumeration value="Manual" />
            </xsd:restriction>
        </xsd:simpleType>
    </xsd:element>
    <xsd:element name="TransactionIsolation">
        <xsd:simpleType>
            <xsd:restriction base="xsd:token">
                <xsd:enumeration value="NotSupported" />
                <xsd:enumeration value="ReadUncommitted" />
                <xsd:enumeration value="ReadCommitted" />
                <xsd:enumeration value="RepeatableRead" />
                <xsd:enumeration value="Serialisable" />
            </xsd:restriction>
        </xsd:simpleType>
    </xsd:element>
    <xsd:element name="ChildSensitiveToParent">
        <xsd:simpleType>
            <xsd:restriction base="xsd:token">
                <xsd:enumeration value="Insensitive" />
                <xsd:enumeration value="Sensitive" />
            </xsd:restriction>
        </xsd:simpleType>
    </xsd:element>
    <xsd:element name="ParentSensitiveToChild">
        <xsd:simpleType>
            <xsd:restriction base="xsd:token">
                <xsd:enumeration value="Insensitive" />
                <xsd:enumeration value="Sensitive" />
            </xsd:restriction>
        </xsd:simpleType>
    </xsd:element>
    <!-- core property and configuration documents -->
    <xsd:complexType name="PropertyDocumentType">
        <xsd:sequence>
            <xsd:element ref="wsdai:DataResourceAbstractName" />
            <xsd:element ref="wsdai:DataResourceManagement" />
            <xsd:element ref="wsdai:ParentDataResource" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:DatasetMap" minOccurs="0" maxOccurs="unbounded" />
            <xsd:element ref="wsdai:ConfigurationMap" minOccurs="0" maxOccurs="unbounded" />
            <xsd:element ref="wsdai:LanguageMap" minOccurs="0" maxOccurs="unbounded" />
            <xsd:element ref="wsdai:DataResourceDescription" />
            <xsd:element ref="wsdai:Readable" />
            <xsd:element ref="wsdai:Writeable" />
            <xsd:element ref="wsdai:ConcurrentAccess" />
            <xsd:element ref="wsdai:TransactionInitiation" />
            <xsd:element ref="wsdai:TransactionIsolation" />
            <xsd:element ref="wsdai:ChildSensitiveToParent" />
            <xsd:element ref="wsdai:ParentSensitiveToChild" />
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="PropertyDocument" type="wsdai:PropertyDocumentType" />
    <xsd:complexType name="ConfigurationDocumentType">
        <xsd:sequence>
            <xsd:element ref="wsdai:DataResourceDescription" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:Readable" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:Writeable" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:TransactionInitiation" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:TransactionIsolation" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:ChildSensitiveToParent" minOccurs="0" maxOccurs="1" />
            <xsd:element ref="wsdai:ParentSensitiveToChild" minOccurs="0" maxOccurs="1" />
        </xsd:sequence>
    </xsd:complexType>
    <!-- The head of the substitution group of configuration documents -->
    <xsd:element name="ConfigurationDocument" type="wsdai:ConfigurationDocumentType" />
</xsd:schema>