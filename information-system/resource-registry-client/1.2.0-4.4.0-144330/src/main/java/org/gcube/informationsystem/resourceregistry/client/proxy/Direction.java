/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.client.proxy;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public enum Direction {

	in, out, both;
	
}
