/**
 * 
 */
package org.gcube.informationsystem.impl.resource;

import org.gcube.informationsystem.impl.entity.ResourceImpl;
import org.gcube.informationsystem.model.resource.Dataset;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class DatasetImpl extends ResourceImpl implements Dataset {
	
}
