/**
 * 
 */
package org.gcube.informationsystem.model.resource;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 * 
 */
public interface HostingNode extends Service {

	public static final String NAME = HostingNode.class.getSimpleName();
	public static final String DESCRIPTION = "Collect Hosting Node information through the list of its facets";
	public static final String VERSION = "1.0.0";
	
}
