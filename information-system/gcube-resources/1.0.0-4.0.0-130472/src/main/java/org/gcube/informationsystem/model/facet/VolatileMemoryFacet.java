/**
 * 
 */
package org.gcube.informationsystem.model.facet;


/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 * 
 */
public interface VolatileMemoryFacet extends MemoryFacet {
	
	public static final String NAME = VolatileMemoryFacet.class.getSimpleName();
	public static final String DESCRIPTION = "Volatile Memory information";
	public static final String VERSION = "1.0.0";
	
}
