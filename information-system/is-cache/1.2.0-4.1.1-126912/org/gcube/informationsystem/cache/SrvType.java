package org.gcube.informationsystem.cache;

/**
 * Valid service types
 * 
 * @author UoA
 *
 */
public enum SrvType {
	/** simple web service */
	SIMPLE, 
	/** factory service */
	FACTORY,
	/** statefull service - WS-resource */
	STATEFULL

}
