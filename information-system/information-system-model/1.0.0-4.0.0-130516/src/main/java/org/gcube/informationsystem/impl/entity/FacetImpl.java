/**
 * 
 */
package org.gcube.informationsystem.impl.entity;

import org.gcube.informationsystem.model.entity.Facet;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public abstract class FacetImpl extends EntityImpl implements Facet {
	
}
