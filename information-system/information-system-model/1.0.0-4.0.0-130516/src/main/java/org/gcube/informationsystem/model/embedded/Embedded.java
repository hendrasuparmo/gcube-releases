/**
 * 
 */
package org.gcube.informationsystem.model.embedded;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public interface Embedded {
	
	public static final String NAME = Embedded.class.getSimpleName();
	
}
