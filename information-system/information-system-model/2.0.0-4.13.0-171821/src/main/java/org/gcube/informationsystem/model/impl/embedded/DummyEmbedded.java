/**
 * 
 */
package org.gcube.informationsystem.model.impl.embedded;

import org.gcube.informationsystem.model.reference.embedded.Embedded;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class DummyEmbedded extends EmbeddedImpl implements Embedded {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 2458531826742344451L;
	
	public DummyEmbedded(){
		super();
	}

}
