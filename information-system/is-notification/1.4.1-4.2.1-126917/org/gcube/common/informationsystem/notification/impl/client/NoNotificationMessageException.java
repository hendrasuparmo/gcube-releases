package org.gcube.common.informationsystem.notification.impl.client;

public class NoNotificationMessageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoNotificationMessageException(String message){
		super(message);
	}

}
