package org.gcube.portal.mailing.message;

/**
 * 
 * @author Massimiliano Assante, CNR-ISTI
 *
 */
public enum RecipientType {
	TO,
	CC,
	BCC;
}
