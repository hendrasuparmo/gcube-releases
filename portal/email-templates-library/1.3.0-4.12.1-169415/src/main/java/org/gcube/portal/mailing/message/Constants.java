package org.gcube.portal.mailing.message;

public class Constants {
	/**
	 * used to generate links to user profile pages
	 */
	public static final String USER_PROFILE_OID = "userIdentificationParameter";
}
