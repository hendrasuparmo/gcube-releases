package gr.cite.shibboleth.util;

public class ShibbolethConstantVariables {
	
	public static final String SHIBBOLETH_AUTH_ENABLED = "shibboleth.auth.enabled";
	
	public static final String USER_EMAIL_ADDRESS_FOR_SESSION_SHIBBOLETH = "shibbolethEmail";
	
	public static final String SHIBBOELTH_ATTRIBUTE_EMAIL = "shibboleth.email.attribute";
	
	public static final String SHIBBOLETH_ATTRIBUTE_FIRST_NAME = "shibboleth.givenName.attribute";
	
	public static final String SHIBBOLETH_ATTRIBUTE_LAST_NAME = "shibboleth.sn.attribute";
}