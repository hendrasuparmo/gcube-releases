package gr.citeadditionalemailaddresses.util;

/**
 * @author mnikolopoulos
 *
 */
public class AdditionalEmailAddressesConstants {

	public final static String CODE = "verificationCodeAdditionalEmail";
	
	public final static String VERIFY = "verifiedAdditionalEmail";
}
