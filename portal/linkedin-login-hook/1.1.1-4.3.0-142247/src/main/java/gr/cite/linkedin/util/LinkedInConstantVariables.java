package gr.cite.linkedin.util;

public class LinkedInConstantVariables {

	public final static String REDIRECT_URL_AFTER_LOGIN = "redirectUrlAfterLogin";
	
	public final static String USER_EMAIL_ADDRESS_FOR_SESSION_LINKEDIN = "userEmailAdressLinkedIn";
	
	public final static String LINKEDIN_CLIENT_ID = "linkedIn.client.id";
	
	public final static String LINKEDIN_CLIENT_SECRET = "linkedIn.client.secret";
	
	public final static String OAUTH_VERIFIER = "oauth_verifier";
	
	public final static String OAUTH_TOKEN = "oauth_token";
	
	public final static String REQUEST_TOKEN_SECRET_LINKEDIN = "requestTokenSecretLinkedIn";
	
	public final static String API_CALL = "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name)?format=json";
	
	public final static String CUSTOM_EXPANDO_COLUMN_FOR_VERIFICATION = "verified";
	
	public final static String RETURN_FROM_LINKEDIN_URL = "linkedIn.api.callback.url"; 
	
	public final static String LINKEDIN_USERID_COLUMN_NAME = "linkedInUserId";//for future reference
	
}
