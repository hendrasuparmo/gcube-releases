package org.gcube.portal.trainingmodule.shared;

public enum ItemType {
	
	FILE,
	VIDEO,
	QUESTIONNAIRE,
	OTHER

}
