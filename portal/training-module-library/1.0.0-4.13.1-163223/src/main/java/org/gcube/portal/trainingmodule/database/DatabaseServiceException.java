package org.gcube.portal.trainingmodule.database;




// TODO: Auto-generated Javadoc
/**
 * The Class DatabaseServiceException.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 10, 2018
 */
public class DatabaseServiceException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2275658965381060196L;

	/**
	 * Instantiates a new database service exception.
	 */
	public DatabaseServiceException(){}
	
	/**
	 * Instantiates a new database service exception.
	 *
	 * @param message the message
	 */
	public DatabaseServiceException(String message) {
		super(message);
	}
}
