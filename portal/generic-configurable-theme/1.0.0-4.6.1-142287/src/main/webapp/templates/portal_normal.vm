<!DOCTYPE html>

#parse ($init)

<html class="$root_css_class" dir="#language ("lang.dir")" lang="$w3c_language_id">

<head>
	<title>$themeDisplay.getSiteGroupName() - $the_title</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<meta property="og:title" content="$the_title - $themeDisplay.getSiteGroupName()" />
	<meta property="og:type" content="article" />
	<meta property="og:description" content="$themeDisplay.getLayout().getGroup().getDescription()" />
	<meta property="og:url" content="$site_default_url" />
	<meta property="og:image" content="$site_logo" />
	
	$theme.include($top_head_include)
	<script type="text/javascript" src="$javascript_folder/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="$javascript_folder/gcube-context.js"></script>	
	<style>
	#if ($theme.getSetting("portlet-topper-css-attributes") !="")	
		.aui .portlet-topper .portlet-title { 
			$theme.getSetting("portlet-topper-css-attributes")
		}
	#end
	#if ($theme.getSetting("body-background-color") !="")
		.aui body {
			background-color: $theme.getSetting("body-background-color");
		}
	#end
	#if ($theme.getSetting("div-wrapper-css-attributes") !="")	
		.aui div#wrapper { 
			$theme.getSetting("div-wrapper-css-attributes")
		}
	#end

	#if ($theme.getSetting("header-background-css-attributes") !="")
		.aui header#banner {
			$theme.getSetting("header-background-css-attributes");
		}
	#end	
	#if ($theme.getSetting("navbar-background-color") !="")
		.aui #navigation .navbar-inner {
			background: $theme.getSetting("navbar-background-color");
		}
	#end
	#if ($theme.getSetting("navbar-tabselected-color") !="")
		.aui #navigation .nav li.active > a {
			background-color: $theme.getSetting("navbar-tabselected-color");
		}
	#end
	#if ($theme.getSetting("navbar-tabhover-color") !="")
		.aui #navigation .nav li.open > a {
			background-color: $theme.getSetting("navbar-tabhover-color");
		}
	#end
	#if ($theme.getSetting("footer-background-color") !="")
		.aui .custom-footer {
			background-color: $theme.getSetting("footer-background-color");
		}
	#end
	#if ($theme.getSetting("login-button-color") !="")
		.aui .portlet-login button.login-sign-in-button {
			background-color: $theme.getSetting("login-button-color");
			padding: 8px;
		}
		.aui .portlet-login button.login-sign-in-button:hover {
			opacity: 0.8;
		}
	#end
	</style>
</head>

<body class="$css_class">

<!-- start customisation -->
<div class="modal" id="expirationModal" tabindex="-1" role="dialog" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="label">Your session expired!</h4>
        </div>
        <div class="modal-body">
            <div style="text-align:center; margin-bottom: 50px;">
            	<i class="icon-time" style="color: #AAA; font-size: 10em;"></i>
            </div>
            <div style="font-size: 18px; text-align:center; margin-bottom: 50px;">
            	Please try <a href="javascript:location.reload();">reload</a> this page or <a href="/c/portal/logout">logout</a>
            </div>
        </div>
    </div>
</div>
</div>
<script>
	injectClientContext();
</script>
<!-- end customisation -->
<a href="#main-content" id="skip-to-content">#language ("skip-to-content")</a>

$theme.include($body_top_include)

<div id="gcube-hide-dockbar">
	#dockbar()
</div>
#if($is_signed_in)
	<div class="feedback-container"><a href="http://support.d4science.org" target="_blank"><span>Report an issue</span><a></div>
#end

<header id="banner" role="banner">
#if ($theme.getSetting("show-powered-d4science") == "true")
<div class="poweredBy-link">       				
	<a href="http://www.d4science.org" target="_blank" class="$logo_css_class">
		<img src="$themeDisplay.getPathThemeImages()/custom/powered-by-d4science.png" alt="D4Science Infrastructure" hspace="5" />
		</a>
</div>
#end
<div id="heading">
	<h1 class="site-title">
		<a class="$logo_css_class" href="$themeDisplay.getPortalURL()" title="#language_format ("go-to-x", ["D4Science Gateway"])">
			<img alt="$logo_description" src="$site_logo" class="d4science-logo"/>
		</a>
	</h1>

	<h2 class="page-title">
		<span>$the_title</span>
	</h2>
</div>		


#if ($has_navigation || $is_signed_in)
	#parse ("$full_templates_path/navigation.vm")
#end

</header>
<div class="container-fluid" id="wrapper" style="background-image: url($div-wrapper-image-name)" >

	<div id="content">
		<!-- nav id="breadcrumbs">#breadcrumbs()</nav -->

		#if ($selectable)
			$theme.include($content_include)
		#else
			$portletDisplay.recycle()

			$portletDisplay.setTitle($the_title)

			$theme.wrapPortlet("portlet.vm", $content_include)
		#end
	</div>

</div>

<footer class="custom-footer">
	<div class="custom-footer-container">
		<a href="/terms-of-use">Terms of Use</a> |
		<a href="/cookie-policy">Cookies Policy</a> |
		<a target="_blank" href="https://www.iubenda.com/privacy-policy/441050">Privacy Policy</a>  |  
		<a target="_blank" href="$theme.getSetting("footer-website-url")">$theme.getSetting("footer-website-name")</a>        
    </div> 
	<div style="float:right; text-align: right;">  
		#if ($theme.getSetting("show-eu-flag") == "true")
		<a href="http://ec.europa.eu/programmes/horizon2020/" target="_blank">
			<img src="$themeDisplay.getPathThemeImages()/custom/logo-ec.jpg" style="width: 35px;" alt="EU H2020 programme" hspace="5" />
		</a>
		#end
		<div style="color: #CCC; font-size: 12px;">$theme.getSetting("footer-text-about")</div>
		</div>	
</footer>
$theme.include($body_bottom_include)

$theme.include($bottom_include)

</body>

</html>