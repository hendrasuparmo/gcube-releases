package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class LikeIDNotFoundException extends Exception {
	public LikeIDNotFoundException(String message) {
		super(message);
	}
}