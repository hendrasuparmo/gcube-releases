package org.gcube.portal.databook.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code> and instances the <code>HandlerManager</code> for IPC
 */
public class GCubeSocialNetworking implements EntryPoint {
	
	public static final String USER_PROFILE_LINK = "/group/data-e-infrastructure-gateway/profile";
	public static final String USER_PROFILE_OID = "userIdentificationParameter";
	public static final String HASHTAG_OID = "hashtagIdentificationParameter";
	public void onModuleLoad() {
	}
		
}
