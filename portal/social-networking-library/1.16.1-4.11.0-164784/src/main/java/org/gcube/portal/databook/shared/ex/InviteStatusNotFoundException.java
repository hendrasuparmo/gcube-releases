package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class InviteStatusNotFoundException extends Exception {
	public InviteStatusNotFoundException(String message) {
		super(message);
	}
}
