package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class FeedIDNotFoundException extends Exception {
	public FeedIDNotFoundException(String message) {
		super(message);
	}
}