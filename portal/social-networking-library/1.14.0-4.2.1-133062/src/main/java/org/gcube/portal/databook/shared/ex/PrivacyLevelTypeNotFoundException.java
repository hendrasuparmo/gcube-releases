package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class PrivacyLevelTypeNotFoundException  extends Exception {
	public PrivacyLevelTypeNotFoundException(String message) {
		super(message);
	}
}