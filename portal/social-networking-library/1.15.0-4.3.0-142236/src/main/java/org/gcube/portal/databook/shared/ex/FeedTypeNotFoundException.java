package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class FeedTypeNotFoundException extends Exception {
	public FeedTypeNotFoundException(String message) {
		super(message);
	}
}
