package org.gcube.portal.databook.shared;
/**
 * @author Massimiliano Assante ISTI-CNR
 * 
 * @version 1.0 July 6th 2012
 */
public enum PrivacyLevel {
	PRIVATE, CONNECTION, VRES, SINGLE_VRE, PORTAL, PUBLIC;
}
