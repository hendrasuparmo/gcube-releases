package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class NotificationIDNotFoundException extends Exception {
	public NotificationIDNotFoundException(String message) {
		super(message);
	}
}