package org.gcube.data.spd.model;

public class Constants {

	public static final String APPLICATION_ROOT_PATH = "/gcube/service";
	
	public static final String MANAGER_PATH = "retrieve";
	
	public static final String RESULTSET_PATH = "resultset";
}
