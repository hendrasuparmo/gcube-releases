package org.gcube.data.spd.model.exceptions;

public class ExternalRepositoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExternalRepositoryException() {
		super();
	}

	public ExternalRepositoryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ExternalRepositoryException(String arg0) {
		super(arg0);
	}

	public ExternalRepositoryException(Throwable arg0) {
		super(arg0);
	}
	
	

}
