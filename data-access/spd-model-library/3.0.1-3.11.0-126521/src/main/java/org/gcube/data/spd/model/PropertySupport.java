package org.gcube.data.spd.model;

import java.util.Set;


public interface PropertySupport {

	public Set<Conditions> getSupportedProperties();
}
