package org.gcube.data.spd.model.util;

public class Labels {

	public static final String RANK_LABEL="rank";
	public static final String ACCORDINGTO_LABEL="accordingTo";
	public static final String SCIENTIFICNAME_LABEL="scientificName";
	public static final String COMMONNAME_LABEL="commonName";
	public static final String COMMONNAMES_LABEL="commonNames";
	public static final String PROVIDER_LABEL="provider";
	public static final String CITATION_LABEL="citation";
	public static final String PARENT_TAG="parent";
		
	public static final String PRODUCTS_LABEL="products";
	
	public static final String PRODUCT_LABEL="product";
	
	public static final String TYPE_LABEL="type";
	public static final String KEY_LABEL="key";
	
	public static final String DATASET_TAG="dataSet";
	public static final String DATAPROVIDER_TAG="dataProvider";
	public static final String NAME_TAG = "name";
	public static final String LANGUAGE_TAG = "language";
	public static final String COUNT_LABEL = "count";
	
	public static final String CREDITS_LABEL = "credits";
	
	
}
