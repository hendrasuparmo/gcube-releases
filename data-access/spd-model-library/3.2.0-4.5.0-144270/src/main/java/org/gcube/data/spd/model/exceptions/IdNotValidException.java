package org.gcube.data.spd.model.exceptions;

public class IdNotValidException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6500170339362279314L;

	public IdNotValidException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdNotValidException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public IdNotValidException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public IdNotValidException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	
	
}
