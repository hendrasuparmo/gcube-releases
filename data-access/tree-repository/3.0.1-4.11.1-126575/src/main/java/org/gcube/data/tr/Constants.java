package org.gcube.data.tr;

/**
 * Plugin constants.
 * 
 * @author Fabio Simeoni
 *
 */
public class Constants {

	/**
	 * The name of the plugin.
	 */
	public static final String TR_NAME="tree-repository";
	
	/**
	 * The description of the plugin.
	 */
	public static final String TR_DESCRIPTION="A plugin that stores trees locally";
	
	
	/**
	 * The storage location.
	 */
	public static final String STORAGE_LOCATION = "store";
	
}
