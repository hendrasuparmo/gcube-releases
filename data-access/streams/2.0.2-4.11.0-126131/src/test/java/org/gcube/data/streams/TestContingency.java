package org.gcube.data.streams;

import org.gcube.data.streams.exceptions.StreamContingency;

@StreamContingency
public class TestContingency extends Exception {
	private static final long serialVersionUID = 1L;
}
