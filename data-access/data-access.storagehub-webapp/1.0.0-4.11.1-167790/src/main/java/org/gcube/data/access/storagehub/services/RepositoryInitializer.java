package org.gcube.data.access.storagehub.services;

import javax.jcr.Repository;

public interface RepositoryInitializer {

	Repository getRepository();
}
