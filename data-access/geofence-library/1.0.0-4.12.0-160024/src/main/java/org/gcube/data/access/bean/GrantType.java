package org.gcube.data.access.bean;

public enum GrantType {
	ALLOW, DENY, LIMIT
}
