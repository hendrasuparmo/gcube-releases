package org.gcube.data.spd.exception;

public class MaxRetriesReachedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MaxRetriesReachedException() {
		// TODO Auto-generated constructor stub
	}

	public MaxRetriesReachedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MaxRetriesReachedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MaxRetriesReachedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
