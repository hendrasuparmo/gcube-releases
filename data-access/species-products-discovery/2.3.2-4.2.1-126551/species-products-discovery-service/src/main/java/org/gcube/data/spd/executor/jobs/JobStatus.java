package org.gcube.data.spd.executor.jobs;

public enum JobStatus {
		PENDING, 
		RUNNING,
		FAILED,
		COMPLETED
}
