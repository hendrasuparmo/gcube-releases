package org.gcube.data.spd.executor.jobs;

public interface URLJob extends SpeciesJob {

	public String getResultURL() ;
	
	public String getErrorURL() ;
	
}
