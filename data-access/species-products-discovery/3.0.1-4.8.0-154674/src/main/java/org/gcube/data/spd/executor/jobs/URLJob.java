package org.gcube.data.spd.executor.jobs;

public abstract class URLJob extends SpeciesJob {

	public abstract String getResultURL() ;
	
	public abstract String getErrorURL() ;
	
}
