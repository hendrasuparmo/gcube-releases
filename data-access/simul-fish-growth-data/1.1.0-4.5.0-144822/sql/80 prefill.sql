﻿delete from Status;
insert into Status (id,	designation) values (1, 'Ready');
insert into Status (id,	designation) values (2, 'Calculating ...');
insert into Status (id,	designation) values (3, 'Calculation failed');

delete from BroodstockQuality;
insert into BroodstockQuality (id, aa, designation) values (1, 1, '*');
insert into BroodstockQuality (id, aa, designation) values (2, 2, '**');
insert into BroodstockQuality (id, aa, designation) values (3, 3, '***');
insert into BroodstockQuality (id, aa, designation) values (4, 4, '****');
insert into BroodstockQuality (id, aa, designation) values (5, 5, '*****');

delete from FeedQuality;
insert into FeedQuality (id, aa, designation) values (1, 1, '*');
insert into FeedQuality (id, aa, designation) values (2, 2, '**');
insert into FeedQuality (id, aa, designation) values (3, 3, '***');
insert into FeedQuality (id, aa, designation) values (4, 4, '****');
insert into FeedQuality (id, aa, designation) values (5, 5, '*****');

delete from OxygenRating;
insert into OxygenRating (id, aa, designation) values (1, 1, '1');
insert into OxygenRating (id, aa, designation) values (2, 2, '2');
insert into OxygenRating (id, aa, designation) values (3, 3, '3');
insert into OxygenRating (id, aa, designation) values (4, 4, '4');
insert into OxygenRating (id, aa, designation) values (5, 5, '5');
insert into OxygenRating (id, aa, designation) values (6, 6, '6');
insert into OxygenRating (id, aa, designation) values (7, 7, '7');
insert into OxygenRating (id, aa, designation) values (8, 8, '8');
insert into OxygenRating (id, aa, designation) values (9, 9, '9');
insert into OxygenRating (id, aa, designation) values (10, 10, '10');
insert into OxygenRating (id, aa, designation) values (11, 11, '11');
insert into OxygenRating (id, aa, designation) values (12, 12, '12');
insert into OxygenRating (id, aa, designation) values (13, 13, '13');
insert into OxygenRating (id, aa, designation) values (14, 14, '14');
insert into OxygenRating (id, aa, designation) values (15, 15, '15');
insert into OxygenRating (id, aa, designation) values (16, 16, '16');
insert into OxygenRating (id, aa, designation) values (17, 17, '17');
insert into OxygenRating (id, aa, designation) values (18, 18, '18');
insert into OxygenRating (id, aa, designation) values (19, 19, '19');
insert into OxygenRating (id, aa, designation) values (20, 20, '20');

delete from CurrentRating;
insert into CurrentRating (id, aa, designation) values (1, 1, '1');
insert into CurrentRating (id, aa, designation) values (2, 2, '2');
insert into CurrentRating (id, aa, designation) values (3, 3, '3');
insert into CurrentRating (id, aa, designation) values (4, 4, '4');
insert into CurrentRating (id, aa, designation) values (5, 5, '5');
insert into CurrentRating (id, aa, designation) values (6, 6, '6');
insert into CurrentRating (id, aa, designation) values (7, 7, '7');
insert into CurrentRating (id, aa, designation) values (8, 8, '8');
insert into CurrentRating (id, aa, designation) values (9, 9, '9');
insert into CurrentRating (id, aa, designation) values (10, 10, '10');
insert into CurrentRating (id, aa, designation) values (11, 11, '11');
insert into CurrentRating (id, aa, designation) values (12, 12, '12');
insert into CurrentRating (id, aa, designation) values (13, 13, '13');
insert into CurrentRating (id, aa, designation) values (14, 14, '14');
insert into CurrentRating (id, aa, designation) values (15, 15, '15');
insert into CurrentRating (id, aa, designation) values (16, 16, '16');
insert into CurrentRating (id, aa, designation) values (17, 17, '17');
insert into CurrentRating (id, aa, designation) values (18, 18, '18');
insert into CurrentRating (id, aa, designation) values (19, 19, '19');
insert into CurrentRating (id, aa, designation) values (20, 20, '20');

delete from Region;
insert into Region (id,	designation) values (1, 'Long/Lat based');

delete from Species;
insert into Species (id, designation) values (1, 'Sea bass');
insert into Species (id, designation) values (2, 'Sea bream');