create index IX_Fcr_simulModelId on Fcr (simulModelId);
create index IX_Mortality_simulModelId on Mortality (simulModelId);
create index IX_Sfr_simulModelId on Sfr (simulModelId);
create index IX_Sgr_simulModelId on Sgr (simulModelId);
create index IX_SimulModel_ownerId on SimulModel (ownerId);
create index IX_Site_ownerId on Site (ownerId);