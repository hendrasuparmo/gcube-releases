package org.gcube.data.simulfishgrowthdata.servlet;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class StartupServicesServlet
 */
public class StartupServicesServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory.getLogger(StartupServicesServlet.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
	}

}
