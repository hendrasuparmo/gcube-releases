package org.apache.jackrabbit.j2ee.workspacemanager;

public class ServletParameter {
	
	public static final String SRC_ABS_PATH 		= "srcAbsPath";
	public static final String DEST_ABS_PATH 		= "destAbsPath";
	public static final String REMOVE_EXISTING 		= "removeExisting";
	public static final String LOGIN 				= "login";
	public static final String SRC_ID 				= "srcId";
	public static final String DEST_ID 				= "destId";
	public static final String ABS_PATH 			= "absPath";
	public static final String TRASH_ID 			= "trashId";
	
	public static final String QUERY 				= "query";
	public static final String LANG 				= "lang";
	public static final String ID 					= "id";
	public static final String PATH 				= "path";
	public static final String LIMIT 				= "limit";
	public static final String UUID 				= "uuid";
	public static final String PARENT_ID 			= "parentId";
	public static final String SHOW_HIDDEN 			= "showHidden";

	public static final String PORTAL_LOGIN 		= "portalLogin";
	public static final String SCOPE 				= "scope";
	public static final String REMOTE_PATH 			= "remotePath";
	public static final String FILENAME 			= "filename";
	public static final String SERVICE_NAME			= "serviceName";
	
	public static final String NAME					= "name";
	public static final String DESCRIPTION			= "description";
	public static final String PARENT_PATH			= "parentPath";
	
	
}