<?xml version="1.0" encoding="UTF-8"?>

<!-- Licensed to the Apache Software Foundation (ASF) under one or more contributor 
	license agreements. See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership. The ASF licenses this file to 
	You under the Apache License, Version 2.0 (the "License"); you may not use 
	this file except in compliance with the License. You may obtain a copy of 
	the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<!-- ====================================================================== -->
	<!-- P R O J E C T D E S C R I P T I O N -->
	<!-- ====================================================================== -->
	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>org.gcube.tools</groupId>
		<version>1.0.0</version>
		<relativePath />
	</parent>


	<groupId>org.gcube.data.access</groupId>
	<artifactId>home-library-webapp</artifactId>
	<version>1.10.2-4.10.0-160502</version>
	<packaging>war</packaging>
	<name>Jackrabbit Web Application</name>
	<description>Web application that hosts and serves a Jackrabbit content repository</description>

	<properties>
		<jackrabbit.version>2.14.0</jackrabbit.version>
		<tomcat.version>7.0.40</tomcat.version>
		<jetty.version>6.1.26</jetty.version>
		<tika.version>1.7</tika.version>
		<slf4j.api.version>1.6.6</slf4j.api.version>
		<slf4j.version>1.7.4</slf4j.version> <!-- sync with logback version -->
		<logback.version>1.0.12</logback.version>

		<distroDirectory>${project.basedir}/distro</distroDirectory>
		<description>REST web service for Jackrabbit</description>
		<warname>home-library-webapp</warname>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

	</properties>
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.gcube.distribution</groupId>
				<artifactId>maven-smartgears-bom</artifactId>
				<version>2.0.1-4.10.0-148595</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<dependencies>

		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>home-library</artifactId>
			<version>2.10.1-4.10.0-160319</version>
			<exclusions>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>home-library-jcr</artifactId>
			<version>2.11.3-4.10.0-162367</version>
			<exclusions>
				<exclusion>
					<artifactId>log4j</artifactId>
					<groupId>log4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>slf4j-log4j12</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-csv</artifactId>
			<version>1.5</version>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-smartgears</artifactId>
			<exclusions>
				<exclusion>
					<artifactId>logback-classic</artifactId>
					<groupId>ch.qos.logback</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>log4j-over-slf4j</artifactId>
			<version>1.7.25</version>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-smartgears-app</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>9.4.1212.jre7</version>
		</dependency>

		<dependency>
			<groupId>com.thoughtworks.xstream</groupId>
			<artifactId>xstream</artifactId>
			<version>1.4.9</version>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpmime</artifactId>
			<version>4.5.2</version>
		</dependency>

		<dependency>
			<groupId>org.gcube.resources</groupId>
			<artifactId>common-gcore-resources</artifactId>
		</dependency>

		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-gcore-stubs</artifactId>
		</dependency>
		<dependency>
			<groupId>org.gcube.resources.discovery</groupId>
			<artifactId>ic-client</artifactId>
		</dependency>
		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-encryption</artifactId>
		</dependency>


		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>
		<dependency>
			<groupId>javax.jcr</groupId>
			<artifactId>jcr</artifactId>
			<version>2.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-api</artifactId>
			<version>${jackrabbit.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-core</artifactId>
			<version>${jackrabbit.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-jcr-server</artifactId>
			<version>${jackrabbit.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-jcr-servlet</artifactId>
			<version>${jackrabbit.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-jcr-rmi</artifactId>
			<version>${jackrabbit.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.tika</groupId>
			<artifactId>tika-parsers</artifactId>
			<version>${tika.version}</version>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>


		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-servlet-api</artifactId>
			<version>${tomcat.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.1</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.10</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-catalina</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-coyote</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-jasper</artifactId>
			<version>${tomcat.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jul-to-slf4j</artifactId>
			<version>${slf4j.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.3.1</version>
		</dependency>

		<!-- http://mvnrepository.com/artifact/org.mortbay.jetty/jetty -->
		<dependency>
			<groupId>org.mortbay.jetty</groupId>
			<artifactId>jetty</artifactId>
			<version>6.1.26</version>
		</dependency>


	</dependencies>

	<build>
		<finalName>${artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<warName>${warname}</warName>
					<webXml>src\main\webapp\WEB-INF\web.xml</webXml>
					<outputFileNameMapping>@{artifactId}@-@{baseVersion}@.@{extension}@</outputFileNameMapping>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>${distroDirectory}/descriptor.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>servicearchive</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>dev</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<war.name>dev</war.name>
			</properties>
		</profile>
		<profile>
			<id>prod</id>
			<properties>
				<war.name>prod</war.name>
			</properties>
		</profile>
	</profiles>

</project>
