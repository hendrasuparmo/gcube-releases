package org.gcube.data.tmf.api;

/**
 * Library-wide constants.
 * 
 * @author Fabio Simeoni
 *
 */
public class Constants {

	/** Namespace. */
	public static final String NS = "http://gcube-system.org/namespaces/data/tm";

}
