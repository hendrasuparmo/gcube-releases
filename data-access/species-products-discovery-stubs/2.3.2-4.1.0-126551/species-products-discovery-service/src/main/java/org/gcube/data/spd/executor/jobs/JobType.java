package org.gcube.data.spd.executor.jobs;

public enum JobType {

	DWCAByChildren,
	DWCAById,
	CSV,
	CSVForOM,
	DarwinCore
	
}
