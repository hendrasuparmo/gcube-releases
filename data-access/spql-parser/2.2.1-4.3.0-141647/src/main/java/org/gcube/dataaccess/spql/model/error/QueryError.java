/**
 * 
 */
package org.gcube.dataaccess.spql.model.error;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface QueryError {
	
	public String getErrorMessage();

}
