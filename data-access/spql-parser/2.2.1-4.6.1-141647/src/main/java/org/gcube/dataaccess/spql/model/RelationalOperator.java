/**
 * 
 */
package org.gcube.dataaccess.spql.model;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public enum RelationalOperator {
	
	LT,
	LE,
	EQ,
	GE,
	GT;

}
