package org.gcube.data.trees;

/**
 * Library constants.
 * 
 * @author Fabio Simeoni
 *
 */
public class Constants {

	/** Serialisation namespace. */
	public static final String TREE_NS = "http://gcube-system.org/namespaces/data/trees";
	
	/**
	 * The first path component of a tree URI.
	 */
	public static final String TREE_URI_PATH_PREFIX = "tree";
	
}
