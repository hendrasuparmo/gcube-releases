package org.gcube.portlets.user.messages.client.interfaces;



/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 *
 */

//Use this interface to subscriber event type (on tree)
public interface GuiEventInterface {
	public EventsTypeEnum getKey();
}
