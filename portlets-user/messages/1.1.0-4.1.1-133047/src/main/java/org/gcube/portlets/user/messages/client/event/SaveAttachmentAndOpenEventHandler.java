package org.gcube.portlets.user.messages.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SaveAttachmentAndOpenEventHandler extends EventHandler {
	void onSaveAttachmentsAndOpen(SaveAttachmentAndOpenEvent saveAttachmentsAndOpenEvent);
}