package org.gcube.portlets.user.messages.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Icons extends ClientBundle {
	
	@Source("icons/table.png")
	ImageResource table();
	
	@Source("icons/close.gif")
	ImageResource close();
	
	@Source("icons/mime/gif.gif")
	ImageResource gif();
	
	@Source("icons/mime/pdf.gif")
	ImageResource pdf();
	
	@Source("icons/mime/jpeg.gif")
	ImageResource jpeg();
	
	@Source("icons/mime/png.gif")
	ImageResource png();
	
	@Source("icons/mime/tiff.gif")
	ImageResource tiff();
	
	@Source("icons/mime/svg.png")
	ImageResource svg();

	@Source("icons/mime/xml.gif")
	ImageResource xml();
	
	@Source("icons/mime/html.gif")
	ImageResource html();
	
	@Source("icons/mime/java.gif")
	ImageResource java();

	@Source("icons/mime/doc.gif")
	ImageResource doc();
	
	@Source("icons/mime/txt.gif")
	ImageResource txt();
	
	@Source("icons/mime/movie.png")
	ImageResource movie();
	
	@Source("icons/mime/addfolder.png")
	ImageResource addFolder();
	
	@Source("icons/mime/deletefolder.jpg")
	ImageResource deleteFolder();

	@Source("icons/mime/delete.png")
	ImageResource deleteItem();
	
	@Source("icons/mime/edit.png")
	ImageResource renameItem();

	@Source("icons/mime/folder.png")
	ImageResource folder();

	@Source("icons/mime/addfolder32.png")
	ImageResource addFolder32();

	@Source("icons/mime/delete.png")
	ImageResource deleteItem32();

	@Source("icons/mime/renameitem32.png")
	ImageResource renameItem32();

	@Source("icons/mime/uploadfile32_2.gif")
	ImageResource uploadFile32();

	@Source("icons/mime/upload.png")
	ImageResource uploadFile();

	@Source("icons/mime/audio.png")
	ImageResource audio();

	@Source("icons/mime/download.png")
	ImageResource download();
	
	@Source("icons/mime/forward.png")
	ImageResource separatorPath();

	@Source("icons/mime/search16.png")
	ImageResource search();

	@Source("icons/mime/harddisk.png")
	ImageResource hardDisk();

	@Source("icons/mime/archiveupload.png")
	ImageResource archiveUpload();

	@Source("icons/mime/buttoncancel.gif")
	ImageResource cancel();

	@Source("icons/mime/togglelist.png")
	ImageResource toggleList();
	
	@Source("icons/mime/togglegroup.png")
	ImageResource toggleGroup();
	
	@Source("icons/mime/toggleicon.png")
	ImageResource toggleIcon();

	@Source("icons/aquamaps.png")
	ImageResource biodiversity();

	@Source("icons/external_image.gif")
	ImageResource images();

	@Source("icons/external_url.png")
	ImageResource links();

	@Source("icons/report.png")
	ImageResource report();
	
	@Source("icons/timeseries.png")
	ImageResource timeSeries();

	@Source("icons/document.png")
	ImageResource documents();
	
	@Source("icons/mime/save.gif")
	ImageResource save();

	@Source("icons/mime/star.png")
	ImageResource star();

	@Source("icons/preview.png")
	ImageResource preview();

	@Source("icons/rightarrow.gif")
	ImageResource show();

	@Source("icons/link.gif")
	ImageResource openUrl();
	
	@Source("icons/linkadd.png")
	ImageResource addUrl();
	
	@Source("icons/send.png")
	ImageResource sendTo();

	@Source("icons/checkuser.png")
	ImageResource checkUser();

	@Source("icons/inbox-download.png")
	ImageResource inboxReceived();

	@Source("icons/inbox-upload.png")
	ImageResource inboxSent();

	@Source("icons/email2.png")
	ImageResource email();

	@Source("icons/email-open.png")
	ImageResource openEmail();

	@Source("icons/save-attach.png")
	ImageResource saveAttachs();

	@Source("icons/download-email.png")
	ImageResource downloadEmail16x16();
	
	@Source("icons/email_download.png")
	ImageResource downloadEmail();

	@Source("icons/email-read.png")
	ImageResource emailRead();

	@Source("icons/email-notread.png")
	ImageResource emailNotRead();

	@Source("icons/email-delete.png")
	ImageResource emailDelete16x16();

	@Source("icons/email_delete.png")
	ImageResource emailDelete();
	
	@Source("icons/email-forward.png")
	ImageResource emailForward16x16();

	@Source("icons/copy.png")
	ImageResource copy();

	@Source("icons/paste.png")
	ImageResource paste();

	@Source("icons/refresh.gif")
	ImageResource refresh();

	@Source("icons/loading.gif")
	ImageResource loading();
	
	@Source("icons/loading-off.gif")
	ImageResource loadingOff();
	
	@Source("icons/loading2.gif")
	ImageResource loading2();

	@Source("icons/delete.gif")
	ImageResource delete2();

	@Source("icons/webdavurl.png")
	ImageResource urlWebDav();

	@Source("icons/cancel.png")
	ImageResource removeFilter();

	@Source("icons/attach.png")
	ImageResource attach();

	@Source("icons/newmail.png")
	ImageResource createNewMail();
	
	@Source("icons/newmail16x16.png")
	ImageResource createNewMail16x16();

	@Source("icons/reporttemplate.png")
	ImageResource reportTemplate();

	@Source("icons/workflowreport.png")
	ImageResource workflowReport();

	@Source("icons/workflowtemplate.png")
	ImageResource workflowTemplate();
	
	@Source("icons/webdav.png")
	ImageResource webDav();
	
	@Source("icons/mail-reply.png")
	ImageResource replyMail16x16();
	
	@Source("icons/email_reply.png")
	ImageResource replyMail();
	
	@Source("icons/mail-reply-all.png")
	ImageResource replyAllMail16x16();
	
	@Source("icons/email_replyall.png")
	ImageResource replyAllMail();
	
	@Source("icons/application-link.png")
	ImageResource resourceLink();
	
	@Source("icons/email_forward.png")
	ImageResource emailForward();

	@Source("icons/gcubeItem.jpeg")
	ImageResource gcubeItem();
}

