package org.gcube.portlets.user.messages.client.interfaces;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 *
 */
public enum EventsTypeEnum 
{
   DELETED_MESSAGE, 
   MARK_MESSAGE_AS_READ,
   SELECTED_MESSAGE,
   CREATE_NEW_MESSAGE, 
   REPLY_FORWARD_MESSAGE,
   LOAD_MESSAGES_EVENT
}