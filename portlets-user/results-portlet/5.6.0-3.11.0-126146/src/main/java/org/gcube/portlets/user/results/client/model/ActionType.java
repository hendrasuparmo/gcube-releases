package org.gcube.portlets.user.results.client.model;

/**
 * 
 * @author massi
 * enum for actions menu type
 */
public enum ActionType {
	
	/**
	 * VIEW CONTENT
	 */
	VIEW_CONTENT, 
	/**
	 * VIEW CONTENT
	 */
	VIEW_PART, 
	/**
	 * VIEW CONTENT
	 */
	VIEW_ALTERNATIVE, 
	
	/**
	 * SAVE_CONTENT
	 */
	SAVE_CONTENT,
	
	/**
	 * VIEW ONTOLOGY
	 */
	VIEW_ONTOLOGY,
	/**
	 * CONTENT_VIEWER
	 */
	CONTENT_VIEWER,
	
	/**
	 * EDIT_METADATA
	 */
	EDIT_METADATA,
	/**
	 * VIEW_METADATA
	 */
	VIEW_METADATA,
	/**
	 * 
	 */
	MANAGE_ANNOTATIONS;	
}