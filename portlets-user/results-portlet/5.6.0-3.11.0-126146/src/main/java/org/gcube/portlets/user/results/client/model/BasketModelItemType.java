package org.gcube.portlets.user.results.client.model;

public enum BasketModelItemType {
	
	/**
	 * Info object.
	 */
	INFO_OBJECT,
	
	/**
	 * Info object link.
	 */
	INFO_OBJECT_LINK,
	
	/**
	 * External image.
	 */
	EXTERNAL_IMAGE,
	
	/**
	 * External file.
	 */
	EXTERNAL_FILE,
	
	/**
	 * External PDF file.
	 */
	EXTERNAL_PDF_FILE,
	
	/**
	 * Query.
	 */
	QUERY,
	
	/**
	 * Report template.
	 */
	REPORT_TEMPLATE,
	
	/**
	 * Report. 
	 */
	REPORT,
	/**
	 * TIME SERIES
	 */
	TIMESERIES,
	/**
	 * GPOD
	 */
	FOLDER,
	/**
	 * OPEN SEARCH
	 */
	OPENSEARCH, 
	
	SHARED_FOLDER;
}