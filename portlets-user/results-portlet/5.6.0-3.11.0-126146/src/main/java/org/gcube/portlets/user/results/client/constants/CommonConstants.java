package org.gcube.portlets.user.results.client.constants;


/**
 * <code> ReportConstants </code> class represent 
 * 
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 * @version October 2008 (0.2) 
 */
public class CommonConstants {
	
	/**
	 * Reg Ex to accept Alphanumeric values 
	 */
	public static final String ACCEPTED_CHARS_ALPHANUM = "[^a-zA-Z0-9]";
	
	/**
	 * Reg Ex to accept Alphanumeric values and _ and - and @
	 */
	public static final String ACCEPTED_CHARS_REG_EX = "[^a-zA-Z0-9\\s@\\-_:,\\*]";
	/**
	 * Reg Ex to accept only numbers
	 */
	public static final String ACCEPTED_CHARS_JUST_NUM = "[^0-9]";
	
	
	
}
