package org.gcube.portlets.user.results.client.constants;

import com.google.gwt.core.client.GWT;

public class FileTypeImagesConstants {
	/**
	 * PDF
	 */
	public static final String BASKET_PDF = GWT.getModuleBaseURL() + "images/basketicons/filetype/pdf.gif";
	/**
	 * BASKET_JPG
	 */
	public static final String BASKET_JPG = GWT.getModuleBaseURL() + "images/basketicons/filetype/jpeg.gif";
	/**
	 * BASKET_PNG
	 */
	public static final String BASKET_PNG = GWT.getModuleBaseURL() + "images/basketicons/filetype/png.gif";
	/**
	 * BASKET_TXT
	 */
	public static final String BASKET_TXT = GWT.getModuleBaseURL() + "images/basketicons/filetype/txt.gif";
	/**
	 * BASKET_ZIP
	 */
	public static final String BASKET_EPS = GWT.getModuleBaseURL() + "images/basketicons/filetype/eps.gif";
	/**
	 * BASKET_ZIP
	 */
	public static final String BASKET_DOC = GWT.getModuleBaseURL() + "images/basketicons/filetype/doc.gif";
	/**
	 * BASKET_ZIP
	 */
	public static final String BASKET_PSD = GWT.getModuleBaseURL() + "images/basketicons/filetype/psd.gif";
	/**
	 * BASKET_ZIP
	 */
	public static final String BASKET_MP3 = GWT.getModuleBaseURL() + "images/basketicons/filetype/mp3.gif";
	/**
	 * BASKET_ZIP
	 */
	public static final String BASKET_ZIP = GWT.getModuleBaseURL() + "images/basketicons/filetype/zip.gif";
	/**
	 * BASKET_HTML
	 */
	public static final String BASKET_HTML = GWT.getModuleBaseURL() + "images/basketicons/xhtml.png";
}
