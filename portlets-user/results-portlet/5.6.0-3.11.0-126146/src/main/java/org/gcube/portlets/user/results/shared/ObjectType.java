package org.gcube.portlets.user.results.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum ObjectType implements IsSerializable{
	GENERIC,
	SPD,
	OAI,
	FIGIS
}
