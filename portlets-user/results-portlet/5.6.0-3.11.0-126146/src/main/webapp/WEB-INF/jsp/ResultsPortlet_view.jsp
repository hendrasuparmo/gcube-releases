<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%-- Uncomment below lines to add portlet taglibs to jsp
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<portlet:defineObjects />
--%>

<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/results_portlet/results_portlet.nocache.js"></script>
<div id="resultsetTOP"></div>
<div id="resultsetDIV"></div>
<div id="maskdiv" class="mask" style="display: none"></div>

