
package org.gcube.portlets.user.results.server.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.gcube.portal.custom.scopemanager.scopehelper.ScopeHelper;

/**
 * ResultsPortlet Portlet Class
 * @author massi
 */
public class ResultsPortlet extends GenericPortlet {

	public void doView(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

		ScopeHelper.setContext(request);

		PortletRequestDispatcher dispatcher =
	        getPortletContext().getRequestDispatcher("/WEB-INF/jsp/ResultsPortlet_view.jsp");
	    dispatcher.include(request, response);		
	}


	public void processAction(ActionRequest request, ActionResponse response)
			throws PortletException, IOException {
	}

}
