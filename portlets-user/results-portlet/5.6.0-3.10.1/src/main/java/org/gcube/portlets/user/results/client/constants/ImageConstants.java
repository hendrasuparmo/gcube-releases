package org.gcube.portlets.user.results.client.constants;

import com.google.gwt.core.client.GWT;


	/**
	 * Simply a class conatining Static Constants 
	 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
	 * @version september 20010 (3.1) 
	 */
	public class ImageConstants {		
		
		public static final String COLLECTIONS_ICON = GWT.getModuleBaseURL() + "../images/page_white_stack.png";
		/**
		 * 
		 */	
		public static final String AJAX_LOADER = GWT.getModuleBaseURL() + "../images/ajax-loader.gif";
		/**
		 * top o fthe page
		 */	
		public static final String ARROW_UP = GWT.getModuleBaseURL() + "../images/arrow_up.gif";
		/**
		 * 
		 */
		public static final String DRAG_HANDLE = GWT.getModuleBaseURL() + "../images/draghandle.gif";
		/**
		 * 
		 */	
		public static final String CLOSE = GWT.getModuleBaseURL() + "../images/close.gif";
		
		/**
		 * 
		 */	
		public static final String INNER_RECORD_LOADER = GWT.getModuleBaseURL() + "../images/arrows-loading.gif";
		/**
		 * 
		 */	
		public static final String D4S_EYE = GWT.getModuleBaseURL() + "../images/d4science_eye.png";
		/**
		 * 
		 */	
		public static final String YOU_CAN_DRAG = GWT.getModuleBaseURL() + "../images/showsYouCanDrag.gif";
		/**
		 * 
		 */	
		public static final String ICO = GWT.getModuleBaseURL() + "../images/d4science_ball.png";
		
		/**
		 * 
		 */	
		public static final String WORLD = GWT.getModuleBaseURL() + "../images/world.gif";
		
		/**
		 * 
		 */	
		public static final String OPEN_TREE = GWT.getModuleBaseURL() + "../images/home-root.gif";
		/**
		 * 
		 */	
		public static final String IMAGE_HOME = GWT.getModuleBaseURL() + "../images/house.png";
		/**
		 * 
		 */	
		public static final String IMAGE_BASKET = GWT.getModuleBaseURL() + "../images/basket.png";
		/**
		 * 
		 */	
		public static final String IMAGE_FOLDER = GWT.getModuleBaseURL() + "../images/folder.png";
		/**
		 * 
		 */	
		public static final String IMAGE_REFRESH = GWT.getModuleBaseURL() + "../images/arrow_refresh_small.png";
		/**
		 * 
		 */	
		public static final String IMAGE_NEXT_PAGE = GWT.getModuleBaseURL() + "../images/control_next_blue.png";
		/**
		 * 
		 */
		public static final String IMAGE_PREV_PAGE = GWT.getModuleBaseURL() + "../images/control_back_blue.png";
		
		/**
		 * 
		 */
		public static final String IMAGE_FIRST_PAGE = GWT.getModuleBaseURL() + "../images/control_start_blue.png";
		
		/**
		 * PDF
		 */
		public static final String PDF_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/pdf.gif";
		/**
		 * XML
		 */
		public static final String XML_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/xml.gif";
		/**
		 * HTML
		 */
		public static final String HTML_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/html.gif";
		/**
		 * GIF_ICON
		 */
		public static final String GIF_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/gif.gif";
		/**
		 * JPG_ICON
		 */
		public static final String JPG_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/jpg.gif";
		/**
		 * BMP_ICON
		 */
		public static final String BMP_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/bmp.gif";
		/**
		 * TIFF_ICON
		 */
		public static final String TIFF_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/tiff.gif";
		/**
		 * PNG_ICON
		 */
		public static final String PNG_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/png.gif";
		/**
		 * URI_ICON
		 */
		public static final String URI_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/uri.gif";
		/**
		 * GOOGLE_ICON
		 */
		public static final String GOOGLE_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/google.gif";
		
		/**
		 * UNKNOWN_ICON
		 */
		public static final String UNKNOWN_ICON = GWT.getModuleBaseURL() + "../images/mimeicons/unknown.gif";
		/**
		 * BASKET_QUERY
		 */
		public static final String BASKET_QUERY = GWT.getModuleBaseURL() + "../images/basketicons/query.png";
	
		/**
		 * BASKET_EXTERNAL
		 */
		public static final String BASKET_EXTERNAL = GWT.getModuleBaseURL() + "../images/basketicons/application.png";
		/**
		 * BASKET_EXTERNAL IMAGE
		 */
		public static final String BASKET_EXTERNAL_IMAGE = GWT.getModuleBaseURL() + "../images/basketicons/jpg.gif";
		/**
		 * BASKET_EXTERNAL IMAGE
		 */
		public static final String BASKET_EXTERNAL_PDF = GWT.getModuleBaseURL() + "../images/basketicons/page_white_acrobat.png";
		/**
		 * BASKET_METADATA
		 */
		public static final String BASKET_METADATA = GWT.getModuleBaseURL() +  "../images/basketicons/gCube_Metadata.gif";
		/**
		 * BASKET_ANNOTATIONS
		 */
		public static final String BASKET_ANNOTATION = GWT.getModuleBaseURL() + "../images/basketicons/gCube_Annotation.gif";
		
		/**
		 * PART
		 */
		public static final String BASKET_PART= GWT.getModuleBaseURL() + "../images/basketicons/gCube_Part.gif";
		/**
		 * ALTERNATIVE
		 */
		public static final String BASKET_ALTERNATIVE= GWT.getModuleBaseURL() + "../images/basketicons/gCube_alter.gif";
		
		/**
		 * GOOGLE
		 */
		public static final String BASKET_GOOGLE= GWT.getModuleBaseURL() + "../images/basketicons/gCube_google.gif";
		/**
		 * BASKET_GENERIC_DSO
		 */
		public static final String BASKET_GENERIC_DSO= GWT.getModuleBaseURL() + "../images/basketicons/gCube_digiObject.gif";
		/**
		 * BASKET_GENERIC_DSO_LITTLE
		 */
		public static final String BASKET_GENERIC_DSO_TINY= GWT.getModuleBaseURL() + "../images/basketicons/gCube_digiObject_little.gif";
		/**
		 * BASKET_METADATA_LITTLE
		 */
		public static final String BASKET_METADATA_TINY = GWT.getModuleBaseURL() +  "../images/basketicons/gCube_Metadata_little.gif";
		/**
		 * BASKET_ANNOTATIONS
		 */
		public static final String BASKET_ANNOTATION_TINY = GWT.getModuleBaseURL() + "../images/basketicons/gCube_Annotation_little.gif";
		
		/**
		 * PART
		 */
		public static final String BASKET_PART_TINY= GWT.getModuleBaseURL() + "../images/basketicons/gCube_Part_little.gif";
		/**
		 * ALTERNATIVE
		 */
		public static final String BASKET_ALTERNATIVE_TINY= GWT.getModuleBaseURL() + "../images/basketicons/gCube_alter_little.gif";
		
		/**
		 * BASKET_ZIP
		 */
		public static final String BASKET_TEMPLATE = GWT.getModuleBaseURL() + "../images/basketicons/template.png";
		
		public static final String BASKET_REPORT = GWT.getModuleBaseURL() + "../images/basketicons/report.png";
		/**
		 * TIME SERIES
		 */
		public static final String BASKET_TS = GWT.getModuleBaseURL() + "../images/basketicons/timeseries.png";
		/**
		 * FOLDER
		 */
		public static final String FOLDER = GWT.getModuleBaseURL() + "../images/basketicons/folder.png";
		
		/**
		 * SHARED FOLDER
		 */
		public static final String SHARED_FOLDER = GWT.getModuleBaseURL() + "../images/basketicons/folder_user.png";
		
		/**
		 * 
		 */
		public static final String IMAGE_CLOSE_15x15 = GWT.getModuleBaseURL() + "../images/close.gif";
		/**
		 * Used when delpoying application in the portlet
		 */
		public static final int WHEN_DEPLOYED_TOP = 150;
		
	}

