package org.gcube.portlets.user.results.client.util;

public enum QuerySearchType {
		/**ADVANCED
		 */
		ADVANCED,
		/**
		 * SIMPLE
		 */
		SIMPLE,
		/**
		 * 
		 */
		BROWSE,
		/**
		 * 
		 */
		BROWSE_FIELDS,
		/**
		 * 
		 */
		GENERIC,
		/**
		 * 
		 */
		CQL_QUERY;
}
