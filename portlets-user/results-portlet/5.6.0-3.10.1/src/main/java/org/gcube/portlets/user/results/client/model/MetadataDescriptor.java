//package org.gcube.portlets.user.results.client.model;
//
//import com.google.gwt.user.client.rpc.IsSerializable;
//
//public class MetadataDescriptor implements IsSerializable {
//
//	private String oid;
//	private String format;
//	private String language;
//	private String contentFormattedXML;
//	private String contentFormatted;
//	
//	public MetadataDescriptor() { }
//
//	public MetadataDescriptor(String oid, String format, String language, String contentFormattedXML, String contentFormatted) {
//		super();
//		this.oid = oid;
//		this.format = format;
//		this.language = language;
//		this.contentFormattedXML = contentFormattedXML;
//		this.contentFormatted = contentFormatted;
//	}
//
//	public String getContentFormatted() {
//		return contentFormatted;
//	}
//
//	public void setContentFormatted(String contentFormatted) {
//		this.contentFormatted = contentFormatted;
//	}
//
//	public String getContentFormattedXML() {
//		return contentFormattedXML;
//	}
//
//	public void setContentFormattedXML(String contentFormattedXML) {
//		this.contentFormattedXML = contentFormattedXML;
//	}
//
//	public String getFormat() {
//		return format;
//	}
//
//	public void setFormat(String format) {
//		this.format = format;
//	}
//
//	public String getLanguage() {
//		return language;
//	}
//
//	public void setLanguage(String language) {
//		this.language = language;
//	}
//
//	public String getOid() {
//		return oid;
//	}
//
//	public void setOid(String oid) {
//		this.oid = oid;
//	}
//
//	
//	
//	
//	
//	
//	
//}
