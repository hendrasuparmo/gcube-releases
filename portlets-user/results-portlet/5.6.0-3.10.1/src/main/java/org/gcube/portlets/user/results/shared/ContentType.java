package org.gcube.portlets.user.results.shared;

public enum ContentType {
	MAIN,
	ALTERNATIVE
}
