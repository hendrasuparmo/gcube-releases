/**
 *
 */
package org.gcube.portlets.user.gisviewerapp.client;


/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 9, 2014
 *
 */
public class ConstantGisViewerApp {

	public static final String GIS_VIEWER_APP = "Gis Viewer App";
	public static final String GET_WMS_PARAMETER = "wmsrequest";
	public static final String GET_UUID_PARAMETER = "uuid";
	public static final String WMS_PARAM_SEPARATOR_REPLACEMENT_KEY = "separtor";

//	public static final Logger logger = Logger.getLogger("GisViewerApp");

}
