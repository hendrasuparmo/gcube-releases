
package org.gcube.portlets.user.gisviewerapp.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 22, 2016
 */
@RemoteServiceRelativePath("gisapp")
public interface GisViewerAppService extends RemoteService {


//	/**
//	 * Gets the styles for wms request.
//	 *
//	 * @param wmsRequest the wms request
//	 * @return the styles for wms request
//	 */
//	GeoStyles getStylesForWmsRequest(String wmsRequest);
//
//	/**
//	 * @param wmsRequest
//	 * @param displayName
//	 * @return
//	 * @throws Exception
//	 */
//	GeoInformation getParametersForWmsRequest(
//		String wmsRequest, String displayName) throws Exception;
}
