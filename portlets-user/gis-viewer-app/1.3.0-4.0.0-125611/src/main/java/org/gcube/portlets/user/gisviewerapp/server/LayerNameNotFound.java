/**
 *
 */

package org.gcube.portlets.user.gisviewerapp.server;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 22, 2016
 */
public class LayerNameNotFound extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param string
	 */
	public LayerNameNotFound(String string) {

		super(string);
	}
}
