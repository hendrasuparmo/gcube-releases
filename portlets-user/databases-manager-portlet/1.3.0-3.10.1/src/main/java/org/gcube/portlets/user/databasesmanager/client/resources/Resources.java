package org.gcube.portlets.user.databasesmanager.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

	@Source("search.png")
	ImageResource iconSearch();

	@Source("cancel.png")
	ImageResource iconCancel();

	@Source("database.png")
	ImageResource iconDatabase();

	@Source("schema.png")
	ImageResource iconSchema();
}
