package org.gcube.portlets.user.databasesmanager.shared;

public class Constants {
	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext";
	public final static String DEFAULT_TOKEN = "16e65d4f-11e0-4e4a-84b9-351688fccc12-98187548";

	// SClientMap
	public static final String SClientMap = "DataMinerClientMap";

	// Div Gwt
	public static final String CONTENTDIV = "contentDiv";

	// Toolbar button
	public static final String TABLESLIST = "Tables List";
	public static final String SUBMITQUERY = "Submit Query";
	public static final String GETINFO = "Get Info";
	public static final String TABLEDETAILS = "Table Details";
	public static final String SAMPLING = "Sampling";
	public static final String SMARTSAMPLING = "Smart Sampling";
	public static final String RANDOMSAMPLING = "Random Sampling";
	public static final String REFRESHCACHEDDATA = "Refresh Data";

	// sql Dialects
	public static final String POSTGRES = "POSTGRES";
	public static final String MYSQL = "MYSQL";
	public static final String NONE = "NONE";

	// element types
	// public static final String DATABASE="Database";
	// public static final String SCHEMA="Schema";

	// elements type
	public static final String RESOURCESLIST = "ResourcesList";
	public static final String RESOURCE = "resource";
	public static final String DATABASE = "database";
	public static final String SCHEMA = "schema";

	// algorithmID
	/*
	public static final String ALGID_GETRESOURCE = "LISTDBNAMES";
	public static final String ALGID_GETDBINFO = "LISTDBINFO";
	public static final String ALGID_GETDBSCHEMA = "LISTDBSCHEMA";
	public static final String ALGID_GETTABLES = "LISTTABLES";
	public static final String ALGID_GETTABLEDETAILS = "GETTABLEDETAILS";
	public static final String ALGID_SUBMITQUERY = "SUBMITQUERY";
	public static final String ALGID_SAMPLEONTABLE = "SAMPLEONTABLE";
	public static final String ALGID_SMARTSAMPLEONTABLE = "SMARTSAMPLEONTABLE";
	public static final String ALGID_RANDOMSAMPLEONTABLE = "RANDOMSAMPLEONTABLE";
	*/
}
