package org.gcube.portlets.user.td.chartswidget.client;

import com.google.gwt.i18n.client.Messages;


/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface ChartsWidgetTDMessages extends Messages {

	@DefaultMessage("Chart Creation")
	String chartCreation();
}
