package org.gcube.portlets.user.td.expressionwidget.client.type;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ReplaceExpressionType {
	 AddColumn, Replace, Template;
}