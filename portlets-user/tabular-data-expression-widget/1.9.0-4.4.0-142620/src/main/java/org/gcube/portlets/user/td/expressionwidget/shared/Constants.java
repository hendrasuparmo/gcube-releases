package org.gcube.portlets.user.td.expressionwidget.shared;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {

	public final static String VERSION = "2.4.0";
	public final static String DEFAULT_USER = "test.user";
	public final static String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	
	public static final String PARAMETER_ENCODING = "encoding";
	public static final String PARAMETER_HASHEADER = "hasHeader";
	public static final String PARAMETER_SEPARATOR = "separator";
	public static final String PARAMETER_COLUMNS = "columns";
	public static final String PARAMETER_URL = "url";
	
	public static final String PARAMETER_REGISTRYBASEURL ="registryBaseUrl";
	public static final String PARAMETER_AGENCY ="agency";
	public static final String PARAMETER_ID ="id";
	public static final String PARAMETER_VERSION ="version";
	
	public static final String C_EXPRESSION_MAP_SERVLET="CExpressionMapServlet";
	
}
