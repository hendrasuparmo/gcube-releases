package org.gcube.portlets.user.td.expressionwidget.shared.exception;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class ReplaceTypeMapException extends Exception {

	private static final long serialVersionUID = -9066034060104406559L;

	/**
	 * 
	 */
	public ReplaceTypeMapException() {
		super();
	}

	/**
	 * 
	 * @param message
	 *            Message
	 */
	public ReplaceTypeMapException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param message
	 *            Message
	 * @param throwable
	 *            Error
	 */
	public ReplaceTypeMapException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
