package org.gcube.portlets.user.td.expressionwidget.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum ReplaceExpressionType {
	 AddColumn, Replace, Template;
}