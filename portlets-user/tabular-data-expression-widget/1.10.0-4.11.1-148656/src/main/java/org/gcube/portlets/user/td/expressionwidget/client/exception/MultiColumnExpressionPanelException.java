package org.gcube.portlets.user.td.expressionwidget.client.exception;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public class MultiColumnExpressionPanelException extends Exception {

	private static final long serialVersionUID = -4619188268507480346L;

	public MultiColumnExpressionPanelException(String message) {
		super(message);
	}

	public MultiColumnExpressionPanelException(String message, Throwable cause) {
		super(message, cause);
	}

}
