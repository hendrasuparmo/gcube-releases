package org.gcube.portlets.user.gcubeloggedin.client;

import com.google.gwt.core.client.GWT;

public class UIConstants {
	
	public static final String LOADING_IMAGE = GWT.getModuleBaseURL() + "../images/loading-bar.gif";
	
	public static final String LOADINGE = GWT.getModuleBaseURL() + "../images/loading.gif";
	
	public static final String HOME = GWT.getModuleBaseURL() + "../images/home.png";

}
