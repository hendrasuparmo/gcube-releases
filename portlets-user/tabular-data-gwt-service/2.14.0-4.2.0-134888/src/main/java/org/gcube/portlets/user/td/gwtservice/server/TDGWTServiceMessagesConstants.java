package org.gcube.portlets.user.td.gwtservice.server;

/**
 * 
 * @author giancarlo email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class TDGWTServiceMessagesConstants {

	public static final String TDGWTServiceMessages = "org.gcube.portlets.user.td.gwtservice.server.TDGWTServiceMessages";

	public static final String sessionExpired = "sessionExpired";

	public static final String securityExceptionRights = "securityExceptionRights";
	
	public static final String tabularResourceIsLocked = "tabularResourceIsLocked";
	
	public static final String tabularResourceIsFinal = "tabularResourceIsFinal";
	
	public static final String noValidTabularResourceIdPresent = "noValidTabularResourceIdPresent";
	
	public static final String operationNotAllowedOnFlow = "operationNotAllowedOnFlow";
	
	public static final String templateNotCompatibleException = "templateNotCompatibleException";
	
	
	

}
