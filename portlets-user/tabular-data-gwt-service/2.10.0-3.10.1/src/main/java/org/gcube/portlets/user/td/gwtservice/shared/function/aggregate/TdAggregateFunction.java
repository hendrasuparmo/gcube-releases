package org.gcube.portlets.user.td.gwtservice.shared.function.aggregate;


/**
 * 
 * @author "Giancarlo Panichi" email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 * 
 */
public class TdAggregateFunction extends TdBaseComboDataBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2341132987652197369L;

	/**
	 * 
	 */
	public TdAggregateFunction() {
	}

	/**
	 * @param id
	 * @param label
	 */
	public TdAggregateFunction(String id, String label) {
		super(id, label);
	}

}
