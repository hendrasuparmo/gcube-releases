package org.gcube.portlets.user.td.gwtservice.server;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class TDGWTServiceMessagesConstants {

	public static final String TDGWTServiceMessages = "org.gcube.portlets.user.td.gwtservice.server.TDGWTServiceMessages";

	public static final String sessionExpired = "sessionExpired";

	public static final String securityExceptionRights = "securityExceptionRights";
	
	public static final String tabularResourceIsLocked = "tabularResourceIsLocked";
	
	public static final String tabularResourceIsFinal = "tabularResourceIsFinal";
	
	public static final String noValidTabularResourceIdPresent = "noValidTabularResourceIdPresent";
	
	public static final String operationNotAllowedOnFlow = "operationNotAllowedOnFlow";
	
	public static final String templateNotCompatibleException = "templateNotCompatibleException";
	
	
	

}
