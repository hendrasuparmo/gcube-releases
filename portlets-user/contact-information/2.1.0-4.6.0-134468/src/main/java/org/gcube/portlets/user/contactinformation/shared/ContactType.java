package org.gcube.portlets.user.contactinformation.shared;

/**
 *Types of contact information
 * @author Massimiliano Assante at ISTI-CNR 
 * (massimiliano.assante@isti.cnr.it)
 */
public enum ContactType {
	GOOGLE, TWITTER, IN, FB, EMAIL, AIM, SKYPE, PHONE, WEBSITE;
}
