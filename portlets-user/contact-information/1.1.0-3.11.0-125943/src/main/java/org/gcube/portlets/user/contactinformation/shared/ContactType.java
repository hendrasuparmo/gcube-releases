package org.gcube.portlets.user.contactinformation.shared;

public enum ContactType {
	GOOGLE, TWITTER, IN, FB, EMAIL, AIM, SKYPE, PHONE, WEBSITE;
}
