/**
 * 
 */
package org.gcube.portlets.user.td.taskswidget.shared.job;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public enum SaveFileFormat {
	
	//OCCURRENCES
	CSV,
	DARWIN_CORE,
	
	//TAXONOMY
	DARWIN_CORE_ARCHIVE;
}
