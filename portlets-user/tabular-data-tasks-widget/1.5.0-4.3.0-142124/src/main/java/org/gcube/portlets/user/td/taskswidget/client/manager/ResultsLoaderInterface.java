/**
 * 
 */
package org.gcube.portlets.user.td.taskswidget.client.manager;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Dec 6, 2013
 *
 */
public interface ResultsLoaderInterface {
	
	void onResulTabulartUpdated(boolean b);
	
	void onResulCollateralUpdated(boolean b);
}
