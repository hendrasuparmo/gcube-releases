package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowManageProductWidgetEventHandler extends EventHandler {

	void onShowManageProductWidget(ShowManageProductWidgetEvent showManageProductWidgetEvent);

}
