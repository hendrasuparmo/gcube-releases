package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowRevertOperationWidgetEventHandler extends EventHandler {
	
	void onShowRevertOperationWidgetEvent(ShowRevertOperationWidgetEvent showRevertOperationWidget);

}
