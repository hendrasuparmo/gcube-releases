/**
 *
 */
package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared;


/**
 * The Ckan role list
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @author Costantino Perciante costantino.perciante@isti.cnr.it
 * Jun 9, 2016
 */
public enum CkanRole {
	MEMBER,
	EDITOR,
	ADMIN
}
