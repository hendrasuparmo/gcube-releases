/**
 * 
 */
package org.gcube.portlets.user.speciesdiscovery.shared;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface FetchingElement {
	
	public int getId();

}
