<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
 
<!--                                           -->
<!-- The module reference below is the link    -->
<!-- between html and your Web Toolkit module  -->		
                                           
<script src='<%=request.getContextPath()%>/speciesdiscovery/speciesdiscovery.nocache.js'></script>
    
<link rel="stylesheet" href="<%= request.getContextPath()%>/SpeciesDiscovery.css" type="text/css">
<link rel="stylesheet" href="<%= request.getContextPath()%>/gxt/css/gxt-all.css" type="text/css">
 
<div id="SpeciesDiscovery" style="width: 100%; height: 100%">
</div>
