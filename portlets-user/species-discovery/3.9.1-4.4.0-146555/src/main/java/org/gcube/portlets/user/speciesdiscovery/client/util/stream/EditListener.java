/**
 * 
 */
package org.gcube.portlets.user.speciesdiscovery.client.util.stream;

import com.extjs.gxt.ui.client.store.Record;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface EditListener {
	
	public void onEdit(Record record);

}
