/**
 * 
 */
package org.gcube.portlets.user.speciesdiscovery.shared;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public enum SaveFileFormat {
	
	//OCCURRENCES
	CSV,
	DARWIN_CORE,
	
	//TAXONOMY
	DARWIN_CORE_ARCHIVE;
}
