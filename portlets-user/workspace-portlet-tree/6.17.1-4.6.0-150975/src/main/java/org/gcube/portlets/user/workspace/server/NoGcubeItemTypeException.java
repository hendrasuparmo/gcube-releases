/**
 *
 */
package org.gcube.portlets.user.workspace.server;

/**
 * The Class NoGcubeItemTypeException.
 *
 * @author Federico De Faveri defaveri@isti.cnr.it
 */
public class NoGcubeItemTypeException extends Exception{

	private static final long serialVersionUID = 3660227882791095368L;

	/**
	 * Instantiates a new no gcube item type exception.
	 *
	 * @param message the message
	 */
	public NoGcubeItemTypeException(String message) {
		super(message);
	}



}
