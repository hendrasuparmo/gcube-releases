/**
 * 
 */
package org.gcube.portlets.user.workspace.client.util;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public enum ImageRequestType {
	
	/**
	 * If the entire image is request.
	 */
	IMAGE,
	
	/**
	 * If the image thumbnail is request.
	 */
	THUMBNAIL;
}
