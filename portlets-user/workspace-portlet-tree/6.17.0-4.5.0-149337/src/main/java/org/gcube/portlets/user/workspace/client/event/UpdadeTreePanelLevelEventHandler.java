package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface UpdadeTreePanelLevelEventHandler extends EventHandler {
	void onUpdateTreePanel(UpdadeTreePanelLevelEvent event);
}