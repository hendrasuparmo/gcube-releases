/**
 *
 */
package org.gcube.portlets.user.workspace.server;

/**
 * The Enum MetadataFormat.
 *
 * @author Federico De Faveri defaveri@isti.cnr.it
 */
public enum MetadataFormat {
	/**
	 * HTML metadata format.
	 */
	HTML,

	/**
	 * XML formatted metadata.
	 */
	FORMATTED_XML,

	/**
	 * raw metadata XML.
	 */
	RAW_XML,

	/**
	 * The XML raw metadata as html.
	 */
	RAW_XML_AS_HTML;

}
