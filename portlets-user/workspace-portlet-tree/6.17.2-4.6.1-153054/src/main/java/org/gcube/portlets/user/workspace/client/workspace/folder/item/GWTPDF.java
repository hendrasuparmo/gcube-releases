package org.gcube.portlets.user.workspace.client.workspace.folder.item;

public interface GWTPDF {

	/**
	 * @return the author
	 */
	public String getAuthor();

	/**
	 * @return the numberOfPages
	 */
	public int getNumberOfPages();

	/**
	 * @return the producer
	 */
	public String getProducer();

	/**
	 * @return the title
	 */
	public String getTitle();

	/**
	 * @return the version
	 */
	public String getVersion();

}