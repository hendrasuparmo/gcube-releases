package org.gcube.portlets.user.workspace.server.util.resource;

@SuppressWarnings("serial")
public class PropertyFileReadingErrorException extends Exception {
	 public PropertyFileReadingErrorException(String message) {
	    super(message);
	  }
}