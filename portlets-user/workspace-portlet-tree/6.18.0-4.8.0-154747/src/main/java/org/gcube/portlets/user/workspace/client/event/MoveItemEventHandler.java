package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MoveItemEventHandler extends EventHandler {
	void onMoveItem(MoveItemEvent event);
}