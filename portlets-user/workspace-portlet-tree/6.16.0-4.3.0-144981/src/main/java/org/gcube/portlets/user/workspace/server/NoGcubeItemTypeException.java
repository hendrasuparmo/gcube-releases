/**
 * 
 */
package org.gcube.portlets.user.workspace.server;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public class NoGcubeItemTypeException extends Exception{

	private static final long serialVersionUID = 3660227882791095368L;

	public NoGcubeItemTypeException(String message) {
		super(message);
	}
	
	

}
