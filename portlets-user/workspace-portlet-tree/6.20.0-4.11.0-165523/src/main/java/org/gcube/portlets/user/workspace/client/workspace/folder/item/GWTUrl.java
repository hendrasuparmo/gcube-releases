/**
 * 
 */
package org.gcube.portlets.user.workspace.client.workspace.folder.item;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public interface GWTUrl {
	
	public String getUrl();

}
