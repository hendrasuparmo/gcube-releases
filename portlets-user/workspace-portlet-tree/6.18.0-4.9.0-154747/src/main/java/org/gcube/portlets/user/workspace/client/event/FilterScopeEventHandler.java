package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface FilterScopeEventHandler extends EventHandler {
	void onClickScopeFilter(FilterScopeEvent filterScopeEvent);
}