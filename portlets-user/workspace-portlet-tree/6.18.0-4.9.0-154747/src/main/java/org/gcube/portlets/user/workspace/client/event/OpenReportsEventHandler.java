package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface OpenReportsEventHandler extends EventHandler {
	void onClickOpenReports(OpenReportsEvent openReportTemplateEvent);
}