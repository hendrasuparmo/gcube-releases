package org.gcube.portlets.user.workspace.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Icons extends ClientBundle {

	@Source("icons/cloud-drive.png")
	ImageResource cloudDrive();

	@Source("icons/table.png")
	ImageResource table();

	@Source("icons/close.gif")
	ImageResource close();

	@Source("icons/mime/gif.gif")
	ImageResource gif();

	@Source("icons/mime/pdf.gif")
	ImageResource pdf();

	@Source("icons/mime/jpeg.gif")
	ImageResource jpeg();

	@Source("icons/mime/png.gif")
	ImageResource png();

	@Source("icons/mime/tiff.gif")
	ImageResource tiff();

	@Source("icons/mime/svg.png")
	ImageResource svg();

	@Source("icons/mime/xml.gif")
	ImageResource xml();

	@Source("icons/mime/ico_htm.gif")
	ImageResource html();

	@Source("icons/mime/doc.gif")
	ImageResource doc();

	@Source("icons/mime/txt.gif")
	ImageResource txt();

	@Source("icons/mime/movie.png")
	ImageResource movie();

	@Source("icons/new-folder.png")
	ImageResource addFolder();

	@Source("icons/mime/deletefolder.jpg")
	ImageResource deleteFolder();

	@Source("icons/mime/delete.png")
	ImageResource deleteItem();

	@Source("icons/rename2.png")
	ImageResource renameItem();

	@Source("icons/folder.png")
	ImageResource folder();

	@Source("icons/mime/addfolder32.png")
	ImageResource addFolder32();

	@Source("icons/mime/delete.png")
	ImageResource deleteItem32();

	@Source("icons/mime/renameitem32.png")
	ImageResource renameItem32();

	@Source("icons/mime/uploadfile32_2.gif")
	ImageResource uploadFile32();

	@Source("icons/upload2.png")
	ImageResource uploadFile();

	@Source("icons/mime/audio.png")
	ImageResource audio();

	@Source("icons/mime/download.png")
	ImageResource download();

	@Source("icons/mime/forward.png")
	ImageResource separatorPath();

	@Source("icons/mime/search16.png")
	ImageResource search();

	@Source("icons/mime/search.png")
	ImageResource search2();

	@Source("icons/mime/harddisk.png")
	ImageResource hardDisk();

	@Source("icons/mime/archiveupload.png")
	ImageResource archiveUpload();

	@Source("icons/mime/buttoncancel.gif")
	ImageResource cancel();

	@Source("icons/mime/togglelist.png")
	ImageResource toggleList();

	@Source("icons/mime/togglegroup.png")
	ImageResource toggleGroup();

	@Source("icons/mime/toggleicon.png")
	ImageResource toggleIcon();

	@Source("icons/aquamaps.png")
	ImageResource biodiversity();

	@Source("icons/external_image.gif")
	ImageResource images();

	@Source("icons/external_url.png")
	ImageResource links();

	@Source("icons/report.png")
	ImageResource report();

	@Source("icons/timeseries.png")
	ImageResource timeSeries();

	@Source("icons/document.png")
	ImageResource documents();

	@Source("icons/mime/save.gif")
	ImageResource save();

	@Source("icons/mime/star.png")
	ImageResource star();

	@Source("icons/preview.png")
	ImageResource preview();

	@Source("icons/rightarrow.gif")
	ImageResource show();

	@Source("icons/link.gif")
	ImageResource openUrl();

	@Source("icons/linkadd.png")
	ImageResource addUrl();

	@Source("icons/send.png")
	ImageResource sendTo();

	@Source("icons/checkuser.png")
	ImageResource checkUser();

	@Source("icons/inbox-download.png")
	ImageResource inboxReceived();

	@Source("icons/inbox-upload.png")
	ImageResource inboxSent();

	@Source("icons/email2.png")
	ImageResource email();

	@Source("icons/email-open.png")
	ImageResource openEmail();

	@Source("icons/save-attach.png")
	ImageResource saveAttachs();

	@Source("icons/download-email.png")
	ImageResource downloadEmail();

	@Source("icons/email-read.png")
	ImageResource emailRead();

	@Source("icons/email-notread.png")
	ImageResource emailNotRead();

	@Source("icons/email-delete.png")
	ImageResource emailDelete();

	@Source("icons/email-forward.png")
	ImageResource emailForward();

	@Source("icons/copy.gif")
	ImageResource copy();

	@Source("icons/paste.png")
	ImageResource paste();

	@Source("icons/odp.png")
	ImageResource odp();

	@Source("icons/odt.png")
	ImageResource odt();

	@Source("icons/ods.png")
	ImageResource ods();

	@Source("icons/odg.png")
	ImageResource odg();

	@Source("icons/ott.png")
	ImageResource ott();

	@Source("icons/refresh.gif")
	ImageResource refresh();

	@Source("icons/loading.gif")
	ImageResource loading();

	@Source("icons/loading-off.gif")
	ImageResource loadingOff();

	@Source("icons/loading2.gif")
	ImageResource loading2();

	@Source("icons/delete.gif")
	ImageResource delete2();

	@Source("icons/webdavurl.png")
	ImageResource urlWebDav();

	@Source("icons/cancel.png")
	ImageResource removeFilter();

	@Source("icons/attach.png")
	ImageResource attach();

	@Source("icons/newmail.png")
	ImageResource createNewMail();

	@Source("icons/reporttemplate.png")
	ImageResource reportTemplate();

	@Source("icons/workflowreport.png")
	ImageResource workflowReport();

	@Source("icons/workflowtemplate.png")
	ImageResource workflowTemplate();

	@Source("icons/webdav.png")
	ImageResource webDav();

	@Source("icons/mail-reply.png")
	ImageResource replyMail();

	@Source("icons/mail-reply-all.png")
	ImageResource replyAllMail();

	@Source("icons/application-link.png")
	ImageResource resourceLink();

	@Source("icons/sharingFolder.png")
	ImageResource shareFolder();

	@Source("icons/folder_shared.png")
	ImageResource sharedFolder();

	@Source("icons/unSharingFolder.gif")
	ImageResource unShareFolder();

	@Source("icons/unShareUsers.png")
	ImageResource unShareUser();

	@Source("icons/categorize2.png")
	ImageResource gridView();

	@Source("icons/users.png")
	ImageResource users();

	@Source("icons/info-icon.png")
	ImageResource info();

	@Source("icons/mime/csv.gif")
	ImageResource csv();

	@Source("icons/excel.gif")
	ImageResource excel();

	@Source("icons/ppt.gif")
	ImageResource ppt();

	@Source("icons/read.png")
	ImageResource read();

	@Source("icons/notread.png")
	ImageResource notread();

	@Source("icons/history.png")
	ImageResource history();

	@Source("icons/cut.png")
	ImageResource cut();

	@Source("icons/new_create.png")
	ImageResource createNew();

	@Source("icons/get-link.png")
	ImageResource refreshFolder();

	@Source("icons/link-try.png")
	ImageResource shareLink();


	@Source("icons/openURL.png")
	ImageResource extLink();

	@Source("icons/sharelink.png")
	ImageResource publicLink();

//	@Source("icons/user_trash_full.png")
	@Source("icons/full_trash.png")
	ImageResource trash_full();

//	@Source("icons/user_trash.png")
	@Source("icons/empty_trash.png")
	ImageResource trash_empty();

	@Source("icons/readonly.png")
	ImageResource readonly();

	@Source("icons/writeown.png")
	ImageResource writeown();

	@Source("icons/writeall.png")
	ImageResource writeall();

	@Source("icons/specialfolder2.png")
	ImageResource vreFolder();

	@Source("icons/admin.png")
	ImageResource administrator();

	@Source("icons/add_admin.png")
	ImageResource addAdmin();

	@Source("icons/admin_keys.png")
	ImageResource manageAdmin();

	@Source("icons/specialfolder2.png")
	ImageResource specialFolder();

	@Source("icons/shareuser.png")
	ImageResource user();

	@Source("icons/sharegroup.png")
	ImageResource groupusers();

	/**
	 * @return
	 */
	@Source("icons/undo.png")
	ImageResource undo();

	/**
	 * @return
	 */
	@Source("icons/aoneleft.png")
	ImageResource selectedLeft();

	/**
	 * @return
	 */
	@Source("icons/aoneright.png")
	ImageResource selectedRight();

	/**
	 * @return
	 */
	@Source("icons/amoreleft.png")
	ImageResource allLeft();

	/**
	 * @return
	 */
	@Source("icons/amoreright.png")
	ImageResource allRight();

	/**
	 * @return
	 */
	@Source("icons/recycle.png")
	ImageResource recycle();

	/**
	 * @return
	 */
	@Source("icons/edit-permissions.png")
	ImageResource permissions();

	@Source("icons/pencil16.png")
	ImageResource pencil();

	@Source("icons/edit16.png")
	ImageResource edit();

	@Source("icons/gcubeItem.jpeg")
	ImageResource gcubeItem();

	@Source("icons/dvi.gif")
	ImageResource dvi();

	@Source("icons/ps.gif")
	ImageResource postscript();

	@Source("icons/shell-script.png")
	ImageResource shell();

	@Source("icons/tex.png")
	ImageResource tex();

	@Source("icons/zip.gif")
	ImageResource zip();

	@Source("icons/archive-icon.png")
	ImageResource archive();

	@Source("icons/java.png")
	ImageResource java();

	/**
	 * @return
	 */
	@Source("icons/hand.gif")
	ImageResource hand();

	@Source("icons/about.png")
	ImageResource about();

	/**
	 * @return
	 */
	@Source("icons/info.png")
	ImageResource information();

	@Source("icons/datacataloguepublish.png")
	ImageResource datacataloguepublish();

	@Source("icons/folder_public.png")
	ImageResource folderPublic();

	@Source("icons/folder_shared_public.png")
	ImageResource folderSharedPublic();

	@Source("icons/folder_public_remove.png")
	ImageResource folderPublicRemove();

	/**
	 * @return
	 */
	@Source("icons/versioning.png")
	ImageResource versioning();

	/**
	 * @return
	 */
	@Source("icons/thredds.gif")
	ImageResource thredds();
	
	/**
	 * Sync icon to.
	 *
	 * @return the image resource
	 */
	@Source("icons/sync-icon-to.png")
	ImageResource syncIconTo();
	
	@Source("icons/sync-icon-from.png")
	ImageResource syncIconFrom();
	
	@Source("icons/sync-icon-synched.png")
	ImageResource syncIconSynched();

}


