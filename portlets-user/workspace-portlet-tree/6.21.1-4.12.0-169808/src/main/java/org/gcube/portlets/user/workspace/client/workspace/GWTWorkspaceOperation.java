/**
 * 
 */
package org.gcube.portlets.user.workspace.client.workspace;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public enum GWTWorkspaceOperation {
	
	DELETE,
	RENAME;

}
