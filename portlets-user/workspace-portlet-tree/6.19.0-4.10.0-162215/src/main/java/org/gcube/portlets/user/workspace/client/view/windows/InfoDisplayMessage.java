package org.gcube.portlets.user.workspace.client.view.windows;



/**
 * 
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Jun 25, 2013
 *
 */
public class InfoDisplayMessage extends InfoDisplay{

	
	/**
	 * @param title
	 * @param text
	 */
	public InfoDisplayMessage(String title, String text) {
		super(title, text);
	}
	
	/**
	 * @param title
	 * @param text
	 */
	public InfoDisplayMessage(String title, String text, int milliseconds) {
		super(title, text, milliseconds);
	}
	
}
