/**
 * 
 */
package org.gcube.portlets.user.workspace.client.workspace.exceptions;


/**
 * The Class WrongItemTypeException.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 11, 2015
 */
public class WrongItemTypeException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = -6229136748121940285L;

	/**
	 * Instantiates a new wrong item type exception.
	 *
	 * @param message the message
	 */
	public WrongItemTypeException(String message) {
		super(message);
	}

}
