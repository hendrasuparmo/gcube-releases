package org.gcube.portlets.user.wswidget.shared;

public enum ImageType {
	DOC, PPT, XLS, NONE, MOVIE, HTML, PDF, IMAGE, RAR, ZIP, TXT  
}
