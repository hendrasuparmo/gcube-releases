package org.gcube.portlets.user.gisviewer.client.resources;

import org.gcube.portlets.user.gisviewer.client.GisViewer;

import com.google.gwt.user.client.ui.AbstractImagePrototype;

public class Images {

	public static AbstractImagePrototype iconAtompub() {
		return AbstractImagePrototype.create(GisViewer.resources.iconAtompub());
	}

	public static AbstractImagePrototype iconGeorss() {
		return AbstractImagePrototype.create(GisViewer.resources.iconGeorss());
	}

	public static AbstractImagePrototype iconGif() {
		return AbstractImagePrototype.create(GisViewer.resources.iconGif());
	}

	public static AbstractImagePrototype iconJpeg() {
		return AbstractImagePrototype.create(GisViewer.resources.iconJpeg());
	}

	public static AbstractImagePrototype iconKml() {
		return AbstractImagePrototype.create(GisViewer.resources.iconKml());
	}

	public static AbstractImagePrototype iconPdf() {
		return AbstractImagePrototype.create(GisViewer.resources.iconPdf());
	}

	public static AbstractImagePrototype iconPng() {
		return AbstractImagePrototype.create(GisViewer.resources.iconPng());
	}

	public static AbstractImagePrototype iconSvg() {
		return AbstractImagePrototype.create(GisViewer.resources.iconSvg());
	}

	public static AbstractImagePrototype iconTiff() {
		return AbstractImagePrototype.create(GisViewer.resources.iconTiff());
	}

	public static AbstractImagePrototype iconExport() {
		return AbstractImagePrototype.create(GisViewer.resources.iconExport());
	}

	public static AbstractImagePrototype iconFilter() {
		return AbstractImagePrototype.create(GisViewer.resources.iconFilter());
	}

	public static AbstractImagePrototype iconTable() {
		return AbstractImagePrototype.create(GisViewer.resources.iconTable());
	}

	public static AbstractImagePrototype iconRefresh() {
		return AbstractImagePrototype.create(GisViewer.resources.iconRefresh());
	}

	public static AbstractImagePrototype iconTransect() {
		return AbstractImagePrototype.create(GisViewer.resources.iconTransect());
	}

	public static AbstractImagePrototype iconLegend() {
		return AbstractImagePrototype.create(GisViewer.resources.iconLegend());
	}

	public static AbstractImagePrototype iconSave() {
		return AbstractImagePrototype.create(GisViewer.resources.iconSave());
	}

	public static AbstractImagePrototype iconExecuteQuery() {
		return AbstractImagePrototype.create(GisViewer.resources.iconArrowBlueRight());
	}

	public static AbstractImagePrototype iconRemoveCqlFilter() {
		return AbstractImagePrototype.create(GisViewer.resources.iconRemoveCqlFilter());
	}

	public static AbstractImagePrototype iconToolbarRemove() {
		return AbstractImagePrototype.create(GisViewer.resources.iconToolbarRemove());
	}

	
	public static AbstractImagePrototype iconMaxExtent() {
		return AbstractImagePrototype.create(GisViewer.resources.iconMaxExtent());
	}

	public static AbstractImagePrototype iconZoomIn() {
		return AbstractImagePrototype.create(GisViewer.resources.iconZoomIn());
	}

	public static AbstractImagePrototype iconZoomOut() {
		return AbstractImagePrototype.create(GisViewer.resources.iconZoomOut());
	}
	
	public static AbstractImagePrototype noLegendAvailable() {
		return AbstractImagePrototype.create(GisViewer.resources.noLegendAvailable());
	}

	public static AbstractImagePrototype iconPan() {
		return AbstractImagePrototype.create(GisViewer.resources.iconPan());
	}

	public static AbstractImagePrototype iconClickData() {
		return AbstractImagePrototype.create(GisViewer.resources.iconClickData());
	}

	public static AbstractImagePrototype iconBoxData() {
		return AbstractImagePrototype.create(GisViewer.resources.iconBoxData());
	}
	
	public static AbstractImagePrototype gisViewerIcon() {
		return AbstractImagePrototype.create(GisViewer.resources.gisViewerIcon());
	}
	
	public static AbstractImagePrototype loading() {
		return AbstractImagePrototype.create(GisViewer.resources.loadingImg());
	}
}
