/**
 * 
 */
package org.gcube.portlets.user.gisviewer.client.commons.beans;

import java.io.Serializable;

/**
 * @author ceras
 *
 */
public class LayerConfiguration implements Serializable {
//
//	String layerName;
//	
//	LayerItem layerItem;
//	
//	boolean isVisible = true;
//	
//	double opacity = Constants.defaultOpacityLayers; // TODO
//
//	public LayerConfiguration() {	
//	}
//	
//	/**
//	 * @param layerName
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(String layerName, boolean isVisible, double opacity) {
//		super();
//		this.layerName = layerName;
//		this.isVisible = isVisible;
//		this.opacity = opacity;
//	}
//
//	/**
//	 * @param layerName
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(String layerName, boolean isVisible) {
//		super();
//		this.layerName = layerName;
//		this.isVisible = isVisible;
//	}
//
//	/**
//	 * @param layerName
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(String layerName, double opacity) {
//		super();
//		this.layerName = layerName;
//		this.opacity = opacity;
//	}
//
//	/**
//	 * @param layerTitle
//	 */
//	public LayerConfiguration(String layerName) {
//		super();
//		this.layerName = layerName;
//	}
//
//	
//	
//	/**
//	 * @param layerItem
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(LayerItem layerItem, boolean isVisible, double opacity) {
//		super();
//		this.layerItem = layerItem;
//		this.isVisible = isVisible;
//		this.opacity = opacity;
//	}
//	
//	/**
//	 * @param layerItem
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(LayerItem layerItem, boolean isVisible) {
//		super();
//		this.layerItem = layerItem;
//		this.isVisible = isVisible;
//	}
//	
//	/**
//	 * @param layerItem
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(LayerItem layerItem, double opacity) {
//		super();
//		this.layerItem = layerItem;
//		this.opacity = opacity;
//	}
//	
//	/**
//	 * @param layerItem
//	 * @param isVisible
//	 * @param opacity
//	 */
//	public LayerConfiguration(LayerItem layerItem) {
//		super();
//		this.layerItem = layerItem;
//	}
//	
//	public LayerConfiguration(String title, String name, String wmsUrl, boolean isVisible, double opacity) {
//		super();
//		LayerItem layerItem = new LayerItem();
//		layerItem.setName(name);
//		layerItem.setTitle(title);
//		layerItem.setUrl(wmsUrl);
//		layerItem.setGeoserverUrl(wmsUrl);
//		layerItem.setGeoserverWmsUrl(wmsUrl);
//		layerItem.setExternal(true);
//		this.setLayerItem(layerItem);
//		this.setVisible(isVisible);
//		this.setOpacity(opacity);
//	}
//
//	/**
//	 * @return the layerName
//	 */
//	public String getLayerName() {
//		return layerName;
//	}
//
//	/**
//	 * @param layerName the layerName to set
//	 */
//	public void setLayerName(String layerName) {
//		this.layerName = layerName;
//	}
//
//	/**
//	 * @return the layerItem
//	 */
//	public LayerItem getLayerItem() {
//		return layerItem;
//	}
//
//	/**
//	 * @param layerItem the layerItem to set
//	 */
//	public void setLayerItem(LayerItem layerItem) {
//		this.layerItem = layerItem;
//	}
//
//	/**
//	 * @return the isVisible
//	 */
//	public boolean isVisible() {
//		return isVisible;
//	}
//
//	/**
//	 * @param isVisible the isVisible to set
//	 */
//	public void setVisible(boolean isVisible) {
//		this.isVisible = isVisible;
//	}
//
//	/**
//	 * @return the opacity
//	 */
//	public double getOpacity() {
//		return opacity;
//	}
//
//	/**
//	 * @param opacity the opacity to set
//	 */
//	public void setOpacity(double opacity) {
//		this.opacity = opacity;
//	}
//	
}
