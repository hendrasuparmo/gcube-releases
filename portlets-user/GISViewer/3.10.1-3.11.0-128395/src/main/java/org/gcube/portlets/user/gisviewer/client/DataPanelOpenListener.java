/**
 * 
 */
package org.gcube.portlets.user.gisviewer.client;

/**
 * @author ceras
 *
 */
public interface DataPanelOpenListener {
	public void dataPanelOpen(boolean isOpen, int panelHeight);
}
