/**
 * 
 */
package org.gcube.portlets.user.gisviewer.server.datafeature;


/**
 * @author ceras
 *
 */
public interface CXmlManager {
	
	public void manage(int index, CXml cXml);
	
}
