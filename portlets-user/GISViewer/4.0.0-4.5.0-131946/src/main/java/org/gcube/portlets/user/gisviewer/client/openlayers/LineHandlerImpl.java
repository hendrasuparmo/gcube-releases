package org.gcube.portlets.user.gisviewer.client.openlayers;

import org.gwtopenmaps.openlayers.client.util.JSObject;

public class LineHandlerImpl {
	public static native JSObject create() /*-{
	return $wnd.OpenLayers.Handler.Line;
}-*/;
}
