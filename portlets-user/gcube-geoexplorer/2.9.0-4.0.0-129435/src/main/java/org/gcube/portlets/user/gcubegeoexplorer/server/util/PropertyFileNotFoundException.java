package org.gcube.portlets.user.gcubegeoexplorer.server.util;

@SuppressWarnings("serial")
public class PropertyFileNotFoundException extends Exception {
	 public PropertyFileNotFoundException(String message) {
	    super(message);
	  }
}