/**
 * 
 */
package org.gcube.portlets.user.gcubegeoexplorer.client;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @May 15, 2013
 *
 */
public class ConstantsGcubeGeoExplorer {
	
//	public static final	String DEFAULT_SCOPE = "/d4science.research-infrastructures.eu/gCubeApps/EcologicalModelling"; //PRODUCTION
//	public static final String DEFAULT_SCOPE = "/gcube/devNext"; //DEVELOPMENT
	public static final String DEFAULT_SCOPE = "/gcube/devsec/devVRE"; //DEVELOPMENT
	
	public static final String TEST_USER = "test.user"; //DEVELOPMENT
}
