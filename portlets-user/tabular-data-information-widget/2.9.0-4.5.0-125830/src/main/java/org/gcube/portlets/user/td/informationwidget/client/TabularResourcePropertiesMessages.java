package org.gcube.portlets.user.td.informationwidget.client;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public interface TabularResourcePropertiesMessages extends Messages {

	//
	@DefaultMessage("Name")
	String nameLabel();
	
	@DefaultMessage("Name of Tabular Resource")
	String nameLabelToolTip();
	
	@DefaultMessage("Description")
	String descriptionLabel();
	
	@DefaultMessage("Description of Tabular Resource")
	String descriptionLabelToolTip();
	
	@DefaultMessage("Type")
	String typeLabel();
	
	@DefaultMessage("Type of Tabular Resource")
	String typeLabelToolTip();
	
	@DefaultMessage("Agency")
	String agencyLabel();
	
	@DefaultMessage("Agency")
	String agencyLabelToolTip();
	
	@DefaultMessage("Creation Date")
	String dateLabel();
	
	@DefaultMessage("Creation Date")
	String dateLabelToolTip();
	
	@DefaultMessage("Table Type")
	String tableTypeNameLabel();
	
	@DefaultMessage("Table Type")
	String tableTypeNameLabelToolTip();
	
	@DefaultMessage("Rights")
	String rightLabel();
	
	@DefaultMessage("Rights")
	String rightLabelToolTip();
	
	@DefaultMessage("Valid From")
	String validFromLabel();
	
	@DefaultMessage("Valid From")
	String validFromLabelToolTip();
	
	@DefaultMessage("Valid Until To")
	String validUntilToLabel();
	
	@DefaultMessage("Valid Until To")
	String validUntilToLabelToolTip();
	
	@DefaultMessage("Licence")
	String licencesLabel();
	
	@DefaultMessage("Licence")
	String licencesLabelToolTip();
	
	@DefaultMessage("Owner")
	String ownerLabel();
	
	@DefaultMessage("Owner")
	String ownerLabelToolTip();
	
	@DefaultMessage("Share")
	String shareLabel();
	
	@DefaultMessage("Share")
	String shareLabelToolTip();
	
	@DefaultMessage("Valid")
	String validLabel();
	
	@DefaultMessage("Valid")
	String validLabelToolTip();
	
	@DefaultMessage("Final")
	String finalizedLabel();
	
	@DefaultMessage("Final")
	String finalizedLabelToolTip();
	
	@DefaultMessage("Save")
	String saveButton();
	
	@DefaultMessage("Save")
	String saveButtonToolTip();
	
	@DefaultMessage("Validations")
	String validationsButton();
	
	@DefaultMessage("Validations")
	String validationsButtonToolTip();
	
	@DefaultMessage("Resources")
	String resourcesButton();
	
	@DefaultMessage("Resources")
	String resourcesButtonToolTip();
	
	
}