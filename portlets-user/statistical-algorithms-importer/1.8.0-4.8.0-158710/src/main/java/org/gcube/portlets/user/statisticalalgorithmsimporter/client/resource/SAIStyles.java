/**
 * 
 */
package org.gcube.portlets.user.statisticalalgorithmsimporter.client.resource;

import com.google.gwt.resources.client.CssResource;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public interface SAIStyles extends CssResource {

	@ClassName("ribbon")
	public String getRibbon();

	@ClassName("workAreaPanel")
	public String getWorkAreaPanel();
	
	@ClassName("logo")
	public String getLogo();

}
