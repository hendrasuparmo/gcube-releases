package org.gcube.portlets.user.statisticalalgorithmsimporter.client.tools.input;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public interface InputVariableMessages extends Messages {

	//
	@DefaultMessage("Input/Output")
	String inputOutputVariables();
	
	@DefaultMessage("Global Variables")
	String globalVariables();

	@DefaultMessage("Interpreter")
	String interpreterInfo();
	
	@DefaultMessage("Info")
	String projectInfo();
	
	
	
}