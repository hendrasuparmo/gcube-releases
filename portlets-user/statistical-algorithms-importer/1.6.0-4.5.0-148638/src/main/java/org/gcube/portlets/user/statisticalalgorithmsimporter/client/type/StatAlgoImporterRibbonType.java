package org.gcube.portlets.user.statisticalalgorithmsimporter.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum StatAlgoImporterRibbonType {
	PROJECT_CREATE,
	PROJECT_OPEN,
	PROJECT_SAVE,
	RESOURCE_ADD,
	RESOURCE_GITHUB,
	SOFTWARE_CREATE, 
	SOFTWARE_PUBLISH,
	SOFTWARE_REPACKAGE,
	HELP;
}
