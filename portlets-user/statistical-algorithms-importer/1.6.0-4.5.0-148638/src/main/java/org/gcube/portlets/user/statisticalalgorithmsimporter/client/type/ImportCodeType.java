package org.gcube.portlets.user.statisticalalgorithmsimporter.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum ImportCodeType {
	FILE,
	WORKSPACE; 
}
