package org.gcube.portlets.user.statisticalalgorithmsimporter.shared;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {

	public static final String APPLICATION_ID = "org.gcube.portlets.user.statisticalalgorithmsimporter.server.portlet.StatAlgoImporterPortlet";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_ID = "SAIId";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_COOKIE = "SAILangCookie";	
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_LANG = "SAILang";
	public final static String DEFAULT_USER = "giancarlo.panichi";
	//public final static String DEFAULT_SCOPE = "/gcube/devNext";
	public final static String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	public static final String FILE_UPLOADED_FIELD = "FileUploadedField";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_JAR_PUBLIC_LINK = "JarPublicLink";
	public static final String RECIPIENTS = "Recipients";
	
	
	
	
}
