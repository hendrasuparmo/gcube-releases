package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.session;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class SessionConstants {

	public static final String FILE_UPLOAD_MONITOR="FILE_UPLOAD_MONITOR";
	public static final String IMPORT_CODE_FILE_UPLOAD_SESSION = "IMPORT_CODE_FILE_UPLOAD_SESSION";
	public static final String PROJECT = "PROJECT";
	
	
}
