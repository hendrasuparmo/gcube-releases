package org.gcube.portlets.user.statisticalalgorithmsimporter.client.ribbon;

import com.google.gwt.i18n.client.Messages;


/**
 * 
 * @author giancarlo
 * 
 *
 */
public interface StatAlgoImporterRibbonMessages extends Messages {
	
	@DefaultMessage("Home")
	String home();
	
	
}