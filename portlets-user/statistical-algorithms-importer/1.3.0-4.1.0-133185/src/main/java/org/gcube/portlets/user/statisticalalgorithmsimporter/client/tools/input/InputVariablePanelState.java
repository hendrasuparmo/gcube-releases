package org.gcube.portlets.user.statisticalalgorithmsimporter.client.tools.input;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum InputVariablePanelState {
	CLOSED,
	OPENED;
}
