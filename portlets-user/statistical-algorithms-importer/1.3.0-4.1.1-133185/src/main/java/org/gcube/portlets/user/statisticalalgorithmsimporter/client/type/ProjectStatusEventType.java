package org.gcube.portlets.user.statisticalalgorithmsimporter.client.type;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ProjectStatusEventType {
	START,
	OPEN,
	MAIN_CODE_SET,
	UPDATE,
	ADD_RESOURCE,
	DELETE_RESOURCE,
	DELETE_MAIN_CODE,
	SOFTWARE_CREATED,
	SOFTWARE_PUBLISH,
	SOFTWARE_REPACKAGE,
	EXPLORER_REFRESH,
	SAVE;
	
	
}
