package org.gcube.portlets.user.statisticalalgorithmsimporter.client.ribbon;

import com.google.gwt.i18n.client.Messages;


/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public interface StatAlgoImporterRibbonMessages extends Messages {
	
	@DefaultMessage("Home")
	String home();
	
	
}