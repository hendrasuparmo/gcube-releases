package org.gcube.portlets.user.statisticalalgorithmsimporter.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum SessionExpiredType {
	EXPIRED,
	EXPIREDONSERVER;
	
}
