package org.gcube.portlets.user.statisticalalgorithmsimporter.shared;

/**
 * 
 * @author giancarlo email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class Constants {

	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String APPLICATION_ID = "org.gcube.portlets.user.statisticalalgorithmsimporter.server.portlet.StatAlgoImporterPortlet";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_ID = "SAIId";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_COOKIE = "SAILangCookie";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_LANG = "SAILang";

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext";
	public final static String DEFAULT_TOKEN = "16e65d4f-11e0-4e4a-84b9-351688fccc12-98187548";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	public static final String FILE_UPLOADED_FIELD = "FileUploadedField";
	public static final String STATISTICAL_ALGORITHMS_IMPORTER_JAR_PUBLIC_LINK = "JarPublicLink";
	public static final String RECIPIENTS = "Recipients";

	// Session
	public static final String CURR_GROUP_ID = "CURR_GROUP_ID";
	public static final String CURR_USER_ID = "CURR_USER_ID";

}
