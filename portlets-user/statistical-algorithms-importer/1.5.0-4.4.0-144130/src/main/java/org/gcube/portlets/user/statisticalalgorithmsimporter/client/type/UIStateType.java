package org.gcube.portlets.user.statisticalalgorithmsimporter.client.type;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum UIStateType {
	START,
	WAITING;
}
