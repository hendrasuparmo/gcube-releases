package org.gcube.portlets.user.statisticalalgorithmsimporter.client.tools.input;

/**
 * 
 * @author giancarlo
 * 
 *
 */
public enum InputVariablePanelState {
	CLOSED,
	OPENED;
}
