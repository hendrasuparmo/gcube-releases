package org.gcube.portlet.user.userstatisticsportlet.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {
	@Source("avatarLoader.gif")
	ImageResource avatarLoader();
	
	@Source("Avatar_default.png")
	ImageResource avatarDefaultImage();
}
