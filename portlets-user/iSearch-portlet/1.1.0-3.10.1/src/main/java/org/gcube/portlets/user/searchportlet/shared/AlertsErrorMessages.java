package org.gcube.portlets.user.searchportlet.shared;

public class AlertsErrorMessages
{	
	public static final String GenericQuerySubmissionFailure = "Failed to submit the query. Please try again";
	
	public static final String SimpleQuerySubmissionFailure = "Failed to submit the simple query. Please try again";

}
