package org.gcube.portlets.user.searchportlet.client;

public class SearchConstantsStrings
{

	protected static final String FREE_TEXT = "Free Text:";

	protected static final String FIELD_TITLE = "&nbsp;Selection Criteria:";
	
	public static final String SEARCH_IN_ANY_FIELD = "Any";
	
	protected static final String RS_FIELD_TITLE = "Initial Query:&nbsp; ";

	protected static final String RESULT_TITLE = "Choose sorting of results:";

	protected static final String SEARCH = "&nbsp;&nbsp;&nbsp;Search&nbsp;&nbsp;&nbsp;&nbsp;";

	protected static final String ADD_CONDITION = "Add Condition";
	
	protected static final String AND = "match all conditions";
	
	protected static final String OR = "match any condition";
	
	protected static final String SORT_TITLE = "&nbsp;Sort order:&nbsp; ";
	
	protected static final String ASCENDING = "ascending";

	protected static final String DESCENDING = "descending";

	protected static final String RETURN_AT_MOST = "return at most";

	protected static final String RESULTS = "results";

	protected static final String HITS_PER_PAGE = "Hits per page";

	public static final String SIMPLE_SEARCH = "Simple Search";

	public static final String ADVANCED_SEARCH = "Combined Search";
	
	public static final String ABSTRACT_SEARCH = "Abstract Search";

	protected static final String BROWSE_SEARCH = "Browse";
	
	protected static final String PREVIOUS_RESULTS_SEARCH = "Refine previous results";
	
	protected static final String BASKET_QUERY_SEARCH = "Search queries from basket";

	public static final String GEOSPATIAL_SEARCH = "Geospatial Search";
	
	public static final String QUICK_SEARCH = "Quick Search";
	
	public static final String GOOGLE_SEARCH = "Google Search";
	
	public static final String GENERIC_SEARCH = "Generic Search";
	
	protected static final String BROWSE_SEARCH_FILTERED_BY = "&nbsp;Browsing Criterion:&nbsp; ";
	
	protected static final String SORT_BY = "&nbsp;Sort by:&nbsp; ";

	protected static final String BROWSE_SEARCH_RESULTS_NO = "&nbsp;Number of results per page:&nbsp;";

	protected static final String BROWSE_SEARCH_BUTTON = "Browse Collection";
	
	protected static final String SEARCH_PER_COLLECTION = "Search per collection";
	
	protected static final String SEARCH_PER_COLLECTION_INFO = "This option enables to view the results per " +
			"collection. It is enabled when more than one collections are selected.";
	
	protected static final String RESULTS_PER_PAGE_INFO = "Specify the number of results you want to receive per page. The maximum number per page is 50.";
	
	protected static final String SCHEMA_DESCRIPTION = "The set of selected collections may be associated with more than one schema. Select the schema and the language" +
			" (if available in more than one languages) you prefer.";
	
	protected static final String SELECT_SCHEMA = "Schema:";

	

	public static final String SESSION_SELECTEDTAB = "tabName";
	public static final String SESSION_SEMANTIC_ENRICHMENT = "sessionSemanticEnrichment";
	public static final String SESSION_RANK = "sessionRankSelected";
	public static final String SESSION_SEARCHABLE = "searchOptions";
	public static final String SESSION_SORTABLE = "sortableValues";
	public static final String SESSION_SELECTEDFIELDS = "selectedFields";
	public static final String SESSION_PREVRS_SELECTEDFIELDS = "prevRSselectedFields";
	public static final String SESSION_SELECTEDCOLLECTIONS = "selectedCollections";
	public static final String SESSION_BOUNDS = "bounds";
	public static final String SESSION_RELATIONS = "relations";
	public static final String SESSION_TIMEINTERVAL = "timeinterval";
	public static final String SESSION_RSEPR = "rsEPR";
	public static final String SESSION_PREVIOUSRESULTSINFO = "previousResultsInfo";
	public static final String SESSION_LASTREPLACED_RSEPR = "lastReplacedRSepr";
	public static final String SESSION_BROWSEFIELDRSEPR = "browseFieldRSepr";
	public static final String SESSION_PREVIOUSQUERYINDEX = "previousQueryIndex";
	public static final String SESSION_RESULTS_NUMBER_PER_PAGE = "RESULTS_NUMBER_PER_PAGE";
	public static String SESSION_ANY_IS_CHOOSED;
	public static String SESSION_SIMPLE_TERM = "SP_simpleSearchTerm";
	public static String SESSION_ADVANCED_OPENED = "SP_isAdvancedOpen";
	public static String SESSION_SELECTED_RADIO_BASKET = "SP_seletedRadioBtn";
	public static final String WORKSPACE_AREA_ATTRIBUTE_NAME = "WORKSPACEAREA";
	public static final String USERAREA_DIR = "usersArea";
	
	public static final String CONDITIONTYPE_AND = "AND";
	public static final String CONDITIONTYPE_OR = "OR";
	
	public static final String ORDERTYPE_ASC = "ASC";
	public static final String ORDERTYPE_DESC = "DESC";

	public static final String TRUE_VALUE = "true";
	public static final String FALSE_VALUE = "false";
	
	public static final String BROWSE_COLLECTION = "collectionType";
	public static final String BROWSE_FIELD = "fieldType";
	
	protected static final String COLLECTIONS_CHANGED = "collectionsChanged";
}
