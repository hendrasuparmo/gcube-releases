package org.gcube.portlets.user.searchportlet.shared;

public class AlertsErrorMessages
{
	public static final String NumberOfCollectionRetrievalFailedError = "Failed to get the number of the selected collections";
	
	public static final String GeospatialQuerySubmissionFailure = "Failed to submit the geospatial query. Please try again";
	
	public static final String AdvancedQuerySubmissionFailure = "Failed to submit the advanced query. Please try again";
	
	public static final String AbstractQuerySubmissionFailure = "Failed to submit the advanced query. Please try again";
	
	public static final String SimpleQuerySubmissionFailure = "Failed to submit the simple query. Please try again";
	
	public static final String BrowseQuerySubmissionFailure = "Failed to submit the browse query. Please try again";
	
	public static final String OneConditionNeeded = "There must be at least one condition";
	
	public static final String RetrievalOfMetadataFailed = "Failed to get the available schemas. Please try again by reselecting collections";
	
	public static final String BrowseFieldFailed = "Failed to browse the selected field. Please try again";

}
