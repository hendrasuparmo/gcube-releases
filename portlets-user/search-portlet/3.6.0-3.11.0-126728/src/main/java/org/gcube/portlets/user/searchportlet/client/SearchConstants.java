package org.gcube.portlets.user.searchportlet.client;

public class SearchConstants
{

	public static final int SPACING = 8;

	public static final String BROWSE_SEARCH_IMAGE = "browse.png";

	public static final String ADVANCED_SEARCH_IMAGE = "zoom_in.png";

	public static final String SIMPLE_SEARCH_IMAGE = "zoom_out.png";
	
	public static final String PREVIOUS_RESULTS_IMAGE = "zoom.png";
	
	public static final String BASKET_IMAGE = "house.png";
	
	public static final String GEOSPATIAL_SEARCH_IMAGE = "geo.gif";

	public static final String LOADING_IMAGE = "loading.gif'";

	public static final int BROWSE_SEARCH = 0;

	public static final int ADVANCED_SEARCH = 1;

	public static final int SIMPLE_SEARCH = 2;

}
