package org.gcube.portlets.user.workflowdocuments.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SelectRowEventHandler extends EventHandler {
  void onRowSelect(SelectRowEvent event);
}
