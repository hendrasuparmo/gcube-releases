package org.gcube.portlets.user.workspaceapplicationhandler.exception;

@SuppressWarnings("serial")
public class PropertyNotFoundException extends Exception {
	 public PropertyNotFoundException(String message) {
	    super(message);
	  }
}