package org.gcube.portlets.user.workspaceapplicationhandler.exception;

@SuppressWarnings("serial")
public class ApplicationEndPointFoundException extends Exception {
	 public ApplicationEndPointFoundException(String message) {
	    super(message);
	  }
}