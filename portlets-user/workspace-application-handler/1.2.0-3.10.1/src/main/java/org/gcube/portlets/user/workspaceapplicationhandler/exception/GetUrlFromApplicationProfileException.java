package org.gcube.portlets.user.workspaceapplicationhandler.exception;

@SuppressWarnings("serial")
public class GetUrlFromApplicationProfileException extends Exception {
	 public GetUrlFromApplicationProfileException(String message) {
		    super(message);
		  }
}
