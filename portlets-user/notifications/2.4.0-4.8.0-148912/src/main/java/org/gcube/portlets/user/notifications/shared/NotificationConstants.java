package org.gcube.portlets.user.notifications.shared;
/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class NotificationConstants {
	/**
	 * the initial number of notification to be loaded at startup
	 */
	public final static int NOTIFICATION_NUMBER_PRE = 70;
	/**
	 * the number of notification to be loaded each time after the startup
	 */
	public final static int NOTIFICATION_NUMBER_PER_REQUEST = 15; 
}
