/**
 *
 */
package org.gcube.portlets.user.geoexplorerportlet.client;

/**
 * The Class Constants.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 1, 2017
 */
public class Constants {

	public static final int panelsWidth = 1015;
	public static final int gisViewerHeight = 500;
	public static final int gisViewerWithDataPanelHeight = 676;
	public static final int geoExplorerHeight = 630;

	public static final String defaultWorkspace = "aquamaps";
	public static final String GET_LAYER_UUID_PARAMETER = "luuid";

}
