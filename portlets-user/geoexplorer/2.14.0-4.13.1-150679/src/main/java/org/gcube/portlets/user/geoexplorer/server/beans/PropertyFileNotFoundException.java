package org.gcube.portlets.user.geoexplorer.server.beans;

@SuppressWarnings("serial")
public class PropertyFileNotFoundException extends Exception {
	 public PropertyFileNotFoundException(String message) {
	    super(message);
	  }
}