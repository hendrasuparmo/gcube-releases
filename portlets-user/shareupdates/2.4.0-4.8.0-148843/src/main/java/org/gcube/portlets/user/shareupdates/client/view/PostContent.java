package org.gcube.portlets.user.shareupdates.client.view;

/**
 * Type of posts.
 * @author Costantino Perciante at ISTI-CNR
 */
public enum PostContent {
	
	ONLY_TEXT, TEXT_AND_LINK, TEXT_AND_ATTACHMENTS

}
