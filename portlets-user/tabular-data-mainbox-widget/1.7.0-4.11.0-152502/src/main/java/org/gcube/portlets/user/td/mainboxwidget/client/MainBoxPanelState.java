package org.gcube.portlets.user.td.mainboxwidget.client;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum MainBoxPanelState {
	CLOSED,
	OPENED;
}
