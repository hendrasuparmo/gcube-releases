package org.gcube.portlets.user.td.mainboxwidget.client.tdx;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public interface TDXTabPanelMessages extends Messages {

	/**
	 * 
	 * @return
	 */
	@DefaultMessage("Table")
	String tabGridLabel();
	
	/**
	 * 
	 * @return
	 */
	@DefaultMessage("Resources")
	String tabResourcesLabel();
	
	
}
