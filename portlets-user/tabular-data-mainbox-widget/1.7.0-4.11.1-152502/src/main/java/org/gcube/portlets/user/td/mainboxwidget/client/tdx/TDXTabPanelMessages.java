package org.gcube.portlets.user.td.mainboxwidget.client.tdx;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public interface TDXTabPanelMessages extends Messages {

	@DefaultMessage("Table")
	String tabGridLabel();

	@DefaultMessage("Resources")
	String tabResourcesLabel();

}
