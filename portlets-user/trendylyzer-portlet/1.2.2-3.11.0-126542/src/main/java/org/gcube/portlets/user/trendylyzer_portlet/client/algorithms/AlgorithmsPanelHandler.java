package org.gcube.portlets.user.trendylyzer_portlet.client.algorithms;



public interface AlgorithmsPanelHandler {
	public void addAlgorithm(AlgorithmPanel algorithmPanel, Algorithm algorithm);
}
