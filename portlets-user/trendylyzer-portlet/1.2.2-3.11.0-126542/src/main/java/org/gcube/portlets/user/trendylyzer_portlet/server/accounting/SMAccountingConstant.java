package org.gcube.portlets.user.trendylyzer_portlet.server.accounting;


public class SMAccountingConstant  {
	protected static final String ALGORITHM_NAME = "algorithmName";

	protected static final String EXECUTION_OUTCOME = "executionOutcome";

	protected static final String EXECUTION_TIME = "executionSecondsTime";
	
	protected static final String FILE_NAME = "fileName";

	protected static final String FILE_TYPE = "fileType";

	protected static final String eqChar = " = ";

	protected static final String andchar = " AND ";

	protected static final String separateCharacters = " | ";

}
