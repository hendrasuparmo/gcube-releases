package org.gcube.portlets.user.trendylyzer_portlet.client.resources;



import org.gcube.portlets.user.trendylyzer_portlet.client.TrendyLyzer_portlet;

import com.google.gwt.user.client.ui.AbstractImagePrototype;

public class Images {
	public static AbstractImagePrototype experiment() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.experiment());
	}
	public static AbstractImagePrototype menuItemComputations() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.menuItemComputations());
	}
	public static AbstractImagePrototype logo() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.logo());
	}
	public static AbstractImagePrototype defaultAlg() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.defaultAlg());
	}

	public static AbstractImagePrototype addOperator() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.addOperator());
	}

	public static AbstractImagePrototype menuOcc() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.menuOcc());
	}
	public static AbstractImagePrototype goBack() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.goBack());
	}

	public static AbstractImagePrototype showAllOperators() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.sortAscending());
	}

	public static AbstractImagePrototype showCategories() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.tree());
	}

	public static AbstractImagePrototype userPerspective() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.userPerspective());
	}

	public static AbstractImagePrototype computationPerspective() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.computationPerspective());
	}

	public static AbstractImagePrototype startComputation() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.startComputation());
	}

	public static AbstractImagePrototype folderExplore() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.folderExplore());
	}

	public static AbstractImagePrototype removeAll() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.removeAll());
	}
	public static AbstractImagePrototype loaderBig() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.loaderBig());
	}
	public static AbstractImagePrototype cancel() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.cancel());
	}

	public static AbstractImagePrototype addl() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.add());
	}

	public static AbstractImagePrototype table() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.table());
	}



	
	public static AbstractImagePrototype save() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.save());
	}

	public static AbstractImagePrototype fileDownload() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.fileDownload());
	}

	public static AbstractImagePrototype infoSp()
	{
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.infoSp());
	}
	public static AbstractImagePrototype tableResult()
	{
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.tableResult());
	}
	public static AbstractImagePrototype detail()
	{
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.details());
	}
	public static AbstractImagePrototype refresh() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.refresh());
	}
	public static AbstractImagePrototype resubmit() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.resubmit());
		
	}
	public static AbstractImagePrototype computationIcon() {
		return AbstractImagePrototype.create(TrendyLyzer_portlet.resources.computationIcon());
		
	}
}
