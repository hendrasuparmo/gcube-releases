package org.gcube.portlets.user.tokengenerator.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface CloseAccordionEventHandler extends EventHandler {
  void onCloseAccordion(CloseAccordionEvent event);
}
