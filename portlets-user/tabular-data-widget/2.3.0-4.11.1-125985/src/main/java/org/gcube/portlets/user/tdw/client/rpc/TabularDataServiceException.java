/**
 * 
 */
package org.gcube.portlets.user.tdw.client.rpc;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public class TabularDataServiceException extends Exception {

	private static final long serialVersionUID = 3591608412845539801L;

	/**
	 * 
	 */
	public TabularDataServiceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public TabularDataServiceException(String message) {
		super(message);
	}
}
