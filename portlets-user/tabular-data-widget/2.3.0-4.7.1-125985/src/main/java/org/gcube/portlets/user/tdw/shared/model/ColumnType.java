/**
 * 
 */
package org.gcube.portlets.user.tdw.shared.model;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public enum ColumnType {
	
	USER,
	SYSTEM;
}
