/**
 * 
 */
package org.gcube.portlets.user.tdw.shared.model;

import java.io.Serializable;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public enum ValueType implements Serializable {
	STRING, 
	INTEGER, 
	BOOLEAN, 
	FLOAT, 
	DOUBLE, 
	LONG, 
	DATE;
}
