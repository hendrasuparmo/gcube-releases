/**
 * 
 */
package org.gcube.portlets.user.tdw.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface CloseTableEventHandler extends EventHandler {
	
	public void onCloseTable(CloseTableEvent event);

}
