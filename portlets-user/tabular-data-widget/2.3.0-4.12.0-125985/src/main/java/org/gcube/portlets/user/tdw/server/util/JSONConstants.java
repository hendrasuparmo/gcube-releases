/**
 * 
 */
package org.gcube.portlets.user.tdw.server.util;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public class JSONConstants {
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String NULL = "null";

}
