/**
 * 
 */
package org.gcube.portlets.user.gcubegisviewer.client;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public class GCubeGisViewerServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8832690347019832416L;
	
	public GCubeGisViewerServiceException(){}

	/**
	 * @param message
	 */
	public GCubeGisViewerServiceException(String message) {
		super(message);
	}

}
