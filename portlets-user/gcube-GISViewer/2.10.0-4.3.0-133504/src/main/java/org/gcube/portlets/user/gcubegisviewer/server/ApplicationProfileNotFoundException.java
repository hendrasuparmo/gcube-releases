package org.gcube.portlets.user.gcubegisviewer.server;

@SuppressWarnings("serial")
public class ApplicationProfileNotFoundException extends Exception {
	 public ApplicationProfileNotFoundException(String message) {
	    super(message);
	  }
}