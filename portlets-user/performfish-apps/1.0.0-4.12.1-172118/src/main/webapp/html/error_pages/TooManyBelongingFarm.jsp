<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<div class="alert alert-info">
	<h4>Please select the farm you wish to operate</h4>
	<strong>You are assigned to more than one company
		Farm. You can work on one farm at a time.</strong>
</div>