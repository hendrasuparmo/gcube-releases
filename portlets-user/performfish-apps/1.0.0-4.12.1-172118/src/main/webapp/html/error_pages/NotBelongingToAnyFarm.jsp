<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<div class="alert alert-error">
  <h4>You are not authorised</h4>
  It seems you don't have the permission to use this application. <strong>You are not associated to any farm of the company.</strong>
</div>
