/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.shared;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 28, 2014
 *
 */
public class OperationNotAvailable extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4748940586135698748L;

	/**
	 * 
	 */
	public OperationNotAvailable() {
	}
	
	/**
	 * 
	 */
	public OperationNotAvailable(String arg0) {
		super(arg0);
	}

}
