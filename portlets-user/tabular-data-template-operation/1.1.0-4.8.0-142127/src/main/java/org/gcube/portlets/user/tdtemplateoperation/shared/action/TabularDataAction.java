/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.shared.action;

/**
 * The Interface TemplateAction.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 25, 2015
 */
public interface TabularDataAction {

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	String getId();
	
	String getDescription();
}
