/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.client;

import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Jun 18, 2014
 *
 */
public interface DeletableContainer {
	/**
	 * @param panel
	 */
	void deleteFired(VerticalLayoutContainer panel);
}
