/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.shared;

/**
 * 
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 25, 2015
 */
public enum ServerObjectType {
	
	TABULAR_RESOURCE, TEMPLATE;

}
