/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.client.properties;

import org.gcube.portlets.user.tdtemplateoperation.shared.TdColumnData;

/**
 * The Interface TdColumnDataPropertiesCombo.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 25, 2015
 */
public interface ComboColumnDataPropertiesCombo extends ComboPropertiesAccess<TdColumnData>{

}
