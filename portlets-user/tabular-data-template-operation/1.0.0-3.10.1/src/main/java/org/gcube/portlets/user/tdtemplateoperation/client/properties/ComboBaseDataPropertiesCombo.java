/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.client.properties;

import org.gcube.portlets.user.tdtemplateoperation.shared.TdBaseData;

/**
 * The Interface TdColumnDataPropertiesCombo.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 25, 2015
 */
public interface ComboBaseDataPropertiesCombo extends ComboPropertiesAccess<TdBaseData>{

}
