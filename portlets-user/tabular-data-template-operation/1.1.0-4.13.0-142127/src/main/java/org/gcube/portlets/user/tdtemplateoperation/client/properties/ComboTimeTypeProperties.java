/**
 * 
 */
package org.gcube.portlets.user.tdtemplateoperation.client.properties;

import org.gcube.portlets.user.tdtemplateoperation.shared.TdBaseComboDataBean;

/**
 * The Interface TimeTypePropertiesCombo.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @May 19, 2014
 */
public interface ComboTimeTypeProperties extends ComboPropertiesAccess<TdBaseComboDataBean> {

}
