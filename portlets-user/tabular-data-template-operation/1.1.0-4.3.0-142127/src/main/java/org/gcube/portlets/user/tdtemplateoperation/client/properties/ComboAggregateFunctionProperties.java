package org.gcube.portlets.user.tdtemplateoperation.client.properties;

import org.gcube.portlets.user.tdtemplateoperation.shared.TdBaseComboDataBean;

/**
 * The Interface AggregateFunctionPropertiesCombo.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Jun 9, 2014
 */
public interface ComboAggregateFunctionProperties extends ComboPropertiesAccess<TdBaseComboDataBean> {
	
}
