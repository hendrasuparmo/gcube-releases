package org.gcube.portlets.user.tdcolumnoperation.client.properties;

import org.gcube.portlets.user.tdcolumnoperation.shared.TdBaseComboDataBean;

/**
 * 
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Jun 9, 2014
 *
 */
public interface AggregateFunctionPropertiesCombo extends TdPropertiesAccessCombo<TdBaseComboDataBean> {
	
}
