/**
 * 
 */
package org.gcube.portlets.user.tdcolumnoperation.shared;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 28, 2014
 *
 */
public class OperationNotAvailable extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3893924116789029299L;

	/**
	 * 
	 */
	public OperationNotAvailable() {
	}
	
	/**
	 * 
	 */
	public OperationNotAvailable(String arg0) {
		super(arg0);
	}

}
