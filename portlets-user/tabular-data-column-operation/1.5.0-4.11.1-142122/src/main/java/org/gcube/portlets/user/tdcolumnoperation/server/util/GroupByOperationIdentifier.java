/**
 * 
 */
package org.gcube.portlets.user.tdcolumnoperation.server.util;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Jun 24, 2014
 *
 */
public final class GroupByOperationIdentifier {

	
	public static final String GROUPBY_COLUMNS = "groupByColumns";
	
	public static final String AGGREGATE_FUNCTION_TO_APPLY = "aggregationFunctions";
	
	public static final String FUNCTION_PARAMETER = "functionParameter";
	
	public static final String TO_AGGREGATE_COLUMNS = "functionMember";
	
	public static final String TIME_DIMENSION_AGGR = "timeDimensionAggr";
	
	public static final String AGGREGATE_BY_TIME = "keyColumns";
	
	
	
}
