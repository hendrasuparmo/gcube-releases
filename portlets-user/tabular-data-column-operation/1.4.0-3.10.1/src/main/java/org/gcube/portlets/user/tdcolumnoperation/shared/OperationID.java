/**
 * 
 */
package org.gcube.portlets.user.tdcolumnoperation.shared;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @May 19, 2014
 *
 */
public enum OperationID {
	
	SPLIT, MERGE;

}
