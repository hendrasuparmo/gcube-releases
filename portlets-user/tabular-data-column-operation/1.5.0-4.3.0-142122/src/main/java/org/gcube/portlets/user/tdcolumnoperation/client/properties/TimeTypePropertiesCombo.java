/**
 * 
 */
package org.gcube.portlets.user.tdcolumnoperation.client.properties;

import org.gcube.portlets.user.tdcolumnoperation.shared.TdBaseComboDataBean;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @param <T>
 * @May 19, 2014
 *
 */
public interface TimeTypePropertiesCombo extends TdPropertiesAccessCombo<TdBaseComboDataBean> {

}
