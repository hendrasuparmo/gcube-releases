/**
 * 
 */
package org.gcube.portlets.user.tdwx.shared;


/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class ServletParameters {
	
	public static final String OFFSET = "offset";
	public static final String LIMIT = "limit";
	public static final String SORTFIELD = "sortField";
	public static final String SORTDIR = "sortDir";
	public static final String TD_SESSION_ID = "tdSessionId";
	
	public static final String FILTERFIELD="filerField";
	public static final String FILTERTYPE="filterType";
	public static final String FILTERCOMPARISON="filterComparison";
	public static final String FILTERVALUE="filterValue";
	
}
