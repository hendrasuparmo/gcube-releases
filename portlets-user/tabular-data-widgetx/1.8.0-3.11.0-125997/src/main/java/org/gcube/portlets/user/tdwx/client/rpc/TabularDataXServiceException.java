/**
 * 
 */
package org.gcube.portlets.user.tdwx.client.rpc;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class TabularDataXServiceException extends Exception {

	private static final long serialVersionUID = 3591608412845539801L;

	/**
	 * 
	 */
	public TabularDataXServiceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public TabularDataXServiceException(String message) {
		super(message);
	}
}
