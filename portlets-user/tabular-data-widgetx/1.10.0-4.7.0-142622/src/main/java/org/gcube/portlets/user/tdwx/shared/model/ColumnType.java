/**
 * 
 */
package org.gcube.portlets.user.tdwx.shared.model;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ColumnType {
	
	USER,
	VALIDATION,
	COLUMNID,
	DIMENSION,
	TIMEDIMENSION,
	VIEWCOLUMN_OF_DIMENSION,
	VIEWCOLUMN_OF_TIMEDIMENSION,
	CODE,
	MEASURE,
	SYSTEM;
}
