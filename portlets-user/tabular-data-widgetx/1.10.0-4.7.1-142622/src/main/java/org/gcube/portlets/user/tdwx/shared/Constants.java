/**
 * 
 */
package org.gcube.portlets.user.tdwx.shared;

/**
 * 
 * @author "Giancarlo Panichi" <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class Constants {

	public final static String VERSION = "1.0.0";

	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String CURR_GROUP_ID = "CURR_GROUP_ID";
	// public static final String CURR_USER_ID = "CURR_USER_ID";

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public static final String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public static final String DEFAULT_TOKEN = "ae1208f0-210d-47c9-9b24-d3f2dfcce05f-98187548";

	public final static String REMOTE_SERVICE_RELATIVE_PATH = "tdwx";

	public static final String TABULAR_DATA_X_SERVLET = "tdwxdata";

}
