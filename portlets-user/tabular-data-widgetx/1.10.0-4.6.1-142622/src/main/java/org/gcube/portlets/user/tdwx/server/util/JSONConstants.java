/**
 * 
 */
package org.gcube.portlets.user.tdwx.server.util;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class JSONConstants {
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String NULL = "null";

}
