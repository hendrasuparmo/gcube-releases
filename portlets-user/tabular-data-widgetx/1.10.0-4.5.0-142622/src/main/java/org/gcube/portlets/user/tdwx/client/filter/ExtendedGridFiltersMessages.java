package org.gcube.portlets.user.tdwx.client.filter;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public interface ExtendedGridFiltersMessages extends Messages {

	//
	@DefaultMessage("Inline Filter")
	String inlineFilter();
	
	
	
}