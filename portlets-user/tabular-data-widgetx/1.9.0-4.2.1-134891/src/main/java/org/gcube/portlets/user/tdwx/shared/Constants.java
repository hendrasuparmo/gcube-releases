/**
 * 
 */
package org.gcube.portlets.user.tdwx.shared;



/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {

	public final static String VERSION = "1.0.0";		
	public final static String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devsec";
	public final static String REMOTE_SERVICE_RELATIVE_PATH= "tdwx";
	

}
