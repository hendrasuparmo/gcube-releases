package org.gcube.portlets.user.templates.client;

import com.google.gwt.core.client.GWT;

/**
 * Simply a class conatining Static Constants 
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 * @version november 2009 (gCube 1.5) 
 */
public class TGenConstants {
	
	public static final boolean isDeployed = true;
	
	
	/**
	 * 
	 */	
	public static final String LOADING_BAR = GWT.getModuleBaseURL() + "../images/loading-bar.gif";	

	/**
	 * 
	 */	
	public static final String RESIZE = GWT.getModuleBaseURL() + "../images/arrow_out.png";
	/**
	 * 
	 */	
	public static final String LOCK = GWT.getModuleBaseURL() + "../images/lock_delete.png";
	/**
	 * 
	 */	
	public static final String LOCK_DARKER = GWT.getModuleBaseURL() + "../images/lock_darker_delete.png";
	/**
	 * 
	 */	
	public static final String LOCK_OPEN = GWT.getModuleBaseURL() + "../images/lock_add.png";
	/**
	 * 
	 */	
	public static final String LOCK_OPEN_DARKER = GWT.getModuleBaseURL() + "../images/lock_darker_add.png";
	/**
	 * 
	 */	
	public static final String IMAGE_BIBLIO = GWT.getModuleBaseURL() + "../images/biblio.jpg";
	/**
	 * 
	 */	
	public static final String IMAGE_TOC = GWT.getModuleBaseURL() + "../images/toc.jpg";	
	/**
	 * 
	 */	
	public static final String IMAGE_NEXT_PAGE = GWT.getModuleBaseURL() + "../images/next_p.gif";
	/**
	 * 
	 */
	public static final String IMAGE_PREV_PAGE = GWT.getModuleBaseURL() + "../images/prev_p.gif";
	/**
	 * 
	 */
	public static final String IMAGE_TITLE_BG = GWT.getModuleBaseURL() + "../images/prev_p.gif";

	/**
	 * 
	 */
	public static final String IMAGE_INSERT_TEXT = GWT.getModuleBaseURL() + "../images/writeico.gif";
	/**
	 * 
	 */
	public static final String IMAGE_INSERT_IMAGE = GWT.getModuleBaseURL() + "../images/insert-image.png";
	/**
	 * 
	 */
	public static final String IMAGE_INSERT_TABLE = GWT.getModuleBaseURL() + "../images/insert-tableb.png";
	/**
	 * 
	 */
	public static final String IMAGE_INSERT_WORKFLOW = GWT.getModuleBaseURL() + "../images/workflow.gif";
	/**
	 * 
	 */
	public static final String IMAGE_INSERT_DROPPING = GWT.getModuleBaseURL() + "../images/insert-droppingimage.gif";
	/**
	 * 
	 */
	public static final String IMAGE_INSERT_INPUT_TEXTBOX = GWT.getModuleBaseURL() + "../images/insert-inputTextbox.gif";
	
	/**
	 * 
	 */
	public static final String IMAGE_DROPPING_AREA_IMG = GWT.getModuleBaseURL() + "../images/droppingImage.gif";
	/**
	 * 
	 */
	public static final String IMAGE_DROPPING_AREA_TXT = GWT.getModuleBaseURL() + "../images/insertText.png";
	
	/**
	 * 
	 */
	public static final String IMAGE_CLOSE_15x15 = GWT.getModuleBaseURL() + "../images/close.gif";
	/**
	 * 
	 */
	public static final String IMAGE_CLOSE_DARKER_15x15 = GWT.getModuleBaseURL() + "../images/close_darker.gif";
	
	/**
	 * 
	 */
	public static final String ADD_STATIC_IMAGE = GWT.getModuleBaseURL() + "../images/add.png";
	/**
	 * 
	 */
	public static final String REMOVE_STATIC_IMAGE = GWT.getModuleBaseURL() + "../images/delete.png";
	/**
	 * 
	 */	
	public static final int TYPE_HOME = 1;
	/**
	 * 
	 */	
	public static final int TYPE_FOLDER = 2;
	/**
	 * 
	 */	
	public static final int TYPE_BASKET = 3;
	/**
	 * 
	 */	
	public static final int TYPE_BASKET_ITEM = 3;
	
	/**
	 * 
	 */	
	public static final int DEFAULT_IMAGE_WIDTH = 700;
	/**
	 * 
	 */	
	public static final int DEFAULT_IMAGE_HEIGHT = 100;
	
	/**
	 * 
	 */
	public static final String ADD_PAGE = GWT.getModuleBaseURL() + "../images/add.png";
	/**
	 * Used when delpoying application in the portlet
	 */
	public static final int WHEN_DEPLOYED_TOP = 150;
	
}
