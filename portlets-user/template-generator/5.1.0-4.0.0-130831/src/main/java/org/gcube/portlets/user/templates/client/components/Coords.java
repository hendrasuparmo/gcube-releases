package org.gcube.portlets.user.templates.client.components;

public class Coords {

	private int X;
	private int Y;
	public Coords(int x, int y) {
		super();
		X = x;
		Y = y;
	}
	public int getX() {
		return X;
	}
	public void setX(int x) {
		X = x;
	}
	public int getY() {
		return Y;
	}
	public void setY(int y) {
		Y = y;
	}
	
	
}
