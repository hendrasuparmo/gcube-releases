package org.gcube.portlets.user.tdwx.datasource.td;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {
	public static final String TDX_DATASOURCE_FACTORY_ID = "TDXDataSourceFactory";
}
