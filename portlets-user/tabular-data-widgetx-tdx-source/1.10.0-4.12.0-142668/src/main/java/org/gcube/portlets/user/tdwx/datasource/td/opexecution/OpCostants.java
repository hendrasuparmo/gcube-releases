package org.gcube.portlets.user.tdwx.datasource.td.opexecution;

/**
 * Constants Parameter Name
 * 
 * @author giancarlo email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class OpCostants {
	
	public static final String PARAMETER_CHANGE_COLUMN_POSITION_POSITION = "position";

}
