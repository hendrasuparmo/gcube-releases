package org.gcube.portlets.user.transect.server.readers;

@SuppressWarnings("serial")
public class RuntimeResourceException extends Exception {
	 public RuntimeResourceException(String message) {
	    super(message);
	  }
}