package org.gcube.portlets.user.transect.server.readers;

@SuppressWarnings("serial")
public class ApplicationProfileException extends Exception {
	 public ApplicationProfileException(String message) {
	    super(message);
	  }
}