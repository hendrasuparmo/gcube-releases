/**
 * 
 */
package org.gcube.portlets.user.csvimportwizard.client.csvimport;

/**
 * @author Federico De Faveri defaveri@isti.cnr.it
 *
 */
public class ImportState {
	
	public static final int CREATE_STATE_SA_CREATION = 0;
	public static final int CREATE_STATE_STORE = 1;
	public static final int CREATE_STATE_APPROVE = 2;

}
