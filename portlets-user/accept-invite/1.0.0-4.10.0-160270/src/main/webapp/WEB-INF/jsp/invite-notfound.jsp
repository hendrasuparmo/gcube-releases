<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />

<div class="portlet-content">
	<h3 class="alert alert-error">Invite Not Found</h3>
	The invite you received could not be found. <br>
	<div class="separator">
		<!-- -->
	</div>
	If you believe this requires support please go to <a
		href="http://www.d4science.org/contact-us" target="_blank">http://www.d4science.org/contact-us</a>
	to ask for D4Science Help Desk support. <br> <br />




</div>