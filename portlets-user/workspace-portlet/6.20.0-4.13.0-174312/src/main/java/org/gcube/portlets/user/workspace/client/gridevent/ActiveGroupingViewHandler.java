package org.gcube.portlets.user.workspace.client.gridevent;

import com.google.gwt.event.shared.EventHandler;

public interface ActiveGroupingViewHandler extends EventHandler {
	void onActiveGroupingView(ActiveGroupingView activeGroupingView);
}