package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ActiveGroupingViewHandler extends EventHandler {
	void onActiveGroupingView(ActiveGroupingView activeGroupingView);
}