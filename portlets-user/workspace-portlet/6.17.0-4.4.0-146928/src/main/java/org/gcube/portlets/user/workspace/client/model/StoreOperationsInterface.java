package org.gcube.portlets.user.workspace.client.model;

import java.util.List;


/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * This class is not used
 */
public interface StoreOperationsInterface {
	
	List<FileGridModel> getListModel();
	void setListModel(List<FileGridModel> listModel);
}
