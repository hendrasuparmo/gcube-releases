The gCube System - gCube Workspace Portlet
--------------------------------------------------
 
gCube Workspace Portlet is a web-gui to manage the gCube workspace a collaborative area where users can exchange and organize information objects (workspace items) according to their specific needs. 
		Every user of any Virtual Research Environment is provided with this area for the exchange of workspace objects (share) with other users. 
		Such an area is further organized in workspaces as to resemble a classic folder-based file system.
 
 
This software is part of the gCube Framework (https://www.gcube-system.org/): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of
European Union programmes including:
* the Sixth Framework Programme for Research and Technological Development -
DILIGENT (grant no. 004260);
* the Seventh Framework Programme for research, technological development and
demonstration - D4Science (grant no. 212488), D4Science-II (grant no.
239019),ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine
(grant no. 283644);
* the H2020 research and innovation programme - BlueBRIDGE (grant no. 675680),
EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant
no. 654119), SoBigData (grant no. 654024);
 
 
Version
--------------------------------------------------
 
6.17.3-4.8.0-158704 (2017-12-01)
 
Please see the file named "changelog.xml" in this directory for the release notes.
 
 
Authors
--------------------------------------------------
 
* Francesco Mangiacrapa (francesco.mangiacrapa-AT-isti.cnr.it), Istituto di Scienza e Tecnologie dell'Informazione "A. Faedo" - CNR, Pisa (Italy). 
 
Maintainers
-----------
 
* Francesco Mangiacrapa (francesco.mangiacrapa-AT-isti.cnr.it), Istituto di Scienza e Tecnologie dell'Informazione "A. Faedo" - CNR, Pisa (Italy). 
 
Download information
--------------------------------------------------
 
Source code is available from SVN: 
    http://svn.d4science.research-infrastructures.eu/gcube/trunk/portlets/user/workspace
 
Binaries can be downloaded from the gCube website: 
   https://www.gcube-system.org/
 
 
Installation
--------------------------------------------------

 
Documentation 
--------------------------------------------------
 
Documentation is available on-line in the gCube Wiki:
    https://wiki.gcube-system.org/gcube/index.php/Workspace
 
Support 
--------------------------------------------------
 
Bugs and support requests can be reported in the gCube issue tracking tool:
    https://support.d4science.org/projects/gcube/
 
 
Licensing
--------------------------------------------------
 
This software is licensed under the terms you may find in the file named "LICENSE" in this directory.