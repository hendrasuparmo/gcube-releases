package org.gcube.portlets.user.workspace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowUrlEventHandler extends EventHandler {
	void onClickUrl(ShowUrlEvent openUrlEvent);
}