package org.gcube.portlets.user.lastupdatedfiles.shared;

public enum ImageType {
	DOC, PPT, XLS, NONE, MOVIE, HTML, PDF, IMAGE, RAR, ZIP,  
}
