package org.gcube.portlets.user.accountingdashboard.client.place;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class NameTokens {
	public static final String MAIN_AREA = "mainarea";

	public static final String SAVE = "save";
	public static final String HELP = "help";

	public static String getMainArea() {
		return MAIN_AREA;
	}

	public static String getSave() {
		return SAVE;
	}

	public static String getHelp() {
		return HELP;
	}

}
