package org.gcube.portlets.user.accountingdashboard.client.application.dialog.error;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface ErrorUiHandlers extends UiHandlers{
	
}
