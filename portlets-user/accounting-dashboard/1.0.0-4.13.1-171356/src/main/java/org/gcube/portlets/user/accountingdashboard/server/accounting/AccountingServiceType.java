package org.gcube.portlets.user.accountingdashboard.server.accounting;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public enum AccountingServiceType {

	PortalContex, CurrentScope;

}
