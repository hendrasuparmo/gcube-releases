package org.gcube.portlets.user.accountingdashboard.client.application;

import com.google.gwt.i18n.client.Messages;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public interface CommonMessages extends Messages {

	//
	@DefaultMessage("Attention")
	String attention();
	
	@DefaultMessage("Error")
	String error();
	
	
		
	
}