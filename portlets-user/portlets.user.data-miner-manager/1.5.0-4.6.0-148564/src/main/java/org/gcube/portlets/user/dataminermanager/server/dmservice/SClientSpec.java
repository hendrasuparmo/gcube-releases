package org.gcube.portlets.user.dataminermanager.server.dmservice;

/**
 * Specification
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SClientSpec {
	private SClient sClient;

	public SClient getSClient() {
		return sClient;
	}

	public void setSClient(SClient sClient) {
		this.sClient = sClient;
	}

}
