package org.gcube.portlets.user.dataminermanager.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum WPSMenuType {
	MENU, INPUT_SPACE, EXPERIMENT, COMPUTATIONS
}
