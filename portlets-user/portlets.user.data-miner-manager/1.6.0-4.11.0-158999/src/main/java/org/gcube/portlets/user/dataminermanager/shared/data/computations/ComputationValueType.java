package org.gcube.portlets.user.dataminermanager.shared.data.computations;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum ComputationValueType {
	FileList, File, Image, String;
}
