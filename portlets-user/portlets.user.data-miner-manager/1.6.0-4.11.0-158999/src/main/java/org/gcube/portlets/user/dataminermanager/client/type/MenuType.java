package org.gcube.portlets.user.dataminermanager.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum MenuType {
		HOME, DATA_SPACE, EXPERIMENT, COMPUTATIONS;
}
