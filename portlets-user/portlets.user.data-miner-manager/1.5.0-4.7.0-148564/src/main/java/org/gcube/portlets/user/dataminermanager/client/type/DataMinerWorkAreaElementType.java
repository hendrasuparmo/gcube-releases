package org.gcube.portlets.user.dataminermanager.client.type;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum DataMinerWorkAreaElementType {
	InputDataSets, OutputDataSets, Computations;
}
