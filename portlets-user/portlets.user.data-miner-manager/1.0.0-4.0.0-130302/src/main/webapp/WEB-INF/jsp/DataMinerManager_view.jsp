<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<!--                                           -->
<!-- The module reference below is the link    -->
<!-- between html and your Web Toolkit module  -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/dataminermanager/reset.css"
	type="text/css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/DataMinerManager.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/gxt/css/gxt-all.css"
	type="text/css">
<script
	src='<%=request.getContextPath()%>/dataminermanager/js/jquery-1.10.1.min.js'></script>
<script
	src='<%=request.getContextPath()%>/dataminermanager/js/bootstrap.min.js'></script>

<script
	src='<%=request.getContextPath()%>/dataminermanager/dataminermanager.nocache.js'></script>


<div class="contentDiv" id="contentDiv"></div>
