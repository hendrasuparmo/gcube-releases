package org.gcube.portlets.user.dataminermanager.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum OperatorsClassificationRequestType {
	Default, ByName;
}
