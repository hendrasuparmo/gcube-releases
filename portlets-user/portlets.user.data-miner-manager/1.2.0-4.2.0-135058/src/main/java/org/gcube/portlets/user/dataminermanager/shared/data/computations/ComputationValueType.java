package org.gcube.portlets.user.dataminermanager.shared.data.computations;

/**
 * 
 * @author Giancarlo Panichi
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ComputationValueType {
	FileList, File, Image, String;
}
