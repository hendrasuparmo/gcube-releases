package org.gcube.portlets.user.dataminermanager.client.custom;

/**
 * Fix for z-index bug between GXT3 and GXT2
 * 
 * @author giancarlo email: <a
 *         href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a>
 *
 */
public class Gxt2ZIndexXDOM extends
		com.sencha.gxt.core.client.dom.XDOM.XDOMImpl {
	
	/*
	public int getTopZIndex() {
		return com.extjs.gxt.ui.client.core.XDOM.getTopZIndex();
	}

	public int getTopZIndex(int i) {
		return com.extjs.gxt.ui.client.core.XDOM.getTopZIndex(i);
	}*/
}