package org.gcube.portlets.user.dataminermanager.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum UIStateEventType {
	START,
	UPDATE,
	WAITING;
}
