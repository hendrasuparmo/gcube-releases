package org.gcube.portlets.user.newsfeed.shared;

public class NewsConstants {
	/**
	 * Feeds Number to who per VRE/Category
	 */
	public static final int FEEDS_NO_PER_CATEGORY = 10;
	
	public static final int FEEDS_MAX_PER_CATEGORY = 30;

	public static final String TEST_USER = "test.user";
}
