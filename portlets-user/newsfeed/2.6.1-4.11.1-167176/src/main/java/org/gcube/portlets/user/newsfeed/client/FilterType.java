package org.gcube.portlets.user.newsfeed.client;

public enum FilterType {
	ALL_UPDATES, CONNECTIONS, RECENT_COMMENTS,  MINE;
}
