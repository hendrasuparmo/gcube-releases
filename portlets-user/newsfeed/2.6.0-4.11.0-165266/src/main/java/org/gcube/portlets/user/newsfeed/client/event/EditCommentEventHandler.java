package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditCommentEventHandler extends EventHandler {
  void onEditComment(EditCommentEvent event);
}
