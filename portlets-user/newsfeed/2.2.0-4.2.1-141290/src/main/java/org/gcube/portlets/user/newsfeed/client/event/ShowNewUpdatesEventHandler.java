package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowNewUpdatesEventHandler extends EventHandler {
  void onShowNewUpdatesClick(ShowNewUpdatesEvent event);
}
