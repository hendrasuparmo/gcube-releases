package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowMoreUpdatesEventHandler extends EventHandler {
  void onShowMoreUpdatesClick(ShowMoreUpdatesEvent event);
}
