package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface OpenPostEventHandler extends EventHandler {
  void onOpenPost(OpenPostEvent event);
}
