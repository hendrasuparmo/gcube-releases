package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SeeCommentsEventHandler extends EventHandler {
  void onSeeComments(SeeCommentsEvent event);
}
