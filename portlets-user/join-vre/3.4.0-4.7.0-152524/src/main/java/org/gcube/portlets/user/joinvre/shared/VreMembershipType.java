package org.gcube.portlets.user.joinvre.shared;

public enum VreMembershipType {
	OPEN,
	RESTRICTED,
	PRIVATE;
}
