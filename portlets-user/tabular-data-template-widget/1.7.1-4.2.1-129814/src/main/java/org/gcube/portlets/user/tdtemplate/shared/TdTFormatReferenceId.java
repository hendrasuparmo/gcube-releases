/**
 * 
 */
package org.gcube.portlets.user.tdtemplate.shared;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Feb 12, 2015
 *
 */
public interface TdTFormatReferenceId {
	
	String getId();

}
