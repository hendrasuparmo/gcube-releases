/**
 * 
 */
package org.gcube.portlets.user.tdtemplate.shared;


/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 17, 2014
 *
 */
public class TdLicenceModel extends TdBaseBeanModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1263516068191149418L;

	public TdLicenceModel(){}
	/**
	 * @param id
	 * @param label
	 */
	public TdLicenceModel(String id, String label) {
		super(id, label);
	}
}
