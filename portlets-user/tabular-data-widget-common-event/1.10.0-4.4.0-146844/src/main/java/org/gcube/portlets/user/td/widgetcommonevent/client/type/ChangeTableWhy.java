package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ChangeTableWhy {
	TABLEUPDATED,
	TABLECURATION,
	TABLECLONED;
}
