/**
 * 
 */
package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum DataViewRequestType {
	OPEN,
	CLOSE,
	UPDATE_TAB_NAME;
}
