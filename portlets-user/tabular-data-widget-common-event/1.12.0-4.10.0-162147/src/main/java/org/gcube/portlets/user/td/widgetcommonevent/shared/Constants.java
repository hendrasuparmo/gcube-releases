package org.gcube.portlets.user.td.widgetcommonevent.shared;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class Constants {

	public static final String APPLICATION_ID = "org.gcube.portlets.user.td.server.portlet.TabularDataPortlet";
	public static final String TABULAR_RESOURCE_ID = "TabularResourceId";
	public static final String TD_LANG_COOKIE = "TDLangCookie";
	public static final String TDX_DATASOURCE_FACTORY_ID = "TDXDataSourceFactory";

}
