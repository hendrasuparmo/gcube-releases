/**
 * 
 */
package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 * Types of events generated by the grid menu
 */
public enum GridHeaderColumnMenuItemType {
	SELECTED;
}
