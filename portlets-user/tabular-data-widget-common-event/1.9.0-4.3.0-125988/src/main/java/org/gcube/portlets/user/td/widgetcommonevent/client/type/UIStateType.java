package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum UIStateType {
	START,
	TR_OPEN,
	TABLEUPDATE,
	TABLECURATION,
	TR_READONLY,
	TR_CLOSE,
	WIZARD_OPEN;
}
