/**
 * 
 */
package org.gcube.portlets.user.td.widgetcommonevent.client.expression;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum ExpressionWrapperType {
	CONDITION_AND_REPLACE_COLUMN_EXPRESSION,
	CONDITION_COLUMN_EXPRESSION,
	REPLACE_COLUMN_EXPRESSION,
	EXPRESSION_NULL;
}
