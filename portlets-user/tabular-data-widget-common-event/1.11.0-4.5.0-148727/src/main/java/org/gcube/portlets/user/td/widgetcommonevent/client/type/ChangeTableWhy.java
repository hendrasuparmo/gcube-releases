package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum ChangeTableWhy {
	TABLEUPDATED,
	TABLECURATION,
	TABLECLONED;
}
