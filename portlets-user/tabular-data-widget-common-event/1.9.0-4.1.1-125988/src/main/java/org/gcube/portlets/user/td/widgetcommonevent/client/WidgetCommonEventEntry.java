package org.gcube.portlets.user.td.widgetcommonevent.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.EntryPoint;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class WidgetCommonEventEntry implements EntryPoint {

	
	public void onModuleLoad() {

		//EventBus eventBus = new SimpleEventBus();
	
		Log.info("WidgetCommonEvent");

	}
}
