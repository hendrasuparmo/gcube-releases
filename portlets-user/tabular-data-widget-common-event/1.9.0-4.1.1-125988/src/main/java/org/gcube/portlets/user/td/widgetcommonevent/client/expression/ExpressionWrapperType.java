/**
 * 
 */
package org.gcube.portlets.user.td.widgetcommonevent.client.expression;

/**
 * 
 * @author "Giancarlo Panichi" 
 * <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public enum ExpressionWrapperType {
	CONDITION_AND_REPLACE_COLUMN_EXPRESSION,
	CONDITION_COLUMN_EXPRESSION,
	REPLACE_COLUMN_EXPRESSION,
	EXPRESSION_NULL;
}
