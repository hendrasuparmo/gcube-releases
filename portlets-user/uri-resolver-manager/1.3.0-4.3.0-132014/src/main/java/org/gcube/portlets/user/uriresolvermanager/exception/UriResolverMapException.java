/**
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.exception;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 14, 2014
 *
 */
public class UriResolverMapException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -914311044943568036L;

	/**
	 * 
	 */
	public UriResolverMapException() {
		super();
	}
	
    public UriResolverMapException(String message) {
        super(message);
    }

}
