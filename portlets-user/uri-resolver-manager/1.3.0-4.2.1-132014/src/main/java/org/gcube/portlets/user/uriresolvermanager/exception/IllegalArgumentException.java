/**
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.exception;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 14, 2014
 *
 */
public class IllegalArgumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 769797235962949500L;
	
	/**
	 * 
	 */
	public IllegalArgumentException() {
		super();
	}
	
    public IllegalArgumentException(String message) {
        super(message);
    }

}
