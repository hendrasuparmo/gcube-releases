package org.gcube.portlets.user.uriresolvermanager.readers;

@SuppressWarnings("serial")
public class RuntimeResourceException extends Exception {
	 public RuntimeResourceException(String message) {
	    super(message);
	  }
}