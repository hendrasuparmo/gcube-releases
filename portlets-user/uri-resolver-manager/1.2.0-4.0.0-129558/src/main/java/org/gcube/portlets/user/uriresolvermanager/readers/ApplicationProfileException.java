package org.gcube.portlets.user.uriresolvermanager.readers;

@SuppressWarnings("serial")
public class ApplicationProfileException extends Exception {
	 public ApplicationProfileException(String message) {
	    super(message);
	  }
}