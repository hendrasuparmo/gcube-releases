/**
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.resolvers;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 14, 2014
 *
 */
public interface LinkResolver {
	String getLink();
}
