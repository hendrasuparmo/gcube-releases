package org.gcube.portlets.user.uriresolvermanager.readers;

/**
 * The Class RuntimeResourceException.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Sep 6, 2016
 */
@SuppressWarnings("serial")
public class RuntimeResourceException extends Exception {

 	/**
 	 * Instantiates a new runtime resource exception.
 	 *
 	 * @param message the message
 	 */
 	public RuntimeResourceException(String message) {
	    super(message);
	  }
}