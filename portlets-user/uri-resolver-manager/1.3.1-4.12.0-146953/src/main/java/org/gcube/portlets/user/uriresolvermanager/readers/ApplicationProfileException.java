package org.gcube.portlets.user.uriresolvermanager.readers;

/**
 * The Class ApplicationProfileException.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Sep 6, 2016
 */
@SuppressWarnings("serial")
public class ApplicationProfileException extends Exception {

 	/**
 	 * Instantiates a new application profile exception.
 	 *
 	 * @param message the message
 	 */
 	public ApplicationProfileException(String message) {
	    super(message);
	  }
}