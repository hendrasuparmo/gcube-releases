/**
 * 
 */
package org.gcube.portlets.user.workspaceexplorerapp.shared;


/**
 * The Class WorkspaceNavigatorServiceException.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 18, 2015
 */
public class WorkspaceNavigatorServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6416932531764081333L;

	/**
	 * Instantiates a new workspace navigator service exception.
	 */
	public WorkspaceNavigatorServiceException() {
	}

	/**
	 * Instantiates a new workspace navigator service exception.
	 *
	 * @param message the message
	 */
	public WorkspaceNavigatorServiceException(String message) {
		super(message);
	}

}
