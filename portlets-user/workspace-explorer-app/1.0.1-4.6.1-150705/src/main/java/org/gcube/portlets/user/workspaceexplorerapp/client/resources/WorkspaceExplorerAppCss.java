/**
 * 
 */
package org.gcube.portlets.user.workspaceexplorerapp.client.resources;

import com.google.gwt.resources.client.CssResource;

/**
 * The Interface WorkspaceLightTreeCss.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jul 8, 2015
 */
public interface WorkspaceExplorerAppCss extends CssResource {
	
	/**
	 * Name error.
	 *
	 * @return the string
	 */
	public String nameError();
	
}
