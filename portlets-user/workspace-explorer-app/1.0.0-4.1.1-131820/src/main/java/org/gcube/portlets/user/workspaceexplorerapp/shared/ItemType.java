/**
 * 
 */
package org.gcube.portlets.user.workspaceexplorerapp.shared;


/**
 * The Enum ItemType.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 18, 2015
 */
public enum ItemType {
	
	FOLDER, //MANDATORY
	EXTERNAL_IMAGE,
	EXTERNAL_FILE,
	EXTERNAL_PDF_FILE,
	EXTERNAL_URL,
	QUERY,
	REPORT_TEMPLATE,
	REPORT,
	DOCUMENT,
	METADATA,
	PDF_DOCUMENT,
	IMAGE_DOCUMENT,
	URL_DOCUMENT,
	GCUBE_ITEM,
	TIME_SERIES,
	UNKNOWN_TYPE;
}
