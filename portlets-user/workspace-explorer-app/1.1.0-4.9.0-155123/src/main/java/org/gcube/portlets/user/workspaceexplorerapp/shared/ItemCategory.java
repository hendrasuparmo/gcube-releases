/**
 * 
 */
package org.gcube.portlets.user.workspaceexplorerapp.shared;


/**
 * The Enum ItemCategory.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 30, 2015
 */
public enum ItemCategory {
	
	HOME,
	VRE_FOLDER;
}
