/**
 *
 */
package org.gcube.portlets.user.workspaceexplorerapp.client.download;


/**
 * The Enum DownloadType.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 7, 2016
 */
public enum DownloadType {

	PREVIEW,
	OPEN,
	DOWNLOAD;
}
