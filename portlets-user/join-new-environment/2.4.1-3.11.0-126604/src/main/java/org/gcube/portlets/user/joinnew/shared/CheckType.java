package org.gcube.portlets.user.joinnew.shared;
/**
 * type check for the infrastructure
 * @author Massimiliano Assante ISTI-CNR
 * 
 * @version 1.0 Jan 12th 2012
 */
public enum CheckType {
	ServiceMap, InformationSystem, ResourceManager
}
