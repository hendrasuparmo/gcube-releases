package org.gcube.portlets.user.joinnew.client.commons;

import com.google.gwt.core.client.GWT;

public class UIConstants {
	public static final String COOKIE_NAME = "gCube-Environment-Restore";
	public static final int COOKIE_MONTHS_EXPIRY_TIME = 6; //6 Months 
		
	public static final String LOADING_IMAGE = GWT.getModuleBaseURL() + "../images/loading-bar.gif";
	public static final String INFO_IMAGE = GWT.getModuleBaseURL() + "../images/icona_info.png";
	public static final String TRASPARENT_IMAGE = GWT.getHostPageBaseURL() + "../images/transparent_7_7.gif";
	
	public static final Integer imageVRE_width = 145;
	
	public static final String filter_label = "";
	
	public static final String SIGN_UP = "Request access";
	public static final String REGISTER_FREE = "Free access";
	public static final String PENDING = "Pending";
	
	public static final String enter_legenda = "Enter";
	public static final String ask_legenda = "Ask For Registration";
	public static final String pending_legenda = "Pending";
	
	public static final String enter_button = "Enter";
	public static final String ask_button = "Ask For Reg.";
	public static final String pending_button = "Pending";
	
	public static final String application_view_icons = GWT.getModuleBaseURL() + "../images/application_view_icons.png";
	public static final String application_view_list = GWT.getModuleBaseURL() + "../images/application_view_list.png";
	public static final String application_view_tile = GWT.getModuleBaseURL() + "../images/application_view_tile.png";
	
	public static final String HELP_ICO = GWT.getModuleBaseURL() + "../images/help.png";
	
	public static final String ENTER_VO_ROOT = GWT.getModuleBaseURL() + "../images/wrench.png";


	public static final int BACK_SPACE_CODE = 8;
}
