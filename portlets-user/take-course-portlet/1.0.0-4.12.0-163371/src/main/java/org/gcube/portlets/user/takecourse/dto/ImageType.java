package org.gcube.portlets.user.takecourse.dto;

public enum ImageType {
	DOC, PPT, XLS, NONE, MOVIE, HTML, PDF, IMAGE, RAR, ZIP,  
}
