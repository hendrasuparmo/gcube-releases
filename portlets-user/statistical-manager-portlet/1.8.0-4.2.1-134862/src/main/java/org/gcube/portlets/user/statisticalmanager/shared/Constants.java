package org.gcube.portlets.user.statisticalmanager.shared;

/**
 * 
 * @author giancarlo
 * email: <a href="mailto:g.panichi@isti.cnr.it">g.panichi@isti.cnr.it</a> 
 *
 */
public class Constants {
	public static final boolean DEBUG_MODE = false;
	
	public static final String APPLICATION_ID = "org.gcube.portlets.user.statisticalmanager.portlet.StatisticalManager";
	public static final String ACCOUNTING_MANAGER_ID = "StatisticalManagerId";
	public static final String AM_LANG_COOKIE = "SMLangCookie";	
	public static final String AM_LANG = "SMLang";
	
	public static final String DEFAULT_USER = "giancarlo.panichi";
	public final static String DEFAULT_SCOPE = "/gcube/devNext";
	public final static String DEFAULT_TOKEN = "16e65d4f-11e0-4e4a-84b9-351688fccc12-98187548";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	
	//public final static String DEFAULT_USER = "test.user";
	//public final static String DEFAULT_SCOPE = "/gcube/devNext";
	//public final static String DEFAULT_SCOPE = "/gcube/devsec/devVRE";
	
	//public final static String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	

	
}
