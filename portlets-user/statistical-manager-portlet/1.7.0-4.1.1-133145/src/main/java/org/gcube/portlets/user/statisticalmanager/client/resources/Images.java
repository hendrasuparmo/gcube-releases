package org.gcube.portlets.user.statisticalmanager.client.resources;

import org.gcube.portlets.user.statisticalmanager.client.StatisticalManager;

import com.google.gwt.user.client.ui.AbstractImagePrototype;

public class Images {

	public static AbstractImagePrototype logo() {
		return AbstractImagePrototype.create(StatisticalManager.resources.logo());
	}

	public static AbstractImagePrototype logoLittle() {
		return AbstractImagePrototype.create(StatisticalManager.resources.logoLittle());
	}

	public static AbstractImagePrototype computationIcon() {
		return AbstractImagePrototype.create(StatisticalManager.resources.computationIcon());
	}

	public static AbstractImagePrototype inputSpaceIcon() {
		return AbstractImagePrototype.create(StatisticalManager.resources.inputSpaceIcon());
	}

	public static AbstractImagePrototype addOperator() {
		return AbstractImagePrototype.create(StatisticalManager.resources.addOperator());
	}

	public static AbstractImagePrototype startComputation() {
		return AbstractImagePrototype.create(StatisticalManager.resources.startComputation());
	}
	
	public static AbstractImagePrototype removeAll() {
		return AbstractImagePrototype.create(StatisticalManager.resources.removeAll());
	}

	public static AbstractImagePrototype showAllOperators() {
		return AbstractImagePrototype.create(StatisticalManager.resources.sortAscending());
	}

	public static AbstractImagePrototype showCategories() {
		return AbstractImagePrototype.create(StatisticalManager.resources.tree());
	}

	public static AbstractImagePrototype folderExplore() {
		return AbstractImagePrototype.create(StatisticalManager.resources.folderExplore());
	}

	public static AbstractImagePrototype cancel() {
		return AbstractImagePrototype.create(StatisticalManager.resources.cancel());
	}

	public static AbstractImagePrototype addl() {
		return AbstractImagePrototype.create(StatisticalManager.resources.add());
	}

	public static AbstractImagePrototype table() {
		return AbstractImagePrototype.create(StatisticalManager.resources.table());
	}

	public static AbstractImagePrototype refresh() {
		return AbstractImagePrototype.create(StatisticalManager.resources.refresh());
	}

	public static AbstractImagePrototype fileDownload() {
		return AbstractImagePrototype.create(StatisticalManager.resources.fileDownload());
	}

	public static AbstractImagePrototype loader() {
		return AbstractImagePrototype.create(StatisticalManager.resources.loader());
	}

	public static AbstractImagePrototype save() {
		return AbstractImagePrototype.create(StatisticalManager.resources.save());
	}

	public static AbstractImagePrototype expand() {
		return AbstractImagePrototype.create(StatisticalManager.resources.expand());
	}

	public static AbstractImagePrototype collapse() {
		return AbstractImagePrototype.create(StatisticalManager.resources.collapse());
	}

	public static AbstractImagePrototype groupBy() {
		return AbstractImagePrototype.create(StatisticalManager.resources.groupBy());
	}
	
	public static AbstractImagePrototype map() {
		return AbstractImagePrototype.create(StatisticalManager.resources.map());
	}
	
	public static AbstractImagePrototype userPerspective() {
		return AbstractImagePrototype.create(StatisticalManager.resources.userPerspective());
	}
	
	public static AbstractImagePrototype computationPerspective() {
		return AbstractImagePrototype.create(StatisticalManager.resources.computationPerspective());
	}
	
}
