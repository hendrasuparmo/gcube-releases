package org.gcube.portlets.user.reportgenerator.client.targets;

import com.google.gwt.user.client.ui.HTML;

public class GroupingInnerArea extends HTML {

	public GroupingInnerArea() {
		super();
		setWidth("700px");
		setHeight("5px");
		setStyleName("grouping-inner");
	}
}
