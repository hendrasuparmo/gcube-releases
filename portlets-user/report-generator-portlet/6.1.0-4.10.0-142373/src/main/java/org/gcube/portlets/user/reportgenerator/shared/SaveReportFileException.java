package org.gcube.portlets.user.reportgenerator.shared;



public class SaveReportFileException extends ReportExporterException {

	private static final long serialVersionUID = -6674611825830219782L;
	
	public SaveReportFileException() {
		
	}

	public SaveReportFileException(String error) {
		super(error);
	}
}
