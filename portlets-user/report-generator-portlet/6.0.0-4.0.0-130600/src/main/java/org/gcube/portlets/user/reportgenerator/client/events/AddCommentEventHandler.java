package org.gcube.portlets.user.reportgenerator.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface AddCommentEventHandler extends EventHandler {
	 void onAddComment(AddCommentEvent event);
}
