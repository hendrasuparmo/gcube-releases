package org.gcube.portlets.user.reportgenerator.client.targets;

import com.google.gwt.user.client.ui.HTML;



public class GroupingDelimiterArea extends HTML {

	public GroupingDelimiterArea(int width, int height) {
		super();
		setWidth(width+"px");
		setHeight(height+"px");
		setStyleName("grouping-border");
	}
}
