package org.gcube.portlets.user.reportgenerator.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface ItemSelectionEventHandler extends EventHandler {

	void onItemSelected(ItemSelectionEvent event);
}
