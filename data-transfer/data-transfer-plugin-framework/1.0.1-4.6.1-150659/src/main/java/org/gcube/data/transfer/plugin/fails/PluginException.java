package org.gcube.data.transfer.plugin.fails;

public class PluginException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4599027006131232307L;

	public PluginException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PluginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PluginException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PluginException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PluginException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	

}
