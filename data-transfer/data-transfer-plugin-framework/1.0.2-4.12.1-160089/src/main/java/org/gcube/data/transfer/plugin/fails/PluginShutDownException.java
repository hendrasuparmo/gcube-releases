package org.gcube.data.transfer.plugin.fails;

public class PluginShutDownException extends PluginException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1801375781439264427L;

	public PluginShutDownException() {
		// TODO Auto-generated constructor stub
	}

	public PluginShutDownException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PluginShutDownException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PluginShutDownException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PluginShutDownException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
