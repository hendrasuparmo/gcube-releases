package org.gcube.data.transfer.plugin.fails;

public class PluginCleanupException extends PluginException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3068399602347922720L;

	public PluginCleanupException() {
		// TODO Auto-generated constructor stub
	}

	public PluginCleanupException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PluginCleanupException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PluginCleanupException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PluginCleanupException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
