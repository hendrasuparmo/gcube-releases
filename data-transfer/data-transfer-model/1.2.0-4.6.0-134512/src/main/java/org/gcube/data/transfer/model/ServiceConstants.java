package org.gcube.data.transfer.model;

public class ServiceConstants {

	public static final String APPLICATION_PATH="/gcube/service/";
	
	//Servlets
	public static final String CAPABILTIES_SERVLET_NAME="Capabilities";
	public static final String REQUESTS_SERVLET_NAME="Requests";
	public static final String STATUS_SERVLET_NAME="TransferStatus";
	
	//Params
	public static final String TRANSFER_REQUEST_OBJECT="request-object";
	public static final String TRANSFER_ID="transfer-id";
	

	
}
