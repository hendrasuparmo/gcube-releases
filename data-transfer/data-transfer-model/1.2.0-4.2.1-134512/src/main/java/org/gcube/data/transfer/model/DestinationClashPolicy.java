package org.gcube.data.transfer.model;

public enum DestinationClashPolicy {

	FAIL,
	REWRITE,
	ADD_SUFFIX,
	APPEND
	
}
