package org.gcube.data.transfer.model.plugins.thredds;

public interface DataSet {

	public String getPath();
	public String getLocation();
	
}
