package org.gcube.data.transfer.library.faults;

public class FailedTransferException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -807695677781676381L;

	public FailedTransferException() {
		// TODO Auto-generated constructor stub
	}

	public FailedTransferException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FailedTransferException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public FailedTransferException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FailedTransferException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
