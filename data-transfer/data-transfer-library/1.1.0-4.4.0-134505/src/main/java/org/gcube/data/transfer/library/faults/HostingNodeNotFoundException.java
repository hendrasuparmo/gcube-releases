package org.gcube.data.transfer.library.faults;

public class HostingNodeNotFoundException extends InitializationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5786009015981659074L;

	public HostingNodeNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public HostingNodeNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public HostingNodeNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public HostingNodeNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public HostingNodeNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
