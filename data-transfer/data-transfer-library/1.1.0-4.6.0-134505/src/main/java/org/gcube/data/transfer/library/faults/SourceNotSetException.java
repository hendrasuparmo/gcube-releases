package org.gcube.data.transfer.library.faults;

public class SourceNotSetException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5868191252361823341L;

	public SourceNotSetException() {
		// TODO Auto-generated constructor stub
	}

	public SourceNotSetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SourceNotSetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SourceNotSetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SourceNotSetException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
