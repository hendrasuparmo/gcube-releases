package org.gcube.data.transfer.library.faults;

public class DestinationNotSetException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4792958203546912779L;

	public DestinationNotSetException() {
		// TODO Auto-generated constructor stub
	}

	public DestinationNotSetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DestinationNotSetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DestinationNotSetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DestinationNotSetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
