package org.gcube.data.transfer.library.faults;

public class InvalidSourceException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7631518288856917202L;

	public InvalidSourceException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidSourceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidSourceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidSourceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidSourceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
