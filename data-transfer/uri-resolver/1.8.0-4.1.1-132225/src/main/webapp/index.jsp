<!DOCTYPE HTML>
<html lang="en-US">
<!--     <head> -->
<!--         <meta charset="UTF-8"> -->
<!--         <meta http-equiv="refresh" content="0; url=https://dev4.d4science.org/ckan"> -->
<!--         <script type="text/javascript"> window.location.href = "https://dev4.d4science.org/ckan" -->
<!--         </script> -->
<!--         <title>Page Redirection</title> -->
<!--     </head> -->
<!--     <body> -->
<!--         Note: don't tell people to `click` the link, just tell them that it is a link. -->
<!--         If you are not redirected automatically, follow the <a href='https://dev4.d4science.org/ckan'>link to example</a> -->
<!--     </body> -->
<body>
	<h2>The URI resolver</h2>
	<p>
		See wiki page: <a
			href="https://gcube.wiki.gcube-system.org/gcube/index.php/URI_Resolver">URI
			Resolver gCube Wiki</a>
	</p>
</body>
</html>