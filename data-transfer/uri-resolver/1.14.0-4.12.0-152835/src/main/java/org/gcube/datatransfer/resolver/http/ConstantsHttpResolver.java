/**
 *
 */
package org.gcube.datatransfer.resolver.http;



/**
 * The Class ConstantsHttpResolver.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Sep 21, 2015
 */
public class ConstantsHttpResolver {

	public static final String CONTENT_DISPOSITION = "content-disposition";
	public static final String DEFAULT_CONTENTTYPE_UNKNOWN_UNKNOWN = "unknown/unknown";
	public static final String DEFAULT_FILENAME_FROM_STORAGE_MANAGER = "fromStorageManager";

	protected static final String HPC = "hproxycheck"; //for hproxycheck
}
