/**
 * 
 */
package org.gcube.datatransfer.resolver.gis.exception;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 14, 2014
 *
 */
public class IllegalArgumentException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8589705350737964325L;

	/**
	 * 
	 */
	public IllegalArgumentException() {
		super();
	}
	
    public IllegalArgumentException(String message) {
        super(message);
    }

}
