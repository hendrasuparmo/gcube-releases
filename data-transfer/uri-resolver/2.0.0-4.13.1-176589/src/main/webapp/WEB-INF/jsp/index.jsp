<!DOCTYPE HTML>
<html lang="en-US">
<head>
<style type="text/css">
body {
	background-color: #f8f6f2;
	color: #225f97;
	font-family: 'Droid Sans', arial, sans-serif;
	font-size: 16px;
}

.mydiv {
	top: 50%;
	left: 50%;
	width: 600px;
	height: 300px;
	margin-top: -150px; /*set to a negative number 1/2 of your height*/
	margin-left: -300px; /*set to a negative number 1/2 of your width*/
/* 	border: 1px solid #ccc;  */
	/* 	background-color: #9b9b9b; */
	position: fixed;
	text-align: center;
/* 	vertical-align: middle; */
}

.myTitle {
	font-size: 22px;
	font-weight: bold;
}
</style>
</head>

<body>
	<div class="mydiv">
		<img alt=""
			src="https://www.d4science.org/image/layout_set_logo?img_id=12630" />
		<div class="myTitle">The URI Resolver</div>
		<p>
			See wiki page at <a
				href="https://gcube.wiki.gcube-system.org/gcube/index.php/URI_Resolver" target="_blank">gCube Wiki URI
				Resolver</a>
		</p>
	</div>
</body>
</html>