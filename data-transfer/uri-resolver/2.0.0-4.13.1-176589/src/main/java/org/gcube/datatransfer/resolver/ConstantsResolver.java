/**
 *
 */
package org.gcube.datatransfer.resolver;


/**
 * The Class ConstantsResolver.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Oct 19, 2018
 */
public class ConstantsResolver {

	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	public static final String DEFAULT_CONTENTTYPE_UNKNOWN_UNKNOWN = "unknown/unknown";
	public static final String DEFAULT_FILENAME_FROM_STORAGE_MANAGER = "fromStorageManager";
	public static final String CONTENT_LENGTH = "Content-Length";
	public static final String CONTENT_TYPE= "Content-Type";

	public static final String HPC = "hproxycheck"; //for hproxycheck

	public static final String QUERY_PARAM_VALIDATION = "validation";
	public static final String QUERY_PARAM_CONTENT_TYPE = "contentType";
	public static final String QUERY_PARAM_FILE_NAME = "fileName";

}
