/**
 *
 */
package org.gcube.datatransfer.resolver.gis.util;



/**
 * The Class NamespaceISO19139.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * May 16, 2017
 */
public abstract class NamespaceISO19139 {


	//Namespaces
	private final String namespaceCSW = "http://www.opengis.net/cat/csw/2.0.2";
	private final String namespaceDC = "http://purl.org/dc/elements/1.1/";
	private final String namespaceOWS = "http://www.opengis.net/ows";
	private final String namespaceGMD = "http://www.isotc211.org/2005/gmd";
	private final String namespaceGCO = "http://www.isotc211.org/2005/gco";
	private final String namespaceXLINK = "http://www.w3.org/1999/xlink";
	private final String namespaceSRV = "http://www.isotc211.org/2005/srv";
	private final String namespaceXSI = "http://www.w3.org/2001/XMLSchema-instance";
	private final String namespaceGML = "http://www.opengis.net/gml";
	private final String namespaceGTS = "http://www.isotc211.org/2005/gts";
	private final String namespaceGEONET = "http://www.fao.org/geonetwork";
	private final String namespaceGMX = "http://www.isotc211.org/2005/gmx";
	private final String namespaceWMS = "http://www.opengis.net/wms";
	private final String namespaceDCT = "http://purl.org/dc/terms/";

	//Prefixs
	private final String prefixCSW = "csw";
	private final String prefixGMD = "gmd";
	private final String prefixOWS = "ows";
	private final String prefixDC = "dc";
	private final String prefixDCT = "dct";
	private final String prefixGCO = "gco";
	private final String prefixXLINK= "xlink";
	private final String prefixSRV = "srv";
	private final String prefixXSI = "xsi";
	private final String prefixGML = "gml";
	private final String prefixGTS = "gts";
	private final String prefixGEONET = "geonet";
	private final String prefixGMX = "gmx";
	private final String prefixWMS = "wms";


	/**
	 * Gets the namespace csw.
	 *
	 * @return the namespace csw
	 */
	public String getNamespaceCSW() {
		return namespaceCSW;
	}

	/**
	 * Gets the namespace dc.
	 *
	 * @return the namespace dc
	 */
	public String getNamespaceDC() {
		return namespaceDC;
	}

	/**
	 * Gets the namespace ows.
	 *
	 * @return the namespace ows
	 */
	public String getNamespaceOWS() {
		return namespaceOWS;
	}

	/**
	 * Gets the namespace gmd.
	 *
	 * @return the namespace gmd
	 */
	public String getNamespaceGMD() {
		return namespaceGMD;
	}

	/**
	 * Gets the namespace gco.
	 *
	 * @return the namespace gco
	 */
	public String getNamespaceGCO() {
		return namespaceGCO;
	}

	/**
	 * Gets the namespace xlink.
	 *
	 * @return the namespace xlink
	 */
	public String getNamespaceXLINK() {
		return namespaceXLINK;
	}

	/**
	 * Gets the namespace srv.
	 *
	 * @return the namespace srv
	 */
	public String getNamespaceSRV() {
		return namespaceSRV;
	}

	/**
	 * Gets the namespace xsi.
	 *
	 * @return the namespace xsi
	 */
	public String getNamespaceXSI() {
		return namespaceXSI;
	}

	/**
	 * Gets the namespace gml.
	 *
	 * @return the namespace gml
	 */
	public String getNamespaceGML() {
		return namespaceGML;
	}

	/**
	 * Gets the namespace gts.
	 *
	 * @return the namespace gts
	 */
	public String getNamespaceGTS() {
		return namespaceGTS;
	}

	/**
	 * Gets the namespace geonet.
	 *
	 * @return the namespace geonet
	 */
	public String getNamespaceGEONET() {
		return namespaceGEONET;
	}

	/**
	 * Gets the prefix csw.
	 *
	 * @return the prefix csw
	 */
	public String getPrefixCSW() {
		return prefixCSW;
	}

	/**
	 * Gets the prefix gmd.
	 *
	 * @return the prefix gmd
	 */
	public String getPrefixGMD() {
		return prefixGMD;
	}

	/**
	 * Gets the prefix ows.
	 *
	 * @return the prefix ows
	 */
	public String getPrefixOWS() {
		return prefixOWS;
	}

	/**
	 * Gets the prefix dc.
	 *
	 * @return the prefix dc
	 */
	public String getPrefixDC() {
		return prefixDC;
	}

	/**
	 * Gets the prefix gco.
	 *
	 * @return the prefix gco
	 */
	public String getPrefixGCO() {
		return prefixGCO;
	}

	/**
	 * Gets the prefix xlink.
	 *
	 * @return the prefix xlink
	 */
	public String getPrefixXLINK() {
		return prefixXLINK;
	}

	/**
	 * Gets the prefix srv.
	 *
	 * @return the prefix srv
	 */
	public String getPrefixSRV() {
		return prefixSRV;
	}

	/**
	 * Gets the prefix gml.
	 *
	 * @return the prefix gml
	 */
	public String getPrefixGML() {
		return prefixGML;
	}

	/**
	 * Gets the prefix gts.
	 *
	 * @return the prefix gts
	 */
	public String getPrefixGTS() {
		return prefixGTS;
	}

	/**
	 * Gets the prefix geonet.
	 *
	 * @return the prefix geonet
	 */
	public String getPrefixGEONET() {
		return prefixGEONET;
	}

	/**
	 * Gets the prefix xsi.
	 *
	 * @return the prefix xsi
	 */
	public String getPrefixXSI() {
		return prefixXSI;
	}

	/**
	 * Gets the prefix gmx.
	 *
	 * @return the prefix gmx
	 */
	public String getPrefixGMX() {
		return prefixGMX;
	}

	/**
	 * Gets the namespace gmx.
	 *
	 * @return the namespace gmx
	 */
	public String getNamespaceGMX() {
		return namespaceGMX;
	}

	/**
	 * Gets the prefix wms.
	 *
	 * @return the prefix wms
	 */
	public String getPrefixWMS() {
		return prefixWMS;
	}

	/**
	 * Gets the namespace wms.
	 *
	 * @return the namespace wms
	 */
	public String getNamespaceWMS() {
		return namespaceWMS;
	}

	/**
	 * Gets the namespace dct.
	 *
	 * @return the namespace dct
	 */
	public String getNamespaceDCT() {
		return namespaceDCT;
	}

	/**
	 * Gets the prefix dct.
	 *
	 * @return the prefix dct
	 */
	public String getPrefixDCT() {
		return prefixDCT;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("NamespaceISO19139 [namespaceCSW=");
		builder.append(namespaceCSW);
		builder.append(", namespaceDC=");
		builder.append(namespaceDC);
		builder.append(", namespaceOWS=");
		builder.append(namespaceOWS);
		builder.append(", namespaceGMD=");
		builder.append(namespaceGMD);
		builder.append(", namespaceGCO=");
		builder.append(namespaceGCO);
		builder.append(", namespaceXLINK=");
		builder.append(namespaceXLINK);
		builder.append(", namespaceSRV=");
		builder.append(namespaceSRV);
		builder.append(", namespaceXSI=");
		builder.append(namespaceXSI);
		builder.append(", namespaceGML=");
		builder.append(namespaceGML);
		builder.append(", namespaceGTS=");
		builder.append(namespaceGTS);
		builder.append(", namespaceGEONET=");
		builder.append(namespaceGEONET);
		builder.append(", namespaceGMX=");
		builder.append(namespaceGMX);
		builder.append(", namespaceWMS=");
		builder.append(namespaceWMS);
		builder.append(", namespaceDCT=");
		builder.append(namespaceDCT);
		builder.append(", prefixCSW=");
		builder.append(prefixCSW);
		builder.append(", prefixGMD=");
		builder.append(prefixGMD);
		builder.append(", prefixOWS=");
		builder.append(prefixOWS);
		builder.append(", prefixDC=");
		builder.append(prefixDC);
		builder.append(", prefixDCT=");
		builder.append(prefixDCT);
		builder.append(", prefixGCO=");
		builder.append(prefixGCO);
		builder.append(", prefixXLINK=");
		builder.append(prefixXLINK);
		builder.append(", prefixSRV=");
		builder.append(prefixSRV);
		builder.append(", prefixXSI=");
		builder.append(prefixXSI);
		builder.append(", prefixGML=");
		builder.append(prefixGML);
		builder.append(", prefixGTS=");
		builder.append(prefixGTS);
		builder.append(", prefixGEONET=");
		builder.append(prefixGEONET);
		builder.append(", prefixGMX=");
		builder.append(prefixGMX);
		builder.append(", prefixWMS=");
		builder.append(prefixWMS);
		builder.append("]");
		return builder.toString();
	}

}