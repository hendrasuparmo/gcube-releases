package org.gcube.data.transfer.service.transfers.engine.faults;

public class NotSupportedMethodException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotSupportedMethodException() {
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMethodException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMethodException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMethodException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMethodException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
