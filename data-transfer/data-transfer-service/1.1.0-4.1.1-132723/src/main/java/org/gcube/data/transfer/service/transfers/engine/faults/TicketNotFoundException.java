package org.gcube.data.transfer.service.transfers.engine.faults;

public class TicketNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5761719004322780267L;

	public TicketNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TicketNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TicketNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TicketNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TicketNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
