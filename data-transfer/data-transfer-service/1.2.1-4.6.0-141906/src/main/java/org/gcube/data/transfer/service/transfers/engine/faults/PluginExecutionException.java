package org.gcube.data.transfer.service.transfers.engine.faults;

public class PluginExecutionException extends ManagedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2681096696757651730L;

	public PluginExecutionException() {
		// TODO Auto-generated constructor stub
	}

	public PluginExecutionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PluginExecutionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PluginExecutionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PluginExecutionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
