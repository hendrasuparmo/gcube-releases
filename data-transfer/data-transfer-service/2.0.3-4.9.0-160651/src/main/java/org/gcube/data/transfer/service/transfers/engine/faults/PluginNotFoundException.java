package org.gcube.data.transfer.service.transfers.engine.faults;

public class PluginNotFoundException extends ManagedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5169583804790487322L;

	public PluginNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public PluginNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PluginNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PluginNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PluginNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
