package org.gcube.data.transfer.service.transfers.engine.faults;

public class DestinationAccessException extends ManagedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4968214602073818019L;

	public DestinationAccessException() {
		// TODO Auto-generated constructor stub
	}

	public DestinationAccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DestinationAccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DestinationAccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DestinationAccessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
