package org.gcube.data.transfer.service.transfers.engine.faults;

public class ManagedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ManagedException() {
		// TODO Auto-generated constructor stub
	}

	public ManagedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ManagedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ManagedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ManagedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
