package org.gcube.data.transfer.service.transfers.engine;

import org.gcube.data.transfer.model.TransferRequest;

public interface RequestManager {

	public boolean put(TransferRequest request);
		
}
