package org.gcube.common.core.security;

import org.gcube.common.core.security.impl.GCUBESimpleServiceSecurityManager;

/**
 * 
 * @deprecated to be removed once the new security framework is available
 *
 */
public class GCUBEServiceSecurityManagerImpl extends
		GCUBESimpleServiceSecurityManager {
	//MS, 01/06/12: an alias to be removed in the next releses
}
