package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBEGenericResource;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBEGenericResource}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBEGenericResourceQuery extends ISTemplateQuery<GCUBEGenericResource> {}
