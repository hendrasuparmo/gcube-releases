/**
 * 
 */
package org.acme.sample;

/**
 * @author Fabio Simeoni
 *
 */
public class Utils {

	public static final String NS="http://acme.org/sample";
	public static final String NAME="acme/sample";
	
	public static final String FACTORY_NAME="acme/sample/factory";
	public static final String STATEFUL_NAME="acme/sample/stateful";
}
