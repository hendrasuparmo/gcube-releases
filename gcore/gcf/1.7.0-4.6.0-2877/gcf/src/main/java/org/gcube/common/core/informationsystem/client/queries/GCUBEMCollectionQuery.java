package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBEMCollection;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBEMCollection}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBEMCollectionQuery extends ISTemplateQuery<GCUBEMCollection> {}
