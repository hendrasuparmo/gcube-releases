package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBEExternalRunningInstance;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBEExternalRunningInstance}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBEExternalRIQuery extends ISTemplateQuery<GCUBEExternalRunningInstance> {}
