package org.gcube.common.core.utils.events;

/**
 *  A tagging interface that marks the intention of {@link GCUBEConsumer} to receive events synchronously, 
 *  i.e. induce the producer to wait for the consumer to finish event processing.
 *  
 */
public interface GCUBESynchronousConsumer {}
