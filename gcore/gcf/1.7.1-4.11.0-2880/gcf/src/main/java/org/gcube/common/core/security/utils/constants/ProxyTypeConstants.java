package org.gcube.common.core.security.utils.constants;

public interface ProxyTypeConstants 
{
	String GT2_PROXY = "GT2_PROXY";
	String GT3_PROXY = "GT3_PROXY";
	String GT4_PROXY = "GT4_PROXY";
	
}
