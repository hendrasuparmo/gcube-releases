package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBECSInstance;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBECSInstance}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBECSInstanceQuery extends ISTemplateQuery<GCUBECSInstance> {}
