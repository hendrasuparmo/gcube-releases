package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBEHostingNode;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBEHostingNode}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBEGHNQuery extends ISTemplateQuery<GCUBEHostingNode> {}
