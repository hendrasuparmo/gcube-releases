package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBECollection;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBECollection}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBECollectionQuery extends ISTemplateQuery<GCUBECollection> {}
