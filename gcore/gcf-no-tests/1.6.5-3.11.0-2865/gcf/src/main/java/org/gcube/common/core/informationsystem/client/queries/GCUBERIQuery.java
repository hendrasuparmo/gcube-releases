package org.gcube.common.core.informationsystem.client.queries;

import org.gcube.common.core.informationsystem.client.ISTemplateQuery;
import org.gcube.common.core.resources.GCUBERunningInstance;

/**
 * A specialisation of {@link ISTemplateQuery} for the generic {@link GCUBERunningInstance}.
 * @author Fabio Simeoni (University of Strathclyde)
 *
 */
public interface GCUBERIQuery extends ISTemplateQuery<GCUBERunningInstance> {}
