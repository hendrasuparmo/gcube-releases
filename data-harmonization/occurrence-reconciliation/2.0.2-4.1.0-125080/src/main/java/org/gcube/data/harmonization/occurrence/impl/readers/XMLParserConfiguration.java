package org.gcube.data.harmonization.occurrence.impl.readers;

import java.nio.charset.Charset;

public class XMLParserConfiguration extends ParserConfiguration {

	public XMLParserConfiguration(Charset charset, char delimiter,
			char comment, boolean hasHeader) {
		super(charset, delimiter, comment, hasHeader);		
	}

	
	
}
