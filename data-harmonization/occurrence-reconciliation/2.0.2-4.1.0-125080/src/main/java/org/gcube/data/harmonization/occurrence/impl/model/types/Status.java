package org.gcube.data.harmonization.occurrence.impl.model.types;

public enum Status {

	PENDING,
	RUNNING,
	STOPPED,
	COMPLETED,
	FAILED
}
