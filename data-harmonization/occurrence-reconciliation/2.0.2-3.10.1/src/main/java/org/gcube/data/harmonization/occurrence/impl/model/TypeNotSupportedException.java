package org.gcube.data.harmonization.occurrence.impl.model;

public class TypeNotSupportedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3928846916250027339L;

	public TypeNotSupportedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TypeNotSupportedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TypeNotSupportedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TypeNotSupportedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
