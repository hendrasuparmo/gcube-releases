package org.gcube.data.harmonization.occurrence.impl.model.types;

public enum ResourceType {
	TABULAR,
	
	FILE,
	
	OBJECT
}
